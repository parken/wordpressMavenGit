package com.sxf.tools;

import java.io.File;

public class Native2Ascii {
	public static void main(String[] args) throws Exception {
		File orgF = new File(
				"src/main/resources/config/resource/ApplicationResources.properties");
		File targF = new File(
				"src/main/webapp/WEB-INF/classes/config/resource/ApplicationResources.properties");
		String command = "cmd /c native2ascii -encoding UTF-8 "
				+ orgF.getAbsolutePath() + " " + targF.getAbsolutePath();
		Process proc = Runtime.getRuntime().exec(command);
		proc.waitFor();

	}
}
