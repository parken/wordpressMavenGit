package com.sxf.test.controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

public class TestAdminController {
	@RequestMapping(value = "/whitelists")
	public String index(ModelMap map) {

		map.put("account", "");
		map.put("groupList", "");
		return "/group/group-index";
	}

	// @ResponseBody ajax响应
	@RequestMapping(value = "/whitelist/{whiteListId}/del", method = {
			RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String delete(@PathVariable Integer whiteListId) {
		// org.springframework.http.converter.StringHttpMessageConverter
		WebApplicationContext wa = ContextLoader
				.getCurrentWebApplicationContext();
		return "你好";
	}
}
