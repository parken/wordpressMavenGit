//package com.sxf.test.quartz;
//
//import org.quartz.CronScheduleBuilder;
//import org.quartz.CronTrigger;
//import org.quartz.Job;
//import org.quartz.JobBuilder;
//import org.quartz.JobDetail;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//import org.quartz.Scheduler;
//import org.quartz.SchedulerFactory;
//import org.quartz.TriggerBuilder;
//import org.quartz.impl.StdSchedulerFactory;
//
///**
// * quartz测试
// *
// * @author phsxf
// *
// */
//public class TestQuartz {
//
//	public static void main(String[] args) {
//		try {
//			// Grab the Scheduler instance from the Factory
//			SchedulerFactory sf = new StdSchedulerFactory();
//			Scheduler sched = sf.getScheduler();
//
//			JobDetail job = JobBuilder.newJob(SimpleJob.class)
//					.withIdentity("job1", "group1").build();
//
//			JobDetail job2 = JobBuilder.newJob(new Job() {
//				@Override
//				public void execute(JobExecutionContext context)
//						throws JobExecutionException {
//					System.out.println("Job2");
//
//				}
//			}.getClass()).withIdentity("job1", "group1").build();
//
//			CronTrigger trigger = TriggerBuilder
//					.newTrigger()
//					.withIdentity("trigger1", "group1")
//					.withSchedule(
//							CronScheduleBuilder.cronSchedule("0/2 * * * * ?"))
//					.build();
//
//			sched.scheduleJob(job, trigger);
//			//sched.addJob(job2, false);
//
//			// and start it off
//			sched.start();
//			Thread.sleep(1000 * 10);
//			sched.shutdown();
//		} catch (Exception se) {
//			se.printStackTrace();
//		}
//	}
//
//}
