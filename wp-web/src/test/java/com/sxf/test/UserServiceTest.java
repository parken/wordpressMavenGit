package com.sxf.test;

import com.sxf.dao.UserDAO;
import com.sxf.domain.user.User;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sf.tools.StringUtils;

import javax.annotation.Resource;

public class UserServiceTest extends SuperBeanTest {
    @Resource
    private PasswordEncoder pe;
    @Resource
    private UserDAO ud;
    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void addUser(){
        User u = new User();
        u.setName("admin");
        u.setPassword(pe.encode("admin"));
        u.setLocked(false);
        u.setActivationKey(StringUtils.uuid32());
        ud.insert(u);
    }
}
