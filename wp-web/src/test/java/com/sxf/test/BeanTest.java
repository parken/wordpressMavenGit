package com.sxf.test;

import com.sxf.dao.ArticleDAO;
import com.sxf.dao.DBAnalysisDAO;
import com.sxf.dao.OptionDAO;
import com.sxf.dao.UserDAO;
import com.sxf.service.CommentService;
import org.junit.Test;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;

public class BeanTest extends SuperBeanTest {

	@Resource
	private DBAnalysisDAO dd;
	@Resource
	private UserDAO ud;
	@Resource
	private ArticleDAO pd;
	@Resource
	private OptionDAO od;
	@Resource
	private CommentService cs;

	@Resource
	private CacheManager cm;

	private static ApplicationContext getApplicationContext() {
		ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("config/spring/applicationContext.xml");
		ac.registerShutdownHook();
		return ac;
	}

	@Test
	public void test() {

	}

	public static void main(String[] args) throws Exception {
		ApplicationContext ac = getApplicationContext();
		ac.getBean("testDAOImpl");

	}
}
