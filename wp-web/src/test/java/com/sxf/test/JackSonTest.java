package com.sxf.test;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JackSonTest {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		ObjectMapper mapper = new ObjectMapper();
		// 设置输出包含的属性
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.getDeserializationConfig().withFeatures(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}
}
