package com.sxf.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Types;
import java.util.Iterator;
import java.util.Map;

import com.sxf.common.tools.JDBCTools;

public class JDBCToolTest {
	static {
		// String driverClassName = "oracle.jdbc.driver.OracleDriver";
		String driverClassName = "com.mysql.jdbc.Driver";

		try {
			Class.forName(driverClassName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Connection getConnection() {
		// String url = "jdbc:oracle:thin:@127.0.0.1:1521:orcl";
		// String username = "scott";
		// String pwd = "tiger";

		String url = "jdbc:mysql://127.0.0.1:3306/wordpress?&useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull";
		String username = "root";
		String pwd = "123456";

		Connection con = null;
		try {
			con = DriverManager.getConnection(url, username, pwd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}

	public static void main(String[] args) throws Exception {
		Connection conn = getConnection();
		Map<String, Object> map = JDBCTools.execProc("{call testProc(?,?)}", new Object[]{1},
				new Integer[]{Types.INTEGER}, conn);
		for (Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator(); it
				.hasNext();) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it
					.next();
			String key = entry.getKey();
			Object value = entry.getValue();
			System.out.println(key +"----->>>>>>>"+ value);

		}		
	}

}
