package com.sxf.test;

import com.sxf.WPApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * bean 测试基类
 * 
 * @author shixiafeng
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {WPApplication.class},webEnvironment= SpringBootTest.WebEnvironment.NONE)
@Rollback
public class SuperBeanTest extends AbstractJUnit4SpringContextTests {

}
