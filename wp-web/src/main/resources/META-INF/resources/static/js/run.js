function getFrontArticles(params) {
    return instance.request({
        url:  '/public/portal/v1/articlePage',
        method: 'get',
        params
    })
}

function getFrontCategorylist(params) {
    return instance.request({
        url:  '/public/portal/v1/getCategorylist',
        method: 'get',
        params
    })
}

function getBasicInfo(params) {
    return instance.request({
        url:  '/public/portal/v1/getInfo',
        method: 'get',
        params
    })
}