package springfox.documentation.oas.web;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.Map;

/**
 *
 */
public class OpenAPIUtils {
    private static Logger logger = LoggerFactory.getLogger(OpenAPIUtils.class);

    /**
     * @param api
     * @return
     */
    public static OpenAPI getOpenAPI(OpenAPI api) {
//        addOpenAPI(api, SetArmModeViewResult.class, "SetArmModeViewResult");
//        addOpenAPI(api, GatewayInfoResult.class, "GatewayInfoResult");
//        addOpenAPI(api, Integer.class, "Integer");
//        addOpenAPI(api, TypeFactory.defaultInstance().constructCollectionType(List.class, GatewayInfoResult.class), "ListGatewayInfoResult");
//        addOpenAPI(api, TypeFactory.defaultInstance().constructCollectionType(List.class, AreaRoomInfo.class), "ListAreaRoomInfo");
//        addOpenAPI(api, TypeFactory.defaultInstance().constructCollectionType(List.class, SubSystemInfo.class), "ListSubSystemInfo");
//        addOpenAPI(api, TypeFactory.defaultInstance().constructCollectionType(List.class, String.class), "ListString");

//        addOpenAPI(api, TypeFactory.defaultInstance().constructParametricType(DeviceResult.class, Map.class), "DeviceResultMap");

        return api;
    }

    /**
     * @param api
     * @param type     类型
     * @param dataName 数据名
     */
    public static void addOpenAPI(OpenAPI api, Type type, String dataName) {
        Components components = api.getComponents();
        Map<String, Schema> schemaMap = components.getSchemas();
        Schema result = SpringDocAnnotationsUtils.extractSchema(api.getComponents(), type, null, null);
        Schema ajaxResult = schemaMap.get("ResultMessage" + dataName);
        if (ajaxResult != null) {
            Map<String, Schema> properties = ajaxResult.getProperties();
            if (properties != null) {
                properties.put("data", result);
            }
        } else {
            logger.error("","error");
        }
    }

}
