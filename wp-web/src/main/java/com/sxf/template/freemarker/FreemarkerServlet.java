package com.sxf.template.freemarker;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author shixiafeng
 *
 */
public class FreemarkerServlet extends freemarker.ext.servlet.FreemarkerServlet {

	public static final String REQUEST = "request";
	public static final String SESSION = "session";
	public static final String APPLICATION = "application";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		setWebObject(request);
		super.doGet(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		setWebObject(request);
		super.doPost(request, response);

	}

	private void setWebObject(HttpServletRequest request) {
		request.setAttribute(REQUEST, request);
		request.setAttribute(SESSION, request.getSession());
		request.setAttribute(APPLICATION, getServletContext());
	}
}
