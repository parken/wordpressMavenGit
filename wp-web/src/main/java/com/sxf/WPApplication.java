package com.sxf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import sf.codegen.EntityEnhancerJavassist;


@SpringBootApplication
@EnableCaching
@EnableAsync
@EnableScheduling
public class WPApplication {
    public static void main(String[] args) throws Exception {
        new EntityEnhancerJavassist().enhance("com.sxf");
        SpringApplication app = new SpringApplication(WPApplication.class);
        app.addListeners(new ApplicationPidFileWriter());
        app.run(args);
    }
}
