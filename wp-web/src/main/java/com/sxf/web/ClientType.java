package com.sxf.web;

/**
 * 客户端类型
 */
public enum ClientType {
    /**
     * PC端
     */
    PC,
    /**
     * 移动端
     */
    MT
}
