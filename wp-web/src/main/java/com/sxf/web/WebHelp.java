package com.sxf.web;

import com.sxf.util.CookieUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.CacheManager;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Map;

@Slf4j
public class WebHelp {

	public static Object getCookieValue(HttpServletRequest request,
			HttpServletResponse response, CacheManager cm, String cookieId,
			String cacheKey) throws Exception {
		String uuid = CookieUtils.findCookie(request, response, cookieId);
		Object o = null;
		if (StringUtils.isNotBlank(uuid)) {
			try {
				o = cm.getCache(cacheKey).get(uuid, Object.class);
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return o;
	}

	/**
	 * 移除spring 相关的值(format)
	 * 
	 * @param request
	 */
	public static void removeParameter(HttpServletRequest request) {
		Map<String, String[]> map = null;
		try {
			map = request.getParameterMap();
			Method method = map.getClass().getMethod("setLocked",
					new Class[] { boolean.class });
			method.invoke(map, new Object[] { new Boolean(false) });
			// map.remove("format");
			map.put("format", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isAjaxRequest(WebRequest webRequest) {
		String requestedWith = webRequest.getHeader("X-Requested-With");
		return requestedWith != null ? "XMLHttpRequest".equals(requestedWith)
				: false;
	}

	public static boolean isAjaxUploadRequest(WebRequest webRequest) {
		return webRequest.getParameter("ajaxUpload") != null;
	}

	public static boolean isAjaxRequest(HttpServletRequest webRequest) {
		String requestedWith = webRequest.getHeader("X-Requested-With");
		return requestedWith != null ? "XMLHttpRequest".equals(requestedWith)
				: false;
	}

	public static boolean isAjaxUploadRequest(HttpServletRequest webRequest) {
		return webRequest.getParameter("ajaxUpload") != null;
	}
}
