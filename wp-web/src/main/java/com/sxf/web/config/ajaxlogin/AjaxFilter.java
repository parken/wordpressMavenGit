package com.sxf.web.config.ajaxlogin;

import com.sxf.common.WPConstants;
import com.sxf.util.security.RSAUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.ReflectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;

/**
 * 自定义登陆(验证码,密码解密,是否被登录锁定等)
 * @author ThinkPad
 */
public class AjaxFilter extends UsernamePasswordAuthenticationFilter {
    // 是否开启验证码功能
    private boolean isOpenValidateCode = false;

    public static final String VALIDATE_CODE = "captcha";

    public static final String LOGIN_VO = "_login_vo";

    @Override
    protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest) {
        String password = (String) authRequest.getCredentials();
        HttpSession session = request.getSession(true);
        if (StringUtils.isNotBlank(password)) {
            //解密
            try {
                String privateKey = (String) session.getAttribute(WPConstants.RSA_PRIVATE_KEY);
                if (privateKey == null) {
                    privateKey = RSAUtils.getPrivateKey();
                }
                password = RSAUtils.decode(password, privateKey);
            } catch (RuntimeException e) {
                throw new AuthenticationServiceException("", e);
            }
            Field f = ReflectionUtils.findField(UsernamePasswordAuthenticationToken.class, "credentials");
            if (f != null) {
                f.setAccessible(true);
                ReflectionUtils.setField(f, authRequest, password);
            }
        }
        super.setDetails(request, authRequest);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        if (isOpenValidateCode) {
            checkValidateCode(request);
        }
        return super.attemptAuthentication(request, response);
    }

    /**
     * @param request
     */
    protected void checkValidateCode(HttpServletRequest request) {
        HttpSession session = request.getSession();

        String sessionValidateCode = obtainSessionValidateCode(session);
        // 让上一次的验证码失效
        session.setAttribute(VALIDATE_CODE, null);
        String validateCodeParameter = obtainValidateCodeParameter(request);
        if (StringUtils.isEmpty(validateCodeParameter)
                || !sessionValidateCode.equalsIgnoreCase(validateCodeParameter)) {
            throw new AuthenticationServiceException("验证码错误！");
        }
    }

    private String obtainValidateCodeParameter(HttpServletRequest request) {
        Object obj = request.getParameter(VALIDATE_CODE);
        return null == obj ? "" : obj.toString();
    }

    protected String obtainSessionValidateCode(HttpSession session) {
        Object obj = session.getAttribute(VALIDATE_CODE);
        return null == obj ? "" : obj.toString();
    }

    public boolean isOpenValidateCode() {
        return isOpenValidateCode;
    }

    public void setOpenValidateCode(boolean openValidateCode) {
        isOpenValidateCode = openValidateCode;
    }
}