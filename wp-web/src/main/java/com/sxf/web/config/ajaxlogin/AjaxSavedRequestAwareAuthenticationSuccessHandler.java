package com.sxf.web.config.ajaxlogin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sxf.common.redis.InvalidLoginTimesRegistry;
import com.sxf.common.WPConstants;
import com.sxf.common.utils.AjaxResult;
import com.sxf.spring.ApplicationUtils;
import com.sxf.web.ClientType;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import com.sxf.web.vo.LoginResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.handler.DispatcherServletWebRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*登录成功后处理
*/
public class AjaxSavedRequestAwareAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(AjaxSavedRequestAwareAuthenticationSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws
            ServletException, IOException {
        AjaxUser mu = (AjaxUser) authentication.getPrincipal();
        WebRequest wq = new DispatcherServletWebRequest(request, response);
        //清除登录用户的次数
        String accountName = (String) authentication.getName();
        InvalidLoginTimesRegistry invalidLoginTimesRegistry = ApplicationUtils.getContext().getBean(InvalidLoginTimesRegistry.class);
        invalidLoginTimesRegistry.delFailedTimes(accountName);

        //保存登录后的信息
        SessionUser su = getSU(request);
        su.setNickName(accountName);
        LoginResultVO loginResultVO = new LoginResultVO();
        request.getSession().setAttribute(WPConstants.LOGIN_USER, su);

        //读取需要跳转回的页面
        String service = request.getParameter("service");
        loginResultVO.setRedirectURL(service);


        clearAuthenticationAttributes(request);
        AjaxResult result = AjaxResult.buildSuccess();
        result.setData(loginResultVO);
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        ObjectMapper om = ApplicationUtils.getContext().getBean(ObjectMapper.class);
        String json = om.writeValueAsString(result);
        response.getWriter().write(json);
        response.getWriter().flush();
    }

    private SessionUser getSU(HttpServletRequest request) {
        SessionUser su = new SessionUser();
        su.setType(ClientType.MT);
        //设置用户ip
        su.setUserIp(LoginUtils.getClientIP(request));
        return su;
    }

}
