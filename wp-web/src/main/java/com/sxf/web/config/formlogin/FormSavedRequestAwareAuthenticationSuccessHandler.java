package com.sxf.web.config.formlogin;

import com.sxf.common.redis.InvalidLoginTimesRegistry;
import com.sxf.common.WPConstants;
import com.sxf.web.ClientType;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.handler.DispatcherServletWebRequest;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*登录成功后处理
*@auther shixiafeng  
*@create 2017-10-27
*/
public class FormSavedRequestAwareAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(FormSavedRequestAwareAuthenticationSuccessHandler.class);

    @Resource
    private InvalidLoginTimesRegistry invalidLoginTimesRegistry;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws
            ServletException, IOException {
        WebRequest wq = new DispatcherServletWebRequest(request, response);
        //清除登录用户的次数
        String accountName = (String) authentication.getName();
        invalidLoginTimesRegistry.delFailedTimes(accountName);

        //保存登录后的信息
        SessionUser su = getSU(request);
        su.setNickName(accountName);
        request.getSession().setAttribute(WPConstants.LOGIN_USER, su);
        super.onAuthenticationSuccess(request, response, authentication);
    }

    private SessionUser getSU(HttpServletRequest request) {
        SessionUser su = new SessionUser();
        if (ClientType.MT.name().equals(request.getHeader("ClientType"))) {
            su.setType(ClientType.MT);
        } else {
            su.setType(ClientType.PC);
        }
        //设置用户ip
        su.setUserIp(LoginUtils.getClientIP(request));
        return su;
    }
}
