package com.sxf.web.config.formlogin;

import com.sxf.domain.user.Role;
import com.sxf.service.UserService;
import com.sxf.spring.ApplicationUtils;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FormUserDetailsService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        UserService as = ApplicationUtils.getContext().getBean(UserService.class);

        // 认证用户信息
        com.sxf.domain.user.User user = as.findByLoginName(userName);

        if (user == null) {
            throw new UsernameNotFoundException("UserName " + userName + " not found");
        }

        //账户被锁定
        if (user.getStatus() == com.sxf.domain.user.User.Status.deny) {
            throw new LockedException("UserName " + userName + " locked");
        }

        // SecurityUser实现UserDetails并将SUser的Email映射为username
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        as.getDao().getDbClient().fetchLinks(user, com.sxf.domain.user.User.CascadeField.roles);
        List<Role> roles =  user.getRoles();
        if (!CollectionUtils.isEmpty(roles)) {
            //添加权限代码
            for (Role r : roles) {
                authorities.add(new SimpleGrantedAuthority(r.getRoleCode()));
            }
        }
        User securityUser = new User(user.getName(), user.getPassword(), authorities);
        return securityUser;
    }

}