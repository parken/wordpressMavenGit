package com.sxf.web.config.formlogin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sxf.common.utils.AjaxResult;
import com.sxf.spring.ApplicationUtils;
import com.sxf.util.WebUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.handler.DispatcherServletWebRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*自定义登出
*@auther shixiafeng  
*@create 2017-10-28
*/
public class FormLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

    public FormLogoutSuccessHandler() {

    }

    public FormLogoutSuccessHandler(String defaultTargetUrl) {
        this.setDefaultTargetUrl(defaultTargetUrl);
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException,
            ServletException {
        WebRequest wq = new DispatcherServletWebRequest(request, response);
        if (WebUtils.supportAjax(wq)) {
            AjaxResult result = AjaxResult.buildSuccess();
            ObjectMapper om = ApplicationUtils.getContext().getBean(ObjectMapper.class);
            String json = om.writeValueAsString(result);
            response.getWriter().write(json);
        }
        super.onLogoutSuccess(request, response, authentication);
    }
}
