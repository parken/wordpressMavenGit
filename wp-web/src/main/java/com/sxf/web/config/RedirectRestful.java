package com.sxf.web.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/*该类主要是为了登录后跳转使用.(配合LoginRestful的登录请求)
 */
@Controller
public class RedirectRestful {
    /**
     * 重定向请求 为登录服务
     * @param login
     * @param inner   是否站内 ,默认站内
     * @param request
     * @param ra
     * @return
     */
    @RequestMapping("/redirect")
    public String redirect(String login, @RequestParam(required = false, defaultValue = "true") Boolean inner, HttpServletRequest request, RedirectAttributes ra) {
        Enumeration<String> enumeration = request.getParameterNames();
        while (enumeration.hasMoreElements()) {
            String name = enumeration.nextElement();
            if (!"login".equals(name)) {
                ra.addAttribute(name, request.getParameter(name));
            }
        }
        return "redirect:" + login;
    }
}
