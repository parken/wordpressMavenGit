package com.sxf.web.config.ajaxlogin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sxf.common.utils.AjaxResult;
import com.sxf.spring.ApplicationUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.handler.DispatcherServletWebRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*自定义登出
 */
public class AjaxLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

    public AjaxLogoutSuccessHandler() {

    }

    public AjaxLogoutSuccessHandler(String defaultTargetUrl) {
        this.setDefaultTargetUrl(defaultTargetUrl);
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException,
            ServletException {
        WebRequest wq = new DispatcherServletWebRequest(request, response);
        AjaxResult result = AjaxResult.buildSuccess();
        ObjectMapper om = ApplicationUtils.getContext().getBean(ObjectMapper.class);
        String json = om.writeValueAsString(result);
        response.getWriter().write(json);
        response.getWriter().flush();
    }
}
