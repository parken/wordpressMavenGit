package com.sxf.web.config.ajaxlogin;

import com.sxf.domain.user.Role;
import com.sxf.domain.user.User;
import com.sxf.service.UserService;
import com.sxf.spring.ApplicationUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//@Service
public class AjaxUserDetailsService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        UserService as = ApplicationUtils.getContext().getBean(UserService.class);

        // 认证用户信息
        User account = as.findByLoginName(userName);

        if (account == null) {
            throw new UsernameNotFoundException("UserName " + userName + " not found");
        }

        //账户被锁定
        boolean lock = false;
        if (account.getStatus() == User.Status.deny) {
            lock = true;
        }

        // SecurityUser实现UserDetails并将SUser的Email映射为username
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        as.getDao().getDbClient().fetchLinks(account,User.CascadeField.roles);
        List<Role> roles =  account.getRoles();
        if (!CollectionUtils.isEmpty(roles)) {
            //添加权限代码
            for (Role r : roles) {
                authorities.add(new SimpleGrantedAuthority(r.getRoleCode()));
            }
        }
        AjaxUser securityUser = new AjaxUser(account.getName(), account.getPassword(), true,
                true, true, !lock, authorities);
        securityUser.setAccount(account);
        return securityUser;
    }

}