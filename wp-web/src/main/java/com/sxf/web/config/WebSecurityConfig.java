package com.sxf.web.config;

import com.sxf.web.config.formlogin.FormAuthenticationProvider;
import com.sxf.web.config.formlogin.FormLogoutSuccessHandler;
import com.sxf.web.config.formlogin.FormSavedRequestAwareAuthenticationSuccessHandler;
import com.sxf.web.config.formlogin.FormSecurityContextLogoutHandler;
import com.sxf.web.config.formlogin.FormSimpleUrlAuthenticationFailureHandler;
import com.sxf.web.config.formlogin.FormUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import javax.annotation.Resource;

/*
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true) // 开启security注解
@EnableConfigurationProperties({LoginProperties.class})
@ConditionalOnProperty(name = "cas.server.use", havingValue = "false", matchIfMissing = false)
public class WebSecurityConfig {

    @Autowired
    private LoginProperties loginProperties;

    @Configuration
    @EnableWebSecurity
    @EnableConfigurationProperties({LoginProperties.class})
    @ConditionalOnProperty(name = "cas.server.use", havingValue = "false", matchIfMissing = false)
    public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

        @Resource
        private SecurityContextLogoutHandler scf;

        @Resource
        private AuthenticationProvider authenticationProvider;

        @Resource
        private SavedRequestAwareAuthenticationSuccessHandler success;

        @Resource
        private SimpleUrlAuthenticationFailureHandler failure;

        @Resource
        private LoginProperties loginProperties;

        UsernamePasswordAuthenticationFilter cf = new UsernamePasswordAuthenticationFilter();

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(authenticationProvider);
            super.configure(auth);
        }

        @Override
        @Bean
        public AuthenticationManager authenticationManagerBean() throws Exception {
            AuthenticationManager am = super.authenticationManagerBean();
            return am;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {

            cf.setAuthenticationManager(authenticationManager());
            cf.setFilterProcessesUrl(loginProperties.getDoLoginUrl());//登录处理器请求
            cf.setAuthenticationSuccessHandler(success);
            cf.setAuthenticationFailureHandler(failure);
            LoginUrlAuthenticationEntryPoint entryPoint = new LoginUrlAuthenticationEntryPoint(loginProperties.getDoLoginUrl());
            http.authorizeRequests().antMatchers(
                    "/center/**",
                    "/usercenter/**")
                    .authenticated().antMatchers("/api/**","/public/**","/main*","/index*").permitAll().anyRequest().permitAll()
                    .and().csrf().disable().anonymous().and().headers().frameOptions().disable()
                    .and().exceptionHandling().authenticationEntryPoint(entryPoint)
                    .and().addFilter(cf)
                    .logout().addLogoutHandler(scf).logoutSuccessUrl(loginProperties.getSuccessUrl())
                    .logoutSuccessHandler(new FormLogoutSuccessHandler(loginProperties.getToLoginUrl()))
                    .permitAll();
        }
    }

    @Bean("authenticationProvider")
    public AuthenticationProvider authenticationProvider(UserDetailsService uds) {
        FormAuthenticationProvider dap = new FormAuthenticationProvider();
        dap.setUserDetailsService(uds);
        return dap;
    }

    /**
     * 登录成功后处理
     * @return
     */
    @Bean("savedRequestAwareAuthenticationSuccessHandler")
    public SavedRequestAwareAuthenticationSuccessHandler savedRequestAwareAuthenticationSuccessHandler() {
        FormSavedRequestAwareAuthenticationSuccessHandler success = new FormSavedRequestAwareAuthenticationSuccessHandler();
        success.setDefaultTargetUrl(loginProperties.getSuccessUrl());
        success.setUseReferer(false);
        return success;
    }

    /**
     * 登录失败后处理
     * @return
     */
    @Bean("simpleUrlAuthenticationFailureHandler")
    public SimpleUrlAuthenticationFailureHandler simpleUrlAuthenticationFailureHandler() {
        FormSimpleUrlAuthenticationFailureHandler failure = new FormSimpleUrlAuthenticationFailureHandler();
        failure.setDefaultFailureUrl(loginProperties.getToLoginUrl());
        return failure;
    }

    @Bean("userDetailsService")
    public UserDetailsService authenticationUserDetailsService() {
        FormUserDetailsService dap = new FormUserDetailsService();
        return dap;
    }


    @Bean
    public SecurityContextLogoutHandler scf() {
        SecurityContextLogoutHandler scf = new FormSecurityContextLogoutHandler();
        return scf;
    }

    /**
     * 设置用户密码的加密方式为BCrypt加密
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
