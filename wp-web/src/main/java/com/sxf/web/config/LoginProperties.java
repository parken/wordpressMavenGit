package com.sxf.web.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/*
 */
@ConfigurationProperties(prefix = "web.login")
public class LoginProperties {
    private String successUrl;
    private String failUrl;
    private String toLoginUrl;
    private String doLoginUrl;
    private String logoutUrl;

    public String getSuccessUrl() {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
    }

    public String getFailUrl() {
        return failUrl;
    }

    public void setFailUrl(String failUrl) {
        this.failUrl = failUrl;
    }

    public String getToLoginUrl() {
        return toLoginUrl;
    }

    public void setToLoginUrl(String toLoginUrl) {
        this.toLoginUrl = toLoginUrl;
    }

    public String getDoLoginUrl() {
        return doLoginUrl;
    }

    public void setDoLoginUrl(String doLoginUrl) {
        this.doLoginUrl = doLoginUrl;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }
}
