package com.sxf.web.config.ajaxlogin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sxf.common.CommonResultCode;
import com.sxf.common.ResultCodeInf;
import com.sxf.common.utils.AjaxResult;
import com.sxf.spring.ApplicationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.handler.DispatcherServletWebRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 暂时无用
 * 处理ajax登录失败的错误提示
 */
public class AjaxLoginUrlAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {
    Logger logger = LoggerFactory.getLogger(AjaxLoginUrlAuthenticationEntryPoint.class);


    public AjaxLoginUrlAuthenticationEntryPoint(String loginFormUrl) {
        super(loginFormUrl);
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException,
            ServletException {
        WebRequest wq = new DispatcherServletWebRequest(request, response);
        //用户未登录或已超时!
        String errorMsg = getMessageFromResource(CommonResultCode.ACCOUNT_CREDENTIAL, null, request);
        logger.error("", authException);
        //ajax登录
        AjaxResult error = AjaxResult.buildFail();
        error.setEnumCode(CommonResultCode.ACCOUNT_CREDENTIAL);
        error.setMessage(errorMsg);
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        ObjectMapper om = ApplicationUtils.getContext().getBean(ObjectMapper.class);
        String json = om.writeValueAsString(error);
        response.getWriter().write(json);
        response.getWriter().flush();
    }

    private String getMessageFromResource(ResultCodeInf code, Object[] args, HttpServletRequest request) {
        MessageSource ms = ApplicationUtils.getContext().getBean(MessageSource.class);
        return ms.getMessage(code.name(), args, code.name(), request.getLocale());
    }
}
