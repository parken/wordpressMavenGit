package com.sxf.web.config.ajaxlogin;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/*
*/
public class AjaxUser extends User {
    private com.sxf.domain.user.User account;

    public AjaxUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public AjaxUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public com.sxf.domain.user.User getAccount() {
        return account;
    }

    public void setAccount(com.sxf.domain.user.User account) {
        this.account = account;
    }
}
