package com.sxf.web.config.formlogin;

import com.sxf.common.redis.InvalidLoginTimesRegistry;
import com.sxf.exception.UserFrozenException;
import com.sxf.spring.ApplicationUtils;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * 自定义登录验证方法:主要判断用户是否被锁定
 */
public class FormAuthenticationProvider extends DaoAuthenticationProvider {
    private InvalidLoginTimesRegistry invalidLoginTimesRegistry;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (invalidLoginTimesRegistry == null) {
            invalidLoginTimesRegistry = ApplicationUtils.getContext().getBean(InvalidLoginTimesRegistry.class);
        }

        String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED"
                : authentication.getName();
        if (!"NONE_PROVIDED".equals(username)) {
            int failedTimes = invalidLoginTimesRegistry.getFailedTimes(username);
            if (failedTimes > 5) {
                //用户被冻结
                throw new UserFrozenException("User has been frozen.");
            }
        }
        return super.authenticate(authentication);
    }
}
