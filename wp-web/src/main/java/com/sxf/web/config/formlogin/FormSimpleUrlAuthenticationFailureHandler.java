package com.sxf.web.config.formlogin;

import com.sxf.common.ResultCodeInf;
import com.sxf.common.redis.InvalidLoginTimesRegistry;
import com.sxf.spring.ApplicationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*错误处理
*@create 2017-10-27
*/
public class FormSimpleUrlAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    private final static Logger logger = LoggerFactory.getLogger(FormSimpleUrlAuthenticationFailureHandler.class);
    @Resource
    private InvalidLoginTimesRegistry invalidLoginTimesRegistry;


    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws
            IOException, ServletException {

        //记录登录失败次数
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() != null) {
            //增加登录失败次数
            invalidLoginTimesRegistry.plusFailedTimes((String) authentication.getPrincipal());
        }
        super.onAuthenticationFailure(request, response, exception);
    }

    private String getMessageFromResource(ResultCodeInf code, Object[] args, HttpServletRequest request) {
        MessageSource ms = ApplicationUtils.getContext().getBean(MessageSource.class);
        return ms.getMessage(code.name(), args, code.name(), request.getLocale());
    }

}
