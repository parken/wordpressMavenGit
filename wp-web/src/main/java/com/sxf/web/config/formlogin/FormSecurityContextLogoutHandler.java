package com.sxf.web.config.formlogin;

import com.alibaba.fastjson.JSON;
import com.sxf.common.CommonResultCode;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import com.sxf.websocket.MessageHandler;
import com.sxf.websocket.vo.WSResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.socket.TextMessage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
 *@auther shixiafeng
 *@create 2017-12-02
 */
public class FormSecurityContextLogoutHandler extends SecurityContextLogoutHandler {

    @Autowired(required = false)
    private MessageHandler messageHandler;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        HttpSession session = request.getSession();
        SessionUser su = LoginUtils.getLoginUserNoException(request);
        if (su != null) {
            // 记录操作日志


            //登出时 同时登出所有页面.
            if (messageHandler != null) {
                WSResult result = WSResult.build();
                result.setEnumCode(CommonResultCode.ACCOUNT_NOT_LOGIN);
                result.setType(WSResult.MessageType.logout);
                TextMessage tm = new TextMessage(JSON.toJSONString(result));
//                messageHandler.sendMessageToHttpSessionId(session.getId(), tm);
            }
        }
        super.logout(request, response, authentication);
    }
}
