package com.sxf.web.advice;

import com.sxf.common.CommonResultCode;
import com.sxf.common.ResultCodeInf;
import com.sxf.common.utils.AjaxResult;
import com.sxf.exception.WPRuntimeException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;

import javax.servlet.http.HttpServletRequest;

/**
 * Restful接口异常处理
 * @author
 */
@ControllerAdvice(annotations = {RestController.class})
public class RestControllerAdvice {
    private static final Logger logger = LoggerFactory.getLogger(RestControllerAdvice.class);

    @Autowired
    MessageSource messageSource;

    @Value("${spring.http.multipart.maxFileSize:1Mb}")
    private String maxFileSize;

    /**
     * 处理HCMRuntimeException
     * @param e
     * @return
     */
    @ExceptionHandler(WPRuntimeException.class)
    @ResponseBody
    public AjaxResult<Void> handleHOSPRuntime(WPRuntimeException e, HttpServletRequest request) {
        logger.error("Error happened, request path [{}]", request.getServletPath());
        ResultCodeInf code = e.getResultCode();
        String msg = "";
        if (code == null) {
            // 没有设置错误码，则读取异常中的消息
            if (StringUtils.isNotBlank(e.getMessage())) {
                msg = e.getMessage();
                logger.error(msg, e);
                return new AjaxResult(CommonResultCode.UNKNOW_ERROR, msg);
            } else {
                code = CommonResultCode.UNKNOW_ERROR;
            }
        }
        msg = getMessageFromResource(code, e.getMessageParam());
        logger.error(msg, e);
        AjaxResult<Void> ar = new AjaxResult<>(code, msg);
        ar.setMessageParam(e.getMessageParam());
        // ar.setErrorMessage(e.getMessage());
        return ar;
    }

    /**
     * 处理上传文件过大异常
     * @param e
     * @return
     */
    @ExceptionHandler(MultipartException.class)
    @ResponseBody
    public AjaxResult<Void> handleFileException(MultipartException e) {
        String msg = "Upload file failed";
        ResultCodeInf code = CommonResultCode.FILE_UPLOAD_FAIL;
        if (e.getCause().getCause() instanceof MaxUploadSizeExceededException) {
            msg = "The image exceeds its maximum permitted size(1Mb)";
            code = CommonResultCode.FILE_EXCEEDS_LIMIT;
        }
        try {
            Object[] args = new Object[1];
            args[0] = maxFileSize;
            msg = getMessageFromResource(code, args);
        } catch (NoSuchMessageException ignored) {
        }
        logger.error(msg, e);
        return new AjaxResult<Void>(code, msg);
    }

    /**
     * 处理系统抛出的未能预知的异常
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public AjaxResult<Void> handleOthers(Exception e, HttpServletRequest request) {
        logger.error("Error happened, request path [{}]", request.getServletPath());
        logger.error(e.getMessage(), e);
        String msg = getMessageFromResource(CommonResultCode.SYSTEM_ERROR, null);
        return new AjaxResult<>(CommonResultCode.SYSTEM_ERROR, msg);
    }

    private String getMessageFromResource(ResultCodeInf code, Object[] args) {
        return messageSource.getMessage(code.name(), args, code.name(), LocaleContextHolder.getLocale());
    }

    /**
     * 权限不足
     * @param e
     * @return
     */
    @ExceptionHandler({AuthenticationException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public AjaxResult<Void> processUnauthenticatedException(AuthenticationException e) {
        return new AjaxResult<>(CommonResultCode.PERMISSION_DENIED);
    }
}
