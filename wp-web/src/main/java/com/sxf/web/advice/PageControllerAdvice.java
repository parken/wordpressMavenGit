package com.sxf.web.advice;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@ControllerAdvice(annotations = {Controller.class})
public class PageControllerAdvice {

    @ModelAttribute
    public void addAttribute(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession(true);
        /*String rsaPublicKey = (String) session.getAttribute(WPConstants.RSA_PUBLIC_KEY);
        if (StringUtils.isBlank(rsaPublicKey)) {
            synchronized (session) {
                session.setAttribute(WPConstants.RSA_PRIVATE_KEY, RSAUtils.getPrivateKey());
                session.setAttribute(WPConstants.RSA_PUBLIC_KEY, RSAUtils.getPublicKey());
            }
        }
        rsaPublicKey = (String) session.getAttribute(WPConstants.RSA_PUBLIC_KEY);
        // 注入RSA public key
        model.addAttribute(RSAUtils.PUBLIC_KEY_BASE64_MODEL_KEY, rsaPublicKey);*/
        // 注入ctx
        String ctx = request.getContextPath();
        model.addAttribute("ctx", ctx);
    }
}
