package com.sxf.web.interceptor;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebBindingInitializer;

import java.util.Date;

public class BindingInitializer implements WebBindingInitializer {
    // @InitBinder
    @Override
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new DateConvertEditor());
    }

}