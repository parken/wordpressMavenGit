package com.sxf.web.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sxf.common.CommonResultCode;
import com.sxf.common.ResultCodeInf;
import com.sxf.common.utils.AjaxResult;
import com.sxf.spring.ApplicationUtils;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*拦截所有的api必须都带有session
 */
public class SecurityInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        SessionUser su = LoginUtils.getLoginUserNoException(request);
        if (su == null) {
            AjaxResult error = AjaxResult.build();
            error.setEnumCode(CommonResultCode.ACCOUNT_NOT_LOGIN);
            error.setCode(CommonResultCode.ACCOUNT_NOT_LOGIN.getCode());
            String errorMsg = getMessageFromResource(CommonResultCode.ACCOUNT_NOT_LOGIN, null, request);
            error.setMessage(errorMsg);
            response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            ObjectMapper om = ApplicationUtils.getContext().getBean(ObjectMapper.class);
            String json = om.writeValueAsString(error);
            response.getWriter().write(json);
            response.getWriter().flush();
            return false;
        }
        return true;
    }

    private String getMessageFromResource(ResultCodeInf code, Object[] args, HttpServletRequest request) {
        MessageSource ms = ApplicationUtils.getContext().getBean(MessageSource.class);
        return ms.getMessage(code.name(), args, code.name(), request.getLocale());
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
