package com.sxf.web.vo;

public class LoginVO {

	/**
	 * 用户名
	 */
	private String username;
	
	/**
	 * 密码
	 */
	private String password;
	
	/**
	 * 验证码
	 */
	private String captcha;
	
	/**
	 * 角色<br>
	 * 1：系统管理员
	 * 2：云分区管理员
	 * 3：组织管理员
	 * 4：普通用户
	 */
	private int role;

	/**
	 * 登录后需要跳转的URL
	 */
	private String service;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}
}
