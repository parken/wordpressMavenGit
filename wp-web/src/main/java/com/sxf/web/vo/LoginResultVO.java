package com.sxf.web.vo;

import java.util.List;

/**
 * 登录结果
 */
public class LoginResultVO {

    /**
     * 下次登录是否需要输入验证码
     */
    private boolean showCaptcha;

    /**
     * 重定向的URL
     */
    private String redirectURL;

    /**
     * 是否需要强制修改密码
     */
    private boolean forceModifyPwd;

    /**
     * 是否被管理员锁定
     */
    private boolean locked;

    private List<String> roles;

    private String username;

    public boolean isShowCaptcha() {
        return showCaptcha;
    }

    public void setShowCaptcha(boolean showCaptcha) {
        this.showCaptcha = showCaptcha;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public boolean isForceModifyPwd() {
        return forceModifyPwd;
    }

    public void setForceModifyPwd(boolean forceModifyPwd) {
        this.forceModifyPwd = forceModifyPwd;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
