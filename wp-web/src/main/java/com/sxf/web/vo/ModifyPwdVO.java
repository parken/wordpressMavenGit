package com.sxf.web.vo;

import javax.validation.constraints.NotNull;

/**
 * 修改密码VO
 *
 */
public class ModifyPwdVO {

	private String accountName;
	
	/**
	 * 操作人密码（当前登录用户密码）
	 */
	@NotNull
	private String optPwd;
	
	/**
	 * 新密码
	 */
	@NotNull
	private String newPwd;
	
	/**
	 * 确认密码
	 */
	@NotNull
	private String confirmPwd;

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getOptPwd() {
		return optPwd;
	}

	public void setOptPwd(String optPwd) {
		this.optPwd = optPwd;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	public String getConfirmPwd() {
		return confirmPwd;
	}

	public void setConfirmPwd(String confirmPwd) {
		this.confirmPwd = confirmPwd;
	}
	
}
