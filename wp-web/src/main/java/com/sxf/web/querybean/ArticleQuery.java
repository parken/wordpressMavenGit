package com.sxf.web.querybean;

import com.sxf.common.vo.PageParam;
import com.sxf.domain.enumration.ArticleStatus;

import java.util.List;

/**
 * 数据库查询bean
 * 
 * @author SXF
 * 
 */
public class ArticleQuery extends PageParam {
	private Long id;
	private String title;// 标题
	private Long author;
	private String type;
	private ArticleStatus status;
	private List<ArticleStatus> statusList;// 状态列表

	private String dateByMonth;// 按月份归类

	// 根据 term查询
	public QueryType queryType;
	private String categoryName;
	private Long categoryId;

	/**
	 * 是否不包含内容
	 */
	private boolean noText;

	private int near;// 查询上一条,下一条(-1:上一条;0:什么都不做;1:下一条)

	public enum QueryType {
		queryArticle, queryByCategory
	}

	private static final long serialVersionUID = 1L;

	public ArticleQuery() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAuthor() {
		return author;
	}

	public void setAuthor(Long author) {
		this.author = author;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getDateByMonth() {
		return dateByMonth;
	}

	public void setDateByMonth(String dateByMonth) {
		this.dateByMonth = dateByMonth;
	}

	public ArticleStatus getStatus() {
		return status;
	}

	public void setStatus(ArticleStatus status) {
		this.status = status;
	}

	public List<ArticleStatus> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<ArticleStatus> statusList) {
		this.statusList = statusList;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 查询上一条,下一条(-1:上一条;0:什么都不做;1:下一条)
	 * 
	 * @return
	 */
	public int getNear() {
		return near;
	}

	/**
	 * 查询上一条,下一条(-1:上一条;0:什么都不做;1:下一条)
	 * 
	 * @param near
	 */
	public void setNear(int near) {
		this.near = near;
	}

	public boolean isNoText() {
		return noText;
	}

	public void setNoText(boolean noText) {
		this.noText = noText;
	}
}
