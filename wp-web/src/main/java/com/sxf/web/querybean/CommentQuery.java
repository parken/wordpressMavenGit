package com.sxf.web.querybean;

import com.sxf.common.vo.PageParam;
import com.sxf.domain.enumration.CommentStatus;

public class CommentQuery extends PageParam {
	private static final long serialVersionUID = 1L;

	private Long id;
	private Long articleId;
	private String author;
	private String authorIp;
	private CommentStatus status;

	public CommentQuery() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthorIp() {
		return authorIp;
	}

	public void setAuthorIp(String authorIp) {
		this.authorIp = authorIp;
	}

	public CommentStatus getStatus() {
		return status;
	}

	public void setStatus(CommentStatus status) {
		this.status = status;
	}
}
