package com.sxf.web.querybean;

import com.sxf.common.vo.PageParam;
import com.sxf.domain.enumration.CategoryType;

public class CategoryQuery extends PageParam {
    private Long id;
    private String title;
    private String slug;
    private CategoryType type;

    private static final long serialVersionUID = 1L;

    public CategoryQuery() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }
}
