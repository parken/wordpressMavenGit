package com.sxf.web.querybean;

import com.sxf.common.vo.PageParam;

/**
 * 数据库查询bean
 * 
 * @author SXF
 * 
 */
public class UserQuery extends PageParam {
	private long id;
	private String loginName;
	private String password;
	private String nicename;

	private static final long serialVersionUID = 1L;

	public UserQuery() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNicename() {
		return nicename;
	}

	public void setNicename(String nicename) {
		this.nicename = nicename;
	}

}
