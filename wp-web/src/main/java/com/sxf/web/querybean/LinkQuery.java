package com.sxf.web.querybean;

import com.sxf.common.vo.PageParam;

public class LinkQuery extends PageParam {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	// 根据 term查询
	private String termName;
	private Long termId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTermName() {
		return termName;
	}

	public void setTermName(String termName) {
		this.termName = termName;
	}

	public Long getTermId() {
		return termId;
	}

	public void setTermId(Long termId) {
		this.termId = termId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
