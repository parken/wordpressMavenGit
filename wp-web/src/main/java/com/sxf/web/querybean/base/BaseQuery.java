package com.sxf.web.querybean.base;

import java.io.Serializable;

/**
 * 分页基类
 * 
 * @author SXF
 * 
 */
public class BaseQuery implements Serializable {
	private static final long serialVersionUID = -8751408931020497781L;

	public final static int NO_ROW_OFFSET = 0;
	public final static int NO_ROW_LIMIT = Integer.MAX_VALUE;
	public final static BaseQuery DEFAULT = new BaseQuery();

	private int offset;
	private int limit;

	private int pageSize;// 每页记录数
	private int currentPage;// 当前页数

	public BaseQuery() {
		this.offset = NO_ROW_OFFSET;
		this.limit = NO_ROW_LIMIT;
	}

	public BaseQuery(int offset, int limit) {
		this.offset = offset;
		this.limit = limit;
	}

	/**
	 * 分页时使用,其他勿用
	 */
	public void calculate() {
		if (pageSize > 30) {
			pageSize = 30;
		}
		if (pageSize <= 0) {
			pageSize = 10;
		}
		if (currentPage <= 0) {
			currentPage = 1;
		}
		if (pageSize != 0 && currentPage != 0) {
			this.limit = pageSize;
			this.offset = (currentPage - 1) * pageSize;
		}
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

}
