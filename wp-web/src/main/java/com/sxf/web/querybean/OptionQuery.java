package com.sxf.web.querybean;

import com.sxf.common.vo.PageParam;

public class OptionQuery extends PageParam {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	private String value;

	public OptionQuery() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
