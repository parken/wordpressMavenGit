package com.sxf.web.querybean;

import com.sxf.common.vo.PageParam;

public class TermTaxonomyQuery extends PageParam {

	private static final long serialVersionUID = 1L;

	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
