package com.sxf.web;


import com.sxf.domain.user.User;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
public class SessionUser implements Serializable {

    private static final long serialVersionUID = -1L;

    private Long userId;

    /**
     * 用户名称
     */
    private String userName;
    /**
     *
     */
    private String nickName;

    private String accountId;
    /**
     * 登录用户ip
     */
    private String userIp;
    /**
     * 登录用户mac
     */
    private String userMac;

    /**
     * 登录用户类型
     */
    private ClientType type;

    /**
     * cas需要
     */
    private String indexCode;
    /**
     * cas需要
     */
    private String tgc;
    /**
     * cas需要
     */
    private String language;

    /**
     * 组织id
     */
    private String orgId;
    private String orgPath;

    /**
     * 能查看数据权限的组织id
     */
    private List<String> dataOrgIds;

    private List<String> roleCodes;

    private List<Integer> roleIds;


    public SessionUser() {

    }

    public SessionUser(User vo) {
        if (!CollectionUtils.isEmpty(vo.getRoles())) {
            this.roleIds = vo.getRoles().parallelStream().map(role -> role.getId()).collect(Collectors.toList());
            this.roleCodes = vo.getRoles().parallelStream().map(role -> role.getRoleCode()).collect(Collectors.toList());
        }
        this.setUserName(vo.getName());
        this.userId = vo.getId();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getUserMac() {
        return userMac;
    }

    public void setUserMac(String userMac) {
        this.userMac = userMac;
    }


    public ClientType getType() {
        return type;
    }

    public void setType(ClientType type) {
        this.type = type;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgPath() {
        return orgPath;
    }

    public void setOrgPath(String orgPath) {
        this.orgPath = orgPath;
    }

    public List<String> getDataOrgIds() {
        return dataOrgIds;
    }

    public void setDataOrgIds(List<String> dataOrgIds) {
        this.dataOrgIds = dataOrgIds;
    }

    public String getTgc() {
        return tgc;
    }

    public void setTgc(String tgc) {
        this.tgc = tgc;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getIndexCode() {
        return indexCode;
    }

    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    public List<String> getRoleCodes() {
        return roleCodes;
    }

    public void setRoleCodes(List<String> roleCodes) {
        this.roleCodes = roleCodes;
    }

    public List<Integer> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Integer> roleIds) {
        this.roleIds = roleIds;
    }

    @Override
    public String toString() {
        return "SessionUser{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", accountName='" + nickName + '\'' +
                ", accountId='" + accountId + '\'' +
                ", userIp='" + userIp + '\'' +
                ", userMac='" + userMac + '\'' +
                ", type=" + type +
                ", indexCode='" + indexCode + '\'' +
                ", tgc='" + tgc + '\'' +
                ", language='" + language + '\'' +
                ", orgId='" + orgId + '\'' +
                ", orgPath='" + orgPath + '\'' +
                ", dataOrgIds=" + dataOrgIds +
                ", roleCodes=" + roleCodes +
                ", roleIds=" + roleIds +
                '}';
    }
}
