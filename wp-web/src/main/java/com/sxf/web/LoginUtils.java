package com.sxf.web;

import com.sxf.common.CommonResultCode;
import com.sxf.common.WPConstants;
import com.sxf.exception.WPRuntimeException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 *@auther shixiafeng
 *@create 2017-10-27
 */
public class LoginUtils {
    private static final String CAS_SESSION_KEY = "_const_cas_assertion_";

    private final static Logger logger = LoggerFactory.getLogger(LoginUtils.class);

    public static String getClientIP(HttpServletRequest request) {
        // HAproxy
        String xForwardedFor = request.getHeader("X-Forwarded-For");
        return StringUtils.isBlank(xForwardedFor) ? request.getRemoteAddr() : xForwardedFor;
    }

    /**
     * 获取登录用户信息
     * @return
     */
    public static SessionUser getLoginUser() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return getLoginUser(request);
    }

    public static SessionUser getLoginUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        SessionUser user = getLoginUser(session, getClientIP(request));
        if (user == null) {
            logger.error("Cookie : " + request.getHeader("Cookie"));
            logger.error("get login user session id = " + (session != null ? session.getId() : " empty"));
            throw new WPRuntimeException(CommonResultCode.ACCOUNT_NOT_LOGIN);
        }
        return user;
    }

    public static SessionUser getLoginUserNoException() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return getLoginUserNoException(request);
    }

    public static SessionUser getLoginUserNoException(HttpServletRequest request) {
        SessionUser user = getLoginUser(request.getSession(), getClientIP(request));
        return user;
    }

    /**
     * 获取登录用户信息
     * @param session
     * @return null代表未登录
     */
    private static SessionUser getLoginUser(HttpSession session, String clientIP) {
        if (session == null) {
            return null;
        }
        //先获取登录用户
        SessionUser loginUser = (SessionUser) session.getAttribute(WPConstants.LOGIN_USER);
        if (loginUser != null && clientIP != null) {
            loginUser.setUserIp(clientIP);
        }
        return loginUser;
    }
}
