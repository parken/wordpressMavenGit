package com.sxf.web.controller.db;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sxf.service.DBAnalysisService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author SXF
 * 
 */
@Controller
@RequestMapping("/db")
public class DBAnalysisController {
	// @Autowired 不推荐使用(org.springframework.beans.factory.annotation.Autowired)
	// 推荐使用
	@Resource
	private DBAnalysisService dbas;

	@RequestMapping(value = "/showFtl", method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView showFtl() throws Exception {
		ModelAndView mv = new ModelAndView("db/proc");
		ModelMap mm = new ModelMap();
		mm.put("aaa", "测试而已");
		mm.put("test", "XXXXXXXX");
		mv.addAllObjects(mm);
		return mv;
	}

	@RequestMapping(value = "show", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String show() throws Exception {
		return "db/all";
	}

	/**
	 * @return
	 */
	@RequestMapping(value = "showAllTables")
	@ResponseBody
	public String showAllTables(HttpServletResponse response) throws Exception {
		Map<Integer, Object> map = dbas.getAllTables();
		// List<Integer> romoveKeys = new ArrayList<Integer>();
		// for (Integer i : map.keySet()) {
		// List<String> list = (List<String>) map.get(i);
		// if (!"TABLE".equals(list.get(3)) && !"VIEW".equals(list.get(3))) {
		// romoveKeys.add(i);
		// }
		// }
		// for (Integer i : romoveKeys) {
		// if (i>0) {
		// map.remove(i);
		// }
		// }
		response.reset();
		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString(map);
		PrintWriter pw = response.getWriter();
		pw.write(json);
		pw.flush();
		return null;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = "showAllTables1")
	@ResponseBody
	public Map<Integer, Object> showAllTables2() throws Exception {
		Map<Integer, Object> map = dbas.getAllTables();
		// List<Integer> romoveKeys = new ArrayList<Integer>();
		// for (Integer i : map.keySet()) {
		// List<String> list = (List<String>) map.get(i);
		// if (!"TABLE".equals(list.get(3)) && !"VIEW".equals(list.get(3))) {
		// romoveKeys.add(i);
		// }
		// }
		// for (Integer i : romoveKeys) {
		// if (i>0) {
		// map.remove(i);
		// }
		// }
		return map;
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "showAllProc", method = { RequestMethod.GET,
			RequestMethod.POST })
	@ResponseBody
	public Map<Integer, List<String>> showAllProc(WebRequest request)
			throws Exception {
		Map<Integer, List<String>> map = dbas.getAllProc();
		// ObjectMapper om = new ObjectMapper();
		// String json = om.writeValueAsString(map);
		return map;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "showAll", method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView showAll(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		ModelAndView mav = new ModelAndView();
		Map<String, ?> map = new HashMap<String, Object>();
		mav.addAllObjects(map);
		mav.setViewName("db/all");
		return mav;
	}

	public void setDbas(DBAnalysisService dbas) {
		this.dbas = dbas;
	}

}
