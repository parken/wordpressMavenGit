package com.sxf.web.controller;

import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Controller
public class PageController {
    @Resource
    private ServerProperties sp;

    /**
     * 展示首页
     * @param map
     * @return
     */
    @GetMapping("/index")
    public String toIndex(ModelMap map) {
        return "index";
    }

    @GetMapping("/")
    public String defaultIndex(ModelMap map) {
        return "redirect:/index";
    }

    /**
     * 登录后首页
     * @return
     */
    @RequestMapping("/main")
    public ModelAndView toMain(ModelMap map) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("main");
        return mav;
    }

    /**
     * 登录页面
     * @return
     */
    @RequestMapping("/toLogin")
    public ModelAndView toLogin() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("login");
        return mav;
    }

    /**
     * 转向任意页面
     * @param page
     * @return
     */
    @GetMapping("/view/{page}")
    public ModelAndView toView(@PathVariable String page) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(page);
        return mav;
    }
}
