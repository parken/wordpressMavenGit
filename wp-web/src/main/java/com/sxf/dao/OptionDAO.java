package com.sxf.dao;

import com.sxf.domain.blog.Option;
import sf.database.mapper.DaoMapper;


public interface OptionDAO extends DaoMapper<Option> {
}
