package com.sxf.dao;

import com.sxf.domain.user.User;
import sf.database.mapper.DaoMapper;

/**
 * Created by shixiafeng on 2017/7/18.
 */
public interface UserDAO extends DaoMapper<User> {

}
