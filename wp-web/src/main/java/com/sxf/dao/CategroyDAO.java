package com.sxf.dao;

import com.sxf.domain.blog.Category;
import com.sxf.web.querybean.CategoryQuery;
import sf.common.wrapper.Page;
import sf.database.mapper.DaoMapper;


public interface CategroyDAO extends DaoMapper<Category> {
    Page<Category> queryPage(CategoryQuery query);
}
