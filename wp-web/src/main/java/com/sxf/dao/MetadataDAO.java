package com.sxf.dao;

import com.sxf.domain.blog.Metadata;
import sf.database.mapper.DaoMapper;

public interface MetadataDAO extends DaoMapper<Metadata> {
}
