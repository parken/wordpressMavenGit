package com.sxf.dao.impl;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sxf.dao.DBAnalysisDAO;

@Repository
public class DBAnalysisDAOImpl implements DBAnalysisDAO {
	@Resource
	private JdbcTemplate jt;

	// Map<Integer,List<String>>
	@Override
	public Map<Integer, Object> getAllTables() {
		Map<Integer, Object> tableList = jt.execute(new ConnectionCallback<Map<Integer, Object>>() {
			@Override
			public Map<Integer, Object> doInConnection(Connection con) throws SQLException, DataAccessException {
				DatabaseMetaData dbmd = con.getMetaData();
				ResultSet rs = dbmd.getTableTypes();
				List<String> typeList = new ArrayList<String>();
				while (rs.next()) {
					typeList.add(rs.getString(1));
				}
				List<String> tab1 = new ArrayList<String>();
				Map<Integer, Object> map = new LinkedHashMap<Integer, Object>();
				int k = 0;
				String[] type = new String[typeList.size()];
				for (int i = 0; i < typeList.size(); i++) {
					type[i] = typeList.get(i);
					// SysLogUtil.info(typeList.get(i));
				}

				// schemaPattern有值,过滤其他杂七杂八的东西
				// types有值 对mysql的系统库无效,需要写为null
				rs = dbmd.getTables(null, dbmd.getUserName(), null, type);
				ResultSetMetaData rsd = rs.getMetaData();

				for (int i = 1; i <= rsd.getColumnCount(); i++) {
					tab1.add(rsd.getColumnName(i));
				}
				map.put(k++, tab1);

				while (rs.next()) {
					tab1 = new ArrayList<String>();
					for (int i = 1; i <= rsd.getColumnCount(); i++) {
						tab1.add(rs.getString(i));
					}
					map.put(k++, tab1);
				}
				return map;
			}
		});
		return tableList;
	}

	@Override
	public Map<Integer, List<String>> getAllProc() {
		Map<Integer, List<String>> procMap = jt.execute(new ConnectionCallback<Map<Integer, List<String>>>() {
			@Override
			public Map<Integer, List<String>> doInConnection(Connection con) throws SQLException, DataAccessException {
				DatabaseMetaData dbmd = con.getMetaData();
				ResultSet rs = null;

				List<String> procList = new ArrayList<String>();
				Map<Integer, List<String>> map = new LinkedHashMap<Integer, List<String>>();
				int k = 0;

				rs = dbmd.getProcedures(null, null, null);
				ResultSetMetaData rsd = rs.getMetaData();

				for (int i = 1; i <= rsd.getColumnCount(); i++) {
					procList.add(rsd.getColumnName(i));
				}
				map.put(k++, procList);

				while (rs.next()) {
					procList = new ArrayList<String>();
					for (int i = 1; i <= rsd.getColumnCount(); i++) {
						procList.add(rs.getString(i));
					}
					map.put(k++, procList);
				}
				return map;
			}
		});
		return procMap;
	}

}
