package com.sxf.dao.impl;

import com.sxf.dao.AttachmentDAO;
import com.sxf.domain.blog.Attachement;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.database.dao.DBClient;

@Transactional
@Repository
public class AttachmentDAOImpl extends SpringDaoMapper<Attachement> implements AttachmentDAO {
    public AttachmentDAOImpl(@Autowired DBClient dbClient) {
        super(Attachement.class, dbClient);
    }
}
