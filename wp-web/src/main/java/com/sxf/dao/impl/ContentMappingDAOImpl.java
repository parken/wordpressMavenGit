package com.sxf.dao.impl;

import com.sxf.dao.ContentMappingDAO;
import com.sxf.domain.blog.ArticleMapping;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.database.dao.DBClient;

@Transactional
@Repository
public class ContentMappingDAOImpl extends SpringDaoMapper<ArticleMapping> implements ContentMappingDAO {
    public ContentMappingDAOImpl(@Autowired DBClient dbClient) {
        super(ArticleMapping.class, dbClient);
    }
}
