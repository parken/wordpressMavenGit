package com.sxf.dao.impl;

import com.querydsl.core.types.Predicate;
import com.querydsl.sql.SQLQuery;
import com.sxf.dao.CategroyDAO;
import com.sxf.domain.blog.Category;
import com.sxf.web.querybean.CategoryQuery;
import org.apache.commons.lang3.StringUtils;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.common.wrapper.Page;
import sf.database.dao.DBClient;
import sf.querydsl.QueryDSLTables;
import sf.querydsl.SQLRelationalPath;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public class CategroyDAOImpl extends SpringDaoMapper<Category> implements CategroyDAO {
    public CategroyDAOImpl(@Autowired DBClient dbClient) {
        super(Category.class, dbClient);
    }

    @Override
    public Page<Category> queryPage(CategoryQuery query) {
        SQLRelationalPath t = getQueryDSLMapper().queryDSLTable();
        SQLQuery q = new SQLQuery();
        int offset = (query.getPageNo() - 1) * query.getPageSize();
        List<Predicate> conditions = new ArrayList<>();
        q.select(t.all()).from(t);
        if (StringUtils.isNotBlank(query.getTitle())){
            conditions.add(t.string(Category.Field.title).like("%"+query.getTitle()+"%",'\\'));
        }
        if (query.getId() != null) {
            conditions.add(t.number(Category.Field.id).eq(query.getId()));
        }
        //添加where条件
        q.where(conditions.toArray(new Predicate[conditions.size()]));
        return getDbClient().getQueryDSL().queryDSLSelectPage(q, Category.class, offset, query.getPageSize());
    }
}
