package com.sxf.dao.impl;

import com.sxf.dao.OptionDAO;
import com.sxf.domain.blog.Option;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.database.dao.DBClient;

@Transactional
@Repository
public class OptionDAOImpl extends SpringDaoMapper<Option> implements OptionDAO {
    public OptionDAOImpl(@Autowired DBClient dbClient) {
        super(Option.class, dbClient);
    }
}
