package com.sxf.dao.impl;

import com.sxf.dao.LinkDAO;
import com.sxf.domain.blog.Link;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.database.dao.DBClient;

@Transactional
@Repository
public class LinkDAOImpl extends SpringDaoMapper<Link> implements LinkDAO {
    public LinkDAOImpl(@Autowired DBClient dbClient) {
        super(Link.class, dbClient);
    }
}
