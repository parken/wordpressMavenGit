package com.sxf.dao.impl;

import com.sxf.dao.UserDAO;
import com.sxf.domain.user.User;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.database.dao.DBClient;

/**
 * Created by shixiafeng on 2017/7/18.
 */
@Transactional
@Repository
public class UserDAOImpl extends SpringDaoMapper<User> implements UserDAO {
    public UserDAOImpl(@Autowired DBClient dbClient) {
        super(User.class, dbClient);
    }
}
