package com.sxf.dao.impl;

import com.sxf.dao.CommentDAO;
import com.sxf.domain.blog.Comment;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.database.dao.DBClient;

@Transactional
@Repository
public class CommentDAOImpl extends SpringDaoMapper<Comment> implements CommentDAO {
    public CommentDAOImpl(@Autowired DBClient dbClient) {
        super(Comment.class, dbClient);
    }
}
