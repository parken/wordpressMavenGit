package com.sxf.dao.impl;

import com.sxf.dao.ArticleMappingDAO;
import com.sxf.domain.blog.ArticleMapping;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.database.dao.DBClient;

@Transactional
@Repository
public class ArticleMappingDAOImpl extends SpringDaoMapper<ArticleMapping> implements ArticleMappingDAO {
    public ArticleMappingDAOImpl(@Autowired DBClient dbClient) {
        super(ArticleMapping.class, dbClient);
    }
}
