package com.sxf.dao.impl;

import com.sxf.dao.MetadataDAO;
import com.sxf.domain.blog.Metadata;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.database.dao.DBClient;

@Transactional
@Repository
public class MetadataDAOImpl extends SpringDaoMapper<Metadata> implements MetadataDAO {
    public MetadataDAOImpl(@Autowired DBClient dbClient) {
        super(Metadata.class, dbClient);
    }
}
