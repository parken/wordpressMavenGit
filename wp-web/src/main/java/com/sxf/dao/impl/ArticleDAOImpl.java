package com.sxf.dao.impl;

import com.querydsl.core.QueryFlag;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringTemplate;
import com.querydsl.sql.SQLQuery;
import com.sxf.dao.ArticleDAO;
import com.sxf.domain.blog.Article;
import com.sxf.domain.blog.ArticleMapping;
import com.sxf.domain.blog.Category;
import com.sxf.web.querybean.ArticleQuery;
import org.apache.commons.lang3.StringUtils;
import org.smallframework.spring.SpringDaoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sf.common.wrapper.Page;
import sf.database.dao.DBClient;
import sf.querydsl.QueryDSLTables;
import sf.querydsl.SQLRelationalPath;
import sf.spring.util.CollectionUtils;

import java.util.*;

@Transactional
@Repository
public class ArticleDAOImpl extends SpringDaoMapper<Article> implements ArticleDAO {
    public ArticleDAOImpl(@Autowired DBClient dbClient) {
        super(Article.class, dbClient);
    }

    private Predicate[] conditionQuery(ArticleQuery q, SQLRelationalPath<?> qu) {
        List<Predicate> list = new ArrayList<Predicate>();
        if (q.getId() != null && q.getId() > 0) {
            Predicate p = qu.number(Article.Field.id).eq(q.getId());
            list.add(p);
        }
        if (StringUtils.isNotBlank(q.getTitle())) {
            Predicate p = qu.string(Article.Field.title).like("%" + q.getTitle() + "%", '\\');
            list.add(p);
        }
        if (!list.isEmpty()) {
            // 注意此处的处理
            Predicate[] p = list.toArray(new Predicate[list.size()]);
            return p;
        }
        return null;
    }

    private SQLQuery<Article> getSQLQuery(ArticleQuery contentQuery) {
        SQLQuery<Article> query = new SQLQuery<>();
        SQLRelationalPath<Article> qu = getQueryDSLMapper().queryDSLTable();
        if (contentQuery.isNoText()) {
            Path[] paths = qu.all();
            List<Path> list = new ArrayList<>();
            for (Path p : paths) {
                if (!Objects.equals(p, qu.string(Article.Field.content))) {
                    list.add(p);
                }
            }
            query.select(list.toArray(new Path[list.size()])).from(qu);
        } else {
            query.select(qu).from(qu);
        }
        Predicate[] p = conditionQuery(contentQuery, qu);
        if (p != null) {
            query.where(p);
        }
        return query;
    }

    @Override
    public List<Article> queryList(ArticleQuery q) {
        SQLQuery<Article> query = getSQLQuery(q);
        return getQueryDSLMapper().queryDSLSelectList(query);
    }

    @Override
    public Article queryOne(ArticleQuery q) {
        SQLQuery<Article> query = getSQLQuery(q);
        return getQueryDSLMapper().queryDSLSelectOne(query,Article.class);
    }

    @Override
    public long queryNumber(ArticleQuery q) {
        SQLQuery<Article> query = new SQLQuery<>();
        SQLRelationalPath<Article> qu = getQueryDSLMapper().queryDSLTable();
        query.select(qu.number(Article.Field.id).count()).from(qu);
        Predicate[] p = conditionQuery(q, qu);
        if (p != null) {
            query.where(p);
        }
        Long l = getQueryDSLMapper().queryDSLSelectOne(query, Long.class);
        return l;
    }

    @Override
    public Page<Article> queryPage(ArticleQuery query) {
        SQLRelationalPath t = QueryDSLTables.relationalPathBase(Article.class);
        SQLQuery q = new SQLQuery();
        int offset = (query.getPageNo() - 1) * query.getPageSize();
        List<Predicate> conditions = new ArrayList<>();

        Path[] paths = t.all();
        if (query.isNoText()) {
            List<Path> list = new ArrayList<>();
            for (Path p : paths) {
                if (!Objects.equals(p, t.string(Article.Field.content))) {
                    list.add(p);
                }
            }
            paths = list.toArray(new Path[list.size()]);
        }

        SQLRelationalPath c = QueryDSLTables.relationalPathBase(null,Category.class);
        SQLRelationalPath m = QueryDSLTables.relationalPathBase(null,ArticleMapping.class);
        if (query.getCategoryId() != null || query.getCategoryName() != null) {
            conditions.add(t.number(Article.Field.id).eq(m.number(ArticleMapping.Field.articleId)));
            conditions.add(m.number(ArticleMapping.Field.categoryId).eq(c.number(Category.Field.id)));
            if (query.getCategoryId() != null) {
                conditions.add(c.number(Category.Field.id).eq(query.getCategoryId()));
            }
            if (StringUtils.isNotBlank(query.getCategoryName())) {
                conditions.add(c.string(Category.Field.title).eq(query.getCategoryName()));
            }
            q.select(paths).from(t, c, m);

        } else {
            q.select(paths).from(t);
        }
        if (query.getId() != null) {
            conditions.add(t.number(Article.Field.id).eq(query.getId()));
        }
        if (StringUtils.isNotBlank(query.getTitle())) {
            conditions.add(t.string(Article.Field.title).like("%" + query.getTitle() + "%", '\\'));
        }
        if (query.getStatus() != null) {
            conditions.add(((EnumPath) t.enums(Article.Field.status)).eq(query.getStatus()));
        }
        if (CollectionUtils.isNotEmpty(query.getStatusList())) {
            conditions.add(((EnumPath) t.enums(Article.Field.status)).in(query.getStatusList()));
        }
        if (StringUtils.isNotBlank(query.getDateByMonth())) {
            //建立格式化模板
            StringTemplate dateExpr = Expressions.stringTemplate("date_format({0},'%Y%m')", t.dateTime(Article.Field.created));
            conditions.add(dateExpr.eq(query.getDateByMonth()));
        }
        //添加where条件
        q.where(conditions.toArray(new Predicate[conditions.size()]));
        return getDbClient().getQueryDSL().queryDSLSelectPage(q, Article.class, offset, query.getPageSize());
    }

    @Override
    public Map<String, Integer> queryStatistic() {
        SQLRelationalPath table = QueryDSLTables.relationalPathBase(Article.class);
        SQLQuery query = new SQLQuery();
        query.select(table.string(Article.Field.status)).from(table).groupBy(table.string(Article.Field.status));
        query.addFlag(QueryFlag.Position.AFTER_SELECT, "count(1) count");
//        String sql = "SELECT t.`status`,count(1) `count` from wp_posts t GROUP BY t.`status`";
        List<Map> mapList = getDbClient().getQueryDSL().queryDSLSelectList(query, Map.class);
        Map<String, Integer> map = new HashMap<>();
        if (!mapList.isEmpty()) {
            for (Map<String, Object> m : mapList) {
                for (Map.Entry<String, Object> entry : m.entrySet()) {
                    Object o = entry.getValue();
                    if (o != null && o instanceof Number) {
                        map.put(entry.getKey(), ((Number) o).intValue());
                        entry.setValue(((Number) o).intValue());
                    }
                }
            }
        }
        return map;
    }

    @Override
    public List<Date> getArchiveArticles() {
        String sql = "SELECT distinct str_to_date( concat(date_format(created,'%Y%m'),'01'),'%Y%m%d') FROM wp_article order by created desc";
        return getDbClient().selectList(Date.class, sql);
    }
}
