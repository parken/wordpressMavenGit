package com.sxf.dao;

import com.sxf.domain.blog.Link;
import sf.database.mapper.DaoMapper;


public interface LinkDAO extends DaoMapper<Link> {

}
