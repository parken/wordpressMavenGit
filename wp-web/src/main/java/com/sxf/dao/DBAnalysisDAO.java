package com.sxf.dao;

import java.util.List;
import java.util.Map;

/**
 * @author phsxf01
 * 
 */
public interface DBAnalysisDAO {
	// abstract 关键字可写可不写
	/**
	 * 获取所有的表名称 :<br>
	 * (Map&lt;Integer,List&lt;String>>)
	 * 
	 * @return
	 */
	Map<Integer, Object> getAllTables();

	Map<Integer, List<String>> getAllProc();

}
