package com.sxf.dao;

import com.sxf.domain.blog.ArticleMapping;
import sf.database.mapper.DaoMapper;


public interface ArticleMappingDAO extends DaoMapper<ArticleMapping> {

}
