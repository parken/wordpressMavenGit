package com.sxf.dao;

import com.sxf.domain.blog.Attachement;
import sf.database.mapper.DaoMapper;


public interface AttachmentDAO extends DaoMapper<Attachement> {

}
