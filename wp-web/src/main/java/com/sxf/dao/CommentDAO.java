package com.sxf.dao;

import com.sxf.domain.blog.Comment;
import sf.database.mapper.DaoMapper;

public interface CommentDAO extends DaoMapper<Comment> {
}
