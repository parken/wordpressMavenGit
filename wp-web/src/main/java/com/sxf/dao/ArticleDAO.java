package com.sxf.dao;

import com.sxf.domain.blog.Article;
import com.sxf.web.querybean.ArticleQuery;
import sf.common.wrapper.Page;
import sf.database.mapper.DaoMapper;

import java.util.Date;
import java.util.List;
import java.util.Map;


public interface ArticleDAO extends DaoMapper<Article> {

    List<Article> queryList(ArticleQuery q);

    Article queryOne(ArticleQuery q);

    long queryNumber(ArticleQuery q);

    Page<Article> queryPage(ArticleQuery query);

    Map<String, Integer> queryStatistic();

    /**获取归档文件
     * @return
     */
    List<Date> getArchiveArticles();
}
