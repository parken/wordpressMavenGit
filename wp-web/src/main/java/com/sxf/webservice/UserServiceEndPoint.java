package com.sxf.webservice;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;

import com.sxf.service.UserService;
import org.springframework.stereotype.Service;

import com.sxf.domain.user.User;
import com.sxf.web.querybean.UserQuery;

/**
 * 访问地址:http://127.0.0.1:9999/userService?wsdl
 * 
 * @author shixiafeng
 *
 */
@Service
@WebService(serviceName = "userService")
public class UserServiceEndPoint {
	@Resource
	private UserService us;

	@WebMethod
	public List<User> queryOne(UserQuery u) {
		return us.queryList(u);
	}
}
