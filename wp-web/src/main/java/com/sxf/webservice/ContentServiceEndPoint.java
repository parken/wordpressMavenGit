package com.sxf.webservice;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;

import com.sxf.service.ArticleService;
import org.springframework.stereotype.Service;

import com.sxf.domain.blog.Article;
import com.sxf.web.querybean.ArticleQuery;

/**
 * 访问地址:http://127.0.0.1:9999/postService?wsdl
 * 
 * @author shixiafeng
 *
 */
@Service
@WebService(serviceName = "contentService")
public class ContentServiceEndPoint {
	@Resource
	private ArticleService ps;

	@WebMethod
	public List<Article> queryOne(ArticleQuery u) {
		return ps.queryList(u);
	}
}
