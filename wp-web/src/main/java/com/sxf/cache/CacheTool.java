package com.sxf.cache;

import com.sxf.common.WPConstants;
import com.sxf.domain.blog.Option;
import com.sxf.service.OptionService;
import com.sxf.spring.ApplicationUtils;
import com.sxf.web.querybean.OptionQuery;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 缓存工具(缓存Option,全局模板)
 * @author SXF
 */
@Service
@SuppressWarnings("unchecked")
public class CacheTool {
    @Resource
    private OptionService optionService;
    @Resource
    private CacheManager cm;

    private static final int DEFAULT_BLOGID = 1;

    private static int DATA_GET_BY = 1;// 0,db;1,cache\

    private static final String OPTION_KEY = "option_key";

    public CacheTool() {

    }

    // spring 启动后执行
    @PostConstruct
    public void init() throws Exception {
        if (DATA_GET_BY == 1) {
            refreshOption();
        }
    }

    /**
     * 以Blogid检索
     * @throws Exception
     */
    private void refreshOption() throws Exception {
        OptionQuery oq = new OptionQuery();
        List<Option> blogOptionList = optionService.queryList(oq);
        if (blogOptionList != null && !blogOptionList.isEmpty()) {
            Map<String, String> optMap = new HashMap<String, String>();
            for (Option opt : blogOptionList) {
                optMap.put(opt.getName(), opt.getValue());
            }
            cm.getCache(WPConstants.BLOG_OPTION_CACHE).evict(OPTION_KEY);
            cm.getCache(WPConstants.BLOG_OPTION_CACHE).put(OPTION_KEY, optMap);
        }
    }

    public static String getOption(String key) {
        String value = "";
        if (DATA_GET_BY == 1) {
            try {
                Map<String, String> map = (ApplicationUtils.getContext().getBean("cacheManager",
                        CacheManager.class)).getCache(WPConstants.BLOG_OPTION_CACHE).get(OPTION_KEY, Map.class);
                if (map != null) {
                    value = map.get(key);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            value = ((ApplicationUtils.getContext().getBean(CacheTool.class))).getOptionByDB(key);
        }
        return value;
    }

    public String getOptionByDB(String key) {
        OptionQuery oq = new OptionQuery();
        oq.setName(key);
        List<Option> optionList = optionService.queryList(oq);
        String value = null;
        if (optionList != null && !optionList.isEmpty()) {
            value = optionList.get(0).getValue();
        }
        return value;
    }
}
