package com.sxf.blogmanage.vo;

import com.sxf.domain.blog.Category;
import com.sxf.domain.enumration.CategoryType;
import sf.database.annotations.Comment;
import sf.tools.StringUtils;

import java.util.Date;

public class CategoryVO {
    private Long id;
    private String title;
    /**
     * 描述
     */
    private String content;

    @Comment("摘要")
    private String summary;
    /**
     * slug
     */
    private String slug;
    /**
     * 类型
     */
    @Comment("类型，比如：分类、tag、专题")
    private CategoryType type;
    /**
     * 图标
     */
    private String icon;
    /**
     * 该分类的内容数量
     */
    private Long count;
    /**
     * 排序编码
     */
    private Long orderNumber;
    /**
     * 父级分类的ID
     */
    private Long parentId;
    /**
     * 创建人员
     */
    private Long userId;
    /**
     * 标识
     */
    private String flag;

    /**
     * SEO关键字
     */
    private String metaKeywords;
    /**
     * SEO描述内容
     */
    private String metaDescription;
    /**
     * 创建日期
     */
    @Comment("创建日期")
    private Date created;

    public CategoryVO() {

    }

    public CategoryVO(Category c) {
        this.id = c.getId();
        this.title = c.getTitle();
        this.content = c.getContent();
        this.summary = c.getSummary();
        this.slug = c.getSlug();
        this.type = c.getType();
        this.icon = c.getIcon();
        this.count = c.getCount();
        this.orderNumber = c.getOrderNumber();
        this.parentId = c.getParentId();
        this.userId = c.getUserId();
        this.flag = c.getFlag();
        this.metaKeywords = c.getMetaKeywords();
        this.metaDescription = c.getMetaDescription();
        this.created = c.getCreated();
    }

    public Category toWebCategory() {
        Category c = new Category();
        if (id != null) {
            c.setId(id);
        }
        if (StringUtils.isNotBlank(title)) {
            c.setTitle(title);
        }
        if (StringUtils.isNotBlank(content)) {
            c.setContent(content);
        }
        if (StringUtils.isNotBlank(summary)) {
            c.setSummary(summary);
        }
        if (StringUtils.isNotBlank(slug)) {
            c.setSlug(slug);
        }
        if (type != null) {
            c.setType(type);
        }
        if (StringUtils.isNotBlank(flag)) {
            c.setFlag(flag);
        }
        if (StringUtils.isNotBlank(icon)) {
            c.setIcon(icon);
        }
        if (orderNumber != null) {
            c.setOrderNumber(orderNumber);
        }
        return c;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
