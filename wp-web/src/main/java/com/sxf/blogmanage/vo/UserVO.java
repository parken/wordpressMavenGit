package com.sxf.blogmanage.vo;

import com.sxf.common.BeanCopierUtils;
import com.sxf.domain.user.User;
import sf.database.annotations.Comment;

import java.util.Date;

public class UserVO {
    private static final long serialVersionUID = 1L;

    private Long id;


    private String name;

    /**
     * 密码
     */
    private String password;
    /**
     * 盐值
     */
    private String salt;
    /**
     * 密码强度
     */
    private String pwdStrength;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 真实名称
     */
    private String realname;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机
     */
    private String phone;

    @Comment("头像")
    private byte[] icon;

    /**
     * 注册时间
     */
    private Date registered;

    /**
     * 激活码
     */
    private String activationKey;

    private User.Status status;

    private Boolean locked;

    private Boolean modifyPwd;

    public UserVO() {

    }

    public UserVO(User u) {
        if (u != null) {
            BeanCopierUtils.copyProperties(u, this);
        }
    }

    public User toUser() {
        User u = new User();
        BeanCopierUtils.copyProperties(this, u);
        return u;
    }

    public User webToUser() {
        User u = new User();
        if (id != null) {
            u.setId(id);
        }
        if (name != null) {
            u.setName(name);
        }
        u.setSalt(salt);
        u.setPassword(password);
        u.setPwdStrength(pwdStrength);
        u.setNickname(nickname);
        u.setRealname(realname);
        u.setEmail(email);
        u.setPhone(phone);
        u.setIcon(icon);
        u.setRegistered(registered);
        u.setActivationKey(activationKey);
        u.setStatus(status);
        u.setLocked(locked);
        u.setModifyPwd(modifyPwd);
        return u;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPwdStrength() {
        return pwdStrength;
    }

    public void setPwdStrength(String pwdStrength) {
        this.pwdStrength = pwdStrength;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public User.Status getStatus() {
        return status;
    }

    public void setStatus(User.Status status) {
        this.status = status;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Boolean getModifyPwd() {
        return modifyPwd;
    }

    public void setModifyPwd(Boolean modifyPwd) {
        this.modifyPwd = modifyPwd;
    }
}
