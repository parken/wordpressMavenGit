package com.sxf.blogmanage.vo;

import com.sxf.common.BeanCopierUtils;
import com.sxf.domain.blog.Link;
import lombok.Data;
import sf.tools.StringUtils;

import java.util.Date;

@Data
public class LinkVO {
    private Long id;

    private String url;

    private String name;

    private String image;

    private String target;

    private String description;

    private Boolean visible;

    private Long owner;

    private Integer rating;// 评分

    private Date created;

    private Date modified;

    private String rel;

    private String rss;

    private String notes;

    public LinkVO(Link l) {
        if (l != null) {
            BeanCopierUtils.copyProperties(l, this);
        }
    }

    public Link toLink() {
        Link link = new Link();
        BeanCopierUtils.copyProperties(this, link);
        return link;
    }

    public Link webToLink() {
        Link l = new Link();
        if (id != null) {
            l.setId(id);
        }
        if (StringUtils.isNotBlank(url)) {
            l.setUrl(url);
        }
        if (StringUtils.isNotBlank(target)) {
            l.setTarget(target);
        }
        if (StringUtils.isNotBlank(description)) {
            l.setDescription(description);
        }
        if (StringUtils.isNotBlank(url)) {
            l.setUrl(url);
        }
        if (visible != null) {
            l.setVisible(true);
        }
        if (StringUtils.isNotBlank(rel)) {
            l.setRel(rel);
        }
        if (StringUtils.isNotBlank(rss)) {
            l.setRss(rss);
        }
        if (StringUtils.isNotBlank(notes)) {
            l.setNotes(notes);
        }
        l.setName(name);
        l.setImage(image);
        return l;
    }
}
