package com.sxf.blogmanage.vo;

import com.sxf.domain.blog.Article;
import com.sxf.domain.blog.Comment;
import com.sxf.domain.enumration.CommentStatus;
import sf.tools.StringUtils;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 查询的comment结果(包含联合查询)
 * @author phsxf01
 */
public class CommentVO {
    private Long id;
    /**
     * 父ID
     */
    private Long parentId;
    /**
     * 评论的内容ID
     */
    private Long articleId;

    /**
     * 评论的用户ID
     */
    private Long fromUserId;

    /**
     * 评论的用户名称
     */
    private String fromUserNickName;

    /**
     * 评论者头像url
     */
    private String fromAvatar;
    /**
     * 被评论的用户ID
     */
    private Long toUserId;

    /**
     * 被评论的用户名称
     */
    private String toUserNickName;

    /**
     * 被评论者头像url
     */
    private String toAvatar;
    /**
     * 评论的回复数量
     */
    private Long replyCount;
    /**
     * 排序编号，常用语置顶等
     */
    private Long orderNumber;

    /**
     * 评论的IP地址
     */
    private String ip;

    /**
     * 评论的内容
     */
    private String content;
    /**
     * 提交评论的浏览器信息
     */
    private String agent;
    /**
     * 评论的时间
     */
    private Date created;
    /**
     * 评论用户的email
     */
    private String email;
    /**
     * 评论的状态
     */
    private CommentStatus status;
    /**
     * “顶”的数量
     */
    private Long voteUp;
    /**
     * “踩”的数量
     */
    private Long voteDown;
    private String version;

    // Post
    private Article article;
    /**
     * 子评论
     */
    private List<CommentVO> replys = new ArrayList<CommentVO>();

    public CommentVO() {

    }

    public CommentVO(Comment c) {
        this.id = c.getId();
        this.parentId = c.getParentId();
        this.articleId = c.getArticleId();
        this.fromUserId = c.getFromUserId();
        this.toUserId = c.getToUserId();
        this.replyCount = c.getReplyCount();
        this.orderNumber = c.getOrderNumber();
        this.ip = c.getIp();
        this.content = c.getContent();
        this.agent = c.getAgent();
        this.created = c.getCreated();
        this.email = c.getEmail();
        this.status = c.getStatus();
        this.voteUp = c.getVoteUp();
        this.voteDown = c.getVoteDown();
        this.version = c.getVersion();
    }

    public Comment toComment() {
        Comment c = new Comment();
        if (id != null) {
            c.setId(id);
        }
        if (status != null) {
            c.setStatus(status);
        }
        if (parentId != null) {
            c.setParentId(parentId);
        }
        if (articleId != null) {
            c.setArticleId(articleId);
        }
        if (fromUserId != null) {
            c.setFromUserId(fromUserId);
        }
        if (toUserId != null) {
            c.setToUserId(toUserId);
        }
        if (StringUtils.isNotBlank(content)) {
            c.setContent(content);
        }
        if (StringUtils.isNotBlank(email)) {
            c.setEmail(email);
        }
        if (StringUtils.isNotBlank(agent)) {
            c.setAgent(agent);
        }
        if (StringUtils.isNotBlank(ip)) {
            c.setIp(ip);
        }
        if (StringUtils.isNotBlank(version)) {
            c.setVersion(version);
        }
        return c;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getFromUserNickName() {
        return fromUserNickName;
    }

    public void setFromUserNickName(String fromUserNickName) {
        this.fromUserNickName = fromUserNickName;
    }

    public String getFromAvatar() {
        return fromAvatar;
    }

    public void setFromAvatar(String fromAvatar) {
        this.fromAvatar = fromAvatar;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public String getToUserNickName() {
        return toUserNickName;
    }

    public void setToUserNickName(String toUserNickName) {
        this.toUserNickName = toUserNickName;
    }

    public String getToAvatar() {
        return toAvatar;
    }

    public void setToAvatar(String toAvatar) {
        this.toAvatar = toAvatar;
    }

    public Long getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(Long replyCount) {
        this.replyCount = replyCount;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CommentStatus getStatus() {
        return status;
    }

    public void setStatus(CommentStatus status) {
        this.status = status;
    }

    public Long getVoteUp() {
        return voteUp;
    }

    public void setVoteUp(Long voteUp) {
        this.voteUp = voteUp;
    }

    public Long getVoteDown() {
        return voteDown;
    }

    public void setVoteDown(Long voteDown) {
        this.voteDown = voteDown;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public List<CommentVO> getReplys() {
        return replys;
    }

    public void setReplys(List<CommentVO> replys) {
        this.replys = replys;
    }
}
