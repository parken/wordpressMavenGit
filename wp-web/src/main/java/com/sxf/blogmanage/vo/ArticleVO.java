package com.sxf.blogmanage.vo;

import com.sxf.domain.blog.Article;
import com.sxf.domain.blog.Category;
import com.sxf.domain.enumration.ArticleStatus;
import com.sxf.domain.enumration.CategoryType;
import org.apache.commons.lang3.StringUtils;
import sf.database.annotations.Comment;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Post扩展属性
 * @author phsxf01
 */
public class ArticleVO {
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    @NotNull
    private String content;

    /**
     * 摘要
     */
    private String summary;

    @Comment("链接到(常用于谋文章只是一个连接)")
    private String linkTo;

    private Article.EditMode editMode;

    /**
     * 缩略图
     */
    private String thumbnail;

    /**
     * 样式
     */
    private String style;
    /**
     * 匿名稿的用户
     */
    private String author;
    /**
     * 匿名稿的邮箱
     */
    private String userEmail;
    /**
     * IP地址
     */
    private String userIp;
    /**
     * 发布浏览agent
     */
    private String userAgent;
    /**
     * 父级内容ID
     */
    private Long parentId;
    /**
     * 排序编号
     */
    private Long orderNumber;
    /**
     * 状态
     */
    @Comment("文章状态:publish(发布),trash(垃圾箱),draft(草稿)")
    private ArticleStatus status;// publish(发布),trash(垃圾箱),draft(草稿)
    /**
     * 支持人数
     */
    @Comment("点赞人数")
    private Long voteUp;
    /**
     * 反对人数
     */
    @Comment("踩人数")
    private Long voteDown;

    /**
     * 评论状态
     */
    private Boolean commentStatus;// open
    /**
     * 评论总数
     */
    private Long commentCount;
    /**
     * 最后评论时间
     */
    private Date commentTime;
    /**
     * 访问量
     */
    private Long viewCount;
    /**
     * 创建日期
     */
    private Date created;
    /**
     * 最后更新日期
     */
    private Date modified;
    /**
     * slug
     */
    private String slug;
    /**
     * 标识
     */
    @Column
    private String flag;

    /**
     * SEO关键字
     */
    private String metaKeywords;
    /**
     * SEO描述信息
     */
    @Comment("SEO描述信息")
    private String metaDescription;
    /**
     * 备注信息
     */
    @Comment("备注信息")
    private String remarks;

    //标签信息
    private List<Long> tagIds;
    //分类信息
    private List<Long> categoryIds;

    //标签信息
    private List<String> tagNames;
    //分类信息
    private List<String> categoryNames;

    public ArticleVO() {

    }

    public ArticleVO(Article a) {
        this.id = a.getId();
        this.userId = a.getUserId();
        this.title = a.getTitle();
        this.content = a.getContent();
        this.summary = a.getSummary();
        this.linkTo = a.getLinkTo();
        this.thumbnail = a.getThumbnail();
        this.style = a.getStyle();
        this.author = a.getAuthor();
        this.userEmail = a.getUserEmail();
        this.userIp = a.getUserIp();
        this.userAgent = a.getUserAgent();
        this.parentId = a.getParentId();
        this.orderNumber = a.getOrderNumber();
        this.status = a.getStatus();
        this.voteUp = a.getVoteUp();
        this.voteDown = a.getVoteDown();
        this.editMode = a.getEditMode();
        this.commentStatus = a.getCommentStatus();
        this.commentCount = a.getCommentCount();
        this.commentTime = a.getCommentTime();
        this.viewCount = a.getViewCount();
        this.created = a.getCreated();
        this.modified = a.getModified();
        this.slug = a.getSlug();
        this.flag = a.getFlag();
        this.metaKeywords = a.getMetaKeywords();
        this.metaDescription = a.getMetaDescription();
        this.remarks = a.getRemarks();

        if (a.getCategories() != null) {
            tagIds = new ArrayList<>();
            tagNames = new ArrayList<>();
            categoryIds = new ArrayList<>();
            categoryNames = new ArrayList<>();
            for (Category c : a.getCategories()) {
                if (c.getType() == CategoryType.tag) {
                    tagIds.add(c.getId());
                    tagNames.add(c.getTitle());
                }
                if (c.getType() == CategoryType.category) {
                    categoryIds.add(c.getId());
                    categoryNames.add(c.getTitle());
                }
            }
        }
    }

    public Article toArticle() {
        Article a = new Article();
        a.setId(id);
        a.setUserId(userId);
        a.setTitle(title);
        a.setContent(content);
        a.setSummary(summary);
        a.setLinkTo(linkTo);
        a.setThumbnail(thumbnail);
        a.setEditMode(editMode);
        a.setStyle(style);
        a.setAuthor(author);
        a.setUserEmail(userEmail);
        a.setUserIp(userIp);
        a.setUserAgent(userAgent);
        a.setParentId(parentId);

        a.setOrderNumber(orderNumber);
        a.setStatus(status);
        a.setVoteUp(voteUp);
        a.setVoteDown(voteDown);
        a.setCommentStatus(commentStatus);
        a.setCommentCount(commentCount);
        a.setCommentTime(commentTime);
        a.setViewCount(viewCount);
        a.setCreated(created);
        a.setModified(modified);
        a.setSlug(slug);
        a.setFlag(flag);
        a.setMetaKeywords(metaKeywords);
        a.setMetaDescription(metaDescription);
        a.setRemarks(remarks);
        return a;
    }

    /**
     * web对象转article对象
     * @return
     */
    public Article webToArticle() {
        Article a = new Article();
        if (id != null) {
            a.setId(id);
        }
        if (commentStatus != null) {
            a.setCommentStatus(commentStatus);
        }
        a.setStatus(status);
        a.setUserId(userId);
        if (StringUtils.isNotBlank(title)) {
            a.setTitle(title);
        }
        if (StringUtils.isNotBlank(author)) {
            a.setAuthor(author);
        }
        if (StringUtils.isNotBlank(content)) {
            a.setContent(content);
        }
        if (StringUtils.isNotBlank(summary)) {
            a.setSummary(summary);
        }
        return a;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getLinkTo() {
        return linkTo;
    }

    public void setLinkTo(String linkTo) {
        this.linkTo = linkTo;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public ArticleStatus getStatus() {
        return status;
    }

    public void setStatus(ArticleStatus status) {
        this.status = status;
    }

    public Long getVoteUp() {
        return voteUp;
    }

    public void setVoteUp(Long voteUp) {
        this.voteUp = voteUp;
    }

    public Long getVoteDown() {
        return voteDown;
    }

    public void setVoteDown(Long voteDown) {
        this.voteDown = voteDown;
    }

    public Long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    public Long getViewCount() {
        return viewCount;
    }

    public void setViewCount(Long viewCount) {
        this.viewCount = viewCount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Article.EditMode getEditMode() {
        return editMode;
    }

    public void setEditMode(Article.EditMode editMode) {
        this.editMode = editMode;
    }

    public Boolean getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(Boolean commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<Long> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<Long> tagIds) {
        this.tagIds = tagIds;
    }

    public List<Long> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Long> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<String> getTagNames() {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = tagNames;
    }

    public List<String> getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(List<String> categoryNames) {
        this.categoryNames = categoryNames;
    }
}
