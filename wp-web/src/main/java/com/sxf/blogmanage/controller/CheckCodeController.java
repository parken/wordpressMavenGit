package com.sxf.blogmanage.controller;

import com.sxf.common.CaptchaCommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.util.Random;

/**
 * 生成验证码
 * 
 * @author phsxf01
 * 
 */
@Controller
@RequestMapping("/public/checkcode")
public class CheckCodeController {

	private static final int count = 4;// 验证码位数

	public CheckCodeController() {
		super();
	}

	public Color getRandColor(int s, int e) {
		Random random = new Random();
		if (s > 255)
			s = 255;
		if (e > 255)
			e = 255;
		int r = s + random.nextInt(e - s);
		int g = s + random.nextInt(e - s);
		int b = s + random.nextInt(e - s);
		return new Color(r, g, b);
	}

	@RequestMapping("/code")
	public void generateCode(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		CaptchaCommonUtils.captcha(req,resp);
	}

}
