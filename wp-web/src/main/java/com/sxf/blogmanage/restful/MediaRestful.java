package com.sxf.blogmanage.restful;

import org.springframework.context.annotation.DependsOn;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 多媒体管理
 * @author phsxf
 */
@RestController
@DependsOn(value = {"PFileRestful"})
@RequestMapping("/api/blogmanage/media")
public class MediaRestful{

}