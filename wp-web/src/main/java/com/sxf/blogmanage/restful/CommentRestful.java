package com.sxf.blogmanage.restful;

import com.sxf.blogmanage.vo.CommentVO;
import com.sxf.common.utils.AjaxResult;
import com.sxf.common.vo.Pagination;
import com.sxf.domain.blog.Comment;
import com.sxf.service.CommentService;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import com.sxf.web.querybean.CommentQuery;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;

/**
 * 后台评论管理
 */
@RestController
@RequestMapping("/api/blogmanage/comment/v1")
public class CommentRestful {
    @Resource
    private CommentService commentService;

    @GetMapping("/statisticsAjax")
    public AjaxResult<Map<String, Object>> statisticsAjax() {
        Map<String, Object> map = commentService.queryStatistic();
        AjaxResult<Map<String, Object>> result = AjaxResult.buildSuccess();
        result.setData(map);
        return result;
    }

    @GetMapping("/getComments")
    public AjaxResult<Pagination<CommentVO>> getComments(CommentQuery query) {
        Pagination<CommentVO> cPage = commentService.queryPage(query);
        AjaxResult<Pagination<CommentVO>> result = AjaxResult.buildSuccess();
        result.setData(cPage);
        return result;
    }

    @PostMapping("/addComment")
    public AjaxResult<Void> addComment(@RequestBody CommentVO vo, HttpServletRequest request) {
        vo.setAgent(request.getHeader("User-Agent"));
        vo.setIp(request.getRemoteAddr());
        SessionUser su = LoginUtils.getLoginUserNoException();
        if (su != null) {
            vo.setFromUserId(su.getUserId());
            vo.setFromUserNickName(su.getNickName());
        }
        commentService.addComment(vo);
        return AjaxResult.buildSuccess();
    }

    @PostMapping("/editComment")
    public AjaxResult<Void> editComment(@Valid @RequestBody CommentVO form, BindingResult br) {
        AjaxResult<Void> result = AjaxResult.buildSuccess();
        Comment c = form.toComment();
        commentService.updateWithVersion(c);
        return result;
    }

    @DeleteMapping("/delete")
    public AjaxResult<Void> delete(Long id) {
        if (id != null) {
            Comment comment = new Comment();
            comment.setId(id);
            commentService.delete(comment);
        }
        return AjaxResult.buildSuccess();
    }
}
