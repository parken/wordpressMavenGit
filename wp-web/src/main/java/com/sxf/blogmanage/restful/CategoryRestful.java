package com.sxf.blogmanage.restful;

import com.sxf.blogmanage.vo.CategoryVO;
import com.sxf.common.utils.AjaxResult;
import com.sxf.common.vo.Pagination;
import com.sxf.domain.blog.Category;
import com.sxf.domain.enumration.CategoryType;
import com.sxf.service.CategoryService;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import com.sxf.web.querybean.CategoryQuery;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author
 */
@RestController
@RequestMapping("/api/blogmanage/category/v1")
public class CategoryRestful {
    @Resource
    private CategoryService cs;

    @GetMapping("/page")
    public AjaxResult<Pagination<Category>> page(@ModelAttribute CategoryQuery query){
        Pagination<Category> page = null;
        if (query != null) {
            page = cs.queryPage(query);
        }
        AjaxResult<Pagination<Category>> result = AjaxResult.buildSuccess();
        result.setData(page);
        return result;
    }

    @GetMapping("/list")
    public AjaxResult<List<Category>> list(CategoryQuery query){
        List<Category> categories = cs.queryList(query);
        AjaxResult<List<Category>> result = AjaxResult.buildSuccess();
        result.setData(categories);
        return result;
    }

    @GetMapping("/getCategorylist")
    public AjaxResult<List<Category>> getCategorylist(){
        CategoryQuery query = new CategoryQuery();
        query.setType(CategoryType.category);
        List<Category> categories = cs.queryList(query);
        AjaxResult<List<Category>> result = AjaxResult.buildSuccess();
        result.setData(categories);
        return result;
    }

    @GetMapping("/getCategoryPage")
    public AjaxResult<Pagination<Category>> getCategoryPage(){
        CategoryQuery query = new CategoryQuery();
        query.setType(CategoryType.category);
        Pagination<Category> categories = cs.queryPage(query);
        AjaxResult<Pagination<Category>> result = AjaxResult.buildSuccess();
        result.setData(categories);
        return result;
    }

    @GetMapping("/one")
    public AjaxResult<Category> getOne(@ModelAttribute CategoryQuery query){
        Category category = null;
        if (query != null) {
            category = cs.queryOne(query);
        }
        AjaxResult<Category> result = AjaxResult.buildSuccess();
        result.setData(category);
        return result;
    }

    @PostMapping("/editCategory")
    public AjaxResult<Void> editCategory(@RequestBody CategoryVO form){
        AjaxResult<Void> result = AjaxResult.buildSuccess();
        Category c = form.toWebCategory();
        if (c.getId() == null) {
            c.setCreated(new Date());
            c.setType(CategoryType.category);
            SessionUser su = LoginUtils.getLoginUser();
            c.setUserId(su.getUserId());
            c.setCount(0L);
        }
        cs.merge(c);
        return result;
    }

    @DeleteMapping("/delete")
    public AjaxResult<Void> delete(@RequestParam Long id){
        if (id != null) {
            CategoryQuery query = new CategoryQuery();
            query.setId(id);
            Category deleteTerm = cs.queryOne(query);
            if (deleteTerm != null) {
                Category tt = new Category();
                tt.setId(id);
                cs.delete(tt);
            }
        }
        AjaxResult<Void> result = AjaxResult.buildSuccess();
        return result;
    }
}
