package com.sxf.blogmanage.restful;

import com.sxf.blogmanage.vo.LinkVO;
import com.sxf.common.utils.AjaxResult;
import com.sxf.common.vo.Pagination;
import com.sxf.domain.blog.Link;
import com.sxf.service.CategoryService;
import com.sxf.service.LinkService;
import com.sxf.web.querybean.LinkQuery;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;


/**
 * @author phsxf01
 */
@RestController
@RequestMapping("/api/blogmanage/link/v1")
public class LinkRestful {
	@Resource
	private LinkService linkService;
	@Resource
	private CategoryService termService;

	@GetMapping("/listAjax")
	public AjaxResult<Pagination<Link>> listAjax(LinkQuery query) {
		AjaxResult<Pagination<Link>> result=AjaxResult.buildSuccess();
		Pagination<Link> linkPage = linkService.queryPage(query);
		result.setData(linkPage);
		return result;
	}

	@PostMapping("/edit")
	public AjaxResult<Void> edit(@Valid @RequestBody LinkVO form) {
		AjaxResult<Void> result = AjaxResult.buildSuccess();
		Link l = form.webToLink();
		l.setCreated(new Date());
		linkService.merge(l);
		return result;
	}

	@DeleteMapping("/delete")
	public AjaxResult<Void> delete(@RequestParam(required = true) Long linkId) {
		if (linkId != null) {
			Link link = new Link();
			link.setId(linkId);
			linkService.delete(link);
		}
		AjaxResult<Void> result = AjaxResult.buildSuccess();
		return result;
	}
}
