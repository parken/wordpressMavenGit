package com.sxf.blogmanage.restful;

import com.sxf.common.utils.AjaxResult;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/public/logout/v1")
public class LogoutRestful {
    @Resource
    private SecurityContextLogoutHandler scf;

    @GetMapping("/logout")
    public AjaxResult<Void> listAjax(HttpServletRequest request, HttpServletResponse response) {
        AjaxResult<Void> result=AjaxResult.buildSuccess();
        scf.logout(request,response,null);
        return result;
    }
}
