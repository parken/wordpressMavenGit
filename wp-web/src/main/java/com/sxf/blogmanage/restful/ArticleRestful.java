package com.sxf.blogmanage.restful;

import com.sxf.blogmanage.vo.ArticleVO;
import com.sxf.common.CommonResultCode;
import com.sxf.common.utils.AjaxResult;
import com.sxf.common.vo.Pagination;
import com.sxf.domain.blog.Article;
import com.sxf.service.ArticleService;
import com.sxf.service.CategoryService;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import com.sxf.web.querybean.ArticleQuery;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import sf.tools.StringUtils;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Date;
import java.util.Map;

/**
 * @author phsxf01
 */
@RestController
@RequestMapping("/api/blogmanage/article/v1")
public class ArticleRestful {
    @Resource
    private ArticleService as;
    @Resource
    private CategoryService cs;

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    /**
     * @param query
     * @return
     */
    @GetMapping("/getArticles")
    public AjaxResult<Pagination<ArticleVO>> getArticles(ArticleQuery query) {
        // ModelMap mm = new ModelMap();
        AjaxResult<Pagination<ArticleVO>> result = AjaxResult.buildSuccess();
        Pagination<ArticleVO> pages = as.queryPage(query);
        // mm.put("page", page);
        result.setData(pages);
        return result;
    }

    @GetMapping("/getArticleOne")
    public AjaxResult<ArticleVO> getArticleOne(ArticleQuery query) {
        // ModelMap mm = new ModelMap();
        AjaxResult<ArticleVO> result = AjaxResult.buildSuccess();
        if (query.getId() != null) {
            ArticleVO vo = as.queryOne(query);
            // mm.put("page", page);
            result.setData(vo);
        }
        return result;
    }

    /**
     * @return
     */
    @GetMapping("/statisticsAjax")
    public AjaxResult<Map<String, Integer>> statisticsAjax() {
        Map<String, Integer> map = as.queryStatistic();
        AjaxResult<Map<String, Integer>> result = AjaxResult.buildSuccess();
        result.setData(map);
        return result;
    }

    @PostMapping("/doAddOrUpdate")
    public AjaxResult<Void> doAddOrUpdate(@Valid @RequestBody ArticleVO vo, BindingResult br) {
        if (br.hasErrors()) {
            FieldError fieldError = br.getFieldError();
            String validMess = fieldError.getDefaultMessage();
            AjaxResult<Void> result = new AjaxResult<>(CommonResultCode.ILLEGAL_ARGUMENT);
            result.setMessage(validMess);
            return result;
        }
        if (vo != null) {
            Article article = vo.webToArticle();
            SessionUser su = LoginUtils.getLoginUserNoException();
            if (su != null && article.getAuthor() == null) {
                article.setAuthor(su.getUserName());
            }
            Date now = new Date();
            if (article.getId() == null) {
                article.setCreated(now);
                article.setModified(now);
                article.setVersion(StringUtils.uuid32());
                as.insert(article);
            } else {
                article.setModified(now);
                as.getDao().updateById(article);
            }
        }
        return AjaxResult.buildSuccess();
    }

    @DeleteMapping("/delete")
    public AjaxResult<Void> delete(Long articleId) throws Exception {
        if (articleId != null) {
            as.deleteById(articleId);
            return AjaxResult.buildSuccess();
        }else {
            return AjaxResult.buildIllegal();
        }

    }
}
