package com.sxf.blogmanage.restful;

import com.sxf.blogmanage.vo.UserVO;
import com.sxf.common.utils.AjaxResult;
import com.sxf.domain.user.User;
import com.sxf.service.UserService;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import com.sxf.web.vo.LoginResultVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Validation;
import javax.validation.Validator;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/api/blogmanage/user/v1")
public class UserRestful {
    @Resource
    private UserService us;
    @Resource
    private PasswordEncoder pe;

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @PostMapping("/addUser")
    public AjaxResult<Void> addUser(@RequestBody UserVO vo) {
        AjaxResult<Void> result = AjaxResult.buildSuccess();
        User u = vo.webToUser();
        Date now = new Date();
        u.setRegistered(now);
        if (StringUtils.isNotBlank(u.getPassword())) {
            u.setPassword(pe.encode(u.getPassword()));
        }
        us.insert(u);
        return result;
    }

    @PutMapping("/updateUser")
    public AjaxResult<Void> updateUser(@RequestBody UserVO vo) {
        AjaxResult<Void> result = AjaxResult.buildSuccess();
        User u = vo.webToUser();
        Date now = new Date();
        u.setRegistered(now);
        us.update(u);
        return result;
    }

    @GetMapping("/getUserInfo")
    public AjaxResult<LoginResultVO> getUserInfo() {
        AjaxResult<LoginResultVO> result = AjaxResult.buildSuccess();
        SessionUser su = LoginUtils.getLoginUser();
        LoginResultVO v = new LoginResultVO();
        v.setUsername(su.getUserName());
        v.setRoles(su.getRoleCodes());
        result.setData(v);
        return result;
    }

    @GetMapping("/getUserIcon")
    public void getUserIcon(HttpServletResponse response) {
        SessionUser su = LoginUtils.getLoginUser();
        User u = new User();
        u.setId(su.getUserId());
        u = us.findOne(u);
        if (u != null && ArrayUtils.isNotEmpty(u.getIcon())) {
            OutputStream os = null;
            try {
                os = response.getOutputStream();
                os.write(u.getIcon());
            } catch (IOException e) {
                log.error("", e);
            } finally {
                if (os != null) {
                    try {
                        os.flush();
                    } catch (IOException e) {
                        log.error("", e);
                    }
                }
            }
        }
    }
}
