package com.sxf.blogmanage.restful;

import com.sxf.common.CaptchaCommonUtils;
import com.sxf.common.CommonResultCode;
import com.sxf.common.redis.InvalidLoginTimesRegistry;
import com.sxf.common.WPConstants;
import com.sxf.common.utils.AjaxResult;
import com.sxf.domain.user.Role;
import com.sxf.domain.user.User;
import com.sxf.exception.UserFrozenException;
import com.sxf.exception.WrongCaptchaException;
import com.sxf.service.UserService;
import com.sxf.util.WebUtils;
import com.sxf.util.security.RSAUtils;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import com.sxf.web.config.LoginProperties;
import com.sxf.web.vo.LoginResultVO;
import com.sxf.web.vo.LoginVO;
import com.sxf.web.vo.ModifyPwdVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/public/login/v1")
public class LoginRestful {

    @Resource
    private InvalidLoginTimesRegistry invalidLoginTimesRegistry;

    @Resource
    private PasswordEncoder pe;
    @Autowired(required = false)
    private LoginProperties properties;
    @Resource
    private UserService us;

    /**
     * 登录
     * @param user 用户名、密码、验证码
     * @return 登录结果
     */
    @PostMapping("/doLogin")
    public AjaxResult<LoginResultVO> login(HttpServletRequest request, HttpServletResponse response, @RequestBody LoginVO user) {
        HttpSession session = request.getSession();
        SessionUser loginUser = null;
        //允许重复登录
//        if (loginUser != null) {
//            return new AjaxResult<>(CommonResultCode.SOME_ACCOUNT_ALREADY_LOGIN);
//        }
        String userName = user.getUsername();
        if (StringUtils.isBlank(userName) || StringUtils.isBlank(user.getPassword())) {
            // 用户名或密码为空
            return new AjaxResult<>(CommonResultCode.USERNAME_OR_PASSWORD_BLANK);
        }

        int tmpFailedTimes = 0;
        LoginResultVO loginResult = new LoginResultVO();
        try {
            //验证码先不调用
            tmpFailedTimes = preLogin(user, request.getSession());
            // RSA解密
            String privateKey = (String) session.getAttribute(WPConstants.RSA_PRIVATE_KEY);
            String orginPwd = user.getPassword();//RSAUtils.decode(user.getPassword(), privateKey);

            String clientIP = LoginUtils.getClientIP(request);
            // 认证用户信息
            User u = new User();
            u.setName(userName);
            User aov = us.findOne(u);
            if (aov == null || !pe.matches(orginPwd, aov.getPassword())) {
                throw new LoginException();
            }


            if (aov.getLocked() != null && aov.getLocked()) {
                // 被锁定
                loginResult.setLocked(true);
                throw new LockedException("account [" + userName + "] is locked");
            } else if (aov.getModifyPwd() != null && aov.getModifyPwd()) {
                // 需要强制修改密码
                SessionUser su = new SessionUser(u);
                su.setNickName(aov.getName());
                su.setUserIp(clientIP);
                session.setAttribute(WPConstants.PRE_LOGIN_USER_PREFIX + userName, su);
                loginResult.setForceModifyPwd(true);
            } else {
                us.getDao().getDbClient().fetchLinks(aov, User.CascadeField.roles);
                loginUser = new SessionUser(aov);
                session.setAttribute(WPConstants.LOGIN_USER, loginUser);
                //不走cas登录
                loginToSpringSecurity(loginUser, aov);
                // 重定向到成功后页面
                if (StringUtils.isBlank(user.getService())) {
                    String targetUrl = getTargetUrl(request, response);
                    if (targetUrl != null) {
                        user.setService(targetUrl);
                    } else {
                        user.setService(WebUtils.getServiceUrl(request) + properties.getSuccessUrl());
                    }
                }
                loginResult.setRedirectURL(user.getService());
            }
            // 记录操作日志
        } catch (WrongCaptchaException e) {
            // 验证码错误
            log.debug("Wrong captcha for user [{}].", userName);
            loginResult.setShowCaptcha(showCaptcha(e.getInvalidTimes()));
            return new AjaxResult(CommonResultCode.WRONG_CAPTCHA, loginResult);
        } catch (UserFrozenException e) {
            // 用户被冻结
            log.debug("User [{}] has been frozen.", userName);
            return new AjaxResult(CommonResultCode.USER_FROZEN, loginResult, new Object[]{invalidLoginTimesRegistry.getFailedTimes(userName)});
        } catch (LockedException e) {
            //账号被锁定
            log.debug("User [{}] has been locked.", userName);
            return new AjaxResult<>(CommonResultCode.USER_LOCKED, loginResult);
        } catch (LoginException e) {
            // 登录失败
            log.debug("User [{}] login failed, current failed times: [{}]", userName, tmpFailedTimes + 1);
            loginResult.setShowCaptcha(showCaptcha(tmpFailedTimes + 1));
            invalidLoginTimesRegistry.plusFailedTimes(userName);
            return new AjaxResult<>(CommonResultCode.USERNAME_OR_PASSWORD_ERROR, loginResult);
        }
        //删除错误次数
        invalidLoginTimesRegistry.delFailedTimes(userName);
        return new AjaxResult<>(CommonResultCode.RESULT_SUCCESS, loginResult);
    }

    /**
     * 登录强制修改密码
     * @param modifyPwd 新密码、确认密码
     * @return
     */
    @PostMapping("/forceModifyPwd")
    public AjaxResult<Void> forceModifyPwd(HttpServletRequest request, @RequestBody ModifyPwdVO modifyPwd) {
        boolean isIlleaglArg = modifyPwd == null || StringUtils.isBlank(modifyPwd.getAccountName()) || StringUtils.isBlank(modifyPwd.getNewPwd()) ||
                StringUtils.isBlank(modifyPwd.getConfirmPwd());
        if (isIlleaglArg) {
            return new AjaxResult<>(CommonResultCode.ILLEGAL_ARGUMENT);
        }
        HttpSession session = request.getSession();
        // RSA解密
        String privateKey = (String) session.getAttribute(WPConstants.RSA_PRIVATE_KEY);
        String ordinalNewPwd = RSAUtils.decode(modifyPwd.getNewPwd(), privateKey);
        String ordinalConfirmPwd = RSAUtils.decode(modifyPwd.getConfirmPwd(), privateKey);
        if (!ordinalNewPwd.equals(ordinalConfirmPwd)) {
            return new AjaxResult<>(CommonResultCode.ILLEGAL_ARGUMENT);
        }

        String sessionKey = WPConstants.PRE_LOGIN_USER_PREFIX + modifyPwd.getAccountName();
        SessionUser su = (SessionUser) session.getAttribute(sessionKey);
        if (su == null) {
            return new AjaxResult<>(CommonResultCode.USER_NO_LOGIN);
        }

        //修改密码
        User user = new User();
        user.setModifyPwd(false);
        user.setName(modifyPwd.getAccountName());
        user.setPassword(pe.encode(ordinalNewPwd));
        us.update(user);

        //设置正常登录后的数据,修改完成后需要重新登录
        session.removeAttribute(sessionKey);
//        session.setAttribute(CommonConstants.LOGIN_USER, su);


//        userService.forceModifyPwd(userAuth, RSAUtils.decode(modifyPwd.getNewPwd()));
        // FIXME 避免Converity报错
//		session.removeAttribute(sessionKey);

        // 记录操作日志

        return new AjaxResult<>(CommonResultCode.RESULT_SUCCESS);
    }

    private void loginToSpringSecurity(SessionUser su, User user) {
        //https://bbs.csdn.net/topics/391493371?page=1
        // 接下来是系统进行身份认证的过程：
        try {
            //1、将用户名、密码封装成一个token
            //2、将token传给AuthenticationManager进行身份认证
            //3、认证完毕，返回一个认证后的身份：
            // SecurityUser实现UserDetails并将SUser的Email映射为username
            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            List<Role> list = user.getRoles();
            if (!CollectionUtils.isEmpty(list)) {
                //添加权限代码
                for (Role r : list) {
                    authorities.add(new SimpleGrantedAuthority(r.getRoleCode()));
                }
            }
            Authentication result = new UsernamePasswordAuthenticationToken(su.getUserName(), "", authorities);
            // 认证后，存储到SecurityContext里 ：
            SecurityContextHolder.getContext().setAuthentication(result);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * 获取访问前的地址
     * @param request
     * @param response
     * @return
     */
    public String getTargetUrl(HttpServletRequest request, HttpServletResponse response) {
        RequestCache requestCache = new HttpSessionRequestCache();
        SavedRequest savedRequest = requestCache.getRequest(request, response);
        if (savedRequest != null) {
            String targetUrl = savedRequest.getRedirectUrl();
            requestCache.removeRequest(request, response);
            if (targetUrl != null) {
                return targetUrl;
            }
        }
        return null;
    }

    private int preLogin(LoginVO user, HttpSession session) throws LoginException {
        boolean frozen = invalidLoginTimesRegistry.isFrozen(user.getUsername());
        if (frozen) {
            throw new UserFrozenException("User has been frozen.");
        }
        int failedTimes = invalidLoginTimesRegistry.getFailedTimes(user.getUsername());
        if (showCaptcha(failedTimes)) {
            String captcha = user.getCaptcha();
            if (!CaptchaCommonUtils.checkCaptcha(captcha, session)) {
                throw new WrongCaptchaException("Wrong captcha.", failedTimes);
            }
        } else if (failedTimes >= 5) {
            throw new UserFrozenException("User has been frozen.");
        }
        return failedTimes;
    }

    private boolean showCaptcha(int failedTimes) {
        return failedTimes >= 3 && failedTimes < 5;
    }
}