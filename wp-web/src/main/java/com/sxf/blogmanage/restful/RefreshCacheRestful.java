package com.sxf.blogmanage.restful;

import com.sxf.cache.CacheTool;
import com.sxf.common.utils.AjaxResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 刷新缓存
 * 
 * @author SXF
 * 
 */
@RestController
@RequestMapping("/api/blogmanage/refreshcache/v1")
public class RefreshCacheRestful {
	@Resource(name = "cacheTool")
	private CacheTool cacheTool;

	@GetMapping(value = "/refresh")
	public AjaxResult<Void> refreshCache() throws Exception {
		cacheTool.init();
		return AjaxResult.buildSuccess();
	}
}
