package com.sxf.config;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;

/**同时开启htt和https访问
 *
 */
//@Configuration
public class WebServerConfig {
	
	/**
	 * 配置Tomcat https
	 * 
	 * @return
	 */
	@Bean
	public ServletWebServerFactory servletContainerFactory() {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory() {

//			protected void postProcessContext(Context context) {
				// SecurityConstraint必须存在，可以通过其为不同的URL设置不同的重定向策略。

				// SecurityConstraint securityConstraint = new
				// SecurityConstraint();
				// securityConstraint.setUserConstraint("CONFIDENTIAL");
				// SecurityCollection collection = new SecurityCollection();
				// collection.addPattern("/*");
				// securityConstraint.addCollection(collection);
				// context.addConstraint(securityConstraint);

//			}
		};
//		factory.addAdditionalTomcatConnectors(createHttpConnector());
		return factory;
	}

//	private Connector createHttpConnector() {
//		Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
//		connector.setScheme("http");
//		connector.setSecure(false);
//		connector.setPort(80);
//		// connector.setRedirectPort(443);
//		return connector;
//	}
}
