package com.sxf.config.jackson;


/**
 * 模拟fastjson的相关默认值配置
 */
public enum SerializerFeature {
    WriteNullListAsEmpty,
    WriteNullStringAsEmpty,
    WriteNullNumberAsZero,
    WriteNullBooleanAsFalse;

    public final int mask;

    SerializerFeature() {
        mask = (1 << ordinal());
    }
}

