package com.sxf.config.i18;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sxf.config.converter.I18nResultMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


@Configuration
@AutoConfigureAfter(LocaleConfiguration.class)
public class I18WebMvcConfiguration implements WebMvcConfigurer {

	@Autowired
	private MessageSource ms;
	@Autowired
	private ObjectMapper om;

	/**支持静态文件浏览(.txt)
	 * @param registry
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("{filename:\\\\w+\\\\.txt}");
	}

	/**
	 * ************************************ 国际化消息转换器，负责根据AjaxResult中的code转成国际化信息
	 * ************************************
	 */
	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		//添加国际化资源转化
		converters.add(0, i18nResultMessageConverter(ms, om));
	}

	@Bean
	public I18nResultMessageConverter i18nResultMessageConverter(MessageSource messageSource, ObjectMapper om) {
		return new I18nResultMessageConverter(messageSource,om);
	}

	/**
	 * ************************************
	 * 设置是否是后缀模式匹配，如“/user”是否匹配/user.*，默认真即匹配；
	 * 设置对uri是否大小写敏感，如“/titleManage”是否匹配/titlemanage
	 * ************************************
	 */
	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		AntPathMatcher antPathMatcher = new AntPathMatcher();
		antPathMatcher.setCaseSensitive(false);
		configurer.setUseSuffixPatternMatch(false);
		configurer.setPathMatcher(antPathMatcher);
	}

}
