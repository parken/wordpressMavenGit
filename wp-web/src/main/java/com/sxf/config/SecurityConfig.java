package com.sxf.config;

import com.sxf.web.interceptor.SecurityInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/*解决session安全问题
 *@auther shixiafeng
 *@create 2018-02-05
 */
@Configuration
@EnableConfigurationProperties({SecurityProperties.class})
public class SecurityConfig extends WebMvcConfigurerAdapter {
    @Autowired
    private SecurityProperties securityProperties;

    /**
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //是否使用安全拦截
        if (securityProperties.isSecurityInterceptUsed()) {
            registry.addInterceptor(new SecurityInterceptor()).addPathPatterns("/api/**", "/mobile/**");
        }
    }

}
