package com.sxf.config;

import freemarker.ext.servlet.FreemarkerServlet;
import org.hibernate.validator.HibernateValidator;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.util.IntrospectorCleanupListener;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@Configuration
public class WebMvcConfig {
	@Resource
	private MessageSource ms;

	// @Bean
	public ConfigurableWebBindingInitializer configurableWebBindingInitializer() {
		ConfigurableWebBindingInitializer ini = new ConfigurableWebBindingInitializer();

		FormattingConversionServiceFactoryBean fcsfb = new FormattingConversionServiceFactoryBean();
		ini.setConversionService(fcsfb.getObject());

		LocalValidatorFactoryBean lvfb = new LocalValidatorFactoryBean();
		lvfb.setProviderClass(HibernateValidator.class);
		lvfb.setValidationMessageSource(ms);
		ini.setValidator(lvfb);
		return ini;
	}

	// @Bean
	/**
	 * jar包中不生效
	 * 
	 * @return
	 */
	public WebApplicationInitializer webApplicationInitializer() {
		WebApplicationInitializer sc = new WebApplicationInitializer() {
			@Override
			public void onStartup(ServletContext servletContext) throws ServletException {
				// TODO Auto-generated method stub
			}
		};
		return sc;
	}

	@Bean
	public ServletContextInitializer servletContextInitializer() {
		ServletContextInitializer sc = new ServletContextInitializer() {

			@Override
			public void onStartup(ServletContext servletContext) throws ServletException {
				// Log4jConfigListener
				servletContext.setInitParameter("log4jConfigLocation", "classpath:log4j.properties");
				servletContext.setInitParameter("webAppRootKey", "myappfuse.root");
				servletContext.setInitParameter("log4jRefreshInterval", "60000");
				// spring security session监听器
				// servletContext.addListener(HttpSessionEventPublisher.class);

				// 可以提取request
				servletContext.addListener(RequestContextListener.class);
				// Spring 刷新Introspector防止内存泄露
				servletContext.addListener(IntrospectorCleanupListener.class);

				// OpenSessionInViewFilter
				// OpenSessionInViewFilter hibernateSessionInViewFilter = new
				// OpenSessionInViewFilter();
				// FilterRegistration.Dynamic filterRegistration =
				// servletContext
				// .addFilter("hibernateFilter", hibernateSessionInViewFilter);
				// filterRegistration.addMappingForUrlPatterns(EnumSet.of(
				// DispatcherType.REQUEST, DispatcherType.FORWARD,
				// DispatcherType.INCLUDE), false, "/");

				// jpa的sessionView

				// JetTemplateServlet

				// freemarkerServlet
				FreemarkerServlet fs = new com.sxf.template.freemarker.FreemarkerServlet();
				ServletRegistration.Dynamic dynamic2 = servletContext.addServlet("freemarker", fs);
				dynamic2.setInitParameter("TemplatePath", "/ftl/");
				dynamic2.setInitParameter("NoCache", "true");
				dynamic2.setInitParameter("ContentType", "text/html; charset=UTF-8");
				// 0 只对开发使用! 否则使用大一点的值.
				dynamic2.setInitParameter("template_update_delay", "0");
				// 模板文件的编码方式.
				dynamic2.setInitParameter("default_encoding", "UTF-8");
				dynamic2.setInitParameter("number_format", "0.##########");
				dynamic2.addMapping("*.ftl");
			}
		};
		return sc;
	}
}
