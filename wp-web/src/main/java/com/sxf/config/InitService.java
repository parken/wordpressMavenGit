package com.sxf.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sxf.config.jackson.FastJsonSerializerFeatureCompatibleForJackson;
import com.sxf.config.jackson.SerializerFeature;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Service
public class InitService {
    @Resource
    private ObjectMapper om;

    @PostConstruct
    public void init() {
        //实现和fastjson的功能对齐
        om.setSerializerFactory(om.getSerializerFactory().withSerializerModifier(new FastJsonSerializerFeatureCompatibleForJackson
                (/*SerializerFeature.WriteNullBooleanAsFalse,*/ SerializerFeature.WriteNullListAsEmpty,
                        /*SerializerFeature.WriteNullNumberAsZero,*/ SerializerFeature.WriteNullStringAsEmpty)));
    }
}
