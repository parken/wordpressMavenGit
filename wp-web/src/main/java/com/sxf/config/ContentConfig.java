package com.sxf.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;

@Configuration
public class ContentConfig {
	/**
	 * WebService
	 * 
	 * @return
	 */
	@Bean
	public SimpleJaxWsServiceExporter getSimpleJaxWsServiceExporter() {
		SimpleJaxWsServiceExporter sjwse = new SimpleJaxWsServiceExporter();
		sjwse.setBaseAddress("http://127.0.0.1:12001/");
		return sjwse;
	}

}
