package com.sxf.config.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sxf.common.utils.AjaxResult;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import sf.tools.StringUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

/**
 * 公共转换类
 */
public class I18nResultMessageConverter extends AbstractHttpMessageConverter<AjaxResult> {

    private MessageSource messageSource;

    private ObjectMapper om;

    public I18nResultMessageConverter(MessageSource messageSource, ObjectMapper om) {
        super(StandardCharsets.UTF_8, MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON_UTF8, MediaType.ALL);
        this.messageSource = messageSource;
        this.om = om;
    }

    @Override
    protected boolean supports(Class<?> aClass) {
        return AjaxResult.class.isAssignableFrom(aClass);
    }

    @Override
    protected AjaxResult readInternal(Class<? extends AjaxResult> aClass, HttpInputMessage httpInputMessage)
            throws IOException, HttpMessageNotReadableException {
        return null;
    }

    @Override
    protected void writeInternal(AjaxResult ajaxResult, HttpOutputMessage httpOutputMessage) throws IOException, HttpMessageNotWritableException {
        if (StringUtils.isBlank(ajaxResult.getMessage()) && ajaxResult.getEnumCode() != null) {
            Locale locale = LocaleContextHolder.getLocale();
            ajaxResult.setMessage(messageSource.getMessage(ajaxResult.getEnumCode().name(), ajaxResult.getMessageParam(),
                    ajaxResult.getEnumCode().name(), locale));
        }
        httpOutputMessage.getBody().write(om.writeValueAsBytes(ajaxResult));
        httpOutputMessage.getBody().flush();
    }
}
