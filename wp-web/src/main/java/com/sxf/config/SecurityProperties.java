package com.sxf.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/*
 *@auther shixiafeng
 *@create 2018-02-07
 */
@ConfigurationProperties(prefix = "web.security")
public class SecurityProperties {
    /**
     * 登录方式
     */
    private LoginType loginType;

    private boolean openValidateCode;

    public enum LoginType {
        AJAX, FORM
    }

    /**
     * 是否使用安全拦截(/api,/mobile)
     */
    private boolean securityInterceptUsed = true;

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    public boolean isOpenValidateCode() {
        return openValidateCode;
    }

    public void setOpenValidateCode(boolean openValidateCode) {
        this.openValidateCode = openValidateCode;
    }

    public boolean isSecurityInterceptUsed() {
        return securityInterceptUsed;
    }

    public void setSecurityInterceptUsed(boolean securityInterceptUsed) {
        this.securityInterceptUsed = securityInterceptUsed;
    }
}
