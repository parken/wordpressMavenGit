package com.sxf.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * 获取spring context
 * 
 * @author sxf
 *
 */
@Service
public class ApplicationUtils implements ApplicationContextAware {
	private static ApplicationContext context;

	@Override
	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		ApplicationUtils.context = context;
	}

	/**
	 * 获取spring context
	 * 
	 * @return
	 */
	public static ApplicationContext getContext() {
		return context;
	}

}
