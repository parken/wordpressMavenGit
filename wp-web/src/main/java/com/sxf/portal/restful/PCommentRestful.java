package com.sxf.portal.restful;

import com.sxf.blogmanage.vo.CommentVO;
import com.sxf.common.WPConstants;
import com.sxf.common.tools.WebTools;
import com.sxf.common.utils.AjaxResult;
import com.sxf.domain.blog.Article;
import com.sxf.domain.blog.Comment;
import com.sxf.domain.user.User;
import com.sxf.service.ArticleService;
import com.sxf.service.CommentService;
import com.sxf.service.UserService;
import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import com.sxf.web.querybean.ArticleQuery;
import com.sxf.web.querybean.CommentQuery;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/public/portal/comment/v1")
public class PCommentRestful {
    @Resource
    private ArticleService articleService;

    @Resource
    private UserService us;

    @Resource
    private CommentService commentService;

    @GetMapping("/getArticleComment")
    public AjaxResult<List<CommentVO>> getArticleComment(Long articleId) {
        List<CommentVO> list = Collections.emptyList();
        if (articleId != null) {
            CommentQuery q = new CommentQuery();
            q.setArticleId(articleId);
            List<Comment> comments = commentService.queryList(q);
            if (!CollectionUtils.isEmpty(comments)) {
                list = comments.stream().map(CommentVO::new).collect(Collectors.toList());
                stack(list);
            }
        }
        AjaxResult<List<CommentVO>> result = AjaxResult.buildSuccess();
        result.setData(list);
        return result;
    }

    /**
     * 设置层级关系
     * @param list
     */
    private void stack(List<CommentVO> list) {
        for (int i = 0; i < list.size(); i++) {
            CommentVO v1 = list.get(i);
            for (int j = 0; j < list.size(); j++) {
                CommentVO v2 = list.get(j);
                if (Objects.equals(v1.getId(), v2.getParentId())) {
                    if (v1.getReplys() == null) {
                        v1.setReplys(new ArrayList<>());
                    }
                    v1.getReplys().add(v2);
                }
            }
        }
        list.removeIf(v -> v.getParentId() != null && v.getParentId() != 0);
    }


    @PostMapping("/addComment")
    public AjaxResult<Void> addComment(@RequestBody CommentVO vo, HttpServletRequest request) {
        vo.setAgent(request.getHeader("User-Agent"));
        vo.setIp(request.getRemoteAddr());
        SessionUser su = LoginUtils.getLoginUserNoException();
        if (su != null) {
            vo.setFromUserId(su.getUserId());
            vo.setFromUserNickName(su.getNickName());
        }
        commentService.addComment(vo);
        return AjaxResult.buildSuccess();
    }

    public String deleteComment() {
        HttpSession session = WebTools.getHttpSession();
        User loginUser = (User) session.getAttribute(WPConstants.LOGIN_USER);
        if (loginUser == null) {
            return null;
        }

        Comment comment = new Comment();
        Date date = new Date();
        comment.setCreated(date);
        // TODO
        ArticleQuery query = new ArticleQuery();
        query.setId(comment.getId());
        List<Article> articleList = articleService.queryList(query);
        if (articleList != null && !articleList.isEmpty()) {
            Article article = articleList.get(0);
            long commentCount = article.getCommentCount();
            commentCount--;
            if (commentCount >= 0) {
                article.setCommentCount(commentCount);
                articleService.merge(article);
            }
        }
        return null;
    }
}
