package com.sxf.portal.restful;

import com.sxf.blogmanage.vo.ArticleVO;
import com.sxf.common.utils.AjaxResult;
import com.sxf.service.ArticleService;
import com.sxf.web.querybean.ArticleQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/public/portal/article/v1")
public class PArticleRestful {
	@Resource
	private ArticleService articleService;

	@GetMapping(value = "/show/content/{year}/{month}/{day}/{postName}")
	public AjaxResult<Void> showContentByDate(@PathVariable("year") String year, @PathVariable("month") String month,
										@PathVariable("day") String day, @PathVariable("postName") String postName) {

		return AjaxResult.buildSuccess();
	}

	@GetMapping("/showContent")
	public AjaxResult<ArticleVO> showContentByP(@RequestParam("p") String p) {
		AjaxResult<ArticleVO> result = AjaxResult.buildSuccess();
		if (StringUtils.isNotBlank(p)) {
			long postId = Long.parseLong(p);
			ArticleQuery postQuery = new ArticleQuery();
			postQuery.setId(postId);
			ArticleVO vo = articleService.queryOne(postQuery);
			result.setData(vo);
		}
		return result;
	}
}
