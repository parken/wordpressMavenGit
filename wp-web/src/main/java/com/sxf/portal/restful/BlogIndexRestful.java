package com.sxf.portal.restful;

import com.sxf.blogmanage.vo.ArticleVO;
import com.sxf.common.utils.AjaxResult;
import com.sxf.common.vo.Pagination;
import com.sxf.domain.blog.Category;
import com.sxf.domain.enumration.ArticleStatus;
import com.sxf.domain.enumration.CategoryType;
import com.sxf.exception.WPRuntimeException;
import com.sxf.portal.vo.UserForm;
import com.sxf.service.ArticleService;
import com.sxf.service.BlogService;
import com.sxf.service.CategoryService;
import com.sxf.web.querybean.ArticleQuery;
import com.sxf.web.querybean.CategoryQuery;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sf.tools.StringUtils;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@RestController
@RequestMapping("/public/portal/v1")
public class BlogIndexRestful {

    @Resource
    private BlogService bs;
    @Resource
    private ArticleService as;
    @Resource
    private CategoryService cs;

    @ApiOperation("获取blog基本信息,包括博客名称,博客标题,博客子标题")
    @GetMapping(value = "/getInfo")
    public AjaxResult<Map<String, Object>> getInfo() {
        // ModelMap mm = new ModelMap();
        AjaxResult<Map<String, Object>> result = AjaxResult.buildSuccess();
        // mm.put("page", page);
        result.setData(bs.getBasicInfo());
        return result;
    }


    /**
     * 默认URL展示方式 http://localhost/wordpress/blog/show?id=1
     * @param query
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/articlePage")
    public AjaxResult<Pagination<ArticleVO>> getArticlePage(ArticleQuery query) {
        // ModelMap mm = new ModelMap();
        AjaxResult<Pagination<ArticleVO>> result = AjaxResult.buildSuccess();
        query.setStatus(ArticleStatus.published);
        Pagination<ArticleVO> pages = as.queryPage(query);
        // mm.put("page", page);
        result.setData(pages);
        return result;
    }

    @GetMapping(value = "/getArchives")
    public AjaxResult<Map<Long, Integer>> getArchives(ArticleQuery query) {
        // ModelMap mm = new ModelMap();
        AjaxResult<Map<Long, Integer>> result = AjaxResult.buildSuccess();
        Pagination<ArticleVO> pages = as.queryPage(query);
        // 文章归档
        List<Date> archiveList = as.getArchiveArticles();
        Map<Long, Integer> map = new HashMap<>();
        for (Date d : archiveList) {
            map.put(d.getTime(), 0);
        }
        result.setData(map);
        return result;
    }

    @GetMapping("/getCategorylist")
    public AjaxResult<List<Category>> getCategorylist() {
        CategoryQuery query = new CategoryQuery();
        query.setType(CategoryType.category);
        List<Category> categories = cs.queryList(query);
        AjaxResult<List<Category>> result = AjaxResult.buildSuccess();
        result.setData(categories);
        return result;
    }

    @ApiOperation("初始化登录用户")
    @PostMapping("/initAdminUser")
    public AjaxResult<Void> initAdminUser(@RequestBody UserForm uf) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<UserForm>> set = validator.validate(uf);
        if(!set.isEmpty()){
            throw new WPRuntimeException(set.iterator().next().getMessage());
        }
        AjaxResult<Void> result = null;
        if (StringUtils.isBlank(uf.getPassword())) {
            result = AjaxResult.buildFail();
            return result;
        }
        if (!Objects.equals(uf.getPassword(), uf.getConfirmPassword())) {
            result = AjaxResult.buildFail();
            return result;
        }
        bs.initAdminUser(uf);
        result = AjaxResult.buildSuccess();
        return result;
    }
}
