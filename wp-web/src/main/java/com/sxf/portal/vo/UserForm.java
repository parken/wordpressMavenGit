package com.sxf.portal.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Formbean
 * 
 * @author SXF
 * 
 */
public class UserForm {

	@NotNull(message = "用户名不能为空！")
	@Pattern(regexp = "[a-z0-9]{3,50}", message = "用户名不符合规范")
	private String username;

	@NotNull(message = "密码不能为空")
	@Size(min = 3, max = 20, message = "密码长度不符合")
	private String password;

	@NotNull(message = "确认密码不能为空")
	@Size(min = 3, max = 20, message = "密码长度不符合")
	private String confirmPassword;

	public UserForm() {

	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
}
