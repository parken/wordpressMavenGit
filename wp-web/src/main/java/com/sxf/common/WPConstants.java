package com.sxf.common;

/**
 * 系统常量 基本数据类型的常量名为全大写，如果是由多个单词构成，可以用下划线隔开
 * 
 */
public class WPConstants {
	public static final String PUBLIC_KEY_BASE64 = "PUBLIC_KEY_BASE64";

	public static final String RSA_PRIVATE_KEY = "rsa_private_key";
	public static final String RSA_PUBLIC_KEY = "rsa_public_key";

	/**
	 * 登陆用户Session信息
	 */
	public final static String LOGIN_USER = "loginUser";// 登陆用户Session信息

	/**
	 * session中保存的需要强制修改密码的用户信息的Key
	 */
	public final static String PRE_LOGIN_USER_PREFIX = "_pre_login_user_";
	
	/**
	 * 首次登陆标志
	 */
	public static final String FIRST_LOGIN_FLAG = "firstLoginFlag";
	/**
	 * session时间,以秒为单位
	 */
	public final static int SESSION_TIME_SECONDS = 30 * 60;

	public final static String EXIST_ADMIN_USER ="existAdminUser";

	// Term分类系统名字
	/**
	 *
	 */
	public final static String TAXONOMY_POST_FORMAT = "post_format";
	/**
	 * 文章分类
	 */
	public final static String TAXONOMY_POST_CATEGORY = "category";
	/**
	 * 标签分类
	 */
	public final static String TAXONOMY_POST_TAG = "post_tag";
	/**
	 * 链接分类
	 */
	public final static String TAXONOMY_LINKA_CATEGORY = "link_category";

	// post文章状态
	/**
	 * 已发布
	 */
	public final static String POST_STATUS_PUBLISH = "publish";

	public final static String POST_STATUS_INHERIT = "inherit";

	public final static String POST_STATUS_AUTO_DRAFT = "auto-draft";

	public final static String POST_STATUS_DRAFT = "draft";

	public final static String POST_STATUS_PENDING = "pending";

	public final static String POST_TYPE_POST = "post";

	public final static String POST_TYPE_PAGE = "page";

	public final static String POST_TYPE_REVISION = "revision";

	public final static String POST_TYPE_ATTACHMENT = "attachment";
	/**
	 * 回收站
	 */
	public final static String POST_STATUS_TRASH = "trash";

	// comment
	/**
	 * 回收站
	 */
	public final static String COMMENT_APPROVED_TRASH = "trash";
	/**
	 * 垃圾评论
	 */
	public final static String COMMENT_APPROVED_SPAM = "spam";
	/**
	 * 审核通过
	 */
	public final static String COMMENT_APPROVED_APPROVED = "1";
	/**
	 * 审核中
	 */
	public final static String COMMENT_APPROVED_MODERATED = "0";

	/**
	 * 返回错误标志
	 */
	public final static String OUT_FLAG = "out_flag";
	/**
	 * 返回错误信息
	 */
	public final static String OUT_MSG = "out_msg";

	/**
	 * 
	 */
	public static final String USER_INFO = "session_user";
	/**
	 * 
	 */
	public static final String USER_CART_INFO = "session_cart";

	/**
	 * blog 缓存key前缀+blogId
	 */
	public static final String BLOG_OPTION_CACHE = "BLOG_OPTION_";
	
	/**
	 * Sessoin缓存
	 */
	public static final String SESSION_CACHE = "SESSION";

	/**
	 * 
	 */
	public static final String SPRING_MEM_CACHE = "spring_memcahced";

	public static final String SPRING_EHCACHE = "spring_ehcache";

}
