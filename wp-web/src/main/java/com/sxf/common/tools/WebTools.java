package com.sxf.common.tools;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class WebTools {

	/**
	 * @return
	 */
	public static HttpServletRequest getHttpServletRequest() {
		return ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
	}

	/**
	 * @return
	 */
	public static HttpSession getHttpSession() {
		HttpServletRequest request = getHttpServletRequest();
		return request.getSession();
	}

	/**
	 * @return
	 */
	public static ServletContext getServletContext() {
		return getHttpSession().getServletContext();
	}

	/**
	 * 是否是异步的ajax请求
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isAjaxRequest(HttpServletRequest request) {
		String header = request.getHeader("X-Requested-With");
		if (header != null && "XMLHttpRequest".equals(header))
			return true;
		else
			return false;
	}
}
