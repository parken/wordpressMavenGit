package com.sxf.common.tools;

import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

/**
 * @author phsxf01
 * 
 */
public class CommonTools {
	/**
	 * 判断是否包含中文
	 * 
	 * @param inStr
	 * @return
	 */
	public static boolean hasFullSize(String inStr) {
		if (inStr.getBytes().length != inStr.length()) {
			return true;
		}
		return false;
	}

	/**
	 * 获取GMT 0 的时间
	 * 
	 * @param d
	 * @return
	 */
	public static Date getGMT0(Date d) {
		Date date = null;
		if (d != null) {
			date = new Date(d.getTime() - TimeZone.getDefault().getRawOffset());
		}
		return date;
	}

	/**
	 * 随机获取UUID字符串(无中划线)
	 * 
	 * @return UUID字符串
	 */
	public static String getUUID() {
		String uuid = UUID.randomUUID().toString();
		return uuid.replaceAll("-", "");
	}
}
