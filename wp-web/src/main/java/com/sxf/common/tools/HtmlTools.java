package com.sxf.common.tools;

import com.sxf.blogmanage.vo.CommentVO;
import org.apache.commons.lang3.RandomUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public class HtmlTools {

    private static List<String> imageList = new ArrayList<String>();

    public static AtomicBoolean run = new AtomicBoolean(true);

    public static String getRandomImageName() {
        String src = null;
        // 随机切换图片
        if (imageList.isEmpty()) {
            getImage();
        } else {
            src = imageList.get(RandomUtils.nextInt(0, imageList.size()));
        }
        return src;
    }

    private static void getImage() {
        try {
            imageList.clear();
            URL url = Thread.currentThread().getContextClassLoader().getResource("");
            URI uri = url.toURI();
            File directory = new File(new URI(uri.toString() + "../../resources/images/headers/"));
            if (!directory.exists()) {
                return;
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        Thread t = new Thread(() -> {
			while (run.get()) {
				try {
					imageList.clear();
					URL url = Thread.currentThread().getContextClassLoader().getResource("");
					URI uri = url.toURI();
					File directory = new File(new URI(uri.toString() + "../../resources/images/headers/"));
					if (directory.isDirectory()) {
						for (String f : directory.list()) {
							if (!f.contains("thumbnail")) {
								imageList.add(f);
							}
						}
					}
					directory = null;

					Thread.sleep(1000 * 20);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
        t.setName("file thread");
        t.start();
    }

    /**
     * 迭代Comment
     * @param out
     * @param comment
     * @throws Exception
     */
    public static void iterationComment(OutputStream out, CommentVO comment, int i) throws Exception {
        StringBuilder sb = new StringBuilder();
        iterationComment(sb, comment, i);
        BufferedWriter bf = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
        bf.append(sb.toString());
        bf.flush();
    }

    /**
     * 迭代Comment
     * @param out
     * @param comment
     * @throws Exception
     */
    public static void iterationComment(StringBuilder out, CommentVO comment, int i) throws Exception {
        List<CommentVO> subCommentList = comment.getReplys();
        if (subCommentList != null && !subCommentList.isEmpty()) {
            out.append("<ul class=\"children\">");
            for (CommentVO subComment : subCommentList) {
                if (!subComment.getReplys().isEmpty()) {
                    out.append("<li id=\"li-comment-" + subComment.getId().toString()
                            + "\" class=\"comment odd alt depth-" + (i++) + "\">");
                } else {
                    out.append("<li id=\"li-comment-" + subComment.getId().toString()
                            + "\" class=\"comment even  depth-" + (i++) + "\">");
                }

                out.append("<article class=\"comment\" id=\"comment-" + subComment.getId().toString() + "\">");
                out.append("<footer class=\"comment-meta\">");
                out.append("<div class=\"comment-author vcard\">");
                out.append("<img width=\"39\" height=\"39\" class=\"avatar avatar-39 photo\" src=\"\" alt=\"\">");
                out.append("<span class=\"fn\"><a class=\"url\" rel=\"external nofollow\" href=\""
                        + "" + "\">" + subComment.getFromUserNickName() + "</a> </span>");
                out.append("在 <a href=\"http://localhost/wordpress/?p=1#comment-7\">");
                out.append("<time	datetime=\"" + subComment.getCreated() + "\" pubdate=\"\">"
                        + new SimpleDateFormat("yyyy年MM月dd日ahh:mm").format(subComment.getCreated()) + "</time>");
                out.append("</a>");
                out.append("<span class=\"says\">说道：</span>");
                out.append("</div>");
                out.append("<!-- .comment-author .vcard -->");
                out.append("</footer>");
                out.append("<div class=\"comment-content\">");
                out.append("<p>" + subComment.getContent() + "</p>");
                out.append("</div>");
                out.append("<div class=\"reply\">");
                out.append(
                        "<a onclick=\"return addComment.moveForm(&quot;comment-7&quot;, &quot;7&quot;, &quot;respond&quot;, &quot;1&quot;)\" ");
                out.append("href=\"/wordpress/?p=1&amp;replytocom=7#respond\" class=\"comment-reply-link\">回复 ");
                out.append("<span>↓</span>");
                out.append("</a>");
                out.append("</div>");
                out.append("<!-- .reply -->");
                out.append("</article> <!-- #comment-## -->");

                iterationComment(out, subComment, i);// 迭代实现

                out.append("</li>");
            }

            out.append("</ul>");
        }
    }
}
