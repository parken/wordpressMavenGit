package com.sxf.common.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * redis操作示例
 */
@Slf4j
@Service
public class InvalidLoginTimesRegistry {

    private final static String invalidLoginTimesCacheName = "hosp:invalidLoginTimesCache";
    private final static String frozenCacheName = "hosp:frozenCache";

    private final static String FAIL_LOGIN_TIMES = "FAIL_LOGIN_TIMES";
    private final static String FROZEN_DATE = "FROZEN_DATE";

    /**
     * 冻结分钟
     */
    @Value("${user.frozen.minutes:60}")
    private int frozenMinutes;

    @Autowired
    private StringRedisTemplate redisTemplate = new StringRedisTemplate();

    private HashOperations<String, String, String> hashOperations;

    @PostConstruct
    public void init() {
        hashOperations = redisTemplate.opsForHash();
    }

    public Integer getFailedTimes(String key) {
        if (key == null) {
            return 0;
        }
        final String failedLoginTimes = hashOperations.get(invalidLoginTimesCacheName + ":" + key, FAIL_LOGIN_TIMES);
        Integer r = failedLoginTimes == null ? 0 : Integer.parseInt(failedLoginTimes);
        log.debug("查询用户名[{}]已累计错误次数[{}]", key, r);
        return r;
    }

    public void plusFailedTimes(String key) {
        Integer times = getFailedTimes(key);
        if (times == null) {
            times = 0;
        }
        hashOperations.put(invalidLoginTimesCacheName + ":" + key, FAIL_LOGIN_TIMES, String.valueOf(times + 1));
        // 设置三分钟过期，即只有3分钟内的登录失败才算连续登录失败
        redisTemplate.expire(invalidLoginTimesCacheName + ":" + key, 3, TimeUnit.MINUTES);
        if (times == 4) {
            plusFrozen(key);
        }
    }

    /**
     * 删除错误次数
     * @param key
     */
    public void delFailedTimes(String key) {
        redisTemplate.delete(invalidLoginTimesCacheName + ":" + key);
    }

    /**
     * 设置冻结key
     * @param key
     */
    public void plusFrozen(String key) {
        hashOperations.put(frozenCacheName + ":" + key, FROZEN_DATE, String.valueOf(System.currentTimeMillis()));
        // 设置过期
        redisTemplate.expire(frozenCacheName + ":" + key, frozenMinutes, TimeUnit.MINUTES);
    }

    public void delFrozen(String key) {
        redisTemplate.delete(frozenCacheName + ":" + key);
    }


    /**
     * 获取当前的key是否被冻结
     * @param key
     * @return
     */
    public boolean isFrozen(String key) {
        if (key == null) {
            return false;
        }
        String frozenName = hashOperations.get(frozenCacheName + ":" + key, FROZEN_DATE);
        if (frozenName != null) {
            return true;
        } else {
            return false;
        }
    }

    public int getFrozenMinutes() {
        return frozenMinutes;
    }

}
