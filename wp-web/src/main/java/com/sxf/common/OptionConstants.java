package com.sxf.common;

/**
 * blog选项常量
 */
public interface OptionConstants {
    /**
     * blog名
     */
    String BLOG_NAME = "blog_name";
    /**
     * blog标题
     */
    String BLOG_TITLE = "blog_title";
    /**
     * blog子标题
     */
    String BLOG_SUB_TITLE = "blog_subtitle";
}
