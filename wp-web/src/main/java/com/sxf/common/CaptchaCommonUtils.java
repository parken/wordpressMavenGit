package com.sxf.common;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author
 */
public abstract class CaptchaCommonUtils {
    public static final String KAPTCHA_SESSION_KEY = "KAPTCHA_SESSION_KEY";

    public static void captcha(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 调用工具类生成的验证码和验证码图片
        CodeUtil.CheckCode cc = CodeUtil.generateCodeAndPic();
        // 将四位数字的验证码保存到Session中。
        HttpSession session = req.getSession();
        session.setAttribute(KAPTCHA_SESSION_KEY, cc.getCode());

        // 禁止图像缓存。
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Cache-Control", "no-cache");
        resp.setDateHeader("Expires", -1);

        resp.setContentType("image/jpeg");

        // 将图像输出到Servlet输出流中。
        ServletOutputStream sos;
        try {
            sos = resp.getOutputStream();
            ImageIO.write(cc.getImage(), "jpeg", sos);
            sos.close();
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
    }

    public static String getGeneratedKey(HttpServletRequest req) {
        String key = (String) req.getSession().getAttribute(KAPTCHA_SESSION_KEY);
        return key;
    }

    public static boolean checkCaptcha(String captcha, HttpServletRequest req) {
        return captcha != null && captcha.equals(getGeneratedKey(req));
    }

    public static boolean checkCaptcha(String captcha, HttpSession session) {
        String key = (String) session.getAttribute(KAPTCHA_SESSION_KEY);
        return captcha != null && captcha.equals(key);
    }
}
