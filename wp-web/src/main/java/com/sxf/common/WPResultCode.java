package com.sxf.common;

public enum  WPResultCode implements ResultCodeInf {
    /**
     * 管理员账号已经存在
     */
    ADMIN_USER_EXIST(10001)
    ;
    private int code;

    private WPResultCode(int code) {
        this.code = code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public int getCode() {
        return this.code;
    }
}
