package com.sxf.common;

import org.springframework.cglib.beans.BeanCopier;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BeanCopierUtils {

    public static Map<String, BeanCopier> beanCopierMap = new ConcurrentHashMap<>();

    /**
     * 使用Copier进行拷贝，增加拷贝效率
     * @param source
     * @param target
     * @param <T>
     * @param <I>
     */
    public static <T, I> void copyProperties(T source, I target) {
        String beanKey = generateKey(source.getClass(), target.getClass());
        BeanCopier copier = beanCopierMap.get(beanKey);
        if (copier == null) {
            copier = BeanCopier.create(source.getClass(), target.getClass(), false);
            beanCopierMap.put(beanKey, copier);
        }
        copier.copy(source, target, null);
    }

    private static String generateKey(Class<?> aClass, Class<?> aClass1) {
        return aClass.toString() + aClass1.toString();
    }


}
