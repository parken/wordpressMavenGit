package com.sxf.service.impl;

import com.sxf.cache.CacheTool;
import com.sxf.common.OptionConstants;
import com.sxf.common.WPConstants;
import com.sxf.common.WPResultCode;
import com.sxf.dao.OptionDAO;
import com.sxf.dao.UserDAO;
import com.sxf.domain.user.User;
import com.sxf.exception.WPRuntimeException;
import com.sxf.portal.vo.UserForm;
import com.sxf.service.BlogService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sf.tools.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Transactional
@Service
public class BlogServiceImpl implements BlogService {
    @Resource
    private OptionDAO od;
    @Resource
    private UserDAO ud;
    @Resource
    private PasswordEncoder pe;

    @Override
    public Map<String, Object> getBasicInfo() {
        Map<String, Object> map = new HashMap<>();
        map.put(OptionConstants.BLOG_NAME, CacheTool.getOption(OptionConstants.BLOG_NAME));
        map.put(OptionConstants.BLOG_TITLE, CacheTool.getOption(OptionConstants.BLOG_TITLE));
        map.put(OptionConstants.BLOG_SUB_TITLE, CacheTool.getOption(OptionConstants.BLOG_SUB_TITLE));
        User q = new User();
        boolean b = ud.exists(q);
        //是否存在管理员账号
        map.put(WPConstants.EXIST_ADMIN_USER, b);
        return map;
    }

    @Override
    public void initAdminUser(UserForm uf) {
        User q = new User();
        boolean b = ud.exists(q);
        if (b) {
            //管理员账号已经存在
            throw new WPRuntimeException(WPResultCode.ADMIN_USER_EXIST);
        }
        User user = new User();
        user.setName(uf.getUsername());
        user.setPassword(pe.encode(uf.getPassword()));
        user.setActivationKey(StringUtils.uuid32());
        user.setLocked(false);
        ud.insert(user);
    }
}
