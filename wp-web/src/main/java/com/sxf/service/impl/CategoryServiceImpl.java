package com.sxf.service.impl;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.sql.SQLQuery;
import com.sxf.common.vo.Pagination;
import com.sxf.dao.CategroyDAO;
import com.sxf.domain.blog.Category;
import com.sxf.service.CategoryService;
import com.sxf.web.querybean.CategoryQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sf.common.wrapper.Page;
import sf.querydsl.QueryDSLTables;
import sf.querydsl.SQLRelationalPath;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Resource
    private CategroyDAO dao;

    private Predicate[] conditionQuery(CategoryQuery q, SQLRelationalPath<?> qu) {
        List<Predicate> list = new ArrayList<Predicate>();
        if (q.getId() != null && q.getId() > 0) {
            Predicate p = qu.number(Category.Field.id).eq(q.getId());
            list.add(p);
        }
        if (StringUtils.isNotBlank(q.getTitle())) {
            Predicate p = qu.string(Category.Field.title).like("%" + q.getTitle() + "%", '\\');
            list.add(p);
        }
        if (q.getType() != null) {
            Predicate p = ((EnumPath) qu.enums(Category.Field.type)).eq(q.getType());
            list.add(p);
        }
        if (!list.isEmpty()) {
            // 注意此处的处理
            Predicate[] p = list.toArray(new Predicate[list.size()]);
            return p;
        }
        return null;
    }

    private SQLQuery<Category> getSQLQuery(CategoryQuery q) {
        SQLQuery<Category> query = new SQLQuery<>();
        SQLRelationalPath<Category> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu).from(qu);
        Predicate[] p = conditionQuery(q, qu);
        if (p != null) {
            query.where(p);
        }
        return query;
    }

    @Override
    public Pagination<Category> queryPage(CategoryQuery q) {
        SQLQuery<Category> query = getSQLQuery(q);
        int pageNo = q.getPageNo();
        int pageSize = q.getPageSize();
        Page<Category> page = dao.getQueryDSLMapper().queryDSLSelectPage(query, (pageNo - 1) * pageSize, pageSize);
        Pagination pg = new Pagination<>(q.getPageNo(), page);
        return pg;
    }

    @Override
    public List<Category> queryList(CategoryQuery q) {
        SQLQuery<Category> query = getSQLQuery(q);
        return dao.getQueryDSLMapper().queryDSLSelectList(query);
    }

    @Override
    public Category queryOne(CategoryQuery q) {
        SQLQuery<Category> query = getSQLQuery(q);
        return dao.getQueryDSLMapper().queryDSLSelectOne(query);
    }

    @Override
    public long queryNumber(CategoryQuery q) {
        SQLQuery<Category> query = new SQLQuery<>();
        SQLRelationalPath<Category> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu.number(Category.Field.id).count()).from(qu);
        Predicate[] p = conditionQuery(q, qu);
        if (p != null) {
            query.where(p);
        }
        Long l = dao.getDbClient().getQueryDSL().queryDSLSelectOne(query, Long.class);
        return l;
    }

    @Override
    public int insert(Category info) {
        dao.insert(info);
        return 0;
    }

    @Override
    public int update(Category info) {
        return dao.updateById(info);
    }

    @Override
    public int merge(Category info) {
        return dao.merge(info);
    }

    @Override
    public int delete(Category info) {
        return dao.deleteById(info.getId());
    }

}
