package com.sxf.service.impl;

import com.querydsl.core.types.Predicate;
import com.querydsl.sql.SQLQuery;
import com.sxf.common.vo.Pagination;
import com.sxf.dao.LinkDAO;
import com.sxf.domain.blog.Link;
import com.sxf.service.LinkService;
import com.sxf.web.querybean.LinkQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import sf.common.wrapper.Page;
import sf.querydsl.QueryDSLTables;
import sf.querydsl.SQLRelationalPath;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class LinkServiceImpl implements LinkService {
    @Resource
    private LinkDAO dao;

    private Predicate[] conditionQuery(LinkQuery q, SQLRelationalPath<?> qu) {
        List<Predicate> list = new ArrayList<Predicate>();
        if (q.getId() > 0) {
            Predicate p = qu.number(Link.Field.id).eq(q.getId());
            list.add(p);
        }
        if (StringUtils.isNotBlank(q.getName())) {
            Predicate p = qu.string(Link.Field.name).eq(q.getName());
            list.add(p);
        }
        if (!list.isEmpty()) {
            // 注意此处的处理
            Predicate[] p = list.toArray(new Predicate[list.size()]);
            return p;
        }
        return null;
    }

    private SQLQuery<Link> getSQLQuery(LinkQuery optionQuery) {
        SQLQuery<Link> query = new SQLQuery<>();
        SQLRelationalPath<Link> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu).from(qu);
        Predicate[] p = conditionQuery(optionQuery, qu);
        if (p != null) {
            query.where(p);
        }
        return query;
    }

    @Override
    public Pagination<Link> queryPage(LinkQuery q) {
        SQLQuery<Link> query = getSQLQuery(q);
        int pageNo = q.getPageNo();
        int pageSize = q.getPageSize();
        Page<Link> page = dao.getQueryDSLMapper().queryDSLSelectPage(query, (pageNo - 1) * pageSize, pageSize);
        Pagination pg = new Pagination<>(q.getPageNo(), page);
        return pg;
    }

    @Override
    public List<Link> queryList(LinkQuery q) {
        SQLQuery<Link> query = getSQLQuery(q);
        return dao.getDbClient().getQueryDSL().queryDSLSelectList(query,Link.class);
    }

    @Override
    public Link queryOne(LinkQuery query) {
        SQLQuery<Link> q = getSQLQuery(query);
        return dao.getQueryDSLMapper().queryDSLSelectOne(q,Link.class);
    }

    @Override
    public long queryNumber(LinkQuery query) {
        SQLQuery<Link> q = new SQLQuery<>();
        SQLRelationalPath<Link> qu = dao.getQueryDSLMapper().queryDSLTable();
        q.select(qu.number(Link.Field.id).count()).from(qu);
        Predicate[] p = conditionQuery(query, qu);
        if (p != null) {
            q.where(p);
        }
        Long l = dao.getQueryDSLMapper().queryDSLSelectOne(q, Long.class);
        return l;
    }

    @Override
    public int insert(Link link) {
        return dao.insert(link);
    }

    @Override
    public int update(Link link) {
        return dao.updateById(link);
    }

    @Override
    public int merge(Link link) {
        return dao.merge(link);
    }

    @Override
    public int delete(Link link) {
        return dao.delete(link);
    }

    public LinkDAO getDao() {
        return dao;
    }
}
