package com.sxf.service.impl;

import com.querydsl.core.QueryFlag;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.Wildcard;
import com.querydsl.sql.SQLQuery;
import com.sxf.blogmanage.vo.CommentVO;
import com.sxf.common.vo.Pagination;
import com.sxf.dao.CommentDAO;
import com.sxf.domain.blog.Comment;
import com.sxf.service.CommentService;
import com.sxf.web.querybean.CommentQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sf.common.wrapper.Page;
import sf.querydsl.QueryDSLTables;
import sf.querydsl.SQLRelationalPath;
import sf.tools.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {
    @Resource
    private CommentDAO dao;

    private Predicate[] conditionQuery(CommentQuery q, SQLRelationalPath<?> qu) {
        List<Predicate> list = new ArrayList<Predicate>();
        if (q.getId() != null && q.getId() > 0) {
            Predicate p = qu.number(Comment.Field.id).eq(q.getId());
            list.add(p);
        }
        if (q.getArticleId() != null) {
            Predicate p = qu.number(Comment.Field.articleId).eq(q.getArticleId());
            list.add(p);
        }
        if (q.getStatus() != null) {
            Predicate p = ((EnumPath) qu.enums(Comment.Field.status)).eq(q.getStatus());
            list.add(p);
        }
        if (!list.isEmpty()) {
            // 注意此处的处理
            Predicate[] p = list.toArray(new Predicate[list.size()]);
            return p;
        }
        return null;
    }

    private SQLQuery<Comment> getSQLQuery(CommentQuery contentQuery) {
        SQLQuery<Comment> query = new SQLQuery<>();
        SQLRelationalPath<Comment> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu).from(qu);
        Predicate[] p = conditionQuery(contentQuery, qu);
        if (p != null) {
            query.where(p);
        }
        query.orderBy(new OrderSpecifier(Order.ASC, qu.number(Comment.Field.orderNumber)));
        return query;
    }

    @Override
    public Pagination<CommentVO> queryPage(CommentQuery q) {
        SQLQuery<Comment> query = getSQLQuery(q);
        int pageNo = q.getPageNo();
        int pageSize = q.getPageSize();
        Page<Comment> page = dao.getDbClient().getQueryDSL().queryDSLSelectPage(query, Comment.class,(pageNo - 1) * pageSize, pageSize);
        Pagination<Comment> pg = new Pagination<>(q.getPageNo(), page);
        Pagination<CommentVO> p = new Pagination<>();
        p.copyIgnoreRows(pg);
        if (pg.getRows() != null)
            p.setRows(pg.getRows().stream().map(CommentVO::new).collect(Collectors.toList()));
        return p;
    }

    @Override
    public List<Comment> queryList(CommentQuery q) {
        SQLQuery<Comment> query = getSQLQuery(q);
        return dao.getQueryDSLMapper().queryDSLSelectList(query);
    }

    @Override
    public Comment queryOne(CommentQuery q) {
        SQLQuery<Comment> query = getSQLQuery(q);
        return dao.getQueryDSLMapper().queryDSLSelectOne(query);
    }

    @Override
    public Map<String, Object> queryStatistic() {
        SQLQuery<Comment> query = new SQLQuery<>();
        SQLRelationalPath<Comment> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu.stringNew(Comment.Field.status), Wildcard.countAsInt.as("c"))/*.addFlag(QueryFlag.Position.AFTER_PROJECTION, " c ")*/.from(qu).groupBy(qu.stringNew(Comment.Field.status));
        List<Map> list = dao.getDbClient().getQueryDSL().queryDSLSelectList(query, Map.class);
        Map<String, Object> results = new HashMap<>();
        for (Map map : list) {
            if (map.get("status") != null) {
                results.put((String) map.get("status"), map.get("c"));
            }
        }
        return results;
    }

    @Override
    public long queryNumber(CommentQuery q) {
        SQLQuery<Comment> query = new SQLQuery<>();
        SQLRelationalPath<Comment> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu.number(Comment.Field.id).count()).from(qu);
        Predicate[] p = conditionQuery(q, qu);
        if (p != null) {
            query.where(p);
        }
        Long l = dao.getQueryDSLMapper().queryDSLSelectOne(query, Long.class);
        return l;
    }

    @Override
    public void addComment(CommentVO vo) {
        Comment comment = vo.toComment();
        Date date = new Date();
        comment.setCreated(date);
        comment.setReplyCount(0L);
        comment.setVersion(StringUtils.uuid32());
        insert(comment);
        stackReplyCountUpdate(comment.getParentId());
    }

    private void stackReplyCountUpdate(Long parentId) {
        if (parentId != null && parentId != 0) {
            Comment c = dao.getDbClient().selectByPrimaryKeys(Comment.class, parentId);
            if (c != null) {
                if (c.getReplyCount() == null) {
                    c.setReplyCount(1L);
                } else {
                    c.setReplyCount(c.getReplyCount() + 1);
                }
                dao.updateById(c);
                if (c.getParentId() != null && c.getParentId() != 0) {
                    stackReplyCountUpdate(c.getParentId());
                }
            }
        }
    }

    @Override
    public int insert(Comment comment) {
        return dao.getDbClient().insert(comment);
    }

    @Override
    public int update(Comment comment) {
        return dao.getDbClient().update(comment);
    }

    @Override
    public int updateWithVersion(Comment comment) {
        //乐观锁更新
        int i = dao.getDbClient().updateWithVersion(comment);
        if (i == 0) {
            throw new RuntimeException("更新失败");
        }
        return i;
    }

    @Override
    public int merge(Comment comment) {
        return dao.merge(comment);
    }

    @Override
    public int delete(Comment comment) {
        return dao.delete(comment);
    }

    public void setDao(CommentDAO dao) {
        this.dao = dao;
    }

}
