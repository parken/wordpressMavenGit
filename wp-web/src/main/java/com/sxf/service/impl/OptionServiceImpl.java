package com.sxf.service.impl;

import com.querydsl.core.types.Predicate;
import com.querydsl.sql.SQLQuery;
import com.sxf.common.vo.Pagination;
import com.sxf.dao.OptionDAO;
import com.sxf.domain.blog.Option;
import com.sxf.web.querybean.OptionQuery;
import com.sxf.service.OptionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import sf.common.wrapper.Page;
import sf.querydsl.QueryDSLTables;
import sf.querydsl.SQLRelationalPath;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class OptionServiceImpl implements OptionService {
    @Resource
    private OptionDAO dao;


    private Predicate[] conditionQuery(OptionQuery q, SQLRelationalPath<?> qu) {
        List<Predicate> list = new ArrayList<Predicate>();
        if (q.getId() != null && q.getId() > 0) {
            Predicate p = qu.number(Option.Field.id).eq(q.getId());
            list.add(p);
        }
        if (StringUtils.isNotBlank(q.getName())) {
            Predicate p = qu.string(Option.Field.name).eq(q.getName());
            list.add(p);
        }
        if (!list.isEmpty()) {
            // 注意此处的处理
            Predicate[] p = list.toArray(new Predicate[list.size()]);
            return p;
        }
        return null;
    }

    private SQLQuery<Option> getSQLQuery(OptionQuery optionQuery) {
        SQLQuery<Option> query = new SQLQuery<>();
        SQLRelationalPath<Option> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu).from(qu);
        Predicate[] p = conditionQuery(optionQuery, qu);
        if (p != null) {
            query.where(p);
        }
        return query;
    }

    @Override
    public Pagination<Option> queryPage(OptionQuery q) {
        SQLQuery<Option> query = getSQLQuery(q);
        int pageNo = q.getPageNo();
        int pageSize = q.getPageSize();
        Page<Option> page = dao.getQueryDSLMapper().queryDSLSelectPage(query, (pageNo - 1) * pageSize, pageSize);
        Pagination pg = new Pagination<>(q.getPageNo(), page);
        return pg;
    }

    @Override
    public List<Option> queryList(OptionQuery optionQuery) {
        SQLQuery<Option> query = getSQLQuery(optionQuery);
        return dao.getDbClient().getQueryDSL().queryDSLSelectList(query,Option.class);
    }


    @Override
    public Map<String, Object> queryMap(OptionQuery optionQuery) {
        SQLQuery<Option> query = getSQLQuery(optionQuery);
        return (Map<String, Object>) dao.getQueryDSLMapper().queryDSLSelectList(query);
    }

    @Override
    public long queryNumber(OptionQuery q) {
        SQLQuery<Option> query = new SQLQuery<>();
        SQLRelationalPath<Option> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu.number(Option.Field.id).count()).from(qu);
        Predicate[] p = conditionQuery(q, qu);
        if (p != null) {
            query.where(p);
        }
        Long l = dao.getQueryDSLMapper().queryDSLSelectOne(query, Long.class);
        return l;
    }

    @Override
    public int insert(Option option) {
        return dao.insert(option);
    }

    @Override
    public int update(Option option) {
        return dao.updateById(option);
    }

    @Override
    public int delete(Option option) {
        return dao.delete(option);
    }

    public void setDao(OptionDAO dao) {
        this.dao = dao;
    }

}
