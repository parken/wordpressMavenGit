package com.sxf.service.impl;

import com.sxf.blogmanage.vo.ArticleVO;
import com.sxf.common.vo.Pagination;
import com.sxf.dao.ArticleDAO;
import com.sxf.dao.MetadataDAO;
import com.sxf.domain.blog.Article;
import com.sxf.domain.blog.Metadata;
import com.sxf.domain.enumration.ObjectType;
import com.sxf.service.ArticleService;
import com.sxf.service.CategoryService;
import com.sxf.web.querybean.ArticleQuery;
import org.springframework.stereotype.Service;
import sf.common.wrapper.Page;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Resource
    private ArticleDAO dao;
    @Resource
    private MetadataDAO md;
    @Resource
    private CategoryService cs;


    @Override
    public Pagination<ArticleVO> queryPage(ArticleQuery q) {
        Page<Article> page = dao.queryPage(q);
        Pagination<ArticleVO> pg = new Pagination<>();
        pg.setPageNo(q.getPageNo());
        pg.setPageSize(page.getPageSize());
        pg.setTotalPage(page.getTotalPage());
        pg.setTotal(page.getTotalCount());
        List<ArticleVO> list = new ArrayList<>();
        for (Article a : page.getList()) {
            dao.getDbClient().fetchLinks(a,Article.CascadeField.categories);
            ArticleVO vo = new ArticleVO(a);
            list.add(vo);
        }
        pg.setRows(list);
        return pg;
    }

    @Override
    public List<Article> queryList(ArticleQuery q) {
        return dao.queryList(q);
    }

    @Override
    public ArticleVO queryOne(ArticleQuery q) {
        Article article= dao.queryOne(q);
        if (article!=null){
            dao.getDbClient().fetchLinks(article,Article.CascadeField.categories);
        }
        return new ArticleVO(article);
    }

    @Override
    public long queryNumber(ArticleQuery q) {
        Long l = dao.queryNumber(q);
        return l;
    }

    @Override
    public Map<String, Integer> queryStatistic() {
        return dao.queryStatistic();
    }

    @Override
    public List<Date> getArchiveArticles() {
        return dao.getArchiveArticles();
    }

    @Override
    public int insert(Article article) {
        return  dao.insert(article);
    }

    @Override
    public int merge(Article article) {
        return dao.merge(article);
    }

    @Override
    public int delete(Article post) {
        return dao.delete(post);
    }

    @Override
    public void deleteById(long articleId) {
        Article post = new Article();
        post.setId(articleId);
        delete(post);
        Metadata pm = new Metadata();
        pm.setObjectType(ObjectType.article);
        pm.setObjectId(articleId);
        md.delete(pm);
    }

    @Override
    public ArticleDAO getDao() {
        return dao;
    }
}
