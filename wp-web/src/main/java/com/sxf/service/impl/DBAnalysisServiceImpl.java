package com.sxf.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.sxf.service.DBAnalysisService;
import org.springframework.stereotype.Service;

import com.sxf.dao.DBAnalysisDAO;

@Service
public class DBAnalysisServiceImpl implements DBAnalysisService {
	@Resource
	private DBAnalysisDAO dd;

	@Override
	public Map<Integer, Object> getAllTables() {
		return dd.getAllTables();
	}

	@Override
	public Map<Integer, List<String>> getAllProc() {
		return dd.getAllProc();
	}

	public void setDd(DBAnalysisDAO dd) {
		this.dd = dd;
	}
}
