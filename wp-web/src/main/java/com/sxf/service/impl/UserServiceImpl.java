package com.sxf.service.impl;

import com.querydsl.core.types.Predicate;
import com.querydsl.sql.SQLQuery;
import com.sxf.common.vo.Pagination;
import com.sxf.dao.UserDAO;
import com.sxf.domain.user.User;
import com.sxf.service.UserService;
import com.sxf.web.querybean.UserQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import sf.common.wrapper.Page;
import sf.querydsl.QueryDSLTables;
import sf.querydsl.SQLRelationalPath;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Resource
    private UserDAO dao;

    private Predicate[] conditionQuery(UserQuery q, SQLRelationalPath<?> qu) {
        List<Predicate> list = new ArrayList<Predicate>();
        if (q.getId() > 0) {
            Predicate p = qu.number(User.Field.id).eq(q.getId());
            list.add(p);
        }
        if (StringUtils.isNotBlank(q.getLoginName())) {
            Predicate p = qu.string(User.Field.name).eq(q.getLoginName());
            list.add(p);
        }
        if (StringUtils.isNotBlank(q.getNicename())) {
            Predicate p = qu.string(User.Field.nickname).eq(q.getNicename());
            list.add(p);
        }
        if (StringUtils.isNotBlank(q.getPassword())) {
            Predicate p = qu.string(User.Field.password).eq(q.getPassword());
            list.add(p);
        }
        if (!list.isEmpty()) {
            // 注意此处的处理
            Predicate[] p = list.toArray(new Predicate[list.size()]);
            return p;
        }
        return null;
    }

    @Override
    public List<User> queryList(UserQuery q) {
        SQLQuery<User> query = new SQLQuery<>();
        SQLRelationalPath<User> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu).from(qu);
        Predicate[] p = conditionQuery(q, qu);
        if (p != null) {
            query.where(p);
        }
        return dao.getQueryDSLMapper().queryDSLSelectList(query);
    }

    @Override
    public Pagination<User> queryPage(UserQuery q) {
        SQLQuery<User> query = new SQLQuery<>();
        SQLRelationalPath<User> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu).from(qu);
        Predicate[] p = conditionQuery(q, qu);
        if (p != null) {
            query.where(p);
        }
        int pageNo = q.getPageNo();
        int pageSize = q.getPageSize();
        Page<User> page = dao.getQueryDSLMapper().queryDSLSelectPage(query, (pageNo - 1) * pageSize, pageSize);
        Pagination pg = new Pagination<>(q.getPageNo(), page);
        return pg;
    }

    @Override
    public long queryNumber(UserQuery q) {
        SQLQuery<User> query = new SQLQuery<>();
        SQLRelationalPath<User> qu = dao.getQueryDSLMapper().queryDSLTable();
        query.select(qu.number(User.Field.id).count()).from(qu);
        Predicate[] p = conditionQuery(q, qu);
        if (p != null) {
            query.where(p);
        }
        Long l = dao.getQueryDSLMapper().queryDSLSelectOne(query, Long.class);
        return l;
    }

    @Override
    public User findByLoginName(String loginName) {
        User u = new User();
        u.useQuery().createCriteria().eq(User.Field.name, loginName);
        return dao.getDbClient().selectOne(u);
    }

    @Override
    public User findOne(User q) {
        return dao.getDbClient().selectOne(q);
    }

    @Override
    public int insert(User User) {
        return dao.insert(User);
    }

    @Override
    public int merge(User user) {
        return dao.merge(user);
    }

    @Override
    public int update(User user) {
        return dao.updateById(user);
    }

    @Override
    public int delete(User user) {
        Assert.notNull(user.getId(), "");
        return dao.deleteById(user.getId());
    }

    @Override
    public UserDAO getDao() {
        return dao;
    }
}
