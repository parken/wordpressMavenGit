package com.sxf.service;

import com.sxf.blogmanage.vo.CommentVO;
import com.sxf.common.vo.Pagination;
import com.sxf.domain.blog.Comment;
import com.sxf.web.querybean.CommentQuery;

import java.util.List;
import java.util.Map;

public interface CommentService {

    Pagination<CommentVO> queryPage(CommentQuery query);

    /**
     * 获取分组后的评论
     * @param query
     * @return
     */
    List<Comment> queryList(CommentQuery query);

    Comment queryOne(CommentQuery query);

    /**
     * 统计信息
     * @return
     */
    Map<String, Object> queryStatistic();

    long queryNumber(CommentQuery query);

    void addComment(CommentVO vo);

    int insert(Comment comment);

    int update(Comment comment);

    int updateWithVersion(Comment comment);

    int merge(Comment comment);

    int delete(Comment comment);

}
