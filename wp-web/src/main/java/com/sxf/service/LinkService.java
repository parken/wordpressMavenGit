package com.sxf.service;

import com.sxf.common.vo.Pagination;
import com.sxf.domain.blog.Link;
import com.sxf.web.querybean.LinkQuery;

import java.util.List;

public interface LinkService {
    Pagination<Link> queryPage(LinkQuery query);

    List<Link> queryList(LinkQuery query);

    long queryNumber(LinkQuery query);

    Link queryOne(LinkQuery query);

    int insert(Link link);

    int update(Link link);

    int merge(Link link);

    int delete(Link link);

}
