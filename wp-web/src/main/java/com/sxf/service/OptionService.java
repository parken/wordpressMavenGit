package com.sxf.service;

import com.sxf.common.vo.Pagination;
import com.sxf.domain.blog.Option;
import com.sxf.web.querybean.OptionQuery;

import java.util.List;
import java.util.Map;

public interface OptionService {
	public Pagination<Option> queryPage(OptionQuery query);

	public List<Option> queryList(OptionQuery query);

	public Map<String, Object> queryMap(OptionQuery query);

	public long queryNumber(OptionQuery query);

	public int insert(Option option);

	public int update(Option option);

	public int delete(Option option);
}
