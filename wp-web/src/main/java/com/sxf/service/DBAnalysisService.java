package com.sxf.service;

import java.util.List;
import java.util.Map;

/**
 * @author phsxf01
 * 
 */
public interface DBAnalysisService {
	/**
	 * Map&lt;Integer,List&lt;String>>
	 * 
	 * @return
	 */
	public Map<Integer, Object> getAllTables();

	public Map<Integer, List<String>> getAllProc();
}
