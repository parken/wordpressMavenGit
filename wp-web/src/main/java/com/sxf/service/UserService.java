package com.sxf.service;

import com.sxf.common.vo.Pagination;
import com.sxf.dao.UserDAO;
import com.sxf.domain.user.User;
import com.sxf.web.querybean.UserQuery;

import java.util.List;

public interface UserService {
    /**
     * @param userQuery
     * @return
     */
    Pagination<User> queryPage(UserQuery userQuery);

    List<User> queryList(UserQuery userQuery);

    long queryNumber(UserQuery userQuery);

    User findByLoginName(String loginName);

    User findOne(User q);

    int insert(User User);

    int merge(User user);

    int update(User User);

    int delete(User user);

    UserDAO getDao();
}
