package com.sxf.service;

import com.sxf.common.vo.Pagination;
import com.sxf.domain.blog.Category;
import com.sxf.web.querybean.CategoryQuery;

import java.util.List;

public interface CategoryService {
    Pagination<Category> queryPage(CategoryQuery q);

    List<Category> queryList(CategoryQuery q);

    Category queryOne(CategoryQuery q);

    long queryNumber(CategoryQuery q);

    public int insert(Category info);

	public int update(Category info);

    int merge(Category info);

    public int delete(Category info);
}
