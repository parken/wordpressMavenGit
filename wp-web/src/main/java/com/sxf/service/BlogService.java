package com.sxf.service;

import com.sxf.portal.vo.UserForm;

import java.util.Map;

public interface BlogService {
    /**
     * 获取blog基本信息
     * @return
     */
    Map<String, Object> getBasicInfo();

    /**
     * 初始化管理员用户
     * @param uf
     */
    void initAdminUser(UserForm uf);
}
