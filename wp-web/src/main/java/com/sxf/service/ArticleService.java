package com.sxf.service;

import com.sxf.blogmanage.vo.ArticleVO;
import com.sxf.common.vo.Pagination;
import com.sxf.dao.ArticleDAO;
import com.sxf.domain.blog.Article;
import com.sxf.web.querybean.ArticleQuery;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ArticleService {

	public Pagination<ArticleVO> queryPage(ArticleQuery q);

	public List<Article> queryList(ArticleQuery q);

	public ArticleVO queryOne(ArticleQuery q);

	public long queryNumber(ArticleQuery q);

	public Map<String, Integer> queryStatistic();

	public int insert(Article post);

	int merge(Article article);

	public int delete(Article post);

	/**
	 * 归档日期数据
	 * 
	 * @return
	 */
	public List<Date> getArchiveArticles();

    void deleteById(long articleId);

    ArticleDAO getDao();
}
