package com.sxf.test;

import com.sxf.portal.vo.UserForm;
import com.sxf.spring.ApplicationUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * 测试Controller
 * @author SXF
 */
@Slf4j
@Controller
@RequestMapping
public class TestController {
    public TestController() {

    }

    @RequestMapping(value = "/login/{user}", method = RequestMethod.GET)
    public ModelAndView myMethod(HttpServletRequest request,
                                 HttpServletResponse response, HttpSession session,
                                 @PathVariable(value = "user") String user, ModelMap modelMap)
            throws Exception {
        // ServletContext context = request.getSession().getServletContext();
        WebApplicationContext wac1 = RequestContextUtils
                .findWebApplicationContext(request);// YES

        WebApplicationContext wac2 = WebApplicationContextUtils
                .getWebApplicationContext(request.getServletContext(),
                        DispatcherServlet.WEB_APPLICATION_CONTEXT_ATTRIBUTE);

        // CotextLoaderListener
        WebApplicationContext wac3 = ContextLoader
                .getCurrentWebApplicationContext();// YES

        WebApplicationContext wac4 = WebApplicationContextUtils
                .getRequiredWebApplicationContext(request.getServletContext());

        WebApplicationContext wac5 = (WebApplicationContext) request.getServletContext()
                .getAttribute(
                        DispatcherServlet.SERVLET_CONTEXT_PREFIX
                                + request.getServletContext().getServletContextName());
        ApplicationContext ac1 = ApplicationUtils.getContext();

        System.out.println(ac1 == wac3);// 推荐

        System.out.println("wac1:" + wac1);
        System.out.println("wac2:" + wac2);
        System.out.println("wac3:" + wac3);
        System.out.println("wac4:" + wac4);
        System.out.println("wac5:" + wac5);

        System.out.println("wac1 == wac2::" + (wac1 == wac2));
        System.out.println(wac2 == null);
        System.out.println("wac1 == wac3::" + (wac1 == wac3));
        System.out.println("wac1==wac4::" + (wac1 == wac4));
        System.out.println("wac1==wac5::" + (wac1 == wac5));
        System.out.println("wac2==wac3::" + (wac2 == wac3));
        System.out.println("wac2==wac4::" + (wac2 == wac4));
        System.out.println("wac2==wac5::" + (wac2 == wac5));
        System.out.println("wac3==wac4::" + (wac3 == wac4));// true
        System.out.println("wac3==wac5::" + (wac3 == wac5));
        System.out.println("wac4==wac5::" + (wac4 == wac5));

        JdbcTemplate lsb = (JdbcTemplate) wac3.getBean("jdbcTemplate");
        System.out.println("3::LSB:" + lsb);

        modelMap.put("loginUser", user);
        return new ModelAndView("/hello", modelMap);
    }

    @RequestMapping(value = "/welcome", method = {RequestMethod.GET,
            RequestMethod.POST})
    public String registPost() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        System.out.println(request.getContextPath() + " "
                + request.getServerPort() + " " + request.getLocalAddr() + " "
                + request.getLocalPort() + " ");
        System.out.println("sdfkdsjfdsjfds");
        return "/welcome";
    }

    @RequestMapping(value = "/testftl", method = {RequestMethod.GET,
            RequestMethod.POST})
    public ModelAndView toTestFtl() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("loginForm", new UserForm());
        mav.setViewName("test/test");
        return mav;
    }

    @RequestMapping(value = "/doTestftl", method = {RequestMethod.GET,
            RequestMethod.POST})
    public ModelAndView doTestFtl(
            @Valid @ModelAttribute("loginForm") UserForm userForm,
            BindingResult br) {
        ModelAndView mav = new ModelAndView();
        if (br.hasErrors()) {
            List<ObjectError> oe = br.getAllErrors();
            for (ObjectError oer : oe) {
                log.info(oer.getDefaultMessage());
            }
            mav.addObject("loginName", userForm);
            mav.setViewName("test/test");
        }
        return mav;
    }

    @RequestMapping(value = "/testvm", method = {RequestMethod.GET,
            RequestMethod.POST})
    public String testVm() {
        @SuppressWarnings("unused")
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();

        // HttpServletResponse response=((servletres) RequestContextHolder
        // .getRequestAttributes()).get();
        return "/test";
    }

}
