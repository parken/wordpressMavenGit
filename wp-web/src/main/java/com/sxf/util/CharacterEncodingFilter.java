package com.sxf.util;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * 编码过滤器
 * 
 * @author soft01
 * 
 */
public class CharacterEncodingFilter implements Filter {
	String requestEncoding, responseEncoding;

	@Override
	public void init(FilterConfig config) throws ServletException {
		requestEncoding = config.getInitParameter("requestEncoding");
		responseEncoding = config.getInitParameter("responseEncoding");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(requestEncoding);
		response.setCharacterEncoding(responseEncoding);
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {

	}

}
