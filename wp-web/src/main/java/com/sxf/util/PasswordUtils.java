package com.sxf.util;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

public class PasswordUtils {
    /**
     * @param msg
     * @return
     * @throws Exception
     */
    public static String md5(String msg) throws Exception {
        String pwd = null;
        if (msg != null && !"".equals(msg.trim())) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] b = msg.getBytes();
            byte[] b1 = md.digest(b);// 加密
            // 用于把字节数组编码成字符串
            pwd = Base64.getEncoder().encodeToString(b1);
        }
        return pwd;
    }

    public static void encodeRandomSalt(String rawPassword) throws Exception {
        byte[] salt = new byte[8];
        SecureRandom random = new SecureRandom();
        random.nextBytes(salt);
        // System.out.println("生成的盐：" + Arrays.toString(salt));

        MessageDigest digest = MessageDigest.getInstance("MD5");// 当然可以使用SHA（160位），MD5(128位)
        digest.update(salt);
        digest.update(rawPassword.getBytes("utf-8"));
        byte[] afterMd5 = digest.digest();
        System.out.println("杂凑值：" + Arrays.toString(afterMd5));

    }

    public static String encode(String rawPassword, String salt)
            throws Exception {
        return encode(null, rawPassword, salt);
    }

    public static String encode(String algorithm, String rawPassword,
                                String salt) throws Exception {
        String pwd = null;
        if (algorithm == null || "".equals(algorithm)) {
            algorithm = "MD5";
        }
        if (rawPassword != null && !"".equals(rawPassword)) {

            MessageDigest digest = MessageDigest.getInstance(algorithm);// 当然可以使用SHA（160位），MD5(128位)
            digest.update(salt.getBytes());
            digest.update(rawPassword.getBytes("utf-8"));
            byte[] afterMd5 = digest.digest();
            // System.out.println("杂凑值：" + Arrays.toString(afterMd5));

            // 用于把字节数组编码成字符串

            pwd = Base64.getEncoder().encodeToString(afterMd5);
        }
        return pwd;
    }

    /**
     * 加密
     * @param algorithm 定义 加密算法,可用 DES,DESede,Blowfish
     * @param key       为加密密钥，长度为24字节
     * @param src       为被加密的数据缓冲区（源）
     * @return
     */
    public static String encryptMode(String algorithm, String key, String src) {
        try {
            byte[] arr = key.getBytes();
            byte[] keybyte = new byte[24];
            for (int i = 0; i < keybyte.length; i++) {
                if (arr.length >= i + 1) {
                    keybyte[i] = arr[i];
                } else {
                    keybyte[i] = 0;
                }
            }
            byte[] encoding = encryptModeByte(algorithm, keybyte,
                    src.getBytes());
            return Base64.getEncoder().encodeToString(encoding);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * 解密
     * @param algorithm 定义 解密算法,可用 DES,DESede,Blowfish
     * @param key       加密密钥，长度为24字节
     * @param src       解密源字符串
     * @return
     */
    public static String decryptMode(String algorithm, String key, String src) {
        try {
            byte[] arr = key.getBytes();
            byte[] keybyte = new byte[24];
            for (int i = 0; i < keybyte.length; i++) {
                if (arr.length >= i + 1) {
                    keybyte[i] = arr[i];
                } else {
                    keybyte[i] = 0;
                }
            }
            byte[] encoding = Base64.getDecoder().decode(src);
            byte[] decode = decryptModeByte(algorithm, keybyte, encoding);
            return new String(decode);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * 加密
     * @param algorithm 定义 加密算法,可用 DES,DESede,Blowfish
     * @param keybyte   为加密密钥，长度为24字节
     * @param src       为被加密的数据缓冲区（源）
     * @return
     */
    public static byte[] encryptModeByte(String algorithm, byte[] keybyte,
                                         byte[] src) {
        try {
            // 生成密钥
            SecretKey deskey = new SecretKeySpec(keybyte, algorithm);
            // 加密
            Cipher c1 = Cipher.getInstance(algorithm);
            c1.init(Cipher.ENCRYPT_MODE, deskey);
            return c1.doFinal(src);
        } catch (java.security.NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (javax.crypto.NoSuchPaddingException e2) {
            e2.printStackTrace();
        } catch (java.lang.Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    /**
     * 解密
     * @param algorithm 定义 解密算法,可用 DES,DESede,Blowfish
     * @param key       加密密钥，长度为24字节
     * @param src       解密源字符串
     * @return
     */
    public static byte[] decryptModeByte(String algorithm, byte[] key,
                                         byte[] src) {
        try {
            // 生成密钥
            SecretKey deskey = new SecretKeySpec(key, algorithm);

            // 解密
            Cipher c1 = Cipher.getInstance(algorithm);
            c1.init(Cipher.DECRYPT_MODE, deskey);
            return c1.doFinal(src);
        } catch (java.security.NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (javax.crypto.NoSuchPaddingException e2) {
            e2.printStackTrace();
        } catch (java.lang.Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(encode("iwueriouewroiewu", "weiuriewurioewu"));
        System.out.println(md5("11111"));
    }
}
