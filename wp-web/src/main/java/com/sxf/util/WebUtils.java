package com.sxf.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

/*
 *@auther shixiafeng
 *@create 2017-10-27
 */
public class WebUtils {
    private static Logger log = LoggerFactory.getLogger(WebUtils.class);

    /**
     * 是否支持ajax请求
     * @param request
     * @return
     */
    public static boolean supportAjax(WebRequest request) {
        return isAjax(request);
    }

    /**
     * 判断不准,默认为true,请勿使用
     * 是否为ajax请求
     * @param request
     * @return
     */
    public static boolean isAjax(WebRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    /**
     * 供下载使用
     * @param in
     * @param response
     */
    public static void writeStreamToResponse(InputStream in, HttpServletResponse response) {
        OutputStream output = null;
        try {
            output = response.getOutputStream();
            byte[] bytes = new byte[1024 * 8];
            int count = 0;
            while ((count = in.read(bytes)) != -1) {
                output.write(bytes, 0, count);
            }
        } catch (Exception e) {
            log.error("", e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error("", e);
                }
            }
            if (output != null) {
                try {
                    output.flush();
                } catch (IOException e) {
                    log.error("", e);
                }
            }
        }
    }

    /**
     * 供下载使用
     * @param in
     * @param response
     */
    public static void writeStreamToResponse(byte[] in, HttpServletResponse response) {
        OutputStream output = null;
        try {
            output = response.getOutputStream();
            output.write(in);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            if (output != null) {
                try {
                    output.flush();
                } catch (IOException e) {
                    log.error("", e);
                }
            }
        }
    }

    /**
     * 获取服务地址
     * @param request
     * @return
     */
    public static String getServiceUrl(HttpServletRequest request) {
        String serviceUrl = "";
        StringBuffer sb = request.getRequestURL();
        if (sb != null && sb.length() > 0) {
            String requestUrl = sb.toString();
            try {
                URL url = new URL(requestUrl);
                serviceUrl = url.getProtocol() + "://" + url.getHost() + (url.getPort() == -1 ? "" : ":" + url.getPort());
            } catch (MalformedURLException e) {
                log.error("", e);
            }
        }
        return serviceUrl;
    }
}
