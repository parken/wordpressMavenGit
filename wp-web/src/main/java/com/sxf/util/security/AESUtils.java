/**
 * @Copyright: 2010 HangZhou Hikvision System Technology Co., Ltd. All Right Reserved.
 * @address: http://www.hikvision.com
 * @date: 2013-3-27 上午09:41:47
 */
package com.sxf.util.security;


import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * 封装V6.x平台中AES加密和解密的算法
 * 
 * @author huanghuafeng 2013-3-27 上午09:41:47
 * @version V1.0
 */
public class AESUtils {
	
	private static final char[] HEX_CHAR = {'0', '1', '2', '3', '4', '5', 
        '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

	private static Cipher cipher = null;

	private static Cipher getCipher() throws NoSuchAlgorithmException, NoSuchPaddingException {
		cipher = Cipher.getInstance("AES/CFB/NoPadding");
		return cipher;
	}

	/**
	 * AES加密原文
	 * 
	 * @author huanghuafeng 2013-4-17 上午11:31:19
	 * @param sSrc
	 *            原文
	 * @param sKey
	 *            密钥
	 * @param ivParameter
	 *            cbc加密的起始向量，长度必须是16
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptWithBase64(byte[] sSrc, String sKey, String ivParameter) throws Exception {
		ensureSrcNotNull(sSrc);
		ensureKeyIsRight(sKey);
		byte[] raw = sKey.getBytes("UTF8");
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = getCipher();
		// 使用CBC模式，需要一个向量iv，可增加加密算法的强度
		IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes(Charset.forName("UTF8")));
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
		byte[] encrypted = cipher.doFinal(sSrc);
		return Base64.getEncoder().encode(encrypted);
	}

	/**
	 * 使用aes算法解密<br>
	 * 算法是方法encryptWithBase64的反向
	 * 
	 * @see AESUtils#decryptWithBase64(String ,String);
	 * @author huanghuafeng 2013-4-17 上午11:34:07
	 * @param sSrc
	 *            密文
	 * @param sKey
	 *            密钥
	 * @param ivParameter
	 *            cbc解密的起始向量，长度必须是16
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptWithBase64(byte[] sSrc, String sKey, String ivParameter) throws Exception {
		ensureSrcNotNull(sSrc);
		ensureKeyIsRight(sKey);
		byte[] raw = sKey.getBytes("UTF8");
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = getCipher();
		// 使用CBC模式，需要一个向量iv，可增加加密算法的强度
		IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes(Charset.forName("UTF8")));
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
		byte[] encrypted = Base64.getDecoder().decode(sSrc);
		byte[] original = cipher.doFinal(encrypted);
		return original;
	}

    public static String encryptWithBase64(String plainText, String sKey, String ivParameter) throws Exception {
        try {
            byte[] bts = plainText.getBytes("UTF-8");
            byte[] encryptedBts = encryptWithBase64(bts, sKey, ivParameter);
            return new String(encryptedBts, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e); //其实不可能发生，utf-8肯定是支持的
        }
    }

    public static String decryptWithBase64(String encrytedText, String sKey, String ivParameter) throws Exception {
        try {
            byte[] bts = encrytedText.getBytes("UTF-8");
            byte[] decryptedBts = decryptWithBase64(bts, sKey, ivParameter);
            return new String(decryptedBts, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e); //其实不可能发生，utf-8肯定是支持的
        }
    }

	private static void ensureSrcNotNull(byte[] sSrc) {
		if (sSrc == null) {
			throw new IllegalArgumentException("src is empty.");
		}
	}

	private static void ensureKeyIsRight(String key) {
		// 判断Key是否正确
		if (key == null) {
			throw new IllegalArgumentException("key is empty.");
		}
		// 判断Key是否为16位
		if (key.length() != 16) {
			throw new IllegalArgumentException("Key length is wrong.");
		}
	}
	
	private static final String FISH = "clown_lie_to_hcm";

	public static String encrypt(String info) {
		try {
			byte[] kbytes = FISH.getBytes();
			SecretKeySpec key = new SecretKeySpec(kbytes, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] encoding = cipher.doFinal(info.getBytes());
//			BigInteger n = new BigInteger(encoding);
//			return n.toString(16);
			return byteToHex(encoding);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	public static String decrypt(String info) {
		try {
			byte[] kbytes = FISH.getBytes();
			SecretKeySpec key = new SecretKeySpec(kbytes, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, key);
//			BigInteger n = new BigInteger(info, 16);
//			byte[] encoding = n.toByteArray();
//			byte[] decode = cipher.doFinal(encoding);
			byte[] decode = cipher.doFinal(hexToByte(info));
			return new String(decode);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	private static String byteToHex(byte[] bytes) {
		System.out.println(Arrays.toString(bytes));
		if (bytes == null) return null;
		// 一个byte为8位，可用两个十六进制位标识
        char[] buf = new char[bytes.length * 2];
        int a = 0;
        int index = 0;
		for (byte b : bytes) { // 使用除与取余进行转换
			if (b < 0) {
				a = 256 + b;
			} else {
				a = b;
			}
			buf[index++] = HEX_CHAR[a / 16];
			buf[index++] = HEX_CHAR[a % 16];
		}
        return new String(buf);
	}
	
	private static byte[] hexToByte(String str) {
		if (str == null || str.length() == 0) return new byte[0];
		int length = str.length();
		if (length % 2 != 0) {
			throw new RuntimeException("DES decrypt string length must be multiplier of 2");
		}
		byte[] bytes = new byte[length / 2];
		int high, low;
        for(int i = 0; i < length / 2; i ++) {
        	high = str.charAt(2 * i);
        	low = str.charAt(2 * i + 1);
        	//如果是数字，-48，如果是小写字母，-87，如果是大写字母，-55
        	//0,1,2,3,4,5,6,7,8,9->0,1,2,3,4,5,6,7,8,9
        	//a,b,c,d,e,f->10,11,12,13,14,15
        	//A,B,C,D,E,F->10,11,12,13,14,15
        	high = high < 58 ? high - 48 : high > 86 ? high - 87 : high - 55;
        	low = low < 58 ? low - 48 : low > 86 ? low - 87 : low - 55;
//            String subStr = str.substring(i * 2, i * 2 + 2);
//            bytes[i] = (byte) Integer.parseInt(subStr, 16);
        	
            bytes[i] = (byte)(high * 16 + low);
        }
        return bytes;
	}

	public static void main(String[] args) {
//		String test = "a";
//		for (int i = 0 ; i < 32 ; i ++) {
//			String encrypt = encrypt(test);
//			System.out.println("origin:" + test + ",encrypt:" + encrypt + ",length:" + encrypt.length());
//			test = test + "a";
//		}
//		String test = "Abc123++";
//		String encode = encrypt(test);
//		System.out.println(encode);
//		System.out.println(Arrays.toString(hexToByte(encode)));
//		System.out.println(decrypt(encode));
//		System.out.println(decrypt("94b504579019aab8f2ec7afc558a8fb7"));
//		System.out.println(encrypt("134"));
//		System.out.println(decrypt("944afba86fe655470d138503aa757049"));
//		System.out.println(encrypt("admin"));
		System.out.println(decrypt("790666f0d37dd8867d1df2f6e56bb7af"));
//		-6bb504579019aab8f2ec7afc558a8fb7
//		94b504579019aab8f2ec7afc558a8fb7
		
//		944afba86fe655470d138503aa757049
//		byte[] bytes = new byte[]{-108, 74, -5, -88, 111, -26, 85, 71, 13, 19, -123, 3, -86, 117, 112, 73};
//		byte[] bytes = new byte[]{83, -113, -112, -27, -71, -26, -101, -62, 113, 12, 54, 100, 4, 31, 64, 80};
//		BigInteger n = new BigInteger(bytes);
//		System.out.println(n.toString(16));
//		char[] arr = new char[]{'0','1'};
	}

}
