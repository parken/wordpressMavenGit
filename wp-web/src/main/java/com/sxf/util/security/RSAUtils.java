package com.sxf.util.security;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Map;

/**
 * RSA相关工具类
 * @author shixiafeng
 */
public class RSAUtils {

    public static final String PUBLIC_KEY_BASE64_MODEL_KEY = "PUBLIC_KEY_BASE64";
    public static String PUBLIC_KEY_BASE64 = "";
    public static String PRIVATE_KEY_BASE64 = "";

    static {
        Map<String, byte[]> keyMap = RSA.generateKeyBytes();
        PUBLIC_KEY_BASE64 = Base64.getEncoder().encodeToString(keyMap.get(RSA.PUBLIC_KEY));
        PRIVATE_KEY_BASE64 = Base64.getEncoder().encodeToString(keyMap.get(RSA.PRIVATE_KEY));
    }

    public static String getPublicKey() {
        return PUBLIC_KEY_BASE64;
    }

    public static String getPrivateKey() {
        return PRIVATE_KEY_BASE64;
    }

    /**
     * 加密
     * @param plainText 明文
     * @return
     */
    public static String encode(String plainText) {
        return encode(plainText, PUBLIC_KEY_BASE64);
    }

    public static String encode(String plainText, String publicKey) {
        PublicKey pk = RSA.restorePublicKey(Base64.getDecoder().decode(publicKey));
        byte[] encodedText = RSA.encode(pk, plainText.getBytes());
        String encode = Base64.getEncoder().encodeToString(encodedText);
        return encode;
    }

    /**
     * 解密
     * @param encodedText 密文
     * @return
     */
    public static String decode(String encodedText) {
        return decode(encodedText, PRIVATE_KEY_BASE64);
    }

    public static String decode(String encodedText, String privateKey) {
        PrivateKey pk = RSA.restorePrivateKey(Base64.getDecoder().decode(privateKey));
        String decoded = RSA.decode(pk, Base64.getDecoder().decode(encodedText));
        return decoded;
    }

}
