package com.sxf.websocket;

import org.springframework.web.socket.WebSocketSession;

import java.util.Map;

/**
 * 用来在WebSocket的日志中输出更多的信息
 *
 */
public class SessionOutput {
	private WebSocketSession session;

	public SessionOutput(WebSocketSession webSocketSession) {
		this.session = webSocketSession;
	}

	@Override
	public String toString() {
		if (session == null) {
			return "NULL";
		}
		Map<String, Object> attr = session.getAttributes();
		Long createTime = (Long) attr.get("WEBSOCKET_CREATED");
		String s = session.toString() + " from[" + session.getRemoteAddress() + "], user[" + attr.get("WEBSOCKET_USERID") + "]";
		if (createTime != null) {
			s += "生存时间:" + (System.currentTimeMillis() - createTime) / 1000 + "秒";
		}
		return s;
	}
}
