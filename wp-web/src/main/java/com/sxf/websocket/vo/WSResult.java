package com.sxf.websocket.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sxf.common.CommonResultCode;
import com.sxf.common.ResultCodeInf;

import java.io.Serializable;
import java.util.Arrays;

/**
 * websocket消息推送类型
 * @param <T>
 */
public class WSResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private int code;

    @JsonIgnore
    private ResultCodeInf enumCode;

    private String message;

    private MessageType type;

    private T data;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JSONField(serialize = false)
    private Object[] messageParam;

    public enum MessageType {
        notice, logout
    }


    public WSResult() {
    }

    public WSResult(ResultCodeInf resultCode) {
        this.enumCode = resultCode;
        this.setCode(enumCode.getCode());
    }

    public WSResult(ResultCodeInf resultCode, String message) {
        this.enumCode = resultCode;
        this.message = message;
        this.setCode(enumCode.getCode());
    }

    public WSResult(ResultCodeInf resultCode, T data) {
        this.enumCode = resultCode;
        this.data = data;
        this.setCode(enumCode.getCode());
    }

    public WSResult(ResultCodeInf resultCode, T data, Object[] messageParam) {
        this.enumCode = resultCode;
        this.data = data;
        this.messageParam = messageParam;
        this.setCode(enumCode.getCode());
    }

    public int getCode() {
        return code;
    }

    public WSResult<T> setCode(int code) {
        this.code = code;
        return this;
    }

    public ResultCodeInf getEnumCode() {
        return enumCode;
    }

    public void setEnumCode(ResultCodeInf enumCode) {
        this.enumCode = enumCode;
        this.setCode(enumCode.getCode());
    }

    public String getMessage() {
        return message;
    }

    public WSResult<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Object[] getMessageParam() {
        return messageParam;
    }

    public void setMessageParam(Object[] messageParam) {
        this.messageParam = messageParam;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public static <T> WSResult<T> build() {
        return buildSuccess();
    }

    public static <T> WSResult<T> buildSuccess() {
        WSResult<T> result = new WSResult<>();
        result.setEnumCode(CommonResultCode.RESULT_SUCCESS);
        result.setCode(result.getEnumCode().getCode());
        return result;
    }

    public static <T> WSResult<T> buildFail() {
        WSResult<T> result = new WSResult<>();
        result.setEnumCode(CommonResultCode.RESULT_FAIL);
        result.setCode(result.getEnumCode().getCode());
        return result;
    }

    public static <T> WSResult<T> buildIllegal() {
        WSResult<T> result = new WSResult<>();
        result.setEnumCode(CommonResultCode.ILLEGAL_ARGUMENT);
        result.setCode(result.getEnumCode().getCode());
        return result;
    }

    @Override
    public String toString() {
        return "AjaxResult{" +
                "resultCode=" + code +
                ", code=" + enumCode +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", messageParam=" + Arrays.toString(messageParam) +
                '}';
    }
}
