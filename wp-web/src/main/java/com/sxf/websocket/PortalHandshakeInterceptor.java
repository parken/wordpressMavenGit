package com.sxf.websocket;

import com.sxf.web.LoginUtils;
import com.sxf.web.SessionUser;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Service
public class PortalHandshakeInterceptor implements HandshakeInterceptor {

    public static final String WEBSOCKET_ACCOUNT_NAME = "WEBSOCKET_ACCOUNT_NAME";

    public static final String WEBSOCKET_CREATED = "WEBSOCKET_CREATED";

    public static final String WEBSOCKET_FIRST_CREATED = "WEBSOCKET_FIRST_CREATED";


    //初次握手访问前
    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Map<String, Object> map) throws Exception {
        if (request instanceof ServletServerHttpRequest) {
            HttpServletRequest servletRequest = ((ServletServerHttpRequest) request).getServletRequest();
            HttpSession session = servletRequest.getSession();
            Boolean socketFirstCreated = (Boolean) session.getAttribute(WEBSOCKET_FIRST_CREATED);
            if(socketFirstCreated == null){
                socketFirstCreated = true;
                session.setAttribute(WEBSOCKET_FIRST_CREATED,socketFirstCreated);
            }else{
                socketFirstCreated = false;
            }

            //获取用户信息
            SessionUser su = LoginUtils.getLoginUser(servletRequest);
            String accountName = su.getNickName();
            //存入数据，方便在hander中获取，这里只是在方便在webSocket中存储了数据，并不是在正常的httpSession中存储，想要在平时使用的session中获得这里的数据，需要使用session 来存储一下
            map.put(WEBSOCKET_ACCOUNT_NAME, accountName);
            map.put(WEBSOCKET_CREATED, System.currentTimeMillis());
            map.put(HttpSessionHandshakeInterceptor.HTTP_SESSION_ID_ATTR_NAME, servletRequest.getSession().getId());
            map.put(WEBSOCKET_FIRST_CREATED,socketFirstCreated);
        }
        return true;
    }

    //初次握手访问后
    @Override
    public void afterHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Exception e) {

    }

}
