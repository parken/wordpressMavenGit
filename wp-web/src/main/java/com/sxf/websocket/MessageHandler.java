package com.sxf.websocket;

import com.alibaba.fastjson.JSON;
import com.sxf.websocket.vo.WSResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;
import org.springframework.web.socket.sockjs.transport.session.AbstractSockJsSession;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class MessageHandler extends TextWebSocketHandler {

    private static final Logger logger = LoggerFactory.getLogger(MessageHandler.class);



    private Map<String, WebSocketSession> sessionMap = new ConcurrentHashMap<>();

    // 初次链接成功执行
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        logger.info("{} 建立websocket链接", new SessionOutput(session));
        sessionMap.put(session.getId(), session);
    }

    // 接受消息处理消息
    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> webSocketMessage) throws Exception {
//        broadcast(new TextMessage(String.valueOf(webSocketMessage.getPayload())));
        //原样返回.
        session.sendMessage(new TextMessage(String.valueOf(webSocketMessage.getPayload())));
    }


    @Override
    public void handleTransportError(WebSocketSession session, Throwable throwable) throws Exception {
        if (session.isOpen()) {
            session.close(CloseStatus.NORMAL);
        }
        sessionMap.remove(session.getId());
        logger.debug("{} websocket被客户端关闭(异常)", new SessionOutput(session));
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        logger.info("{} 关闭websocket链接(正常) {}", new SessionOutput(session), closeStatus);
        sessionMap.remove(session.getId());
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    /**
     * 给所有在线用户发送消息
     *
     * @param message
     * @throws IOException
     */
    public void broadcast(String accountName, TextMessage message) {
        for (Entry<String, WebSocketSession> entry : sessionMap.entrySet()) {
            WebSocketSession session = entry.getValue();
            if (entry.getKey().equals(accountName)) {
                if (isOpen(session)) {
                    try {
                        session.sendMessage(message);
                    } catch (Exception e) {
                        logger.error("{} Send message error", new SessionOutput(session), e);
                    }
                }
            }
        }
    }

    public <T> void sendMessageToUser(String userId, WSResult<T> message) {
        if (message != null) {
            TextMessage tm = new TextMessage(JSON.toJSONString(message));
            sendMessageToUser(userId, tm);
        }
    }

    /**
     * 给某个用户发送消息
     *
     * @param userId
     * @param message
     * @throws IOException
     */
    public void sendMessageToUser(String userId, TextMessage message) {
        Assert.notNull(userId);
        int count = 0;
        for (WebSocketSession session : sessionMap.values()) {
            if (userId.equals(session.getAttributes().get(PortalHandshakeInterceptor.WEBSOCKET_ACCOUNT_NAME)) && isOpen(session)) {
                count++;
                try {
                    session.sendMessage(message);
                } catch (Exception e) {
                    logger.error("{} Send unread count to user {} error", new SessionOutput(session), userId, e);
                }
            }
        }
        if (count == 0) {
            logger.warn("The message was not sent to user {}, since there's no session of the user", userId);
        }
    }

    /**
     * 给某个session id发送消息
     *
     * @param webSocketSessionId websocket的session id
     * @param message
     */
    public void sendMessageToSessionId(String webSocketSessionId, TextMessage message) {
        Assert.notNull(webSocketSessionId);
        int count = 0;
        for (Entry<String, WebSocketSession> entry : sessionMap.entrySet()) {
            String sessionId = entry.getKey();
            WebSocketSession session = entry.getValue();
            if (webSocketSessionId.equals(sessionId) && isOpen(session)) {
                count++;
                try {
                    session.sendMessage(message);
                } catch (Exception e) {
                    logger.error("{} Send unread count to user {} error", new SessionOutput(session), webSocketSessionId, e);
                }
            }
        }
        if (count == 0) {
            logger.warn("The message was not sent to user {}, since there's no session of the user", webSocketSessionId);
        }
    }

    /**
     * 给某个session id发送消息
     *
     * @param httpSessionId websocket的session id
     * @param message
     */
    public void sendMessageToHttpSessionId(String httpSessionId, TextMessage message) {
        Assert.notNull(httpSessionId);
        int count = 0;
        for (Entry<String, WebSocketSession> entry : sessionMap.entrySet()) {
            WebSocketSession session = entry.getValue();
            String httpSessionIdAttr = (String) session.getAttributes().get(HttpSessionHandshakeInterceptor.HTTP_SESSION_ID_ATTR_NAME);
            if (httpSessionId.equals(httpSessionIdAttr) && isOpen(session)) {
                count++;
                try {
                    session.sendMessage(message);
                } catch (Exception e) {
                    logger.error("{} Send message to user {} error", new SessionOutput(session), httpSessionId, e);
                }
            }
        }
        if (count == 0) {
            logger.warn("The message was not sent to user {}, since there's no session of the user", httpSessionId);
        }
    }

    private boolean isOpen(WebSocketSession session) {
        if (session instanceof AbstractSockJsSession) {
            return ((AbstractSockJsSession) session).isActive();
        } else {
            return session.isOpen();
        }
    }
}