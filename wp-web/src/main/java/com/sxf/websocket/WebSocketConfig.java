package com.sxf.websocket;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        //支持websocket
        registry.addHandler(new MessageHandler(), "/websocket/socketServer.do").addInterceptors(new PortalHandshakeInterceptor());
        //支持sockjs
        registry.addHandler(new MessageHandler(), "/sockjs/socketServer.do").addInterceptors(new PortalHandshakeInterceptor()).withSockJS();
    }
}
 