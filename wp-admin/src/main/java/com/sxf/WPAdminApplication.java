package com.sxf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.groovy.template.GroovyTemplateAutoConfiguration;
import org.springframework.boot.autoconfigure.jersey.JerseyAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication(exclude={RedisAutoConfiguration.class, RedisRepositoriesAutoConfiguration.class,
		JerseyAutoConfiguration.class, GroovyTemplateAutoConfiguration.class})
@EnableCaching
@EnableAsync
@EnableScheduling
public class WPAdminApplication {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(WPAdminApplication.class, args);
	}
}
