<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ExtJs主页</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/extjs/resources/css/ext-all.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/extjs/ext-debug.js"></script>

<script type="text/javascript" des="var">

</script>

<script type="text/javascript" des="ui">


	Ext.require('Ext.container.Viewport');
	Ext.application({
		name : 'HelloExt',
		launch : function() {
		/*	Ext.create('Ext.container.Viewport', {
				layout : 'fit',
				items : [ {
					title : 'Hello Ext',
					html : 'Hello! Welcome to Ext JS.'
				} ,
				{
					title : 'Hello Ext',
					html : ""
				} 
				]
			}); */
			
			Ext.create('Ext.panel.Panel', {
			    renderTo: Ext.getBody(),
			    width: 400,
			    height: 200,
			    title: 'Container Panel',
			    layout: 'column',
			    items: [
			        {
			            xtype: 'panel',
			            title: 'Child Panel 1',
			            height: 100,
			            columnWidth: 0.5
			        },
			        {
			            xtype: 'panel',
			            title: 'Child Panel 2',
			            height: 100,
			            columnWidth: 0.5
			        }
			    ]
			});
		}
	});
<%-- 
	//Card布局__类似向导
    Ext.application({
        name: 'HelloExt',
        launch: function () {
            var navigate = function (panel, direction) {
                var layout = panel.getLayout();
                layout[direction]();
                Ext.getCmp('move-prev').setDisabled(!layout.getPrev());
                Ext.getCmp('move-next').setDisabled(!layout.getNext());
            };
            Ext.create('Ext.panel.Panel', {
                title: 'Card布局示例',
                width: 300,
                height: 202,
                layout: 'card',
                activeItem: 0,
                x: 30,
                y: 60,
                bodyStyle: 'padding:15px',
                defaults: { border: false },
                bbar: [{
                    id: 'move-prev',
                    text: '上一步',
                    handler: function (btn) {
                        navigate(btn.up("panel"), "prev");
                    },
                    disabled: true
                },
                '->',
                {
                    id: 'move-next',
                    text: '下一步',
                    handler: function (btn) {
                        navigate(btn.up("panel"), "next");
                    }
                }],
                items: [{
                    id: 'card-0',
                    html: '<h1>第一步</h1>'
                },
                {
                    id: 'card-1',
                    html: '<h1>第二步</h1>'
                },
                {
                    id: 'card-2',
                    html: '<h1>最后一步</h1>'
                }],
                renderTo: Ext.getBody()
            });
        }
    });
--%>
	
</script>
</head>
<body>

</body>
</html>