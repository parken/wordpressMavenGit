<%--
  Created by IntelliJ IDEA.
  User: ThinkPad
  Date: 13-9-7
  Time: 下午12:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <title></title>　　
    <jsp:include page="/js/extjs.jsp"/>
</head>
<script type="text/javascript" kind="var">
    var logf;
    var vf;
</script>
<script type="text/javascript" kind="method">
    function submitOnClick() {
        //获取当前的表单form
        var form = this.up('form').getForm();
        //判断否通过了表单验证，如果不能空的为空则不能提交
        if (form.isValid()) {
            //alert("可以提交");
            form.submit(
                    {
                        clientValidation:true,
                        waitMsg:'请稍候',
                        waitTitle:'正在验证登录',
                        url:'${pageContext.request.contextPath }/blog/login/doLogin',
                        params:'format=json',
                        success:function (form, action) {
                            //登录成功后的操作，这里只是提示一下
                            Ext.MessageBox.show({
                                width:150,
                                title:"登录成功",
                                buttons:Ext.MessageBox.OK,
                                msg:action.result.msg
                            })
                        },
                        failure:function (form, action) {
                            Ext.MessageBox.show({
                                width:150,
                                title:"登录失败",
                                buttons:Ext.MessageBox.OK,
                                msg:action.result.msg
                            })
                        }

                    }
            )
        }
    }

    function resetOnClick() {
        console.log(this);
        //点击取消，关闭登录窗口
        var form = this.up('form');
        form.getForm().reset();
    }
</script>
<script type="text/javascript" kind="ui">
    Ext.onReady(function () {
        //begin
        logf = Ext.create('Ext.form.Panel', {
                    frame:true,
                    title:'管理后台',
                    width:300,
                    height:170,
                    //渲染到页面中的loginForm层中
                    renderTo:'loginForm',
                    //是否可以拖动
                    draggable:true,
                    //使buttons中的button居中显示
                    buttonAlign:'center',
                    fieldDefaults:{
                        //居左
                        labelAlign:'center',
                        //宽度
                        labelWidth:50,
                        //anchor: '90%',
                        //错误提示显示在一边(side)，还可以配置为under、title、none
                        msgTarget:'side'
                    },
                    items:[
                        {
                            xtype:'textfield',
                            fieldLabel:'用户名',
                            name:'loginName',
                            //不允许为空
                            width:250,
                            allowBlank:false,
                            blankText:'用户名不能为空'
                        },
                        {
                            xtype:'textfield',
                            fieldLabel:'密&nbsp;&nbsp;&nbsp;码',
                            name:'password',
                            inputType:'password',
                            width:250,
                            allowBlank:false,
                            blankText:'密码不能为空'
                        },
                        {
                            xtype:'checkboxgroup',
                            fieldLabel:'记住我',
                            height:20,
                            allowBlank:true,
                            items:[
                                {
                                    boxLabel:'&nbsp;&nbsp;<img style="height: 10px;" src="../../js/extjs/resources/themes/images/default/dd/drop-yes.gif" title="勾选后，5日内无需登录" />',
                                    itemCls:'required',
                                    id:'_acegi_security_remember_me',
                                    name:'_acegi_security_remember_me',
                                    inputValue:'true'
                                }
                            ]
                        }
                    ],
                    buttons:[
                        {
                            text:'登录',
                            width:80,
                            height:30,
                            handler:submitOnClick
                        },
                        {
                            text:'重置',
                            width:80,
                            height:30,
                            handler:resetOnClick
                        }
                    ]
                }
        );

        //end
        vp = Ext.getBody();
        var x = (vp.getSize().width - logf.getSize().width) / 2;
        var y = (document.body.clientHeight - logf.getSize().height) / 2;
        logf.setPosition(x, y);
    })

</script>
<body id="body">
<div id="loginForm"></div>
</body>
</html>