<#include "../../template/page.ftl">
<@compress >
    <@head title="媒体编辑">
    <style type="text/css">
        input[type="text"],textarea{
            width: 460px;
        }
    </style>
    <script type="text/javascript">

    </script>
    </@head>
<body>
<div>
    <h2>
        编辑媒体 <a href="${path}/blog/admin/media/toAdd" style="font-family: sans-serif;font-size: 12px;margin-left: 10px;">添加</a>
    </h2>
    <a href="javascript:void(0);" class="easyui-linkbutton"
       onclick="window.location.href='${path}/blog/admin/media';return false;">返回</a>
    <form method="post" action="${path}/blog/admin/media/doEdit" class="media-upload-form" id="media-single-form">
        <p class="submit" style="padding-bottom: 0;">
            <input type="submit" name="save" id="save" class="button-primary" value="更新媒体"></p>

        <div class="media-single">
            <div id="media-item-24" class="media-item">
                <#if post??>
                    <input type="hidden" id="type-of-24" value="audio">
                    <input type="hidden" name="id" value="${post.id!}">
                    <input type="hidden" name="menuOrder" value="${post.menuOrder!}">
                </#if>

                <table class="slidetoggle describe ">
                    <thead class="media-item-info" id="media-head-24">
                    <tr valign="top">
                        <td class="A1B1" id="thumbnail-head-24">
                            <p><a href="${path}/?attachment_id=<#if post??>${post.id!}</#if>" target="_blank"><img
                                    class="thumbnail"
                                    src="http://localhost/wordpress/wp-includes/images/crystal/audio.png" alt=""></a>
                            </p>

                            <p></p>
                        </td>
                        <td>
                            <p><strong>文件名：</strong><#if post??>${post.name!}</#if></p>

                            <p><strong>文件类型：</strong><#if post??>${post.mimeType!}</#if></p>

                            <p><strong>上传日期：</strong><#if post??>${post.date?string("yyyy 年 MM 月 dd 日")}</#if></p></td>
                    </tr>

                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2" class="imgedit-response" id="imgedit-response-24"></td>
                    </tr>
                    <tr>
                        <td style="display:none" colspan="2" class="image-editor" id="image-editor-24"></td>
                    </tr>
                    <tr class="post_title">
                        <th valign="top" scope="row" class="label"><label for="title"><span
                                class="alignleft">标题</span><br class="clear"></label></th>
                        <td class="field"><input type="text" class="text" id="title"
                                                 name="title" value="<#if post??>${post.title!}</#if>"></td>
                    </tr>
                    <tr class="post_excerpt">
                        <th valign="top" scope="row" class="label"><label for="excerpt"><span
                                class="alignleft">说明</span><br class="clear"></label></th>
                        <td class="field"><textarea name="excerpt"
                                                    id="excerpt"><#if post??>${post.excerpt!}</#if></textarea></td>
                    </tr>
                    <tr class="post_content">
                        <th valign="top" scope="row" class="label"><label for="content"><span
                                class="alignleft">描述</span><br class="clear"></label></th>
                        <td class="field"><textarea id="content"
                                                    name="content"><#if post??>${post.content!}</#if></textarea></td>
                    </tr>
                    <tr class="image_url">
                        <th valign="top" scope="row" class="label"><label for="url"><span
                                class="alignleft">文件 URL</span><br class="clear"></label></th>
                        <td class="field"><input type="text" class="text urlfield" readonly="readonly"
                                                 name="guid" id="url"
                                                 value="<#if post??>${post.guid!}</#if>"><br>

                            <p class="help">上传文件的位置。</p></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p class="submit"><input type="submit" name="save" id="save" class="button-primary" value="更新媒体"></p>
    </form>
</div>
</body>
</html>
</@compress>