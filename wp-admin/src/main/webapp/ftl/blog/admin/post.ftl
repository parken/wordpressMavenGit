<#include "../../template/page.ftl">
<@compress >
    <@head title="文章管理">
    <script type="text/javascript">
        /**
         * 查询条件
         * @type {String}
         */
        var queryCondition = "";
    </script>
    <#-- datatables专用 -->
    <script type="text/javascript">
        var oTable;
        $(document).ready(function () {
            oTable = initDataTables();
        });

        function initDataTables() {
            var tableId = "postTable";
            var oTable = $('#' + tableId).dataTable({
                "dom":'<"H"lfr>t<"F"ip>T', //<"H"lfr>t<"F"ip>
                "pageLength":10, //每页显示个数
                "scrollCollapse":true,
                "lengthChange":true, //每页显示的记录数
                "paging":true, //是否显示分页
                "searching":true, //搜索栏
                "ordering":false, //是否支持排序功能
                "info":true, //显示表格信息
                //  "scrollY": "200px",//显示滚动条
                "autoWidth":false, //自适应宽度
                "stateSave":false, //保存状态到cookie *************** 很重要 ，当搜索的时候页面一刷新会导致搜索的消失。使用这个属性就可避免了
                "language":{
                    "url":"${path}/ui/datatables/dtCH.txt" //多语言配置文件，可将language的设置放在一个txt文件中，例：Javascript/datatable/dtCH.txt
                }, //多语言配置
                "jQueryUI":true, //可以添加 jqury的ui theme  需要添加css
                "lengthMenu":[
                    [1, 2, 10, 15, 20],
                    ["1", "2", "10", "15", "20"]
                ],
                //   "bDeferRender":true, //是否使用延迟加载
                "serverSide":true,
                "ajax": function (data, callback, settings) {
					var page = Math.floor(parseInt(data.start) / parseInt(data.length)) + 1;
					var rows = parseInt(data.length);
					if (queryCondition != '') {
					    queryCondition = '&' + queryCondition;
					}
					$.ajax({
					    dataType:'json',
					    type:'POST',
					    cache:false,
					    url:"${path}/blog/admin/post/listAjax",
					    async:false,
					    data:$.param({dtjson:$.toJSON(data)}) + "&page=" + page + "&rows=" + rows + queryCondition,
					    success:function (response) {
					        var pageSize = response.pageSize;
					        var total = response.total;
					        var pageTotal = response.pageTotal;
					        var currentPage = response.currentPage;
					        response.recordsTotal = total;
					        response.recordsFiltered = total;
					        response.data = response.rows;
					        callback(response);
					    }
					});
                },
                "processing":true,
                "pagingType":/*"two_button",*/"full_numbers",
                "columns":[
                    { "data":null, "render":function (data, type, full, meta) {
                    	return meta.row + meta.settings._iDisplayStart + 1;
                    }},
                    { "data":null, "render":function (data, type, full, meta) {
                        return '<input type="checkbox" name="ck"/>';
                    }},
                    { "data":'title'},
                    { "data":'authorName' },
                   /* { "data":'author',
                        "bVisible":false
                    },*/
                    { "data":"termList", "render":function (data, type, full, meta) {
                        //  alert(data);
                        //  console.log(data);
                        return showTerm(data, '${TAXONOMY_POST_CATEGORY}');
                    }},
                    { "data":'termList', "render":function (data, type, full, meta) {
                        return showTerm(data, '${TAXONOMY_POST_TAG}');
                    }},
                    { "data":'commentCount' },
                    { "data":'date' },
                    { "data":'status', "render":function (data, type, full, meta) {
                        var displayStatus = "";
                        if (data == '${POST_STATUS_PUBLISH}') {
                            displayStatus = "已发布";
                        } else if (data == '${POST_STATUS_DRAFT}') {
                            displayStatus = "草稿";
                        } else if (data == '${POST_STATUS_TRASH}') {
                            displayStatus = "回收站";
                        } else if (data == '${POST_STATUS_PENDING}') {
                            displayStatus = "等待审核";
                        }
                        return displayStatus;
                    }},
                    { "data":'id',
                        "render":function (data, type, full, meta) {
                            var id = data;
                            var html = '<a href="${path}/blog/admin/post/edit?postId=' + id + '">修改</a>';
                            html = html + '&nbsp;<span class="spanLink" onclick="changePost(\'' + id + '\',\'trash\');">放入回收站</span>&nbsp;' +
                                    '<a href="${path}/blog/show?p='+id+'" target="_blank">查看</a>&nbsp;' +
                                    '<span class="spanLink" onclick="deletePost(\'' + id + '\');">删除</span>';
                            return html;
                        },
                        "orderable":false
                    }
                ],
                "rowCallback":function (nRow, aData, iDisplayIndex) {
                    //单行选中
                    $(nRow).click(function () {
                        $(oTable.fnSettings().aoData).each(function () {
                            $(this.nTr).removeClass('row_selected');
                        });
                        if ($(this).hasClass('row_selected'))
                            $(this).removeClass('row_selected');
                        else
                            $(this).addClass('row_selected');
                    });
                    return nRow;
                }
            });
            // addDatatablesButton(tableId);
            return oTable;
        }
        function addDatatablesButton(tableId) {
                $('#del').click(function () {
                    var tt = TableTools.fnGetInstance(tableId);
                    var anSelected = tt.fnGetSelected();
                    if (anSelected.length !== 0) {
                        oTable.fnDeleteRow(anSelected[0]);
                    }
                });
        }

        function selectAll(rootCKid, subCKname) {
            if ($('#' + rootCKid).attr("checked") == 'checked') {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).attr("checked", "checked");
                });
            } else {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).removeAttr("checked");
                });
            }
        }
        /**
         *刷新表格
         */
        function refreshTable() {
            oTable.fnClearTable(0);
            oTable.fnDraw();
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            statistic();
        });

        function deletePost(postId) {
            if (postId == undefined || postId == '') {
                return;
            }
            if (confirm("确定删除?")) {
                $.ajax({
                    type:"POST",
                    async:false,
                    url:"${path}/blog/admin/post/delete",
                    data:"postId=" + postId,
                    success:function (msg) {
                        refreshTable();
                        //window.location.href = "${path}/blog/admin/term/toCategory";
                    }
                });
            }
        }
        /**
         * 统计信息
         */
        function statistic() {
            $.ajax({
                type:"POST",
                async:false,
                url:"${path}/blog/admin/post/statisticsAjax",
                data:"",
                dataType:"json",
                success:function (msg) {
                    var data = msg;
                    if (data != undefined && data != null) {
                        var all = 0;
                        var publish = data['publish'];
                        var pending = data['${POST_STATUS_PENDING}'];
                        var draft = data['draft'];
                        var trash = data['trash'];
                        /*var moderated = "0";
                     var approved = "1";*/
                        $('.all-count').html('0');
                        $('.publish-count').html('0');
                        $('.draft-count').html('0');
                        $('.trash-count').html('0');
                        $('.pending-count').html('0');
                        if (publish != undefined && publish != null) {
                            all = all + publish;
                            $('.publish-count').html(publish);
                        }
                        if (draft != undefined && draft != null) {
                            all = all + draft;
                            $('.draft-count').html(draft);
                        }
                        if (trash != undefined && trash != null) {
                            all = all + trash;
                            $('.trash-count').html(trash);
                        }
                        if (pending != undefined && pending != null) {
                            all = all + pending;
                            $('.pending-count').html(pending);
                        }
                        $('.all-count').html(all);
                    }
                }
            });
        }

        function setStatus(val) {
            $('#status').val(val);
            refreshTable();
        }
        function queryByTerm(term) {
            if (term == undefined || term == null) {
                term = '';
            }
            queryCondition = "status=" + term;
            refreshTable();
        }
        /**
         * 筛选
         */
        function queryPost() {
            var dateByMonth = $('#m').val();//按月份归类
            var postCategory = $('#cat').val();
            queryCondition = 'dateByMonth=' + dateByMonth + "&postCategory=" + postCategory;
            refreshTable();
        }
        /**
         * 搜索文章
         */
        function searchPost() {
            var title = $('#post-search-input').val();
            queryCondition = "title=" + title;
            refreshTable();
        }
        function changePost(postId, status) {
            if (postId == undefined || postId == '') {
                return;
            }
            $.ajax({
                type:"POST",
                async:false,
                url:"${path}/blog/admin/post/doAddOrUpdate?format=json",
                data:"id=" + postId + "&status=" + status,
                success:function (msg) {
                    refreshTable();
                    //window.location.href = "${path}/blog/admin/term/toCategory";
                    statistic();
                }
            });
        }
    </script>
    </@head>
<body>
<h2>文章</h2>

<div style="margin-bottom:2px;height: 35px;">
    <div style="position: relative;margin-right: 10px;float: right;">
        <input type="search" id="post-search-input" name="s" value="">&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton" onclick="searchPost();return false;">搜索</a></div>
    <div style="margin: 5px;float:left;">
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('');return false;">全部(<span
                class="all-count">0</span>)</a>&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('${POST_STATUS_PUBLISH}');return false;">已发布(<span class="publish-count">0</span>)</a>&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('${POST_STATUS_PENDING}');return false;">等待审核(<span class="pending-count">0</span>)</a>&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('${POST_STATUS_DRAFT}');return false;">草稿(<span class="draft-count">0</span>)</a>&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('${POST_STATUS_TRASH}');return false;">回收站(<span class="trash-count">0</span>)</a>
    </div>
</div>
<div style="margin-bottom: 5px;"><select name="action">
    <option value="" selected="selected">批量操作</option>
    <option value="edit" class="hide-if-no-js">编辑</option>
    <option value="trash">移至回收站</option>
</select>&nbsp;&nbsp;<a href="javascript:void(0);" class="easyui-linkbutton" onclick=" return false;">应用</a>&nbsp;&nbsp;<select
        name="m" id="m">
    <option selected="selected" value="">显示所有日期</option>
    <#if archiveList?? &&(archiveList?size>0)>
        <#list archiveList as obj>
            <option value="${obj?string("yyyyMM")}">${obj?string("yyyy年MM月")}</option>
        </#list>
    </#if>
</select>&nbsp;&nbsp;<select name="cat" id="cat">
    <option value="">查看所有分类目录</option>
    <#if termList??>
        <#list termList as obj>
            <option value="${obj.taxonomyId!}">${obj.name!}</option>
        </#list>
    </#if>
</select>&nbsp;&nbsp;<a href="javascript:void(0);" class="easyui-linkbutton" onclick="queryPost();return false;">筛选</a>
</div>


<table id="postTable" cellpadding="0" cellspacing="0" border="0" class="display">
    <thead>
    <tr align="left">
        <th width="10px">&nbsp;</th>
        <th style="width: 10px;"><input type="checkbox" id="rootCK" onclick="selectAll('rootCK','ck')"></th>
        <th>标题</th>
        <th>作者</th>
        <#--<th>作者ID</th>-->
        <th>分类目录</th>
        <th>标签</th>
        <th>评论数目</th>
        <th>日期</th>
        <th>状态</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot align="left">
    <th width="10px">&nbsp;</th>
    <th style="width: 10px;">&nbsp;</th>
    <th>标题</th>
    <th>作者</th>
    <#--<th>作者ID</th>-->
    <th>分类目录</th>
    <th>标签</th>
    <th>评论数目</th>
    <th>日期</th>
    <th>状态</th>
    <th>操作</th>
    </tfoot>
</table>
</body>
</html>
</@compress>