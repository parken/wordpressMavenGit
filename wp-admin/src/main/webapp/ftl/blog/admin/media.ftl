<#include "../../template/page.ftl">
<@compress >
    <#assign title="媒体库"/>
    <@head title=title>
    <script type="text/javascript">
        /**
         * 查询条件
         * @type {String}
         */
        var queryCondition = "";
    </script>
    <#-- datatables专用 -->
    <script type="text/javascript">
        var oTable;
        $(document).ready(function () {
            oTable = initDataTables();
        });

        function initDataTables() {
            var tableId = "postTable";
            var oTable = $('#' + tableId).dataTable({
                "sDom":'<"H"lfr>t<"F"ip>T', //<"H"lfr>t<"F"ip>
                "iDisplayLength":10, //每页显示个数
                "bScrollCollapse":true,
                "bLengthChange":true, //每页显示的记录数
                "bPaginate":true, //是否显示分页
                "bFilter":true, //搜索栏
                "bSort":false, //是否支持排序功能
                "bInfo":true, //显示表格信息
                //  "sScrollY": "200px",//显示滚动条
                "bAutoWidth":false, //自适应宽度
                "bStateSave":false, //保存状态到cookie *************** 很重要 ，当搜索的时候页面一刷新会导致搜索的消失。使用这个属性就可避免了
                "oLanguage":{
                    "sUrl":"${path}/ui/datatables/dtCH.txt" //多语言配置文件，可将oLanguage的设置放在一个txt文件中，例：Javascript/datatable/dtCH.txt
                }, //多语言配置
                "bJQueryUI":true, //可以添加 jqury的ui theme  需要添加css
                "aLengthMenu":[
                    [1, 2, 10, 15, 20],
                    ["1", "2", "10", "15", "20"]
                ],
                //   "bDeferRender":true, //是否使用延迟加载
                "bServerSide":true,
                "sAjaxSource":"${path}/blog/admin/media/listAjax",
                "fnServerParams":function (aoData) {
                    // aoData.push( { "name": "more_data", "value": "my_value" } );
                },
                "fnServerData":function (sSource, aoData, fnCallback) {
                    var dtjson = $.parseToJson($.param(aoData));
                    var page = Math.floor(parseInt(dtjson.iDisplayStart) / parseInt(dtjson.iDisplayLength)) + 1;
                    var rows = parseInt(dtjson.iDisplayLength);
                    if (queryCondition != '') {
                        queryCondition = '&' + queryCondition;
                    }
                    $.ajax({
                        dataType:'json',
                        type:'POST',
                        cache:false,
                        url:sSource,
                        async:false,
                        data:"dtjson="+encodeURI($.toJSON(dtjson)) + "&page=" + page + "&rows=" + rows + queryCondition,
                        success:function (response) {
                            var pageSize = response.pageSize;
                            var total = response.total;
                            var pageTotal = response.pageTotal;
                            var currentPage = response.currentPage;
                            response.sEcho = parseInt(dtjson.sEcho);
                            response.iTotalRecords = total;
                            response.iTotalDisplayRecords = total;
                            fnCallback(response);
                        }
                    });
                },
                "sAjaxDataProp":"rows",
                "sServerMethod":"POST",
                "bProcessing":true,
                "sPaginationType":/*"two_button",*/"full_numbers",
                "aoColumns":[
                    { "mData":null, "fnRender":function (obj) {
                        return obj.iDataRow + obj.oSettings._iDisplayStart + 1;
                    }},
                    { "mData":null, "mRender":function (data, type, full) {
                        return '<input type="checkbox" name="ck"/>';
                    }},
                    { "mDataProp":'title'},
                    { "mDataProp":'authorName' },
                    { "mDataProp":'author',
                        "bVisible":false
                    },
                    { "mDataProp":'commentCount' },
                    { "mDataProp":'date' },
                    { "mDataProp":'id',
                        "mRender":function (data, type, full) {
                            var id = data;
                            var html = '<a href="${path}/blog/admin/media/edit?postId=' + id + '">编辑</a>';
                            html = html + '&nbsp;<span class="spanLink" onclick="deletePost(\'' + id + '\');"><strong style="color: #cc4433;">永久删除</strong></span>' +
                                    '&nbsp;<a href="${path}/?attachment_id='+id+'" title="查看“'+full.title+'”" target="_blank">查看</a>';
                            return html;
                        },
                        "bSortable":false
                    }
                ],
                "fnRowCallback":function (nRow, aData, iDisplayIndex) {
                    //单行选中
                    $(nRow).click(function () {
                        $(oTable.fnSettings().aoData).each(function () {
                            $(this.nTr).removeClass('row_selected');
                        });
                        if ($(this).hasClass('row_selected'))
                            $(this).removeClass('row_selected');
                        else
                            $(this).addClass('row_selected');
                    });
                    return nRow;
                },
                "oTableTools":{"sSwfPath":"${path}/ui/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"}
            });
            return oTable;
        }

        function selectAll(rootCKid, subCKname) {
            if ($('#' + rootCKid).attr("checked") == 'checked') {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).attr("checked", "checked");
                });
            } else {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).removeAttr("checked");
                });
            }
        }
        /**
         *刷新表格
         */
        function refreshTable() {
            oTable.fnClearTable(0);
            oTable.fnDraw();
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            statistic();
        });

        function deletePost(postId) {
            if (postId == undefined || postId == '') {
                return;
            }
            if (confirm("确定删除?")) {
                $.ajax({
                    type:"POST",
                    async:false,
                    url:"${path}/blog/admin/media/delete",
                    data:"postId=" + postId,
                    success:function (msg) {
                        refreshTable();
                        //window.location.href = "${path}/blog/admin/term/toCategory";
                    }
                });
            }
        }
        /**
         * 统计信息
         */
        function statistic() {
            $.ajax({
                type:"POST",
                async:false,
                url:"${path}/blog/admin/post/statisticsAjax",
                data:"",
                dataType:"json",
                success:function (msg) {
                    var data = msg;
                    if (data != undefined && data != null) {
                        var all = 0;
                        var publish = data['publish'];
                        var pending = data['${POST_STATUS_PENDING}'];
                        var draft = data['draft'];
                        var trash = data['trash'];
                        /*var moderated = "0";
                     var approved = "1";*/
                        $('.all-count').html('0');
                        $('.publish-count').html('0');
                        $('.draft-count').html('0');
                        $('.trash-count').html('0');
                        $('.pending-count').html('0');
                        if (publish != undefined && publish != null) {
                            all = all + publish;
                            $('.publish-count').html(publish);
                        }
                        if (draft != undefined && draft != null) {
                            all = all + draft;
                            $('.draft-count').html(draft);
                        }
                        if (trash != undefined && trash != null) {
                            all = all + trash;
                            $('.trash-count').html(trash);
                        }
                        if (pending != undefined && pending != null) {
                            all = all + pending;
                            $('.pending-count').html(pending);
                        }
                        $('.all-count').html(all);
                    }
                }
            });
        }

        function setStatus(val) {
            $('#status').val(val);
            refreshTable();
        }
        function queryByTerm(term) {
            if (term == undefined || term == null) {
                term = '';
            }
            queryCondition = "status=" + term;
            refreshTable();
        }
        /**
         * 筛选
         */
        function queryPost() {
            var dateByMonth = $('#m').val();//按月份归类
            var postCategory = $('#cat').val();
            queryCondition = 'dateByMonth=' + dateByMonth + "&postCategory=" + postCategory;
            refreshTable();
        }
        /**
         * 搜索文章
         */
        function searchPost() {
            var title = $('#post-search-input').val();
            queryCondition = "title=" + title;
            refreshTable();
        }
    </script>
    </@head>
<body>
<h2>${title!}<a href="${path}/blog/admin/media/toAdd" style="font-family: sans-serif;font-size: 12px;margin-left: 10px;">添加</a></h2>

<div style="margin-bottom:2px;height: 35px;">
    <div style="position: relative;margin-right: 10px;float: right;">
        <input type="search" id="post-search-input" name="s" value="">&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton" onclick="searchPost();return false;">搜索</a></div>
    <div style="margin: 5px;float:left;">
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('');return false;">全部(<span
                class="all-count">0</span>)</a>&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('${POST_STATUS_PUBLISH}');return false;">已发布(<span class="publish-count">0</span>)</a>&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('${POST_STATUS_PENDING}');return false;">等待审核(<span class="pending-count">0</span>)</a>&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('${POST_STATUS_DRAFT}');return false;">草稿(<span class="draft-count">0</span>)</a>&nbsp;&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton"
           onclick="queryByTerm('${POST_STATUS_TRASH}');return false;">回收站(<span class="trash-count">0</span>)</a>
    </div>
</div>
<div style="margin-bottom: 5px;"><select name="action">
    <option value="" selected="selected">批量操作</option>
    <option value="edit" class="hide-if-no-js">编辑</option>
    <option value="trash">移至回收站</option>
</select>&nbsp;&nbsp;<a href="javascript:void(0);" class="easyui-linkbutton" onclick=" return false;">应用</a>&nbsp;&nbsp;<select
        name="m" id="m">
    <option selected="selected" value="">显示所有日期</option>
    <#if archiveList?? &&(archiveList?size>0)>
        <#list archiveList as obj>
            <option value="${obj?string("yyyyMM")}">${obj?string("yyyy年MM月")}</option>
        </#list>
    </#if>
</select>&nbsp;&nbsp;<a href="javascript:void(0);" class="easyui-linkbutton" onclick="queryPost();return false;">筛选</a>
</div>


<table id="postTable" cellpadding="0" cellspacing="0" border="0" class="display">
    <thead>
    <tr align="left">
        <th width="10px">&nbsp;</th>
        <th style="width: 10px;"><input type="checkbox" id="rootCK" onclick="selectAll('rootCK','ck')"></th>
        <th>文件</th>
        <th>作者</th>
        <th>附加到</th>
        <th>评论</th>
        <th>日期</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot align="left">
    <th width="10px">&nbsp;</th>
    <th style="width: 10px;">&nbsp;</th>
    <th>文件</th>
    <th>作者</th>
    <th>附加到</th>
    <th>评论</th>
    <th>日期</th>
    <th>操作</th>
    </tfoot>
</table>
</body>
</html>
</@compress>