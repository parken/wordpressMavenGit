<#include "../../template/page.ftl">
<@compress >
    <#assign title="上传新媒体文件"/>
    <@head title=title>
    <script type="text/javascript">

    </script>
    </@head>
<body>
<div>
    <h2>上传新文件</h2>
    <form enctype="multipart/form-data" method="post" action="${path}/blog/admin/media/doAdd" class="" id="file-form">
        <div id="media-upload-notice"></div>
        <div id="media-upload-error"></div>

        <div id="html-upload-ui" class="hide-if-js">
            <p id="async-upload-wrap">
                <label class="screen-reader-text" for="async-upload">上传</label>
                <input type="file" name="file" id="async-upload">
            </p>

            <div class="clear"></div>
            <p class="upload-html-bypass hide-if-no-js">
                您正在使用浏览器内置的标准上传工具。</p>
        </div>

        <span class="max-upload-size">上传文件大小限制：2MB。</span><br>
        <span class="after-file-upload">文件上传之后，您可以为它添加标题和描述。</span>
        <input type="hidden" name="post_id" id="post_id" value="0">

        <p>
            <input type="submit" name="save" id="save"
                   class="" value="保存所有更改">
        </p></form>
</div>
</body>
</html>
</@compress>