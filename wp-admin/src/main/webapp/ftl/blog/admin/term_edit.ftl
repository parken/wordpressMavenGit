<#include "../../template/page.ftl">
<@compress >
    <#if taxonomy??>
        <#if taxonomy==TAXONOMY_POST_CATEGORY>
            <#assign title="编辑分类目录"/>
        <#elseif taxonomy==TAXONOMY_POST_TAG>
            <#assign title="编辑标签"/>
        <#elseif taxonomy==TAXONOMY_LINKA_CATEGORY>
            <#assign title="编辑链接分类目录"/>
        </#if>
    <#else >
        <#assign title="编辑术语"/>
    </#if>
    <@head title=title>
    <script type="text/javascript">

    </script>
    </@head>
<body fit="true">
<div>
    <div class="wrap">
        <div id="icon-edit" class="icon32 icon32-posts-post"><br></div>
        <h2>${title!}</h2>

        <div id="ajax-response"></div>
        <form name="edittag" id="edittag" method="post" action="${path}/blog/admin/term/doAddOrUpdate" class="validate">
            <input type="hidden" name="action" value="editedtag">
            <input type="hidden" name="termId" value="${(term.id)!}">
            <input type="hidden" name="taxonomyId" value="${(term.taxonomyId)!}">
            <input type="hidden" name="taxonomy" value="${(term.taxonomy)!}">
            <input type="hidden" id="_wpnonce" name="_wpnonce" value="0f06f862e9">

            <table class="form-table">
                <tbody>
                <tr class="form-field form-required">
                    <th scope="row" valign="top"><label for="name">名称</label></th>
                    <td><input name="name" id="name" type="text" value="${(term.name)!}" size="40"
                               class="easyui-validatebox"
                               data-options="required:true,validType:'length[1,250]'">

                        <p class=" description">这将是它在站点上显示的名字。</p></td>
                </tr>
                <tr class="form-field">
                    <th scope="row" valign="top"><label for="slug">别名</label></th>
                    <td><input name="slug" id="slug" type="text" value="${(term.slug)!}" size="40">

                        <p class="description">“别名”是在 URL 中使用的别称，它可以令 URL 更美观。通常使用小写，只能包含字母，数字和连字符（-）。</p></td>
                </tr>
                <tr class="form-field" <#if (TAXONOMY_POST_CATEGORY!=taxonomy)!false>style="display: none;" </#if>>
                    <th scope="row" valign="top"><label for="parent">父级</label></th>
                    <td>
                        <select name="parent" id="parent" class="postform">
                            <option value="">无</option>
                            <#if (parentTermList?size>0)!false>
                                <#list parentTermList as obj>
                                    <option value="${obj.taxonomyId!}"
                                            <#if (term.parent==obj.taxonomyId)!false>selected="selected" </#if>>${obj.name!}</option>
                                </#list>
                            </#if>
                        </select>

                        <p class="description">分类目录和标签不同，它可以有层级关系。您可以有一个“音乐”分类目录，在这个目录下可以有叫做“流行”和“古典”的子目录。</p>
                    </td>
                </tr>
                <tr class="form-field">
                    <th scope="row" valign="top"><label for="description">描述</label></th>
                    <td><textarea name="description" id="description" rows="5" cols="50"
                                  class="large-text">${(term.description)!}</textarea><br>
                        <span class="description">描述只会在一部分主题中显示。</span></td>
                </tr>
                </tbody>
            </table>
            <p class="submit"><input type="submit" name="submit" id="submit" class="button-primary" value="更新">
            <button type="button" onclick="window.history.go(-1);">返回</button>
            </p>
        </form>
    </div>
</div>
</body>
</html>
</@compress>