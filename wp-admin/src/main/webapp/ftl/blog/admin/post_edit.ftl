<#include "../../template/page.ftl">
<@compress >
    <#if post??>
        <#assign title="文章修改"/>
        <#assign postExist=true/>
    <#else >
        <#assign title="文章添加"/>
        <#assign postExist=false/>
    </#if>
    <@head title=title>
    <style>
        form {
            margin: 0;
        }

        textarea {
            display: block;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#post').submit(function () {
                return $('#post').form('validate');
                //   return true;
            });

            loadCategroy();
            loadTag();

            openAddCategoryW();
            $('#addcategoryButton').click(function () {
                $('#addcategorydiv').window('open');
            });

            <!-- 表单回填 -->
            <#if postExist>
                <#list post.termList as term>
                    <#if (term.taxonomy==TAXONOMY_POST_CATEGORY)!false>
                        $("input[id^='category_'][type='checkbox'][value='${term.taxonomyId!}']").attr('checked', true);
                    <#elseif (term.taxonomy==TAXONOMY_POST_TAG)!false>
                        $("input[id^='tag_'][type='checkbox'][value='${term.taxonomyId!}']").attr('checked', true);
                    </#if>
                </#list>
            </#if>

        });

        function titleTipShow() {
            var title = $('#title').val();
            if (title == '') {
                $('#title-prompt-text').show();
            } else {
                $('#title-prompt-text').hide();
            }
        }
        //设置登录窗口
        function openAddCategoryW() {
            $('#addcategorydiv').window({
                title:'添加分类',
                width:320,
                modal:true,
                shadow:true,
                closed:true,
                height:160,
                resizable:true
            });
            loadParentCategory();
        }
        //关闭登录窗口
        function closeAddCategoryW() {
            $('#addcategorydiv').window('close');
        }

        var editor;
        KindEditor.ready(function (K) {
            editor = K.create('textarea[name="content"]', {
                allowFileManager:true
            });
        });
        function loadCategroy() {
            try {
                var data = loadTermAjax(POST_CATEGORY);
                var termList = data;
                if (termList != null && termList.length > 0) {
                    var html = '';
                    $.each(termList, function (i, n) {
                        html = html + '<input type="checkbox" id="category_' + n.taxonomyId + '" name="categorys" value="' + n.taxonomyId + '"><label for="category_' + n.taxonomyId + '">' + n.name + '</label>';
                    });
                    $('#category').html(html);
                }
            } catch (e) {
                console.log(e);
            }
        }

        function loadParentCategory() {
            var data = loadTermAjax(POST_CATEGORY);
            try {
                var termList = data;
                if (termList != null && termList.length > 0) {
                    var html = '<option value="">无</option>';
                    $.each(termList, function (i, n) {
                        html = html + '<option value="' + n.taxonomyId + '">' + n.name + '</option>';
                    });
                    $('#newcategory_parent').html(html);
                }
            } catch (e) {
                console.log(e);
            }
        }

        function loadTag() {
            var data = loadTermAjax(POST_TAG);
            try {
                var termList = data;
                if (termList != null && termList.length > 0) {
                    var html = '';
                    $.each(termList, function (i, n) {
                        html = html + '<input type="checkbox" id="tag_' + n.taxonomyId + '" name="tags" value="' + n.taxonomyId + '"><label for="tag_' + n.taxonomyId + '">' + n.name + '</label>';
                    });
                    $('#tag').html(html);
                }
            } catch (e) {
                console.log(e);
            }
        }

        function loadTermAjax(taxonomy) {
            if (taxonomy == undefined || taxonomy == null || taxonomy == '') {
                return;
            }
            var data = '';
            $.ajax({
                type:"POST",
                dataType:'json',
                async:false,
                url:"${path}/blog/admin/term/listAjax",
                data:"taxonomy=" + taxonomy,
                success:function (msg) {
                    data = msg;
                }
            });
            return data;
        }

        <#-- ========================================================================= -->
        function addCategory() {
            var data = $('#addcategoryForm').serialize();
            addTermAjax(data, POST_CATEGORY);
            loadCategroy();
        }

        function addTag() {
            var data = $('#new-tag-post_tag').val();
            if (data.trim() != '') {
                data = "newtag[post_tag]=" + encodeURI(data);
                var result = addTermAjax(data, POST_TAG);
                if (result != null) {
                    loadTag();
                    $('#new-tag-post_tag').val('');
                }

            }
        }

        function addTermAjax(data, taxonomy) {
            if (taxonomy == undefined || taxonomy == null || taxonomy == '') {
                return;
            }
            var url = "${path}/blog/admin/term/";
            if (POST_CATEGORY == taxonomy) {
                url = url + "doAddPostCategoryAjax.json";
            }
            if (POST_TAG == taxonomy) {
                url = url + "doAddPostTagAjax.json";
            }
            var result = "";
            $.ajax({
                type:"POST",
                async:false,
                url:url,
                data:data,
                success:function (msg) {
                    result = msg;
                }
            });
            return result;
        }

    </script>
    </@head>
<body fit="true">
<div style="margin:10px 0;"></div>
<form id="post" name="post" action="${path}/blog/admin/post/doAddOrUpdate" method="post" accept="text/html">
    <#if postExist>
        <input type="hidden" name="id" value="${post.id!}">
        <input type="hidden" name="author" value="<#if user??>${user.id!}</#if>">
        <input type="hidden" name="userId" value="<#if user??>${user.id!}</#if>">
    </#if>
    <input type="hidden" name="type" value="post">
    <input type="hidden" name="name" value="">
    <input type="hidden" name="excerpt" value="">
    <input type="hidden" name="toPing" value="">

    <div class="easyui-resizable" data-options="minWidth:50,minHeight:50"
         style="width:820px;height:870px;border:1px solid #ccc;margin-left: 10px;">
        <div style="padding: 8px;"><span style="font-family: 宋体;font-weight: bold;font-size: 16px;">${title!}</span>
            <a href="javascript:void(0);" class="easyui-linkbutton"
               onclick="window.location.href='${path}/blog/admin/post';return false;">返回文章列表</a>
        </div>
        <a href="javascript:void(0);" class="easyui-linkbutton" onclick=";return false;">存为草稿</a>&nbsp;
        <input type="submit" id="publish" name="publish" value="立即发表">&nbsp;
        <a href="javascript:void(0);" class="easyui-linkbutton" onclick=";return false;">预览</a>
        <label>状态:</label>&nbsp;<select id="status" class="" name="status"><#-- 状态:draft,草稿,publish,发布 -->
        <#if postExist>
            <option value="publish">已发布</option></#if>
        <option value="pending">等待复核</option>
        <option value="draft">草稿</option>
        <#if postExist>
            <option value="trash">回收站</option></#if>
    </select>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#status').val('<#if postExist>${post.status}</#if>');
            });
        </script>
        &nbsp;<label>发布时间:</label>&nbsp;<input type="text" id="postTime" name="date" style="width: 180px;"
                                                    value="<#if postExist && (post.date)??>${(post.date)?string("yyyy-MM-dd HH:mm:ss")}<#else >${.now?string("yyyy-MM-dd HH:mm:ss")}</#if>"
                                                    onclick="WdatePicker({isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'});"
                                                    class="Wdate">
        <hr>
        <div style="float: left;">

            <table>
                <tr>
                    <td><span>标题(必填)</span></td>
                </tr>
                <tr>
                    <td><label
                            style="position: absolute;color: #333;vertical-align:middle;cursor:text;font-size: 16px;display: none;"
                            id="title-prompt-text" for="title">在此键入标题</label>
                        <input type="text" id="title" name="title" style="width: 500px;height: 30px;"
                               class="easyui-validatebox" value="<#if postExist>${post.title!}</#if>"
                               data-options="required:true,validType:'length[1,250]'"
                               placeholder="请在此处输入文章标题">
                    <#-- <label>存放于:</label<select name="category" id="category">
                        <#if (termList?size gt 0)!false >
                            <#list termList as list>
                                <option value="${list.taxonomyId!}">${list.name!}</option>
                            </#list>
                        </#if>
                    </select>-->
                    </td>
                </tr>
                <tr>
                    <td><span>分类管理:</span>
                        <a href="javascript:void(0);" class="easyui-linkbutton" id="addcategoryButton"
                           onclick=";return false;">添加</a>

                        <div id="category"></div>
                    </td>
                </tr>
                <tr>
                    <td><span>内容</span></td>
                </tr>
                <tr>
                    <td><textarea name="content"
                                  style="width:800px;height:500px;visibility:hidden;" class="easyui-validatebox"
                                  data-options="required:true"><#if postExist>${post.content!}</#if>
                    </textarea></td>
                </tr>
                <tr>
                    <td><label>形式:</label>
                        <input type="radio" id="post-format-0" name="formats" value="0" checked="checked"><label
                                for="post-format-0">标准</label>
                        <input type="radio" id="post-format-aside" name="formats" value="aside"><label
                                for="post-format-aside">日志</label>
                        <input type="radio" id="post-format-link" name="formats" value="link"><label
                                for="post-format-link">链接</label>
                        <input type="radio" id="post-format-gallery" name="formats" value="gallery"><label
                                for="post-format-gallery">相册</label>
                        <input type="radio" id="post-format-status" name="formats" value="status"><label
                                for="post-format-status">状态</label>
                        <input type="radio" id="post-format-quote" name="formats" value="quote"><label
                                for="post-format-quote">引语</label>
                        <input type="radio" id="post-format-image" name="formats" value="image"><label
                                for="post-format-image">图像</label>
                    </td>
                </tr>
                <tr>
                    <td><span>标签:</span><input type="text" name="newtag[post_tag]" id="new-tag-post_tag">
                        <a href="javascript:void(0);" class="easyui-linkbutton"
                           onclick="addTag();return false;">添加</a><span style="margin-left: 5px;">多个标签请用英文逗号(,)分开</span>

                        <div id="tag"></div>

                    </td>
                </tr>
                <tr>
                    <td><span>隐私设置:</span>
                        <input type="radio" id="visibility-radio-public" name="visibility" value="public"
                               checked="checked"><label
                                for="visibility-radio-public">公开</label>
                        <sub style="font-size: 12px;"><input id="sticky" name="sticky"
                                    type="checkbox" value="sticky"
                                    tabindex="4"><label for="sticky"
                                                        class="selectit">将文章置于首页顶端</label></sub>
                        <input type="radio" id="visibility-radio-password" name="visibility" value="password"><label
                                for="visibility-radio-password">密码保护</label>
                        <sub style="font-size: 12px;"><span>密码:</span><input type="text" value="<#if postExist>${post.password!}</#if>"
                                                    id="password"
                                                    name="password"></sub>
                        <input type="radio" id="visibility-radio-private" name="visibility" value="private"><label
                                for="visibility-radio-private">私密</label></td>
                </tr>
                <tr>
                    <td><span>评论设置:</span><input type="radio" name="commentStatus" value="open" id="commentStatus_open"
                                                 <#if !postExist || (post.commentStatus=="open")>checked="checked"</#if>><label
                            for="commentStatus_open">开放</label><input type="radio" name="commentStatus" value="close"
                                                                      id="commentStatus_close"
                                                                      <#if postExist && (post.commentStatus=="close")>checked="checked"</#if>><label
                            for="commentStatus_close">关闭</label></td>
                </tr>
                <tr>
                    <td><span>ping设置:</span><input type="radio" name="pingStatus" value="open" id="pingStatus_open"
                                                   <#if !postExist||( post.pingStatus=="open")>checked="checked"</#if>><label
                            for="pingStatus_open">开放</label><input type="radio" name="pingStatus" value="close"
                                                                   id="pingStatus_close"
                                                                   <#if postExist && (post.pingStatus=="close")>checked="checked"</#if>><label
                            for="pingStatus_close">关闭</label></td>
                </tr>
                <tr>
                    <td><span>特色图像:</span><a href="">设置特色图像</a></td>
                </tr>
            </table>
        </div>
    </div>
</form>
<#--
<div id="submitdiv" class="easyui-draggable easyui-resizable" data-options="handle:'#submitTitle'"
     style="width:300px;height:250px;border:1px solid #ccc;left: 830px;top:10px;position: absolute;">
    <div id="submitTitle" style="background:#ddd;padding:5px;">发布</div>
    <div style="padding:20px">
        <a href="javascript:void(0);" class="easyui-linkbutton">保存草稿</a> <a href="javascript:void(0);"
                                                                            class="easyui-linkbutton"
                                                                            style="margin-left: 10px;">预览</a>
    </div>
</div>

<div id="categroydiv" class="easyui-draggable easyui-resizable" data-options="handle:'#categorytitle'"
     style="width:300px;height:250px;border:1px solid #ccc;left: 830px;top:270px;position: absolute;">
    <div id="categorytitle" style="background:#ddd;padding:5px;">添加分类</div>
    <div style="padding:20px">Drag and Resize Me</div>
</div>

<div id="stylediv" class="easyui-draggable easyui-resizable" data-options="handle:'#styletitle'"
     style="width:300px;height:250px;border:1px solid #ccc;left:1140px;top:10px;position: absolute;">
    <div id="styletitle" style="background:#ddd;padding:5px;">形式</div>
    <div style="padding:20px">Drag and Resize Me</div>
</div>

<div id="tagdiv" class="easyui-draggable easyui-resizable" data-options="handle:'#tagtitle'"
     style="width:300px;height:250px;border:1px solid #ccc;left: 1140px;top:270px;position: absolute;">
    <div id="tagtitle" style="background:#ddd;padding:5px;">标签</div>
    <div style="padding:20px">Drag and Resize Me</div>
</div>

<div id="postimagediv" class="easyui-draggable easyui-resizable" data-options="handle:'#postimagetitle'"
     style="width:300px;height:100px;border:1px solid #ccc;left: 830px;top:530px;position: absolute;">
    <div id="postimagetitle" style="background:#ddd;padding:5px;">特色图像</div>
    <div style="padding:20px">Drag and Resize Me</div>
</div>
-->

<!--添加分类窗口-->
<div id="addcategorydiv" class="easyui-window" title="添加分类" collapsible="false" minimizable="false"
     maximizable="false" icon="icon-save" style="width: 320px; height: 150px; padding: 5px;
        background: #fafafa;">
    <div class="easyui-layout" fit="true">
        <form id="addcategoryForm" onsubmit="return false;">
            <div region="center" border="false" style="padding: 10px; background: #fff; border: 1px solid #ccc;">
                <table cellpadding=3>
                    <tr>
                        <td>分类名：</td>
                        <td><input id="newcategory" name="newcategory" type="text" class="txt01"/></td>
                    </tr>
                    <tr>
                        <td>父级目录：</td>
                        <td><select id="newcategory_parent" name="newcategory_parent">
                            <option value="">— 父级分类目录 —</option>
                        </select></td>
                    </tr>
                </table>
            </div>
            <div region="south" border="false" style="text-align: right; height: 30px;margin-top: 5px;">
                <a id="btnEp" class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0)"
                   onclick="addCategory();return false;">
                    确定</a> <a id="btnCancel" class="easyui-linkbutton" icon="icon-cancel"
                              href="javascript:void(0)" onclick="closeAddCategoryW();return false;">取消</a>
            </div>
        </form>
    </div>
</div>

</body>
</html>
</@compress>