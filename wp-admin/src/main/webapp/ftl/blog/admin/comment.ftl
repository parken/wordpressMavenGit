<#-- 评论 -->
<#include "../../template/page.ftl">
<@compress >
    <@head title="评论管理">
    <#-- datatables专用 -->
    <script type="text/javascript">
        var oTable;
        $(document).ready(function () {
            oTable = initDataTables("commentTable");
        });

        function initDataTables(tableId) {
            var oTable = $('#' + tableId).dataTable({
                "sDom":'<"H"lfr>t<"F"ip>', //<"H"lfr>t<"F"ip>
                "iDisplayLength":10, //每页显示个数
                "bScrollCollapse":true,
                "bLengthChange":true, //每页显示的记录数
                "bPaginate":true, //是否显示分页
                "bFilter":true, //搜索栏
                "bSort":false, //是否支持排序功能
                "bInfo":true, //显示表格信息
                //  "sScrollY": "200px",//显示滚动条
                "bAutoWidth":false, //自适应宽度
                "bStateSave":false, //保存状态到cookie *************** 很重要 ，当搜索的时候页面一刷新会导致搜索的消失。使用这个属性就可避免了
                "oLanguage":{
                    "sUrl":"${path}/ui/datatables/dtCH.txt" //多语言配置文件，可将oLanguage的设置放在一个txt文件中，例：Javascript/datatable/dtCH.txt
                }, //多语言配置
                "bJQueryUI":true, //可以添加 jqury的ui theme  需要添加css
                "aLengthMenu":[
                    [1, 2, 10, 15, 20],
                    ["1", "2", "10", "15", "20"]
                ],
                //   "bDeferRender":true, //是否使用延迟加载
                "bServerSide":true,
                "sAjaxSource":"${path}/blog/admin/comment/listAjax",
                "fnServerParams":function (aoData) {
                    // aoData.push( { "name": "more_data", "value": "my_value" } );
                },
                "fnServerData":function (sSource, aoData, fnCallback) {
                    var dtjson = $.parseToJson($.param(aoData));
                    var page = Math.floor(parseInt(dtjson.iDisplayStart) / parseInt(dtjson.iDisplayLength)) + 1;
                    var rows = parseInt(dtjson.iDisplayLength);
                    var status = $('#status').val();
                    $.ajax({
                        dataType:'json',
                        type:'POST',
                        cache:false,
                        url:sSource,
                        async:false,
                        data:"dtjson =" + $.toJSON(dtjson) + "&page=" + page + "&rows=" + rows + "&status=" + status,
                        success:function (response) {
                            var pageSize = response.pageSize;
                            var total = response.total;
                            var pageTotal = response.pageTotal;
                            var currentPage = response.currentPage;
                            response.sEcho = parseInt(dtjson.sEcho);
                            response.iTotalRecords = total;
                            response.iTotalDisplayRecords = total;
                            fnCallback(response);
                        }
                    });
                },
                "sAjaxDataProp":"rows",
                "sServerMethod":"POST",
                "bProcessing":true,
                "sPaginationType":/*"two_button",*/"full_numbers",
                "aoColumns":[
                    { "mData":null, "render":function (data, type, full, meta) {
                    	return meta.row + meta.settings._iDisplayStart + 1;
                    }},
                    { "mData":null, "mRender":function (data, type, full) {
                        return '<input type="checkbox" name="ck"/>';
                    }},
                    { "mData":'author', "sTitle":"作者"},
                    { "mData":'content', "sTitle":"评论" },
                    { "mData":"parentPost", "sTitle":"回应给", "mRender":function (data, type, full) {
                        var html = "";
                        if (data != undefined && data != null) {
                            html = '<a href="${path}/blog/admin/post/edit?postId=' + data.id + '">' + data.title + '</a>';
                        }
                        return html;
                    }},
                    { "mData":"id", "sTitle":"操作", "mRender":function (data, type, full) {
                        var id = data;
                        var html = '';
                        var approved = full.approved;
                        if (approved != undefined) {
                            if (approved == '0') {
                                html = html + '<span class="spanLink" onclick="changeComment(\'' + id + '\',\'1\');">批准</span>';
                            } else if (approved == '1') {
                                html = html + '<span class="spanLink" onclick="changeComment(\'' + id + '\',\'0\');">驳回</span>';
                            }
                        }
                        html = html +
                                '&nbsp;<a href="${path}/blog/admin/comment/toAddOrUpdate?commentId=' + id + '">回复</a>' +
                                '&nbsp;<a href="${path}/blog/admin/comment/toAddOrUpdate?commentId=' + id + '">编辑</a>' +
                                '&nbsp;<a href="${path}/blog/admin/comment/toAddOrUpdate?commentId=' + id + '">快速编辑</a><br>' +
                                '&nbsp;<span class="spanLink" style="color: red;" onclick="changeComment(\'' + id + '\',\'spam\');">垃圾评论</span>' +
                                '&nbsp;<span class="spanLink" style="color: red;" onclick="changeComment(\'' + id + '\',\'trash\');">移至回收站</span>' +
                                '&nbsp;<span class="spanLink" style="color: red;" onclick="deleteComment(\'' + id + '\');">删除</span>';
                        return html;
                    }}
                ],
                "fnRowCallback":function (nRow, aData, iDisplayIndex) {
                    //单行选中
                    $(nRow).click(function () {
                        $(oTable.fnSettings().aoData).each(function () {
                            $(this.nTr).removeClass('row_selected');
                        });
                        if ($(this).hasClass('row_selected'))
                            $(this).removeClass('row_selected');
                        else
                            $(this).addClass('row_selected');
                    });
                    return nRow;
                }
            });
            return oTable;
        }

        function selectAll(rootCKid, subCKname) {
            if ($('#' + rootCKid).attr("checked") == 'checked') {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).attr("checked", "checked");
                });
            } else {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).removeAttr("checked");
                });
            }
        }
        /**
         *刷新表格
         */
        function refreshTable() {
            oTable.fnClearTable(0);
            oTable.fnDraw();
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            statistic();
        });
        function deleteComment(commentId) {
            if (commentId == undefined || commentId == '') {
                return;
            }
            if (confirm("确定删除?")) {
                $.ajax({
                    type:"POST",
                    async:false,
                    url:"${path}/blog/admin/comment/delete",
                    data:"commentId=" + commentId,
                    success:function (msg) {
                        refreshTable();
                        //window.location.href = "${path}/blog/admin/term/toCategory";
                        statistic();
                    }
                });
            }
        }
        function changeComment(commentId, approved) {
            if (commentId == undefined || commentId == '') {
                return;
            }
            $.ajax({
                type:"POST",
                async:false,
                url:"${path}/blog/admin/comment/doAddOrUpdate?format=json",
                data:"id=" + commentId + "&approved=" + approved,
                success:function (msg) {
                    refreshTable();
                    //window.location.href = "${path}/blog/admin/term/toCategory";
                    statistic();
                }
            });
        }
        function statistic() {
            $.ajax({
                type:"POST",
                async:false,
                url:"${path}/blog/admin/comment/statisticsAjax",
                data:"",
                dataType:"json",
                success:function (msg) {
                    var data = msg;
                    var spam = msg['spam'];
                    var trash = msg['trash'];
                    var moderated = msg['0'];
                    var approved = msg['1'];
                    /*var moderated = "0";
                    var approved = "1";*/
                    $('.trash-count').html('0');
                    $('.spam-count').html('0');
                    $('.pending-count').html('0');
                    $('.approved-count').html('0');
                    if (trash != undefined && trash != null) {
                        $('.trash-count').html(trash);
                    }
                    if (spam != undefined && spam != null) {
                        $('.spam-count').html(spam);
                    }
                    if (moderated != undefined && moderated != null) {
                        $('.pending-count').html(moderated);
                    }
                    if (approved != undefined && approved != null) {
                        $('.approved-count').html(approved);
                    }

                }
            });
        }

        function setStatus(val){
            $('#status').val(val);
            refreshTable();
        }
    </script>
    </@head>
<body>
<h2>评论管理</h2>
<div>
    <span class="all"><span onclick="setStatus('');" class="spanLink">全部</span> |</span>
    <span class="moderated"><span onclick="setStatus('0');" class="spanLink">待审 <span class="count">(<span
            class="pending-count">0</span>)</span></span> |
    </span>
    <span class="approved"><span onclick="setStatus('1');" class="spanLink">获准(<span
            class="approved-count">0</span>)</span></span> |</span>
    <span class="spam"><span onclick="setStatus('spam');" class="spanLink">垃圾评论 <span class="count">(<span
            class="spam-count">0</span>)</span></span>
        |
    </span>
    <span class="trash"><span onclick="setStatus('trash');" class="spanLink">回收站 <span class="count">(<span
            class="trash-count">0</span>)</span></span></span>
</div>
<div class="tablenav" style="margin: 5px;">
    <div class="alignleft actions" style="float: left;">
        <select name="action">
            <option value="" selected="selected">批量操作</option>
            <option value="unapprove">驳回</option>
            <option value="approve">批准</option>
            <option value="spam">标记为垃圾评论</option>
            <option value="trash">移至回收站</option>
        </select>
        <input type="submit" name="" id="doaction" class="button-secondary action" value="应用">
    </div>
    <div class="alignleft actions" style="float: left;">
        <select name="comment_type">
            <option value="">显示所有评论类型</option>
            <option value="comment">评论</option>
            <option value="pings">Ping 通告</option>
        </select>
        <input type="submit" name="" id="post-query-submit" class="button-secondary" value="筛选"></div>
    <br class="clear">
</div>

<div>
    <input type="hidden" id="status" value="">
    <table id="commentTable" cellpadding="0" cellspacing="0" border="0" class="display">
    </table>
</div>
</body>
</html>
</@compress>