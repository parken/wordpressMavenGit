<#include "../../template/page.ftl">
<@compress >
    <#if comment??>
        <#assign title="评论修改"/>
        <#assign commentExist=true/>
    <#else >
        <#assign title="评论添加"/>
        <#assign commentExist=false/>
    </#if>
    <@head title=title>
    <script type="text/javascript">
        var editor;
        KindEditor.ready(function (K) {
            editor = K.create('textarea[name="content"]', {
                allowFileManager:true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            <#if commentExist>
                $('input[type="radio"][name="status"][value="' + '${comment.approved!}' + '"]').attr("checked", "checked");
            </#if>
        });
    </script>
    </@head>
<body>
<div>
    <form name="post" action="${path}/blog/admin/comment/doAddOrUpdate" method="post" id="post">
        <#if commentExist>
            <input type="hidden" name="id" value="${comment.id!}">
            <input type="hidden" name="postId" value="${comment.postId!}">
            <input type="hidden" name="userId" value="${comment.userId!}">
            <input type="hidden" name="action" value="editedcomment">

        </#if>

        <div class="wrap">
            <h2 style="margin: 5px 0 5px 0;">编辑评论</h2>

                <div id="post-body" >
                    <div id="post-body-content" style="float: left;width: 680px;margin-right: 10px;">
                        <div id="namediv" class="easyui-panel" title="作者"
                             data-options="iconCls:'',collapsible:true,minimizable:false,maximizable:false,closable:false">
                            <div class="inside">
                                <table style="width: 100%;">
                                    <tbody>
                                    <tr valign="top">
                                        <td class="first" style="width: 20%;">名称：</td>
                                        <td><input type="text" name="author" size="30" style="width: 80%"
                                                   value="<#if commentExist>${comment.author!}</#if>"
                                                   tabindex="1" id="name"></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="first">
                                            电子邮件：
                                        </td>
                                        <td><input type="text" name="authorEmail" size="30" style="width: 80%"
                                                   value="<#if commentExist>${comment.authorEmail!}</#if>"
                                                   tabindex="2" id="email"></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="first">
                                            URL(<a href="<#if commentExist>${comment.authorUrl!}</#if>"
                                                   rel="external nofollow" target="_blank">访问站点</a>)：
                                        </td>
                                        <td><input type="text" id="authorUrl" name="newcomment_author_url" style="width: 80%;"
                                                   size="30" class="code"
                                                   value="<#if commentExist>${comment.authorUrl!}</#if>" tabindex="3">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                        <div id="postdiv" style="margin: 10px 0 10px 0;">
                            <textarea name="content" id="content"
                                      rows="15" cols="40" style="width:100%;visibility:hidden;"
                                      class="easyui-validatebox"
                                      data-options="required:true"><#if commentExist>${comment.content!}</#if>
                            </textarea>
                        </div>
                    </div>
                    <!-- /post-body-content -->

                    <div id="postbox-container-1" class="easyui-panel" title="状态" style="float: left;width: 300px;"
                         data-options="iconCls:'',collapsible:true,minimizable:false,maximizable:false,closable:false">

                        <div id="look-comment" style="margin: 5px 0px 5px 5px;">
                            <a class="easyui-linkbutton"
                               href="${path}/?p=1#comment-1"
                               target="_blank">查看评论</a>
                        </div>

                        <div id="comment-status-radio" style="margin: 5px 0 5px 5px;">
                            <label class="approved"><input type="radio"
                                                           name="status"
                                                           value="1">已审核</label>
                            <label class="waiting"><input type="radio" name="status"
                                                          value="0">待审</label>
                            <label class="spam"><input type="radio" name="status"
                                                       value="spam">垃圾评论</label>
                            <label class="spam"><input type="radio" name="status"
                                                       value="trash">移至回收站</label>
                        </div>
                        <div class="misc-pub-section curtime" style="margin: 5px 0 5px 5px;">
                        <span id="timestamp">提交于：<input type="text" id="" name="date"
                                                        style="width: 250px;"
                                                        value="<#if commentExist &&(comment.date??)>${comment.date?string("yyyy-MM-dd HH:mm:ss")}</#if>"
                                                        onclick="WdatePicker({isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'});"
                                                        class="Wdate"></span>
                        </div>

                        <div id="publishing-action" style="margin: 5px 0 5px 5px;">
                            <input type="submit" name="save" id="save" class="button-primary" value="更新"
                                   tabindex="4">
                        </div>

                    </div>
                    <!-- /submitdiv -->

                </div>
                <!-- /post-body -->
            </div>
    </form>
</div>
</body>
</html>
</@compress>