<#include "../../template/page.ftl">
<@compress >
    <#if link??>
        <#assign title="链接修改"/>
        <#assign linkExist=true/>
    <#else >
        <#assign title="链接添加"/>
        <#assign linkExist=false/>
    </#if>
    <@head title=title>
    <script type="text/javascript">
        $(document).ready(function () {
            loadLinkCategory();
            $('#me').click(function () {
                if (this.checked) {
                    $('#link_rel').val("me");
                    $('.valinp').each(function () {
                        $(this).attr("disabled", "disabled");
                    });
                } else {
                    $('.valinp').each(function () {
                        $(this).removeAttr("disabled");
                    });
                    choice();
                }
            });
            $('.valinp').click(function () {
                choice();
            });
            <!-- 表单回填 -->
            <#if linkExist>
                <#list link.termList as term>
                    <#if (term.taxonomy==TAXONOMY_LINKA_CATEGORY)!false>
                        $("input[id^='in-link-category-'][type='checkbox'][value='${term.taxonomyId!}']").attr('checked','checked');
                    </#if>
                </#list>
                <#assign relArr=link.rel?split(" ")/>
                <#list relArr as obj>
                $("input[value='${obj!}']").attr("checked","checked");
                </#list>
            </#if>
        });
        function choice() {
            var rel = "";
            $('.valinp').each(function () {
                if (this.checked) {
                    if (rel == "") {
                        rel = rel + this.value;
                    } else {
                        rel = rel + " " + this.value;
                    }
                }
            });
            $('#link_rel').val(rel);
        }

        function loadLinkCategory() {
            try {
                var data = loadTermAjax(LINKA_CATEGORY);
                var termList = data;
                if (termList != null && termList.length > 0) {
                    var html = '';
                    $.each(termList, function (i, n) {
                        html = html + ' <li id="link-category-' + n.taxonomyId + '"><label for="in-link-category-' + n.taxonomyId +
                                '" class="selectit"><input type="checkbox" id="in-link-category-' + n.taxonomyId +
                                '" name="link_category[]" value="' + n.taxonomyId + '">' + n.name + '</lable></li>';
                    });
                    $('#categorychecklist').html(html);
                }
            } catch (e) {
                console.log(e);
            }
        }
        function loadTermAjax(taxonomy) {
            if (taxonomy == undefined || taxonomy == null || taxonomy == '') {
                return;
            }
            var data = '';
            $.ajax({
                type:"POST",
                dataType:'json',
                async:false,
                url:"${path}/blog/admin/term/listAjax",
                data:"taxonomy=" + taxonomy,
                success:function (msg) {
                    data = msg;
                }
            });
            return data;
        }
        /******************************************************************/
        function addLinkCategory() {
            var data = $('#newcat').val();
            if (data.trim() != '') {
                data = "newcat=" + encodeURI(data);
                var result = addTermAjax(data, LINKA_CATEGORY);
                if (result != null) {
                    loadLinkCategory();
                    $('#newcat').val('');
                }

            }
        }
        function addTermAjax(data, taxonomy) {
            if (taxonomy == undefined || taxonomy == null || taxonomy == '') {
                return;
            }
            var result = "";
            var url = "${path}/blog/admin/term/";
            if (LINKA_CATEGORY == taxonomy) {
                url = url + "doAddLinkCategoryAjax.json";
                $.ajax({
                    type:"POST",
                    async:false,
                    url:url,
                    data:data,
                    success:function (msg) {
                        result = msg;
                    }
                });
            }
            return result;
        }
    </script>
    </@head>
<body fit="true">
<div>
<h2>${title!}</h2>

<form name="addlink" id="addlink" method="post" action="${path}/blog/admin/link/doAddOrUpdate">
<label for="link_private" class="selectit">
    <input id="link_private" name="visible"
           type="checkbox" value="N" <#if (link.visible=='N')!false>checked="checked" </#if>> 将这个链接设为私密链接</label>
<button type="submit">${title!}</button><a class="easyui-linkbutton" onclick="window.history.go(-1);return false;">返回</a>
    <#if link??><input type="hidden" id="id" name="id" value="${link.id!}"></#if>
    <#if link??><input type="hidden" id="owner" name="owner" value="${link.owner!}"></#if>
<input type="hidden" id="_wpnonce" name="_wpnonce" value="f127b37d54">
<input type="hidden" name="_wp_http_referer" value="/wordpress/wp-admin/link-add.php">
<input type="hidden" id="closedpostboxesnonce" name="closedpostboxesnonce" value="29141a8e5b">
<input type="hidden" id="meta-box-order-nonce" name="meta-box-order-nonce" value="1ce3bff7c2">

<div id="poststuff">
<div id="post-body" class="metabox-holder columns-2">
<div id="post-body-content">
    <div id="namediv" class="stuffbox">
        <h3><label for="link_name">名称</label></h3>

        <div class="inside">
            <input type="text" name="name" class="easyui-validatebox"
                   data-options="required:true,validType:'length[1,30]'" value="<#if link??>${link.name!}</#if>"
                   id="link_name">

            <p>例如：好用的博客软件</p>
        </div>
    </div>

    <div id="addressdiv" class="stuffbox">
        <h3><label for="link_url">Web 地址</label></h3>

        <div class="inside">
            <input type="text" name="url" class="easyui-validatebox" tabindex="1" value="<#if link??>${link.url!}</#if>"
                   id="link_url"
                   data-options="required:true,validType:'length[1,30]'">

            <p>例子：<code>http://cn.wordpress.org/</code> —— 不要忘了 <code>http://</code></p>
        </div>
    </div>

    <div id="descriptiondiv" class="stuffbox">
        <h3><label for="link_description">描述</label></h3>

        <div class="inside">
            <input type="text" name="description" size="30" tabindex="1" value="<#if link??>${link.description!}</#if>"
                   id="link_description">

            <p>通常，当访客将鼠标光标悬停在链接表链接的上方时，它会显示出来。根据主题的不同，也可能显示在链接下方。</p>
        </div>
    </div>
</div>
<!-- /post-body-content -->

<div id="postbox-container-2" class="postbox-container">
<div id="normal-sortables" class="meta-box-sortables ui-sortable">
<div id="linkcategorydiv" class="postbox ">
    <div class="handlediv" title="点击以切换"><br></div>
    <h3 class="hndle"><span>分类目录</span></h3>

    <div class="inside">
        <div id="taxonomy-linkcategory" class="categorydiv">
            <ul id="category-tabs" class="">
                <li class=""><a href="javascript:void(0);">所有分类目录</a></li>
            </ul>

            <div id="categories-all" class="">
                <ul id="categorychecklist" class="categorychecklist form-no-clear">
                </ul>
            </div>

            <div id="category-adder" class="wp-hidden-children">
                <h4><a id="category-add-toggle" href="#category-add">+ 添加分类目录</a></h4>

                <p id="link-category-add" class="wp-hidden-child">
                    <label class="screen-reader-text" for="newcat">+ 添加分类目录</label>
                    <input type="text" name="newcat" id="newcat" data-options="required:false"
                           placeholder="新分类目录名称" class="easyui-validatebox">
                    <a class="easyui-linkbutton" id="link-category-add-submit" onclick="addLinkCategory();return false;">添加</a>
                    <span id="category-ajax-response"></span>
                </p>
            </div>
        </div>
    </div>
</div>
<div id="linktargetdiv" class="postbox ">
    <div class="handlediv" title="点击以切换"><br></div>
    <h3 class="hndle"><span>打开方式</span></h3>

    <div class="inside">
        <fieldset>
            <legend class="screen-reader-text"><span>打开方式</span></legend>
            <p><label for="link_target_blank" class="selectit">
                <input id="link_target_blank" type="radio" name="target" value="_blank" <#if linkExist&& link.target=='_blank'>checked="checked"</#if>>
                <code>_blank</code> — 新窗口或新标签。</label></p>

            <p><label for="link_target_top" class="selectit">
                <input id="link_target_top" type="radio" name="target" value="_top" <#if linkExist&& link.target=='_top'>checked="checked"</#if>>
                <code>_top</code> — 不包含框架的当前窗口或标签。</label></p>

            <p><label for="link_target_none" class="selectit">
                <input id="link_target_none" type="radio" name="target" value="" <#if linkExist&& link.target==''>checked="checked"</#if>>
                <code>_none</code> — 同一窗口或标签。</label></p>
        </fieldset>
        <p>为您的链接选择目标框架。</p>
    </div>
</div>
<div id="linkxfndiv" class="postbox ">
    <div class="handlediv" title="点击以切换"><br></div>
    <h3 class="hndle"><span>链接关系网（XFN）</span></h3>

    <div class="inside">
        <table class="links-table" cellspacing="0">
            <tbody>
            <tr>
                <th scope="row"><label for="link_rel">关系（rel）：</label></th>
                <td><input type="text" name="rel" id="link_rel" value="<#if linkExist>${link.rel!}</#if>" readonly="" style="width: 350px;"></td>
            </tr>
            <tr>
                <th scope="row">同一个人</th>
                <td>
                    <fieldset>
                        <legend class="screen-reader-text"><span>同一个人</span></legend>
                        <label for="me">
                            <input type="checkbox" name="identity" value="me" id="me">
                            我的另一个 web 地址</label>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row">友情</th>
                <td>
                    <fieldset>
                        <legend class="screen-reader-text"><span>友情</span></legend>
                        <label for="contact">
                            <input class="valinp" type="radio" name="friendship" value="contact" id="contact">&nbsp;偶有联系
                        </label>
                        <label for="acquaintance">
                            <input class="valinp" type="radio" name="friendship" value="acquaintance"
                                   id="acquaintance">&nbsp;熟人 </label>
                        <label for="friend">
                            <input class="valinp" type="radio" name="friendship" value="friend" id="friend">&nbsp;朋友
                        </label>
                        <label for="friendship">
                            <input name="friendship" type="radio" class="valinp" value="" id="friendship"
                                   checked="checked">&nbsp;无 </label>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"> 网下接触</th>
                <td>
                    <fieldset>
                        <legend class="screen-reader-text"><span>网下接触</span></legend>
                        <label for="met">
                            <input class="valinp" type="checkbox" name="physical" value="met" id="met">&nbsp;已见过面
                        </label>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"> 职场关系</th>
                <td>
                    <fieldset>
                        <legend class="screen-reader-text"><span>职场关系</span></legend>
                        <label for="co-worker">
                            <input class="valinp" type="checkbox" name="professional" value="co-worker"
                                   id="co-worker">&nbsp;同事 </label>
                        <label for="colleague">
                            <input class="valinp" type="checkbox" name="professional" value="colleague"
                                   id="colleague">&nbsp;同行 </label>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row">地理关系</th>
                <td>
                    <fieldset>
                        <legend class="screen-reader-text"><span> 地理关系 </span></legend>
                        <label for="co-resident">
                            <input class="valinp" type="radio" name="geographical" value="co-resident"
                                   id="co-resident">&nbsp;同住 </label>
                        <label for="neighbor">
                            <input class="valinp" type="radio" name="geographical" value="neighbor"
                                   id="neighbor">&nbsp;邻居 </label>
                        <label for="geographical">
                            <input class="valinp" type="radio" name="geographical" value="" id="geographical"
                                   checked="checked">&nbsp;无 </label>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row">家庭关系</th>
                <td>
                    <fieldset>
                        <legend class="screen-reader-text"><span> 家庭关系 </span></legend>
                        <label for="child">
                            <input class="valinp" type="radio" name="family" value="child" id="child">&nbsp;子女
                        </label>
                        <label for="kin">
                            <input class="valinp" type="radio" name="family" value="kin" id="kin">&nbsp;亲戚
                        </label>
                        <label for="parent">
                            <input class="valinp" type="radio" name="family" value="parent" id="parent">&nbsp;父母
                        </label>
                        <label for="sibling">
                            <input class="valinp" type="radio" name="family" value="sibling" id="sibling">&nbsp;兄弟姐妹
                        </label>
                        <label for="spouse">
                            <input class="valinp" type="radio" name="family" value="spouse" id="spouse">&nbsp;配偶
                        </label>
                        <label for="family">
                            <input class="valinp" type="radio" name="family" value="" id="family"
                                   checked="checked">&nbsp;无 </label>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row">情感关系</th>
                <td>
                    <fieldset>
                        <legend class="screen-reader-text"><span> 情感关系 </span></legend>
                        <label for="muse">
                            <input class="valinp" type="checkbox" name="romantic" value="muse" id="muse">&nbsp;灵感女神
                        </label>
                        <label for="crush">
                            <input class="valinp" type="checkbox" name="romantic" value="crush" id="crush">&nbsp;迷恋
                        </label>
                        <label for="date">
                            <input class="valinp" type="checkbox" name="romantic" value="date" id="date">&nbsp;交往中
                        </label>
                        <label for="romantic">
                            <input class="valinp" type="checkbox" name="romantic" value="sweetheart"
                                   id="romantic">&nbsp;恋人 </label>
                    </fieldset>
                </td>
            </tr>

            </tbody>
        </table>
        <p>如果链接指向某个人，您可以用上面的表单指定您与此人的关系。如果您想了解更多，请查看：<a href="http://gmpg.org/xfn/">XFN</a>。</p>
    </div>
</div>
<div id="linkadvanceddiv" class="postbox ">
    <div class="handlediv" title="点击以切换"><br></div>
    <h3 class="hndle"><span>高级</span></h3>

    <div class="inside">
        <table class="links-table" cellpadding="0">
            <tbody>
            <tr>
                <th scope="row"><label for="link_image">图像地址</label></th>
                <td><input type="text" name="image" class="code" id="link_image"
                           value="<#if link??>${link.image!}</#if>"></td>
            </tr>
            <tr>
                <th scope="row"><label for="rss_uri">RSS 地址</label></th>
                <td><input name="rss" class="code" type="text" id="rss_uri" value="<#if link??>${link.rss!}</#if>"></td>
            </tr>
            <tr>
                <th scope="row"><label for="link_notes">备注</label></th>
                <td><textarea name="notes" id="link_notes" rows="10"><#if link??>${link.notes!}</#if></textarea></td>
            </tr>
            <tr>
                <th scope="row"><label for="link_rating">评分</label></th>
                <td><select name="rating" id="link_rating" size="1">
                    <#list 0..10 as i>
                        <option value="${i}" <#if (link.rating==i)!false>selected="selected"</#if>>${i}</option>
                    </#list>
                </select>&nbsp;（0 代表不评分。）
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
<div id="advanced-sortables" class="meta-box-sortables ui-sortable"></div>
</div>
<input type="hidden" name="action" value="add">
</div>
</div>
</form>
</div>
</body>
</html>
</@compress>