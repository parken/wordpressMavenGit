<#include "../../template/page.ftl">
<@compress >
    <#if taxonomy??>
        <#if taxonomy==TAXONOMY_POST_CATEGORY>
            <#assign title="分类目录"/>
        <#elseif taxonomy==TAXONOMY_POST_TAG>
            <#assign title="标签"/>
        <#elseif taxonomy==TAXONOMY_LINKA_CATEGORY>
            <#assign title="链接分类目录"/>
        </#if>
    <#else >
        <#assign title="术语"/>

    </#if>
    <@head title=title>
    <#-- datatables专用 -->
    <script type="text/javascript">
        var oTable;
        $(document).ready(function () {
            oTable = initDataTables();
            $('#delete').click(function () {
                var anSelected = fnGetSelected(oTable);
                if (anSelected.length !== 0) {
                    oTable.fnDeleteRow(anSelected[0]);
                }
            });
        });

        function initDataTables() {
            var tableId = "term";
            var oTable = $('#' + tableId).dataTable({
                //  "sDom": '<"dt_top">rt<"dt_bottom"ilp><"clear">',
                "iDisplayLength":10, //每页显示个数
                "bScrollCollapse":true,
                "bLengthChange":true, //每页显示的记录数
                "bPaginate":true, //是否显示分页
                "bFilter":true, //搜索栏
                "bSort":false, //是否支持排序功能
                "bInfo":true, //显示表格信息
                "bAutoWidth":false, //自适应宽度
                "bStateSave":true, //保存状态到cookie *************** 很重要 ，当搜索的时候页面一刷新会导致搜索的消失。使用这个属性就可避免了
                "oLanguage":{
                    "sUrl":"${path}/ui/datatables/dtCH.txt" //多语言配置文件，可将oLanguage的设置放在一个txt文件中，例：Javascript/datatable/dtCH.txt
                }, //多语言配置
                "bJQueryUI":true, //可以添加 jqury的ui theme  需要添加css
                "aLengthMenu":[
                    [1, 2, 10, 15, 20],
                    ["1", "2", "10", "15", "20"]
                ],
                //   "bDeferRender":true, //是否使用延迟加载
                "bServerSide":true,
                "sAjaxSource":"${path}/blog/admin/term/pageAjax",
                "fnServerParams":function (aoData) {
                    // aoData.push( { "name": "more_data", "value": "my_value" } );
                },
                "fnServerData":function (sSource, aoData, fnCallback) {
                    //aoData.push( { "name": "customerName", "value":"ooisd" } );
                    $.ajax({
                        dataType:'json',
                        type:'POST',
                        cache:false,
                        url:sSource,
                        async:false,
                        data:$.param({dtjson:$.toJSON($.parseToJson($.param(aoData)))}) + "&taxonomy=${taxonomy!}",
                        success:function (response) {
                            fnCallback(response);
                        }
                    });
                },
                // "sAjaxDataProp":"aaData",
                "sServerMethod":"POST",
                "bProcessing":true,
                "sPaginationType":/*"two_button",*/"full_numbers",
                "aoColumnDefs":[
                    { "mData":null, "render":function (data, type, full, meta) {
                    	return meta.row + meta.settings._iDisplayStart + 1;
                    }, "aTargets":[0]},
                    { "mData":null, "mRender":function (data, type, full) {
                        return '<input type="checkbox" name="ck">';
                    }, "aTargets":[1]},
                    { "mData":'name', "aTargets":[2]},
                    { "mData":'description', "aTargets":[3] },
                    { "mData":'slug',
                        "mRender":function (data, type, full) {
                            var slug = data;
                            if (slug != undefined) {
                                slug = decodeURI(slug);
                            }
                            return slug;
                        }, "aTargets":[4]
                    },
                    { "mData":'count', "aTargets":[5] },
                    { "mData":'id',
                        "mRender":function (data, type, full) {
                            var id = data;
                            var html = '<a href="${path}/blog/admin/term/toEdit?termId=' + id + '">修改</a>';
                            if (id != 1 && id != 2) {
                                html = html + '&nbsp;<span class="spanLink" onclick="deleteTerm(\'' + full.taxonomyId + '\');">删除</span>';
                            }
                            return html;
                        },
                        "bSortable":false, "aTargets":[6]
                    }
                ],
                "fnRowCallback":function (nRow, aData, iDisplayIndex) {
                    //单行选中
                    // $('#example tr').click( function() {
                    $(nRow).click(function () {
                        $(oTable.fnSettings().aoData).each(function () {
                            $(this.nTr).removeClass('row_selected');
                        });
                        if ($(this).hasClass('row_selected'))
                            $(this).removeClass('row_selected');
                        else
                            $(this).addClass('row_selected');
                    });
                    return nRow;
                },
                "fnDrawCallback":function (oSettings) {
                    //显示索引
                    for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                        this.fnUpdate(i + 1, oSettings.aiDisplay[i], 0, false, false);
                    }
                }
            });
            return oTable;
        }

        function selectAll(rootCKid, subCKname) {
            if ($('#' + rootCKid).attr("checked") == 'checked') {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).attr("checked", "checked");
                });
            } else {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).removeAttr("checked");
                });
            }
        }


        /**
         *刷新表格
         */
        function refreshTable() {
            oTable.fnClearTable(0);
            oTable.fnDraw();
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            // loadTerm();
        });
        /**
         * 加载父标签
         */
        function loadTerm() {
            var data = loadTermAjax('${taxonomy!}');
            try {
                var termList = data;
                if (termList != null && termList.length > 0) {
                    var html = '<option value="">无</option>';
                    $.each(termList, function (i, n) {
                        html = html + '<option value="' + n.taxonomyId + '">' + n.name + '</option>';
                    });
                    $('#parent').html(html);
                }
            } catch (e) {
                console.log(e);
            }
        }

        function loadTermAjax(taxonomy) {
            if (taxonomy == undefined || taxonomy == null || taxonomy == '') {
                return;
            }
            var data = '';
            $.ajax({
                type:"POST",
                dataType:'json',
                async:false,
                url:"${path}/blog/admin/term/listAjax",
                data:"taxonomy=" + taxonomy,
                success:function (msg) {
                    data = msg;
                }
            });
            return data;
        }

        function addTerm() {
            var data = $('#addtag').serialize();
            if (!$('#addtag').form('validate')) {
                return;
            }
            $.ajax({
                type:"POST",
                dataType:'json',
                async:false,
                url:"${path}/blog/admin/term/doAddOrUpdateAjax",
                data:data,
                success:function (msg) {
                    // data = msg;
                    document.getElementById("addtag").reset();
                    refreshTable();
                    //window.location.href = "${path}/blog/admin/term/toCategory";
                }
            });
        }

        function deleteTerm(taxonomyId) {
            if (taxonomyId == undefined || taxonomyId == '') {
                return;
            }
            if (confirm("确定删除?")) {
                $.ajax({
                    type:"POST",
                    async:false,
                    url:"${path}/blog/admin/term/delete",
                    data:"taxonomyId=" + taxonomyId,
                    success:function (msg) {
                        refreshTable();
                        //window.location.href = "${path}/blog/admin/term/toCategory";
                    }
                });
            }
        }
    </script>
    </@head>
<body fit="true">
<div>
    <h2><@spring.message (taxonomy!)+".head"/></h2>

    <div>
        <form id="addtag" action="" method="post">
            <input type="hidden" name="action" value="add-tag"/>
            <input type="hidden" name="screen" value="edit-category"/>
            <input type="hidden" name="taxonomy" value="${taxonomy!}"/>
            <input type="hidden" name="post_type" value="post"/>
            <input type="hidden" id="_wpnonce_add-tag" name="_wpnonce_add-tag" value="2b3ab6de87"/>

            <div class="form-field form-required">
                <label for="tag-name">名称:</label>
                <input name="name" id="tag-name" type="text" value="" size="40" class="easyui-validatebox"
                       data-options="required:true,validType:'length[1,250]'"/>
                <span>这将是它在站点上显示的名字。</span>
            </div>
            <div class="form-field">
                <label for="tag-slug">别名:</label>
                <input name="slug" id="tag-slug" type="text" value="" size="40" class="easyui-validatebox"/>
                <span>“别名”是在 URL 中使用的别称，它可以令 URL 更美观。通常使用小写，只能包含字母，数字和连字符（-）。</span>
            </div>
            <div class="form-field" <#if taxonomy?? && (TAXONOMY_POST_CATEGORY!=taxonomy)>style="display: none;" </#if>>
                <label for="parent">父级:</label>
                <select name='parent' id='parent' class='postform'>
                    <option value="">无</option>
                    <#if parentTermList?? && (parentTermList?size>0)>
                        <#list parentTermList as term>
                            <option value="${term.taxonomyId!}">${term.name!}</option>
                        </#list>
                    </#if>
                </select>
                <span>分类目录和标签不同，它可以有层级关系。您可以有一个“音乐”分类目录，在这个目录下可以有叫做“流行”和“古典”的子目录。</span>
            </div>
            <div class="form-field">
                <label for="tag-description">描述:</label>
                <textarea name="description" id="tag-description" rows="5" cols="40"></textarea>
                <span>描述只会在一部分主题中显示。</span>
            </div>

            <p class="submit"><input type="button" name="submit" id="submit" class="button" onclick="addTerm();"
                                     value="<@spring.message (taxonomy!)+".head"/>"/></p>
        </form>
    </div>
    <div style="margin:5px 0 50px 0;">
        <span id=​"delete" class="spanLink" style="display:none;">​Delete selected row​</span>​
        <table id="term" cellpadding="0" cellspacing="0" border="0" class="display" style="">
            <thead>
            <tr align="left">
                <th width="10px;">&nbsp;</th>
                <th style="width: 10px;"><input type="checkbox" id="rootCK" onclick="selectAll('rootCK','ck')"></th>
                <th>名称</th>
                <th>描述</th>
                <th>别名</th>
                <th>文章</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr align="left">
                <th width="3%">&nbsp;</th>
                <th>&nbsp;</th>
                <th>名称</th>
                <th>描述</th>
                <th>别名</th>
                <th>文章</th>
                <th>操作</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <@spring.message (taxonomy!)+".bottom.tip"/>
</div>
</body>
</html>
</@compress>