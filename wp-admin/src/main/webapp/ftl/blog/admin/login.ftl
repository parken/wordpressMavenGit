<#ftl strip_whitespace=true>
<#import "org/springframework/web/servlet/view/freemarker/spring.ftl" as spring>
<#include "/template/common/constants.ftl">
<#include "/template/common/function.ftl">
<#assign path=request.contextPath/>
<#assign title="管理后台"/>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="${path}/favicon.ico">
<title>${title}</title>
<!-- Bootstrap core CSS -->
<link type="text/css" rel="stylesheet"
	href="${path}/ui/bootstrap/css/bootstrap.min.css" />
<script type="text/javascript"
            src="${path}/ui/jquery/jquery-1.11.3.js"></script>		
<script type="text/javascript"
            src="${path}/ui/bootstrap/js/bootstrap.min.js"></script>
<!-- Custom styles for this template -->
<style type="text/css">
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}

.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="text"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
</style>
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="${path}/ui/bootstrap/bugfix/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
   <script type="text/javascript">
        // 登录页面若在框架内，则跳出框架
        if (self != top) {
            top.location = self.location;
        }
    </script>    
</head>
<body>
	<div class="container">
		<form class="form-signin" id="loginForm" name="loginForm" action="${path}/blog/login/doLogin" method="post">
			<h2 class="form-signin-heading">
				<a href="" title="基于 Bootstrap" target="_blank">${title}</a>
			</h2>
			<label for="inputName" class="sr-only">用户名</label> <input name="loginName"
				type="text" id="inputName" class="form-control"
				placeholder="用户名" required autofocus><span style="color: red;"><@errors path="loginForm.loginName" /></span>
			<br>
			<label
				for="inputPassword" class="sr-only">密码</label> <input
				type="password" id="inputPassword" class="form-control" name="password"
				placeholder="密码" required><span style="color: red;"><@errors path="loginForm.password" /></span>
			<div class="checkbox">
				<label><input name="rememberme" type="checkbox" id="rememberme" value="forever" tabindex="90">
					记住我
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit" id="submit" tabindex="100">登陆</button>
			<input type="hidden" name="redirect_to" value="http://localhost/wordpress/wp-admin/">
            <input type="hidden" name="easyui" value="1">
		</form>
		<p id="nav" class="form-signin">
        <a href="http://localhost/wordpress/wp-login.php?action=lostpassword"
           title="找回密码">忘记密码？</a>&nbsp;&nbsp;<a href="${path}/" title="返回">←
       		 回到博客首页</a>
    	</p>
	</div>
	<!-- /container -->
	
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="${path}/ui/bootstrap/bugfix/ie10-viewport-bug-workaround.js"></script>
</body>
</html>