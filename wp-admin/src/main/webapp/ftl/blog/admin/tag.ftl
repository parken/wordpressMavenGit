<#include "../../template/page.ftl">
<#compress >
    <@head title="文章管理">
    <script type="text/javascript">
        $(document).ready(function () {
            init();
        });

        function init() {
//datagrid初始化
            $('#dg').datagrid({
                title:'文章列表',
                iconCls:'icon-edit', //图标
                width:'auto',
                height:'auto',
                nowrap:false,
                striped:true,
                border:true,
                collapsible:false, //是否可折叠的
//  fit:true, //自动大小
                url:'${path}/blog/admin/post/list',
//sortName: 'code',
//sortOrder: 'desc',
                columns:[
                    [
                        {field:'title', title:'标题', width:100},
                        {field:'authorName', title:'作者', width:100},
                        {field:'author', title:'作者ID', width:100},
                        {field:'termList', title:'分类目录', width:100, formatter:category},
                        {field:'termList', title:'标签', width:100, formatter:tag},
                        {field:'commentCount', title:'评论审核标志', width:100},
                        {field:'date', title:'作者ID', width:100, formatter:formatDate},
                        {field:'status', title:'状态', width:100}
                    ]
                ],
                remoteSort:false,
                idField:'id',
                singleSelect:false, //是否单选
                pagination:true, //分页控件
                rownumbers:true, //行号
                fitColumns:true,
                frozenColumns:[
                    [
                        {field:'ck', checkbox:true}
                    ]
                ],
                toolbar:[
                    {
                        text:'添加',
                        iconCls:'icon-add',
                        handler:function () {
                            openDialog("add_dialog", "add");
                        }
                    },
                    '-',
                    {
                        text:'修改',
                        iconCls:'icon-edit',
                        handler:function () {
                            openDialog("add_dialog", "edit");
                        }
                    },
                    '-',
                    {
                        text:'删除',
                        iconCls:'icon-remove',
                        handler:function () {
                            delAppInfo();
                        }
                    }
                ]
            });
//设置分页控件
            var p = $('#dg').datagrid('getPager');
            $(p).pagination({
                pageSize:10, //每页显示的记录条数，默认为10
                pageList:[5, 10, 15], //可以设置每页记录条数的列表
                beforePageText:'第', //页数文本框前显示的汉字
                afterPageText:'页    共 {pages} 页',
                displayMsg:'当前显示 {from} - {to} 条记录   共 {total} 条记录'
                /*onBeforeRefresh:function(){
                $(this).pagination('loading');
                alert('before refresh');
                $(this).pagination('loaded');
                }*/
            });
        }
    </script>
    </@head>
<body>
<h2>标签</h2>
<table id="dg" title="文章" class="easyui-datagrid" style="width:700px;height:250px"
<#--  url="${path}/blog/admin/post/list"
toolbar="#toolbar" pagination="true"
rownumbers="true" fitColumns="true" singleSelect="true">--> >
    <thead>
    <tr>
        <th field="title" width="100">标题</th>
        <th field="authorName" width="100">作者</th>
        <th field="author" width="100" hidden="true">作者ID</th>
        <th field="termList" width="100" formatter="category">分类目录</th>
        <th field="termList" width="100" formatter="tag">标签</th>
        <th field="commentCount" width="100">评论审核标志</th>
        <th field="date" width="100" formatter="formatDate">日期</th>
        <th field="status" width="100">状态</th>
    </tr>
    </thead>
</table>
</body>
</html>
</#compress>