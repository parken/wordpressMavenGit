<#include "../../template/page.ftl">
<@compress >
    <@head title="链接管理">
    <script type="text/javascript">
        var queryCondition = '';
    </script>
    <#-- datatables专用 -->
    <script type="text/javascript">
        var oTable;
        $(document).ready(function () {
            oTable = initDataTables("linkTable");
        });

        function initDataTables(tableId) {
            var oTable = $('#' + tableId).dataTable({
                "sDom":'<"H"lfr>t<"F"ip>T', //<"H"lfr>t<"F"ip>
                "iDisplayLength":10, //每页显示个数
                "bScrollCollapse":true,
                "bLengthChange":true, //每页显示的记录数
                "bPaginate":true, //是否显示分页
                "bFilter":true, //搜索栏
                "bSort":false, //是否支持排序功能
                "bInfo":true, //显示表格信息
                //  "sScrollY": "200px",//显示滚动条
                "bAutoWidth":false, //自适应宽度
                "bStateSave":false, //保存状态到cookie *************** 很重要 ，当搜索的时候页面一刷新会导致搜索的消失。使用这个属性就可避免了
                "oLanguage":{
                    "sUrl":"${path}/ui/datatables/dtCH.txt" //多语言配置文件，可将oLanguage的设置放在一个txt文件中，例：Javascript/datatable/dtCH.txt
                }, //多语言配置
                "bJQueryUI":true, //可以添加 jqury的ui theme  需要添加css
                "aLengthMenu":[
                    [1, 2, 10, 15, 20],
                    ["1", "2", "10", "15", "20"]
                ],
                //   "bDeferRender":true, //是否使用延迟加载
                "bServerSide":true,
                "sAjaxSource":"${path}/blog/admin/link/listAjax",
                "fnServerParams":function (aoData) {
                    // aoData.push( { "name": "more_data", "value": "my_value" } );
                },
                "fnServerData":function (sSource, aoData, fnCallback) {
                    var dtjson = $.parseToJson($.param(aoData));
                    var page = Math.floor(parseInt(dtjson.iDisplayStart) / parseInt(dtjson.iDisplayLength)) + 1;
                    var rows = parseInt(dtjson.iDisplayLength);
                    if (queryCondition != '') {
                        queryCondition = '&' + queryCondition;
                    }
                    $.ajax({
                        dataType:'json',
                        type:'POST',
                        cache:false,
                        url:sSource,
                        async:false,
                        data:$.param({dtjson:$.toJSON(dtjson)}) + "&page=" + page + "&rows=" + rows + queryCondition,
                        success:function (response) {
                            var pageSize = response.pageSize;
                            var total = response.total;
                            var pageTotal = response.pageTotal;
                            var currentPage = response.currentPage;
                            response.sEcho = parseInt(dtjson.sEcho);
                            response.iTotalRecords = total;
                            response.iTotalDisplayRecords = total;
                            fnCallback(response);
                        }
                    });
                },
                "sAjaxDataProp":"rows",
                "sServerMethod":"POST",
                "bProcessing":true,
                "sPaginationType":/*"two_button",*/"full_numbers",
                "aoColumns":[
                    { "mData":null, "render":function (data, type, full, meta) {
                    	return meta.row + meta.settings._iDisplayStart + 1;
                    }},
                    { "mData":null, "mRender":function (data, type, full, meta) {
                        return '<input type="checkbox" name="ck"/>';
                    }},
                    { "mDataProp":'name'},
                    { "mDataProp":'url' },
                    { "mData":"termList", "mRender":function (data, type, full, meta) {
                        return showTerm(data, '${TAXONOMY_LINKA_CATEGORY}');
                    }},
                    { "mData":null},
                    { "mDataProp":'visible', "mRender":function (visible, type, full, meta) {
                        if (visible == 'Y') {
                            return '是';
                        } else if (visible == 'N') {
                            return '否';
                        }
                        return '';
                    } },
                    { "mDataProp":'rating' },
                    { "mDataProp":'id',
                        "mRender":function (data, type, full, meta) {
                            var id = data;
                            var html = '<a href="${path}/blog/admin/link/toAddOrUpdate?linkId=' + id + '">编辑</a>';
                            html = html + '&nbsp;<span class="spanLink" onclick="deleteLink(\'' + id + '\');">删除</span>';
                            return html;
                        },
                        "bSortable":false
                    }
                ],
                "fnRowCallback":function (nRow, aData, iDisplayIndex) {
                    //单行选中
                    $(nRow).click(function () {
                        $(oTable.fnSettings().aoData).each(function () {
                            $(this.nTr).removeClass('row_selected');
                        });
                        if ($(this).hasClass('row_selected'))
                            $(this).removeClass('row_selected');
                        else
                            $(this).addClass('row_selected');
                    });
                    return nRow;
                },
                "oTableTools":{"sSwfPath":"${path}/ui/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"}
            });
            return oTable;
        }

        function selectAll(rootCKid, subCKname) {
            if ($('#' + rootCKid).attr("checked") == 'checked') {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).attr("checked", "checked");
                });
            } else {
                $('input[type="checkbox"][name="' + subCKname + '"]').each(function () {
                    $(this).removeAttr("checked");
                });
            }
        }
        /**
         *刷新表格
         */
        function refreshTable() {
            oTable.fnClearTable(0);
            oTable.fnDraw();
        }
    </script>
    <script type="text/javascript">
        function deleteLink(linkId) {
            if (linkId == undefined || linkId == '') {
                return;
            }
            if (confirm("确定删除?")) {
                $.ajax({
                    type:"POST",
                    async:false,
                    url:"${path}/blog/admin/link/delete",
                    data:"linkId=" + linkId,
                    success:function (msg) {
                        refreshTable();
                        //window.location.href = "${path}/blog/admin/term/toCategory";
                    }
                });
            }
        }

        /**
         * 筛选
         */
        function queryLink() {
            var linkCategory = $('#cat_id').val();//
            queryCondition = "linkCategory=" + linkCategory;
            refreshTable();
        }
    </script>
    </@head>
<body>
<h2>链接</h2>
<a href="${path}/blog/admin/link/toAddOrUpdate" class="easyui-linkbutton">添加</a>

<div style="margin: 5px 0 15px 0;">

    <div style="float: left;">
        <select name="action">
            <option value="-1" selected="selected">批量操作</option>
            <option value="delete">删除</option>
        </select>
        <a href="javascript:void(0);" id="doaction" class="easyui-linkbutton">应用</a>
    </div>
    <div style="float: left;">
        <select name="cat_id" id="cat_id" class="postform">
            <option value="">查看所有分类目录</option>
            <#if termList??>
                <#list termList as obj>
                    <option value="${obj.taxonomyId!}">${obj.name!}</option>
                </#list>
            </#if>
        </select>
        <a href="javascript:void(0);" id="post-query-submit" class="easyui-linkbutton" onclick="queryLink();return false;">筛选</a></div>
    <br>
</div>
<div>
    <table id="linkTable" cellpadding="0" cellspacing="0" border="0" class="display">
        <thead>
        <tr align="left">
            <th width="10px">&nbsp;</th>
            <th style="width: 10px;"><input type="checkbox" id="rootCK" onclick="selectAll('rootCK','ck')"></th>
            <th>名称</th>
            <th>URl</th>
            <th>分类目录</th>
            <th>关系</th>
            <th>可见性</th>
            <th>评分</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody></tbody>
        <tfoot align="left">
        <th width="10px">&nbsp;</th>
        <th style="width: 10px;">&nbsp;</th>
        <th>名称</th>
        <th>URl</th>
        <th>分类目录</th>
        <th>关系</th>
        <th>可见性</th>
        <th>评分</th>
        <th>操作</th>
        </tfoot>
    </table>
</div>

</body>
</html>
</@compress>