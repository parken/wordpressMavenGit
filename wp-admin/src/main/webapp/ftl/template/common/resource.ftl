<#compress >
<link rel="stylesheet" type="text/css" href="${path}/css/web.css">
<#--easyui 文件 -->
<link rel="stylesheet" type="text/css"
      href="${path}/ui/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
      href="${path}/ui/easyui/themes/icon.css">

<script type="text/javascript"
        src="${path}/ui/jquery/jquery-1.11.3.js"></script>
<script type="text/javascript"
        src="${path}/ui/jquery/jquery.json-2.3.js"></script>

<script type="text/javascript"
        src="${path}/ui/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
        src="${path}/ui/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${path}/ui/common.js"></script>

<#-- datatables -->
<style type="text/css" title="currentStyle">
    <#--  @import "${path}/ui/datatables/media/css/demo_page.css";
  @import "${path}/ui/datatables/media/css/demo_table.css"; -->
    @import "${path}/ui/datatables/media/css/jquery.dataTables.css";
    <#--  @import "${path}/ui/datatables/extras/TableTools/media/css/TableTools_JUI.css";-->
</style>
<#--<link rel="stylesheet" type="text/css" media="screen" href="${path}/ui/datatables/media/css/jquery.dataTables.css"/>-->
<script type="text/javascript" charset="utf-8" src="${path}/ui/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="${path}/ui/datatables/jquery.jeditable.js"></script>
<#-- kindeditor -->
<script charset="utf-8" src="${path}/ui/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="${path}/ui/kindeditor/lang/zh_CN.js"></script>

<#-- My97Datepaicker -->
<script type="text/javascript" src="${path}/ui/My97DatePicker/WdatePicker.js"></script>
<#--个性化文件 -->
<script type="text/javascript" src="${path}/js/blog_admin.js"></script>
</#compress>