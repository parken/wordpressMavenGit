<#-- 常量 -->
<#setting number_format="#">
<#setting date_format="yyyy-MM-dd">
<#setting datetime_format="yyyy-MM-dd">
<#setting time_format="yyyy-MM-dd">
<#assign path=request.contextPath/>
<#assign TAXONOMY_POST_CATEGORY="category"/>
<#assign TAXONOMY_POST_TAG="post_tag"/>
<#assign TAXONOMY_LINKA_CATEGORY="link_category"/>
<#assign TAXONOMY_POST_FORMAT="post_format"/>


<#--post文章状态-->
<#-- 已发布-->

<#assign POST_STATUS_PUBLISH = "publish">
<#assign POST_STATUS_INHERIT = "inherit">
<#assign POST_STATUS_AUTO_DRAFT = "auto-draft">
<#assign POST_STATUS_DRAFT = "draft">
<#--回收站-->
<#assign POST_STATUS_TRASH = "trash">
<#assign POST_STATUS_PENDING="pending">