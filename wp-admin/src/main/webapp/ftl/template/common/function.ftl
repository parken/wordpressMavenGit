<#ftl strip_whitespace=true>
<#-- 公共方法 -->
<#--
读取错误信息
-->
<#macro errors path >
    <@spring.bind path />
    <@spring.showErrors ";"/>
</#macro>
