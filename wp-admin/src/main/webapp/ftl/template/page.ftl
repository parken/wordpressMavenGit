<#ftl strip_whitespace=true>
<#import "org/springframework/web/servlet/view/freemarker/spring.ftl" as spring>
<#include "/template/common/constants.ftl">
<#include "/template/common/function.ftl">
<#-- 引用JspTaglibs严重影响首次加载性能
<#assign springTagsJsp=JspTaglibs["http://www.springframework.org/tags"] />
<#assign springFormJsp=JspTaglibs["http://www.springframework.org/tags/form"] />
-->
<#--
-->
<#macro compress isCompress="false">
    <#if isCompress=="true">
        <#compress >
            <#nested >
        </#compress >
    <#else >
        <#nested >
    </#if>
</#macro>

<#macro head title="">
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="shortcut icon" type="image/ico" href="${path}/images/favicon.ico"/>
    <title>${title!}</title>
    <#include "/template/common/resource.ftl">
    <#nested />
</head>
</#macro>

<#--
 | 通用的HTML模板宏，加入常用的js引用和公用的部分
 | 作者: XXX
 | 日期: 2010-06-09
 -->
<#macro html>
<!DOCTYPE html>
<html>
    <@head/>
<body>
    <#nested>
</body>
    <@footer/>
</html>
</#macro>

<#macro footer>
<P id=copyRight align=center>
<TABLE>
    <TBODY>
    <TR align=middle>
        <TD>版权所有</TD>
    </TR>
    <TR align=middle>
        <TD><A target=_blank>联系我们</A></TD>
    </TR>
    </TBODY>
</TABLE>
</#macro>
