/**
 * Created with IntelliJ IDEA.
 * User: phsxf01
 * Date: 12-11-12
 * Time: 下午11:36
 * To change this template use File | Settings | File Templates.
 */
/**
 *获取分类目录
 * @param termList
 */
function showTerm(termList, taxonomy) {
    var returnVal = "";
    if (termList != undefined && termList != '' && taxonomy != undefined && taxonomy != '') {
        for (var i in termList) {
            if (taxonomy == termList[i].taxonomy) {
                if (returnVal != '') {
                    returnVal = returnVal + ',' + termList[i].name;
                } else {
                    returnVal = returnVal + termList[i].name;
                }
            }
        }
    }
    return returnVal;
}
