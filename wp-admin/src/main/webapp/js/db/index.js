$(window).load(function() {
	showAllTables();
	showAllProc();
});
function showAllTables() {
	$.ajax({
		type : "POST",
		url : $('#showAllTablesUrl').val(),
		data : '',
		async : false,
		success : function(html) {
			var tb = eval('(' + html + ')');

			var table = document.getElementById('tables');
			var length = table.rows.length;
			for ( var i = length - 1; i >= 1; i--) {
				table.deleteRow(i);
			}
			for ( var i in tb) {
				var tr = table.insertRow(-1);
				if (i == 0) {
					tr.bgColor = "#f1f9fe";
				} else {
					tr.bgColor = "#ffffff";
				}

				var data = tb[i];
				for ( var row in data) {
					var td = tr.insertCell(-1);
					td.innerHTML = '<span " title="' + data[row] + '">'
							+ data[row] + '</span>';
				}
			}
		}
	});
}

function showAllProc() {
	$.ajax({
		type : "POST",
		url : $('#showAllProcUrl').val(),
		data : '',
		async : false,
		success : function(html) {
			// var tb = eval('(' + html + ')');
			var tb = html;

			var table = document.getElementById('proc');
			var length = table.rows.length;
			for ( var i = length - 1; i >= 1; i--) {
				table.deleteRow(i);
			}
			for ( var i in tb) {
				var tr = table.insertRow(-1);
				if (i == 0) {
					tr.bgColor = "#f1f9fe";
				} else {
					tr.bgColor = "#ffffff";
				}

				var data = tb[i];
				for ( var row in data) {
					var td = tr.insertCell(-1);
					td.innerHTML = '<span " title="' + data[row] + '">'
							+ data[row] + '</span>';
				}
			}
		}
	});
}