/**
 * 时间对象的格式化
 */
Date.prototype.format = function (format) {
    /*
     * format="yyyy-MM-dd hh:mm:ss";
     */
    var o = {
        "M+":this.getMonth() + 1,
        "d+":this.getDate(),
        "h+":this.getHours(),
        "m+":this.getMinutes(),
        "s+":this.getSeconds(),
        "q+":Math.floor((this.getMonth() + 3) / 3),
        "S":this.getMilliseconds()
    }

    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4
            - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1
                ? o[k]
                : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}
/**
 * 添加jquery的url参数转换为json格式
 * @param string
 * @param overwrite 是否覆盖
 * @return {Object}
 */
$.parseToJson = function (string, overwrite) {
    if (overwrite == undefined) {
        overwrite = false;
    }
    var obj = {},
        pairs = string.split('&'),
        d = decodeURIComponent,
        name,
        value;
    $.each(pairs, function (i, pair) {
        pair = pair.split('=');
        name = d(pair[0]);
        value = d(pair[1]);
        obj[name] = overwrite || !obj[name] ? value :
            [].concat(obj[name]).concat(value);
    });
    return obj;
};
/**
 *分类
 * @type {String}
 */
var POST_CATEGORY = 'category';
/**
 *标签
 * @type {String}
 */
var POST_TAG = 'post_tag';
/**
 *
 * @type {String}
 */
var LINKA_CATEGORY = "link_category";