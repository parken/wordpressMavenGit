<%@page import="java.io.File"%>
<%@page import="java.util.concurrent.ConcurrentHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.net.URL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	private static List<String> imageList = new ArrayList<String>();
	private static ConcurrentHashMap<String, Date> modifyTime = new ConcurrentHashMap<String, Date>();
%>
<%!public static String getRandomImageName() {
		String src = null;
		// 随机切换图片
		if (imageList.isEmpty()) {
			getImage();
		} else {
			src = imageList.get(RandomUtils.nextInt(imageList.size()));
		}
		return src;
	}%>
<%!private static void getImage() {
		Timer timer = new Timer();

		timer.schedule(new TimerTask() {
			@Override
			public void run() {

				try {
					imageList.clear();
					URL url = Thread
							.currentThread()
							.getContextClassLoader()
							.getResource(
									"../../resources/wp-content/themes/twentyeleven/images/headers/");
					URI uri = url.toURI();
					File directory = new File(uri);
					if (directory.isDirectory()) {
						for (String f : directory.list()) {
							if (!f.contains("thumbnail")) {
								imageList.add(f);
							}
						}
					}
					directory = null;

					Thread.sleep(1000 * 5);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}, 0, 10000);
	}%>