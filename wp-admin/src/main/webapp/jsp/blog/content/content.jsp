<%@page import="com.sxf.common.tools.HtmlTools"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.sxf.domain.Comment"%>
<%@page import="com.sxf.cache.CacheTool"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.sxf.domain.viewbean.CommentVO"%>
<%@include file="../../common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${post.title } | <%=CacheTool.getOption("blogdescription")%></title>
<meta name="layout" content="index_frame">
</head>
<body
	class="single single-post postid-21 single-format-standard single-author singular two-column right-sidebar">
	<div id="main">
		<div id="primary">
			<div id="content" role="main">


				<nav id="nav-single">
				<h3 class="assistive-text">文章导航</h3>
                    <c:if test="${prePost != null}">
                    <span class="nav-previous">
                        <a href="${pageContext.request.contextPath}/blog/show?p=${prePost.id}" rel="prev"><span
                            class="meta-nav">←</span> 上一篇</a>
                    </span>
                    </c:if>
                    <c:if test="${nextPost != null}">
                    <span class="nav-next"><a
					href="${pageContext.request.contextPath}/blog/show?p=${nextPost.id}" rel="next">下一篇 <span
						class="meta-nav">→</span></a> </span>
                    </c:if></nav>
				<!-- #nav-single -->


				<article id="post-21"
					class="post-21 post type-post status-publish format-standard hentry category-4">
				<header class="entry-header">
				<h1 class="entry-title">${post.title }</h1>

				<div class="entry-meta">
					<span class="sep">发表于 </span><a
						href="${pageContext.request.contextPath }/blog/show?p=${post.id}"
						title="<fmt:formatDate value="${post.date }" pattern="a h:mm"/>"
						rel="bookmark"><time class="entry-date"
							datetime="${post.dateGmt }" pubdate=""> <fmt:formatDate
							value="${post.date }" pattern="yyyy年MM月dd日" /></time> </a><span
						class="by-author"> <span class="sep"> 由 </span> <span
						class="author vcard"><a class="url fn n"
							href="${pageContext.request.contextPath }/blog/show?author=${user.id}"
							title="查看所有由 ${user.loginName } 发布的文章" rel="author">${user.loginName
								}</a> </span>
					</span>
				</div>
				<!-- .entry-meta --> </header><!-- .entry-header --> <c:if
					test="${post.termList != null && fn:length(post.termList)>0}">
					<c:forEach items="${post.termList }" var="var">
						<c:if test="${var.taxonomy=='category' }">
							<%--查找分类标签 --%>
							<c:set var="postTerm" value="${var }"></c:set>
						</c:if>
					</c:forEach>
				</c:if>

				<div class="entry-content">
					<p>${post.content }</p>
				</div>
				<c:if test="${postTerm !=null}">
					<!-- .entry-content -->
					<footer class="entry-meta"> 此条目是由 <a
						href="${pageContext.request.contextPath }/blog/show?author=${user.id }">${user.loginName
						}</a> 发表在 <a
						href="${pageContext.request.contextPath }/blog/show?id=${user.id }&cat=${postTerm.id}"
						title="查看 ${postTerm.name} 中的全部文章" rel="category">${postTerm.name}</a>
					分类目录的。将<a
						href="${pageContext.request.contextPath }/blog/show?p=${post.id}"
						title="链向 你的代码可以是优雅的，但是还有更重要的 的固定链接" rel="bookmark">固定链接</a>加入收藏夹。
					</footer>
					<!-- .entry-meta -->
				</c:if> </article>
				<!-- #post-21 -->

				<div id="comments">


					<h2 id="comments-title">
						《<span>${post.title }</span>》上有 ${post.commentCount } 条评论
					</h2>


					<ol class="commentlist">
						<%
							Map<Long,CommentVO> commentMap = (Map<Long,CommentVO>)request.getAttribute("commentMap");
							if(commentMap!=null &&!commentMap.isEmpty()){
							for (Iterator it = commentMap.keySet().iterator(); it.hasNext();) {
								Long key=(Long)it.next();
                                CommentVO comment=commentMap.get(key);
						%>

						<li <%if(!comment.getSubCommentList().isEmpty()){%>
							class="comment even thread-even depth-1" <%} else{%>
							class="comment even thread-odd thread-alt depth-1" <%}%>
							id="li-comment-<%=comment.getId()%>"><article id="comment-4"
								class="comment"> <footer class="comment-meta">
							<div class="comment-author vcard">
								<img alt="" src="" class="avatar avatar-68 photo" height="68"
									width="68"><span class="fn"><a
									href="<%=comment.getAuthorUrl()%>" rel="external nofollow"
									class="url"><%=comment.getAuthor()%></a> </span> 在 <a
									href="http://localhost/wordpress/?p=21#comment-4"><time
										pubdate="" datetime="<%=comment.getDateGmt()%>"> <%=new SimpleDateFormat("yyyy年MM月dd日ahh:mm").format(comment.getDate())%></time>
								</a> <span class="says">说道：</span>
							</div>
							<!-- .comment-author .vcard --> <%
 	if("0".equals(comment.getApproved())){
 %> <em class="comment-awaiting-moderation">您的评论正等待审核。</em> <br>
							<%
								}
							%>
							<div class="comment-content">
								<p><%=comment.getContent()%></p>
							</div>

							<div class="reply">
								<a class="comment-reply-link"
									href="http://localhost/wordpress/?p=21&amp;replytocom=4#respond"
									onclick='return addComment.moveForm("comment-4", "4", "respond", "21")'>回复
									<span>↓</span>
								</a>
							</div>
							<!-- .reply --></article> <!-- #comment-## --> <%
 	HtmlTools.iterationComment(response.getOutputStream(),comment,2);
 	%></li>
						<%
							}
						}
						%>
					</ol>



					<div id="respond">
						<h3 id="reply-title">
							发表评论 <small><a rel="nofollow"
								id="cancel-comment-reply-link"
								href="http://localhost/wordpress/?p=21#respond"
								style="display: none;">取消回复</a> </small>
						</h3>
						<form action="http://localhost/wordpress/wp-comments-post.php"
							method="post" id="commentform">
							<p class="comment-notes">
								电子邮件地址不会被公开。 必填项已用 <span class="required">*</span> 标注
							</p>
							<p class="comment-form-author">
								<label for="author">姓名</label> <span class="required">*</span><input
									id="author" name="author" value="sdsd" size="30"
									aria-required="true" type="text">
							</p>
							<p class="comment-form-email">
								<label for="email">电子邮件</label> <span class="required">*</span><input
									id="email" name="email" value="1@2.com" size="30"
									aria-required="true" type="text">
							</p>
							<p class="comment-form-url">
								<label for="url">站点</label><input id="url" name="url"
									value="http://www.163.com" size="30" type="text">
							</p>
							<p class="comment-form-comment">
								<label for="comment">评论</label>
								<textarea id="comment" name="comment" cols="45" rows="8"
									aria-required="true"></textarea>
							</p>
							<p class="form-allowed-tags">
								您可以使用这些 <abbr title="HyperText Markup Language">HTML</abbr>
								标签和属性：
								<code>&lt;a href="" title=""&gt; &lt;abbr title=""&gt;
									&lt;acronym title=""&gt; &lt;b&gt; &lt;blockquote cite=""&gt;
									&lt;cite&gt; &lt;code&gt; &lt;del datetime=""&gt; &lt;em&gt;
									&lt;i&gt; &lt;q cite=""&gt; &lt;strike&gt; &lt;strong&gt; </code>
							</p>
							<p class="form-submit">
								<input name="submit" id="submit" value="发表评论" type="submit">
								<input name="comment_post_ID" value="21" id="comment_post_ID"
									type="hidden"> <input name="comment_parent"
									id="comment_parent" value="0" type="hidden">
							</p>
						</form>
					</div>
					<!-- #respond -->
				</div>
				<!-- #comments -->

			</div>
			<!-- #content -->
		</div>
		<!-- #primary -->

	</div>
	<!-- #main -->
	<script type="text/javascript">
		document.getElementsByTagName('body')[0].className = 'single single-post postid-21 single-format-standard single-author singular two-column right-sidebar';
	</script>
</body>
</html>
