<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@include file="../../common/taglib.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>${loginUser.loginName } 的管理界面</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">

    <%@include file="../../common/pack.jsp" %>

    <script>
        function loadJSP(url) {
            $("#tab1").load(url);
        }

        function loadOverall() {
            loadJSP('${pageContext.request.contextPath }/blog/admin/overall');
        }

        //设置登录窗口
        function openPwd() {
            $('#w').window({
                title:'修改密码',
                width:300,
                modal:true,
                shadow:true,
                closed:true,
                height:180,
                resizable:false
            });
        }
        //关闭登录窗口
        function closePwd() {
            $('#w').window('close');
        }
        $(document).ready(function () {
            $('#loading-mask').fadeOut();
        });
    </script>
</head>
<body class="easyui-layout">
<div region="north"
     style="overflow: hidden; padding: 10px; background: #D2E0F2; line-height: 20px; color: #350670; font-family: Verdana, 微软雅黑, 黑体"
     border="false">
		<span style="float: right; padding-right: 20px;" class="head">欢迎
			${loginUser.loginName }&nbsp;&nbsp;<span id="editpass"
                                                     class="spanLink">修改密码</span>&nbsp;&nbsp; <span id="loginOut"
                                                                                                    class="spanLink">安全退出</span>
		</span> <span style="padding-left: 10px; font-size: 16px;"><img
        src="images/blocks.gif" width="20" height="20" align="absmiddle"/>
			Wordpress后台管理系统</span>
</div>
<div region="south" split="true"
     style="height: 30px; background: #D2E0F2;">
    <div class="footer">版权所有:SXF(2012)</div>
</div>
<div region="east" iconCls="icon-reload" split="true" border="false"
     style="width: 10px; background: #D2E0F2;"></div>
<div region="west" split="true" title="主菜单"
     style="width: 150px; padding: 1px; overflow: hidden;">
    <div class="easyui-accordion" fit="true" border="false">
        <div title="仪表盘" selected="true"
             style="padding: 10px; overflow: auto;">
            <p>
                <span><a href="javascript:void(0);" onclick="loadOverall();">首页</a></span>
            </p>

            <p>
                <span><a href="" target="iframe" title="">我的站点</a></span>
            </p>
        </div>
        <div title="文章" style="padding: 10px;">
            <p>
                <span><a href="">所有文章</a></span>
            </p>

            <p>
                <span><a href="">写文章</a></span>
            </p>

            <p>
                <span><a href="">分类目录</a></span>
            </p>

            <p>
                <span><a href="">标签</a></span>
            </p>
        </div>
        <div title="媒体" style="padding: 10px">
            <p>
                <span><a href="">媒体库</a></span>
            </p>

            <p>
                <span><a href="">添加</a></span>
            </p>
        </div>
        <div title="链接" style="padding: 10px">
            <p>
                <span><a href="">全部链接</a></span>
            </p>

            <p>
                <span><a href="">添加</a></span>
            </p>

            <p>
                <span><a href="">链接分类目录</a></span>
            </p>
        </div>
        <div title="页面" style="padding: 10px">
            <p>
                <span><a href="">所有页面</a></span>
            </p>

            <p>
                <span><a href="">新建页面</a></span>
            </p>
        </div>
        <div title="评论" style="padding: 10px">
            <p>
                <span><a href="">评论</a></span>
            </p>
        </div>
        <div title="外观" style="padding: 10px">
            <p>
                <span><a href="">主题</a></span>
            </p>

            <p>
                <span><a href="">小工具</a></span>
            </p>

            <p>
                <span><a href="">菜单</a></span>
            </p>

            <p>
                <span><a href="">主题选项</a></span>
            </p>

            <p>
                <span><a href="">背景</a></span>
            </p>

            <p>
                <span><a href="">顶部</a></span>
            </p>
        </div>
        <div title="插件" style="padding: 10px">
            <p>
                <span><a href="">插件</a></span>
            </p>
        </div>
        <div title="用户" style="padding: 10px">
            <p>
                <span><a href="">所有用户</a></span>
            </p>

            <p>
                <span><a href="">添加用户</a></span>
            </p>

            <p>
                <span><a href="">我的个人资料</a></span>
            </p>
        </div>
        <div title="工具" style="padding: 10px">
            <p>
                <span><a href="">可用工具</a></span>
            </p>

            <p>
                <span><a href="">导入</a></span>
            </p>

            <p>
                <span><a href="">导出</a></span>
            </p>
        </div>
        <div title="设置" style="padding: 10px">
            <p>
                <span><a href="">常规</a></span>
            </p>

            <p>
                <span><a href="">撰写</a></span>
            </p>

            <p>
                <span><a href="">阅读</a></span>
            </p>

            <p>
                <span><a href="">讨论</a></span>
            </p>

            <p>
                <span><a href="">媒体</a></span>
            </p>

            <p>
                <span><a href="">隐私</a></span>
            </p>

            <p>
                <span><a href="">固定链接</a></span>
            </p>

            <p>
                <span><a href="">中文本地化</a></span>
            </p>
        </div>
    </div>
</div>
<div region="center" title="" style="overflow: hidden;">
    <div class="easyui-tabs" fit="true" border="false">
        <div title="Tab1" style="padding: 5px; overflow: hidden;" id="tab1">
            <iframe scrolling="auto" frameborder="0" id="iframe" src="" style="width:100%;height:100%;"></iframe>
        </div>
        <div title="Tab2" closable="true" style="padding: 20px;" id="tab2">
            This is Tab2 width close button. <input type="hidden" name="n1"/>
            <input type="text" name="n2"/> <input type="text" name="n3">
        </div>
        <div title="Tab3" iconCls="icon-reload" closable="true"
             style="overflow: hidden; padding: 5px;" id="tab3">
            <table id="tt2"></table>
        </div>
    </div>
</div>

<!--修改密码窗口-->
<div id="w" class="easyui-window" title="修改密码" collapsible="false"
     minimizable="false" maximizable="false" icon="icon-save"
     style="width: 300px; height: 180px; padding: 5px; background: #fafafa;">
    <div class="easyui-layout" fit="true">
        <div region="center" border="false"
             style="padding: 10px; background: #fff; border: 1px solid #ccc;">
            <table cellpadding=3>
                <tr>
                    <td>新密码：</td>
                    <td><input id="txtNewPass" type="Password" class="txt01"/></td>
                </tr>
                <tr>
                    <td>确认密码：</td>
                    <td><input id="txtRePass" type="Password" class="txt01"/></td>
                </tr>
            </table>
        </div>
        <div region="south" border="false"
             style="text-align: right; height: 30px; line-height: 30px;">
            <a id="btnEp" class="easyui-linkbutton" icon="icon-ok"
               href="javascript:void(0)"> 确定</a> <a id="btnCancel"
                                                    class="easyui-linkbutton" icon="icon-cancel"
                                                    href="javascript:void(0)">取消</a>
        </div>
    </div>
</div>

<div id="mm" class="easyui-menu" style="width: 150px;">
    <div id="tabupdate">刷新</div>
    <div class="menu-sep"></div>
    <div id="close">关闭</div>
    <div id="closeall">全部关闭</div>
    <div id="closeother">除此之外全部关闭</div>
    <div class="menu-sep"></div>
    <div id="closeright">当前页右侧全部关闭</div>
    <div id="closeleft">当前页左侧全部关闭</div>
    <div class="menu-sep"></div>
    <div id="exit">退出</div>
</div>
</body>
</html>