<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="../../common/taglib.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="noindex,nofollow">
<title>WordPress后台登录</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/web.css">
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/ui/bootstrap/css/bootstrap.min.css" />
<script type="text/javascript"
            src="${pageContext.request.contextPath}/ui/jquery/jquery-1.11.3.js"></script>		
<script type="text/javascript"
            src="${pageContext.request.contextPath}/ui/bootstrap/js/bootstrap.min.js"></script>	
<script type="text/javascript">
	// 登录页面若在框架内，则跳出框架
	if (self != top) {
		top.location = self.location;
	}
</script>
</head>
<body >
	<div class="container">
	<div class="col-sm-5 row">
	</div>
	<div class="col-sm-6 row" style="margin-top: 120px;">
		<h1>
			<a href="" title="基于 WordPress">管理后台</a>
		</h1>
		<form:form modelAttribute="loginForm"
			action="${pageContext.request.contextPath }/blog/login/doLogin"
			method="post">
			<p>
				<label for="loginName">用户名<br> <input type="text"
					name="loginName" id="loginName" class="input" size="20"
					tabindex="10">
				</label> <span style="color: red;"><form:errors path="loginName" /></span>
			</p>

			<p>
				<label for="password">密码<br> <input type="password"
					name="password" id="password" class="input" value="" size="20"
					tabindex="20">
				</label> <span style="color: red;"><form:errors path="password" /></span>
			</p>

			<p class="forgetmenot">
				<label for="rememberme"><input name="rememberme"
					type="checkbox" id="rememberme" value="forever" tabindex="90">
					记住我的登录信息</label>
			</p>

			<p class="submit">
				<input type="submit" name="wp-submit" id="wp-submit"
					class="button-primary" value="登录" tabindex="100"> <input
					type="hidden" name="easyui" value="1">
			</p>
		</form:form>

		<p id="nav">
			<a href="http://localhost/wordpress/wp-login.php?action=lostpassword"
				title="找回密码">忘记密码？</a>		
			<a href="${pageContext.request.contextPath }/" title="不知道自己在哪？">←
				回到博客首页</a>
		</p>
		</div>
	</div>
	<div class="clear"></div>
</body>
</html>