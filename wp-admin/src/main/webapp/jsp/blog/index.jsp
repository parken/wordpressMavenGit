<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="index_frame">
<title><c:choose>
		<c:when test="${month!=null }">
			<fmt:formatDate value="${month}" pattern="MM月" />
		</c:when>
		<c:when test="${term!=null }">${term.name }</c:when>
		<c:otherwise>${blogname}</c:otherwise>
	</c:choose> | ${blogdescription}</title>
</head>
<body>
	<div id="main">
		<div id="primary">
			<div id="content" role="main">
				<c:if test="${month!=null }">
					<header class="page-header">
						<h1 class="page-title">
							月度归档:<span><fmt:formatDate value="${month}"
									pattern="yyyy年MM月" /> </span>
						</h1>
					</header>
				</c:if>
				<c:if test="${term!=null }">
					<header class="page-header">
						<h1 class="page-title">
							分类目录归档:<span>${term.name }</span>
						</h1>
					</header>
				</c:if>
				<c:if test="${article != null }">
					<c:forEach items="${article}" var="post" varStatus="status">
						<article id="post-${post.id }"
							class="post-${post.id } post type-post status-publish format-standard hentry category-uncategorized">
							<header class="entry-header">
								<h1 class="entry-title">
									<a
										href="${pageContext.request.contextPath }/blog/show?p=${post.id}"
										title="链向 ${post.title } 的固定链接" rel="bookmark">${post.title
										}</a>
								</h1>

								<div class="entry-meta">
									<span class="sep">发表于 </span><a
										href="${pageContext.request.contextPath }/blog/show?p=${post.id}"
										title='<fmt:formatDate value="${post.date }" pattern="a h:mm"/>'
										rel="bookmark"><time class="entry-date"
											datetime="${post.dateGmt }" pubdate>
											<fmt:formatDate value="${post.date }" pattern="yyyy年MM月dd日" />
										</time> </a><span class="by-author"> <span class="sep"> 由 </span>
										<span class="author vcard"><a class="url fn n"
											href="http://localhost/wordpress/?author=1"
											title="查看所有由 ${user.loginName } 发布的文章" rel="author">${user.loginName}</a>
									</span>
									</span>
								</div>
								<!-- .entry-meta -->

								<div class="comments-link">
									<a
										href="${pageContext.request.contextPath }/blog/show?p=${post.id}#comments"
										title="《${post.title }》上的评论"> <c:if
											test="${post.commentCount>0 }">${post.commentCount}</c:if>
									</a>
								</div>
							</header>
							<!-- .entry-header -->

							<div class="entry-content">
								<p>${post.content }</p>
							</div>
							<c:if
								test="${post.termList != null && fn:length(post.termList)>0}">
								<c:forEach items="${post.termList }" var="var">
									<c:if test="${var.taxonomy=='category' }">
										<%--查找分类标签 --%>										
										<c:set var="postTerm" value="${var }"></c:set>
									</c:if>
								</c:forEach>
							</c:if>
							<!-- .entry-content -->
							<footer class="entry-meta">
								<span class="cat-links"> <span
									class="entry-utility-prep entry-utility-prep-cat-links">发表在</span>
									<a
									href="${pageContext.request.contextPath }/blog/show?id=${user.id }&cat=${postTerm.id}"
									title="查看 ${postTerm.name } 中的全部文章" rel="category">${postTerm.name
										}</a>
								</span> <span class="sep"> | </span> <span class="comments-link">
									<c:choose>
										<c:when test="${post.commentCount ==0}">
											<a
												href="${pageContext.request.contextPath }/blog/show?p=${post.id}#comments"
												title="《${post.title }》上的评论"><span class="leave-reply">发表回复</span>
											</a>
										</c:when>
										<c:otherwise>
											<a
												href="${pageContext.request.contextPath }/blog/show?p=${post.id}#comments"
												title="《${post.title }》上的评论"><b>${post.commentCount
													}</b> 条回复</a>
										</c:otherwise>
									</c:choose>
								</span> <span class="edit-link"><a class="post-edit-link"
									href="http://localhost/wordpress/wp-admin/post.php?post=1&amp;action=edit"
									title="编辑文章">编辑</a> </span>
							</footer>
							<!-- #entry-meta -->
						</article>
						<!-- #post-${post.id } -->

					</c:forEach>
				</c:if>
			</div>
			<!-- #content -->
		</div>
		<!-- #primary -->

		<div id="secondary" class="widget-area" role="complementary">
			<aside id="search-2" class="widget widget_search">
				<form method="get" id="searchform"
					action="http://localhost/wordpress/">
					<label for="s" class="assistive-text">搜索</label> <input type="text"
						class="field" name="s" id="s" placeholder="搜索" /> <input
						type="submit" class="submit" name="submit" id="searchsubmit"
						value="搜索" />
				</form>
			</aside>
			<aside id="recent-posts-2" class="widget widget_recent_entries">
				<h3 class="widget-title">近期文章</h3>
				<ul>
					<c:forEach items="${article}" var="post" varStatus="status">
						<c:if test="${status.index<=4 }">
							<li><a
								href="${pageContext.request.contextPath }/blog/show?p=${post.id}"
								title="${post.title}">${post.title}</a></li>
						</c:if>
					</c:forEach>
				</ul>
			</aside>
			<aside id="recent-comments-2" class="widget widget_recent_comments">
				<h3 class="widget-title">近期评论</h3>
				<ul id="recentcomments">
					<c:forEach items="${commentList }" var="comment">
						<li class="recentcomments"><a href='${comment.authorUrl }'
							rel='external nofollow' class='url'>${comment.author }</a> 发表在《<a
							href="${pageContext.request.contextPath }/blog/show?p=${comment.postId }#comment-1">${comment.parentPost.title
								}</a>》</li>
					</c:forEach>
				</ul>
			</aside>
			<aside id="archives-2" class="widget widget_archive">
				<h3 class="widget-title">文章归档</h3>
				<ul>
					<c:forEach items="${archiveList }" var="archive">
						<li><a
							href='${pageContext.request.contextPath }/blog/show?id=${user.id }&m=<fmt:formatDate value="${archive}"  pattern="yyyyMM" />'
							title='<fmt:formatDate value="${archive}"
									pattern="yyyy年MM月" />'><fmt:formatDate
									value="${archive}" pattern="yyyy年MM月" /> </a></li>
					</c:forEach>
				</ul>
			</aside>
			<aside id="categories-2" class="widget widget_categories">
				<h3 class="widget-title">分类目录</h3>
				<ul>
					<c:forEach items="${termList }" var="term">
						<li class="cat-item cat-item-1"><a
							href="${pageContext.request.contextPath }/blog/show?id=${user.id }&cat=${term.id}"
							title="查看 ${term.name} 下的所有文章">${term.name}</a></li>
					</c:forEach>
				</ul>
			</aside>
			<aside id="meta-2" class="widget widget_meta">
				<h3 class="widget-title">功能</h3>
				<ul>
					<li><a
						href="${pageContext.request.contextPath }/blog/login/doLogin">管理站点</a></li>
					<li><c:choose>
							<c:when test="${userLogin!=null }">
								<a
									href="http://localhost/wordpress/wp-login.php?action=logout&#038;_wpnonce=feb77c6d71">登出</a>
							</c:when>
							<c:otherwise>
								<a
									href="${pageContext.request.contextPath }/blog/login">登陆</a>
							</c:otherwise>
						</c:choose></li>
					<li><a href="http://localhost/wordpress/?feed=rss2"
						title="使用 RSS 2.0 订阅本站点内容">文章 <abbr
							title="Really Simple Syndication">RSS</abbr>
					</a></li>
					<li><a href="http://localhost/wordpress/?feed=comments-rss2"
						title="使用 RSS 订阅本站点的所有文章的近期评论">评论 <abbr
							title="Really Simple Syndication">RSS</abbr>
					</a></li>
					<li><a href="http://wordpress.org/"
						title="基于 WordPress，一个优美、先进的个人信息发布平台。">WordPress.org</a></li>
				</ul>
			</aside>
		</div>
		<!-- #secondary .widget-area -->

	</div>
	<!-- #main -->

</body>
</html>