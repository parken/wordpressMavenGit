<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="../../common/taglib.jsp"%>
<%@page trimDirectiveWhitespaces="true"%>
<!--[if IE 6]>
<html id="ie6" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html dir="ltr" lang="zh-CN">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width" />
<title><sitemesh:write property='title' /></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all"
	href="${pageContext.request.contextPath }/resources/wp-content/themes/twentyeleven/style.css" />
<link rel="pingback" href="http://localhost/wordpress/xmlrpc.php" />
<!--[if lt IE 9]>
<script src="http://localhost/wordpress/wp-content/themes/twentyeleven/js/html5.js" type="text/javascript"></script>
<![endif]-->
<meta name='robots' content='noindex,nofollow' />

<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 可选的Bootstrap主题文件（一般不用引入） -->
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<meta name="generator" content="WordPress 3.3.1" />

<c:if test="${false}">
	<style type="text/css" media="screen">
html {
	margin-top: 28px !important;
}

* html body {
	margin-top: 28px !important;
}
</c:if>
</style>
	<%
		//<sitemesh:write property='head' />
	%>

</head>
<body
	class="home blog logged-in admin-bar single-author two-column right-sidebar">
	<div id="page" class="hfeed">
		<jsp:include page="../app_frame/header.jsp" />
		<!-- #branding -->
		<sitemesh:write property='body' />
		<!-- #main -->

		<footer id="colophon" role="contentinfo">

			<div id="site-generator">
				<a href="http://cn.wordpress.org/" title="优雅的个人发布平台" rel="generator">自豪地采用
					WordPress</a>
			</div>
		</footer>
		<!-- #colophon -->
	</div>
	<!-- #page -->
	<c:if test="${false}">
		<%@include file="../app_frame/header_in.jsp"%>
	</c:if>
</body>
</html>