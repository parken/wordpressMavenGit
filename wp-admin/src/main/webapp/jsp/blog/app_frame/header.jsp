<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.sxf.common.tools.HtmlTools"%>
<%@page import="com.sxf.cache.CacheTool"%>
<%@page import="com.sxf.common.tools.WebTools"%>
<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<header id="branding" role="banner">
	<hgroup>
		<h1 id="site-title">
			<span><a
				href="http://${pageContext.request.localAddr }:${pageContext.request.localPort }${pageContext.request.contextPath }/"
				title="<%=CacheTool.getOption("blogname") %>" rel="home"><%=CacheTool.getOption("blogname")%></a>
			</span>
		</h1>
		<h2 id="site-description"><%=CacheTool.getOption("blogdescription")%></h2>
	</hgroup>
	<%
		String src = HtmlTools.getRandomImageName();		
		if (StringUtils.isBlank(src)) {
			src = "willow.jpg";
		}
	%>	
	<a href="${pageContext.request.contextPath }/"> <img
		src="${pageContext.request.contextPath }/resources/wp-content/themes/twentyeleven/images/headers/<%=src %>"
		width="1000" height="288" alt="" />
	</a>

	<form method="get" id="searchform" action="http://localhost/wordpress/">
		<label for="s" class="assistive-text">搜索</label> <input type="text"
			class="field" name="s" id="s" placeholder="搜索" /> <input
			type="submit" class="submit" name="submit" id="searchsubmit"
			value="搜索" />
	</form>
		
	<div class='navbar navbar-inverse'>
		  <div class='nav-collapse' style="height: auto;">
		    <ul class="nav nav-pills">
			  <li role="presentation" class="active"><a
					href="${pageContext.request.contextPath }" title="首页">首页</a></li>
			  <li role="presentation"><a
					href="http://localhost/wordpress/?page_id=2">示例页面</a></li>			 
			</ul>
		  </div>
		</div>	
	<!-- #access -->
</header>