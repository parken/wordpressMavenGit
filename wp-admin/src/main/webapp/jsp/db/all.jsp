<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@include file="../common/taglib.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>数据库总揽</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <script type="text/javascript"
            src="${pageContext.request.contextPath }/js/jquery.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath }/js/db/index.js"></script>
    <!--
     <link rel="stylesheet" type="text/css" href="styles.css">
     -->

</head>
<body align="center">
<h3 align="center">AllTables</h3>

<div style="text-align: center;">
    <table id="tables" align="center" cellspacing="1" bgcolor="#2b698c"></table>
</div>
<h3 align="center">AllProcs</h3>

<div style="text-align: center;">
    <table id="proc" align="center" cellpadding="1" bgcolor="#2b698c"
           border="0"></table>
</div>
<input type="hidden" id="showAllTablesUrl"
       value="${pageContext.request.contextPath }/db/showAllTables"/>
<input type="hidden" id="showAllProcUrl"
       value="${pageContext.request.contextPath }/db/showAllProc"/>
<spring:message code="test" text="测试而已" arguments=""/>
</body>
</html>
