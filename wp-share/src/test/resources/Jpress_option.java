package com.my.db;

public class Jpress_option {
  private Long id;
  private String option_key;
  private String option_value;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOption_key() {
    return option_key;
  }

  public void setOption_key(String option_key) {
    this.option_key = option_key;
  }

  public String getOption_value() {
    return option_value;
  }

  public void setOption_value(String option_value) {
    this.option_value = option_value;
  }
}
