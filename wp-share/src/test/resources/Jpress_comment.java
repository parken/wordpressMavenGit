package com.my.db;

public class Jpress_comment {
  private Long id;
  private Long parent_id;
  private Long content_id;
  private String content_module;
  private Long comment_count;
  private Long order_number;
  private Long user_id;
  private String ip;
  private String author;
  private String type;
  private String text;
  private String agent;
  private java.sql.Timestamp created;
  private String slug;
  private String email;
  private String status;
  private Long vote_up;
  private Long vote_down;
  private String flag;
  private Double lat;
  private Double lng;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getParent_id() {
    return parent_id;
  }

  public void setParent_id(Long parent_id) {
    this.parent_id = parent_id;
  }

  public Long getContent_id() {
    return content_id;
  }

  public void setContent_id(Long content_id) {
    this.content_id = content_id;
  }

  public String getContent_module() {
    return content_module;
  }

  public void setContent_module(String content_module) {
    this.content_module = content_module;
  }

  public Long getComment_count() {
    return comment_count;
  }

  public void setComment_count(Long comment_count) {
    this.comment_count = comment_count;
  }

  public Long getOrder_number() {
    return order_number;
  }

  public void setOrder_number(Long order_number) {
    this.order_number = order_number;
  }

  public Long getUser_id() {
    return user_id;
  }

  public void setUser_id(Long user_id) {
    this.user_id = user_id;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getAgent() {
    return agent;
  }

  public void setAgent(String agent) {
    this.agent = agent;
  }

  public java.sql.Timestamp getCreated() {
    return created;
  }

  public void setCreated(java.sql.Timestamp created) {
    this.created = created;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Long getVote_up() {
    return vote_up;
  }

  public void setVote_up(Long vote_up) {
    this.vote_up = vote_up;
  }

  public Long getVote_down() {
    return vote_down;
  }

  public void setVote_down(Long vote_down) {
    this.vote_down = vote_down;
  }

  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public Double getLng() {
    return lng;
  }

  public void setLng(Double lng) {
    this.lng = lng;
  }
}
