package com.my.db;

public class Jpress_taxonomy {
  private Long id;
  private String title;
  private String text;
  private String slug;
  private String type;
  private String icon;
  private String content_module;
  private Long content_count;
  private Long order_number;
  private Long parent_id;
  private Long object_id;
  private String flag;
  private Double lat;
  private Double lng;
  private String meta_keywords;
  private String meta_description;
  private java.sql.Timestamp created;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public String getContent_module() {
    return content_module;
  }

  public void setContent_module(String content_module) {
    this.content_module = content_module;
  }

  public Long getContent_count() {
    return content_count;
  }

  public void setContent_count(Long content_count) {
    this.content_count = content_count;
  }

  public Long getOrder_number() {
    return order_number;
  }

  public void setOrder_number(Long order_number) {
    this.order_number = order_number;
  }

  public Long getParent_id() {
    return parent_id;
  }

  public void setParent_id(Long parent_id) {
    this.parent_id = parent_id;
  }

  public Long getObject_id() {
    return object_id;
  }

  public void setObject_id(Long object_id) {
    this.object_id = object_id;
  }

  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public Double getLng() {
    return lng;
  }

  public void setLng(Double lng) {
    this.lng = lng;
  }

  public String getMeta_keywords() {
    return meta_keywords;
  }

  public void setMeta_keywords(String meta_keywords) {
    this.meta_keywords = meta_keywords;
  }

  public String getMeta_description() {
    return meta_description;
  }

  public void setMeta_description(String meta_description) {
    this.meta_description = meta_description;
  }

  public java.sql.Timestamp getCreated() {
    return created;
  }

  public void setCreated(java.sql.Timestamp created) {
    this.created = created;
  }
}
