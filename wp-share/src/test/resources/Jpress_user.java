package com.my.db;

public class Jpress_user {
  private Long id;
  private String username;
  private String nickname;
  private String realname;
  private String password;
  private String salt;
  private String email;
  private String email_status;
  private String mobile;
  private String mobile_status;
  private String telephone;
  private Double amount;
  private String gender;
  private String role;
  private String signature;
  private Long content_count;
  private Long comment_count;
  private String qq;
  private String wechat;
  private String weibo;
  private String facebook;
  private String linkedin;
  private java.sql.Timestamp birthday;
  private String company;
  private String occupation;
  private String address;
  private String zipcode;
  private String site;
  private String graduateschool;
  private String education;
  private String avatar;
  private String idcardtype;
  private String idcard;
  private String status;
  private java.sql.Timestamp created;
  private String create_source;
  private java.sql.Timestamp logged;
  private java.sql.Timestamp activated;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getRealname() {
    return realname;
  }

  public void setRealname(String realname) {
    this.realname = realname;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getSalt() {
    return salt;
  }

  public void setSalt(String salt) {
    this.salt = salt;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmail_status() {
    return email_status;
  }

  public void setEmail_status(String email_status) {
    this.email_status = email_status;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getMobile_status() {
    return mobile_status;
  }

  public void setMobile_status(String mobile_status) {
    this.mobile_status = mobile_status;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public Long getContent_count() {
    return content_count;
  }

  public void setContent_count(Long content_count) {
    this.content_count = content_count;
  }

  public Long getComment_count() {
    return comment_count;
  }

  public void setComment_count(Long comment_count) {
    this.comment_count = comment_count;
  }

  public String getQq() {
    return qq;
  }

  public void setQq(String qq) {
    this.qq = qq;
  }

  public String getWechat() {
    return wechat;
  }

  public void setWechat(String wechat) {
    this.wechat = wechat;
  }

  public String getWeibo() {
    return weibo;
  }

  public void setWeibo(String weibo) {
    this.weibo = weibo;
  }

  public String getFacebook() {
    return facebook;
  }

  public void setFacebook(String facebook) {
    this.facebook = facebook;
  }

  public String getLinkedin() {
    return linkedin;
  }

  public void setLinkedin(String linkedin) {
    this.linkedin = linkedin;
  }

  public java.sql.Timestamp getBirthday() {
    return birthday;
  }

  public void setBirthday(java.sql.Timestamp birthday) {
    this.birthday = birthday;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getOccupation() {
    return occupation;
  }

  public void setOccupation(String occupation) {
    this.occupation = occupation;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getSite() {
    return site;
  }

  public void setSite(String site) {
    this.site = site;
  }

  public String getGraduateschool() {
    return graduateschool;
  }

  public void setGraduateschool(String graduateschool) {
    this.graduateschool = graduateschool;
  }

  public String getEducation() {
    return education;
  }

  public void setEducation(String education) {
    this.education = education;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getIdcardtype() {
    return idcardtype;
  }

  public void setIdcardtype(String idcardtype) {
    this.idcardtype = idcardtype;
  }

  public String getIdcard() {
    return idcard;
  }

  public void setIdcard(String idcard) {
    this.idcard = idcard;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public java.sql.Timestamp getCreated() {
    return created;
  }

  public void setCreated(java.sql.Timestamp created) {
    this.created = created;
  }

  public String getCreate_source() {
    return create_source;
  }

  public void setCreate_source(String create_source) {
    this.create_source = create_source;
  }

  public java.sql.Timestamp getLogged() {
    return logged;
  }

  public void setLogged(java.sql.Timestamp logged) {
    this.logged = logged;
  }

  public java.sql.Timestamp getActivated() {
    return activated;
  }

  public void setActivated(java.sql.Timestamp activated) {
    this.activated = activated;
  }
}
