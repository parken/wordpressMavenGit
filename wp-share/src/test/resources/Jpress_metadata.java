package com.my.db;

public class Jpress_metadata {
  private Long id;
  private String meta_key;
  private String meta_value;
  private String object_type;
  private Long object_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getMeta_key() {
    return meta_key;
  }

  public void setMeta_key(String meta_key) {
    this.meta_key = meta_key;
  }

  public String getMeta_value() {
    return meta_value;
  }

  public void setMeta_value(String meta_value) {
    this.meta_value = meta_value;
  }

  public String getObject_type() {
    return object_type;
  }

  public void setObject_type(String object_type) {
    this.object_type = object_type;
  }

  public Long getObject_id() {
    return object_id;
  }

  public void setObject_id(Long object_id) {
    this.object_id = object_id;
  }
}
