package com.my.db;

public class Jpress_content {
  private Long id;
  private String title;
  private String text;
  private String summary;
  private String link_to;
  private Long markdown_enable;
  private String thumbnail;
  private String module;
  private String style;
  private Long user_id;
  private String author;
  private String user_email;
  private String user_ip;
  private String user_agent;
  private Long parent_id;
  private Long object_id;
  private Long order_number;
  private String status;
  private Long vote_up;
  private Long vote_down;
  private Long rate;
  private Long rate_count;
  private Double price;
  private String comment_status;
  private Long comment_count;
  private java.sql.Timestamp comment_time;
  private Long view_count;
  private java.sql.Timestamp created;
  private java.sql.Timestamp modified;
  private String slug;
  private String flag;
  private Double lng;
  private Double lat;
  private String meta_keywords;
  private String meta_description;
  private String remarks;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getLink_to() {
    return link_to;
  }

  public void setLink_to(String link_to) {
    this.link_to = link_to;
  }

  public Long getMarkdown_enable() {
    return markdown_enable;
  }

  public void setMarkdown_enable(Long markdown_enable) {
    this.markdown_enable = markdown_enable;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public String getModule() {
    return module;
  }

  public void setModule(String module) {
    this.module = module;
  }

  public String getStyle() {
    return style;
  }

  public void setStyle(String style) {
    this.style = style;
  }

  public Long getUser_id() {
    return user_id;
  }

  public void setUser_id(Long user_id) {
    this.user_id = user_id;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getUser_email() {
    return user_email;
  }

  public void setUser_email(String user_email) {
    this.user_email = user_email;
  }

  public String getUser_ip() {
    return user_ip;
  }

  public void setUser_ip(String user_ip) {
    this.user_ip = user_ip;
  }

  public String getUser_agent() {
    return user_agent;
  }

  public void setUser_agent(String user_agent) {
    this.user_agent = user_agent;
  }

  public Long getParent_id() {
    return parent_id;
  }

  public void setParent_id(Long parent_id) {
    this.parent_id = parent_id;
  }

  public Long getObject_id() {
    return object_id;
  }

  public void setObject_id(Long object_id) {
    this.object_id = object_id;
  }

  public Long getOrder_number() {
    return order_number;
  }

  public void setOrder_number(Long order_number) {
    this.order_number = order_number;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Long getVote_up() {
    return vote_up;
  }

  public void setVote_up(Long vote_up) {
    this.vote_up = vote_up;
  }

  public Long getVote_down() {
    return vote_down;
  }

  public void setVote_down(Long vote_down) {
    this.vote_down = vote_down;
  }

  public Long getRate() {
    return rate;
  }

  public void setRate(Long rate) {
    this.rate = rate;
  }

  public Long getRate_count() {
    return rate_count;
  }

  public void setRate_count(Long rate_count) {
    this.rate_count = rate_count;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String getComment_status() {
    return comment_status;
  }

  public void setComment_status(String comment_status) {
    this.comment_status = comment_status;
  }

  public Long getComment_count() {
    return comment_count;
  }

  public void setComment_count(Long comment_count) {
    this.comment_count = comment_count;
  }

  public java.sql.Timestamp getComment_time() {
    return comment_time;
  }

  public void setComment_time(java.sql.Timestamp comment_time) {
    this.comment_time = comment_time;
  }

  public Long getView_count() {
    return view_count;
  }

  public void setView_count(Long view_count) {
    this.view_count = view_count;
  }

  public java.sql.Timestamp getCreated() {
    return created;
  }

  public void setCreated(java.sql.Timestamp created) {
    this.created = created;
  }

  public java.sql.Timestamp getModified() {
    return modified;
  }

  public void setModified(java.sql.Timestamp modified) {
    this.modified = modified;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }

  public Double getLng() {
    return lng;
  }

  public void setLng(Double lng) {
    this.lng = lng;
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public String getMeta_keywords() {
    return meta_keywords;
  }

  public void setMeta_keywords(String meta_keywords) {
    this.meta_keywords = meta_keywords;
  }

  public String getMeta_description() {
    return meta_description;
  }

  public void setMeta_description(String meta_description) {
    this.meta_description = meta_description;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }
}
