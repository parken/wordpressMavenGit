package com.my.db;

public class Jpress_mapping {
  private Long id;
  private Long content_id;
  private Long taxonomy_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getContent_id() {
    return content_id;
  }

  public void setContent_id(Long content_id) {
    this.content_id = content_id;
  }

  public Long getTaxonomy_id() {
    return taxonomy_id;
  }

  public void setTaxonomy_id(Long taxonomy_id) {
    this.taxonomy_id = taxonomy_id;
  }
}
