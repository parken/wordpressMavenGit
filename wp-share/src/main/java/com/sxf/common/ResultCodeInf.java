package com.sxf.common;

public interface ResultCodeInf {
    String name();

    int getCode();
}
