package com.sxf.common.utils;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sxf.common.CommonResultCode;
import com.sxf.common.ResultCodeInf;

import java.io.Serializable;
import java.util.Arrays;

public class AjaxResult<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private int code;

	@JsonIgnore
	private ResultCodeInf enumCode;

	private String message;

	private T data;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@JSONField(serialize = false)
	private Object[] messageParam;


	public AjaxResult() {
	}

	public AjaxResult(ResultCodeInf code) {
		this.enumCode = code;
		this.setCode(enumCode.getCode());
	}

	public AjaxResult(ResultCodeInf code, String message) {
		this.enumCode = code;
		this.message = message;
		this.setCode(enumCode.getCode());
	}

	public AjaxResult(ResultCodeInf code, T data) {
		this.enumCode = code;
		this.data = data;
		this.setCode(enumCode.getCode());
	}

	public AjaxResult(ResultCodeInf code, T data, Object[] messageParam) {
		this.enumCode = code;
		this.data = data;
		this.messageParam = messageParam;
		this.setCode(enumCode.getCode());
	}

	public int getCode() {
		return code;
	}

	public AjaxResult<T> setCode(int code) {
		this.code = code;
		return this;
	}

	public ResultCodeInf getEnumCode() {
		return enumCode;
	}

	public void setEnumCode(ResultCodeInf enumCode) {
		this.enumCode = enumCode;
		this.setCode(enumCode.getCode());
	}

	public String getMessage() {
		return message;
	}

	public AjaxResult<T> setMessage(String message) {
		this.message = message;
		return this;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Object[] getMessageParam() {
		return messageParam;
	}

	public void setMessageParam(Object[] messageParam) {
		this.messageParam = messageParam;
	}

	public static <T> AjaxResult<T> build() {
		return buildSuccess();
	}

	public static <T> AjaxResult<T> buildSuccess() {
		AjaxResult<T> result = new AjaxResult<>();
		result.setEnumCode(CommonResultCode.RESULT_SUCCESS);
		result.setCode(result.getEnumCode().getCode());
		return result;
	}

	public static <T> AjaxResult<T> buildFail() {
		AjaxResult<T> result = new AjaxResult<>();
		result.setEnumCode(CommonResultCode.RESULT_FAIL);
		result.setCode(result.getEnumCode().getCode());
		return result;
	}

	public static <T> AjaxResult<T> buildIllegal() {
		AjaxResult<T> result = new AjaxResult<>();
		result.setEnumCode(CommonResultCode.ILLEGAL_ARGUMENT);
		result.setCode(result.getEnumCode().getCode());
		return result;
	}

	@Override
	public String toString() {
		return "AjaxResult{" +
				"resultCode=" + code +
				", code=" + enumCode +
				", message='" + message + '\'' +
				", data=" + data +
				", messageParam=" + Arrays.toString(messageParam) +
				'}';
	}
}
