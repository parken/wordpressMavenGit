package com.sxf.common;


/**
 * 对照messages.properties看各码的意思
 * @author
 * @date
 */
public enum CommonResultCode implements ResultCodeInf {

    // 调用结果：成功
    RESULT_SUCCESS(0),//

    // 调用结果：失败
    RESULT_FAIL(1),//

    // 调用结果：异常
    //记录已存在
    RECORD_EXIST(3),
    RESULT_ERROR(2),//

    ILLEGAL_CODE(4),
    ILLEGAL_ARGUMENT(5),
    SYSTEM_ERROR(6),
    UNKNOW_ERROR(1004),
    //资源被挂起
    RESOURCE_HANGUP(11003),



    CAS_NOT_LOGIN(1008),
    DATA_INIT_EXCEPTION(1009),
    ILLEGAL_INVOCATION(1010),
    /**
     * 无相关数据
     */
    NO_DATA(1011),

    //////////////////////////用户模块
    USERNAME_OR_PASSWORD_ERROR(1100),
    USERNAME_OR_PASSWORD_BLANK(1101),
    SOME_ACCOUNT_ALREADY_LOGIN(1102),
    WRONG_OLD_PASSWORD(1103),
    NEED_CAPTCHA(1104),
    WRONG_CAPTCHA(1105),
    USER_FROZEN(1106),
    USERNAME_EXISTS(1107),
    USER_DELETED(1108),
    EMAIL_SEND_ERROR(1109),
    USER_NO_ACTIVE(1110),
    USER_LOCKED(1111),
    USER_NOT_EXISTS(1112),
    USER_PASSWORD_ERROR(1113),
    USER_CREDENTIAL(1114),
    USER_NO_LOGIN(1115),

    LOGIN_FAIL(11000),

    /**
     * 用户名或密码不能为空
     */
    BLANK_USERNAME_OR_PASSWORD(11002),

    /**
     * 用户名或密码错误
     */
    WRONG_USERNAME_OR_PASSWORD(11005),

    /**
     * 不允许风险密码
     */
    RISK_PASSWORD_LEVEL(11006),



    /**
     * 未登录或登录已过期
     */
    ACCOUNT_NOT_LOGIN(10003),//


    LOGIN_NAME_NOT_EXIST(6),

    ACCOUNT_LOCKED(8),



    /**
     * 账户凭证错误,请刷新页面!
     */
    ACCOUNT_CREDENTIAL(10),

    /**
     * 旧密码错误
     */
    OLD_PASSWORD_ERROR(11),



    /**
     * 权限不足
     */
    PERMISSION_DENIED(10018),//
    /**
     * 参数有误
     */
    PARAM_IS_ERROR(10019),//
    /**
     * 核心服务调用失败
     */
    CALL_BIC_API_FAIL(10020),
    /**
     * 记录不存在
     */
    RECORD_NOT_EXIST(6),//

    /**
     * 分页大小不能为零
     */
    PAGE_SIZE_IS_ZERO(7),
    /**
     * 分页大小不能小于零
     */
    PAGE_SIZE_IS_LT_ZERO(8),

    FILE_UPLOAD_FAIL(10016),
    FILE_EXCEEDS_LIMIT(10017),





    //license相关
    LICENSE_NOT_EXIST(50005),
    LICENSE_EXPIRED(500006),
    LICENSE_ERROR(500007),
    ;

    private int code;

    private CommonResultCode(int code) {
        this.code = code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public int getCode() {
        return this.code;
    }
}
