package com.sxf.exception;

import javax.security.auth.login.LoginException;


public class WrongCaptchaException extends LoginException {
    private int invalidTimes;

    public int getInvalidTimes(){
        return invalidTimes;
    }

    public WrongCaptchaException() {
    }

    public WrongCaptchaException(int invalidTimes) {
        this.invalidTimes = invalidTimes;
    }


    public WrongCaptchaException(String msg, int invalidTimes) {
        super(msg);
        this.invalidTimes = invalidTimes;
    }

    public WrongCaptchaException(String msg) {
        super(msg);
    }
}
