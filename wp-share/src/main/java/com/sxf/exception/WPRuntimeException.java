package com.sxf.exception;

import com.sxf.common.ResultCodeInf;

import java.util.Arrays;

public class WPRuntimeException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private ResultCodeInf resultCode;

    private Object[] messageParam;

    public WPRuntimeException() {
        super();
    }

    public WPRuntimeException(ResultCodeInf resultCode) {
        super();
        this.resultCode = resultCode;
    }

    public WPRuntimeException(ResultCodeInf resultCode, Throwable cause) {
        super(cause);
        this.resultCode = resultCode;
    }

    public WPRuntimeException(String message, ResultCodeInf resultCode, Object[] messageParam) {
        super(message);
        this.resultCode = resultCode;
        this.messageParam = messageParam;
    }

    public WPRuntimeException(ResultCodeInf resultCode, Object[] messageParam) {
        super();
        this.resultCode = resultCode;
        this.messageParam = messageParam;
    }

    public WPRuntimeException(ResultCodeInf resultCode, Object[] messageParam, Throwable cause) {
        super(cause);
        this.resultCode = resultCode;
        this.messageParam = messageParam;
    }

    public WPRuntimeException(String message) {
        super(message);
    }

    public WPRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public WPRuntimeException(Throwable cause) {
        super(cause);
    }

    protected WPRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ResultCodeInf getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCodeInf resultCode) {
        this.resultCode = resultCode;
    }

    public Object[] getMessageParam() {
        return messageParam;
    }

    public void setMessageParam(Object[] messageParam) {
        this.messageParam = messageParam;
    }

    @Override
    public String toString() {
        return "HOSPRuntimeException{" +
                "resultCode=" + resultCode +
                ", messageParam=" + Arrays.toString(messageParam) +
                '}';
    }
}
