package com.sxf.exception;

import org.springframework.security.core.AuthenticationException;


public class UserFrozenException extends AuthenticationException {
    private int invalidTimes;

    public int getInvalidTimes() {
        return invalidTimes;
    }

    public UserFrozenException(String msg, Throwable t, int invalidTimes) {
        super(msg, t);
        this.invalidTimes = invalidTimes;
    }

    public UserFrozenException(String msg, int invalidTimes) {
        super(msg);
        this.invalidTimes = invalidTimes;
    }

    public UserFrozenException(int invalidTimes) {
        super("");
        this.invalidTimes = invalidTimes;
    }

    public UserFrozenException(String msg, Throwable t) {
        super(msg, t);
    }

    public UserFrozenException(String msg) {
        super(msg);
    }

}
