package com.sxf.domain.multisite;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * WpRegistrationLog entity.
 * 
 * @author
 */

public class RegistrationLog implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	// Fields
	@Id
	private Long id;
	@Column
	private String email;
	@Column
	private String ip;
	@Column
	private Long blogId;
	@Column
	private Date dateRegistered;

	// Constructors

	/** default constructor */
	public RegistrationLog() {
	}

	/** full constructor */
	public RegistrationLog(String email, String ip, Long blogId,
			Date dateRegistered) {
		this.email = email;
		this.ip = ip;
		this.blogId = blogId;
		this.dateRegistered = dateRegistered;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getBlogId() {
		return this.blogId;
	}

	public void setBlogId(Long blogId) {
		this.blogId = blogId;
	}

	public Date getDateRegistered() {
		return this.dateRegistered;
	}

	public void setDateRegistered(Date dateRegistered) {
		this.dateRegistered = dateRegistered;
	}

}