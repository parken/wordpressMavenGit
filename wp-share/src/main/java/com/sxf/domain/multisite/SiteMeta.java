package com.sxf.domain.multisite;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * WpSitemeta entity.
 * 
 * @author
 */

public class SiteMeta implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	@Column
	private Long siteId;
	@Column
	private String key;
	@Column
	private String value;

	public SiteMeta() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}