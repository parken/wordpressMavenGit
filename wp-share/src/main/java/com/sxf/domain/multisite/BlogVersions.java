package com.sxf.domain.multisite;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class BlogVersions implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long blogId;

	@Column(name = "db_version")
	private String dbVersion;

	@Column(name = "last_updated")
	@Temporal(TemporalType.TIME)
	private Date lastUpdated;

	public BlogVersions() {
	}

	public Long getBlogId() {
		return blogId;
	}

	public void setBlogId(Long blogId) {
		this.blogId = blogId;
	}

	public String getDbVersion() {
		return dbVersion;
	}

	public void setDbVersion(String dbVersion) {
		this.dbVersion = dbVersion;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

}