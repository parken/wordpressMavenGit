package com.sxf.domain.multisite;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * WpSite entity.
 * 
 * @author
 */

public class Site implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	// Fields
	@Id
	private Long id;
	@Column
	private String domain;
	@Column
	private String path;

	// SiteMeta
	private List<SiteMeta> siteMetaList = new ArrayList<SiteMeta>();

	// Constructors

	/** default constructor */
	public Site() {
	}

	/** full constructor */
	public Site(String domain, String path) {
		this.domain = domain;
		this.path = path;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDomain() {
		return this.domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<SiteMeta> getSiteMetaList() {
		return siteMetaList;
	}

	public void setSiteMetaList(List<SiteMeta> siteMetaList) {
		this.siteMetaList = siteMetaList;
	}

}