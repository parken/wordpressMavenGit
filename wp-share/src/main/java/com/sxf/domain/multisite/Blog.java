package com.sxf.domain.multisite;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

public class Blog implements Serializable {
	private static final long serialVersionUID = -9016017512109476511L;

	@Id
	private Long blogId;
	@Column
	private Long siteId;
	@Column
	private String domain;
	@Column
	private String path;
	@Column
	private Date registered;
	@Column
	private Date lastUpdated;
	@Column
	private Short public_;
	@Column
	private String archived;
	@Column
	private Short mature;
	@Column
	private Short spam;
	@Column
	private Short deleted;
	@Column
	private Integer langId;

	public Blog() {

	}

	public Long getBlogId() {
		return blogId;
	}

	public void setBlogId(Long blogId) {
		this.blogId = blogId;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Date getRegistered() {
		return registered;
	}

	public void setRegistered(Date registered) {
		this.registered = registered;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Short getPublic_() {
		return public_;
	}

	public void setPublic_(Short public_) {
		this.public_ = public_;
	}

	public String getArchived() {
		return archived;
	}

	public void setArchived(String archived) {
		this.archived = archived;
	}

	public Short getMature() {
		return mature;
	}

	public void setMature(Short mature) {
		this.mature = mature;
	}

	public Short getSpam() {
		return spam;
	}

	public void setSpam(Short spam) {
		this.spam = spam;
	}

	public Short getDeleted() {
		return deleted;
	}

	public void setDeleted(Short deleted) {
		this.deleted = deleted;
	}

	public Integer getLangId() {
		return langId;
	}

	public void setLangId(Integer langId) {
		this.langId = langId;
	}

}
