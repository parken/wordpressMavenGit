package com.sxf.domain.enumration;

/**
 *
 */
public enum CommentStatus {
    /**
     * 审核中
     */
    review,
    /**
     * 驳回
     */
    unapproved,
    /**
     * 通过
     */
    approved,
    /**
     * 垃圾评论
     */
    spam,
    /**
     * 移至回收站
     */
    trash
}
