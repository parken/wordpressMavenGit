package com.sxf.domain.enumration;

public enum ArticleStatus {
    /**
     * 发布
     */
    published,
    /**
     * 草稿
     */
    draft,
    /**
     * 垃圾箱
     */
    trash,
    /**
     * 删除
     */
    deleted
}