package com.sxf.domain.enumration;

public enum CategoryType {
    /**
     * 标签
     */
    tag,
    /**
     * 分类
     */
    category,
    /**
     * 主题
     */
    subject
}
