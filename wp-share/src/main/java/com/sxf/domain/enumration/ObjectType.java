package com.sxf.domain.enumration;

public enum ObjectType {
    /**
     * 文章
     */
    article
    /**
     *评论
     */
    , comment
}
