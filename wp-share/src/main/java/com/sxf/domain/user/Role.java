package com.sxf.domain.user;

import sf.core.DBCascadeField;
import sf.core.DBField;
import sf.core.DBObject;

import javax.persistence.*;
import java.util.List;

/**
 * 角色表
 *
 * @author
 */
@Entity
@Table(name = "role")
public class Role extends DBObject {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "role_code", length = 200)
    private String roleCode;

    @Column(name = "role_name", length = 200)
    private String roleName;

    @Column
    private String description;

    @Column
    private Boolean available;

    @ManyToMany
    @JoinTable(name = "role_priv", joinColumns = {
            @JoinColumn(name = "role_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "privilege_id", referencedColumnName = "id")})
    private List<Privilege> privileges;

    public Role() {
    }

    public enum Field implements DBField {
        id, roleCode,roleName, description, available
    }

    public enum CascadeField implements DBCascadeField {
        privileges
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public List<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<Privilege> privileges) {
        this.privileges = privileges;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
