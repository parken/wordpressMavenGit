package com.sxf.domain.user;

/**
 * 权限类型
 */
public enum PrivilegeType {
    menu, button
}
