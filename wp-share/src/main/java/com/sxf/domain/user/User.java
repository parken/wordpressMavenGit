package com.sxf.domain.user;

import sf.core.DBCascadeField;
import sf.core.DBField;
import sf.core.DBObject;
import sf.database.annotations.Comment;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * 用户类
 * @author SXF
 */
@Entity
@Table(name = "wp_users")
public class User extends DBObject {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Comment("登陆名")
    @Column(name = "name", length = 60, nullable = false)
    private String name;

    /**
     * 密码
     */
    @Column(length = 128)
    private String password;
    /**
     * 盐值
     */
    @Column
    private String salt;
    /**
     * 密码强度
     */
    @Column(name = "pwd_strength")
    private String pwdStrength;

    /**
     * 昵称
     */
    @Column(name = "nickname",length = 50)
    private String nickname;

    /**
     * 真实名称
     */
    @Column(length = 50)
    private String realname;

    /**
     * 邮箱
     */
    @Column(length = 100)
    private String email;

    /**
     * 手机
     */
    @Column(length = 100)
    private String phone;

    @Comment("头像")
    @Lob
    @Column
    private byte[] icon;

    /**
     * 注册时间
     */
    @Column
    @Temporal(TemporalType.DATE)
    private Date registered;

    /**
     * 激活码
     */
    @Column(name = "activation_key", length = 60, nullable = false)
    private String activationKey;

    @Column
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(columnDefinition = "bit(1) DEFAULT 0")
    private Boolean locked;

    @Column(name = "modify_pwd")
    private Boolean modifyPwd;

    @ManyToMany
    @Transient
    @JoinTable(name = "user_role", joinColumns = {
            @JoinColumn(name = "user_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

    @Transient
    @OneToMany(targetEntity = UserMeta.class, mappedBy = "userId")
    // @JoinColumn(name = "id", referencedColumnName = "userId")
    private Set<UserMeta> userMetaSet = new LinkedHashSet<UserMeta>();

    public static enum Status {
        used("使用中"), deny("禁止"), delete("被删除");
        public String text;

        private Status(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

    /**
     * 普通字段
     */
    public enum Field implements DBField {
        id, name, password, salt, pwdStrength, nickname, realname, email, phone, icon, registered, activationKey, status,
        locked, modifyPwd
    }

    /**
     * 级联字段
     */
    public enum CascadeField implements DBCascadeField {
        roles, userMetaSet
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPwdStrength() {
        return pwdStrength;
    }

    public void setPwdStrength(String pwdStrength) {
        this.pwdStrength = pwdStrength;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Boolean getModifyPwd() {
        return modifyPwd;
    }

    public void setModifyPwd(Boolean modifyPwd) {
        this.modifyPwd = modifyPwd;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Set<UserMeta> getUserMetaSet() {
        return userMetaSet;
    }

    public void setUserMetaSet(Set<UserMeta> userMetaSet) {
        this.userMetaSet = userMetaSet;
    }
}
