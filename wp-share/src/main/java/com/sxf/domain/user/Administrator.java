package com.sxf.domain.user;

import com.alibaba.fastjson.annotation.JSONField;
import sf.core.DBField;
import sf.core.DBObject;
import sf.database.annotations.Comment;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 管理员表
 * @author shixiafeng
 */
@Entity
@Table(name = "wp_admin")
public class Administrator extends DBObject {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private String id;
    @Column(name = "username")
    private String username;
    @Column(name = "realname")
    private String realname;
    @Column
    private Integer status;
    @Column
    private String email;
    @Column(name = "phone")
    private String phone;
    @JSONField(serialize = false)
    @Column
    private String password;
    @Column(name = "pwd_strength")
    private Integer pwdStrength;
    @Column
    private String department;
    @Column(name = "create_time")
    private Date createTime;
    @Column
    private Boolean superior;
    /**
     * 是否强制修改密码
     */
    @Column(name = "force_modify_pwd")
    @Comment("是否强制修改密码")
    private Boolean forceModifyPwd;

    public enum Field implements DBField {
        id, username, realname, status, email, phone, password, pwdStrength, department, createTime, superior, forceModifyPwd
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPwdStrength() {
        return pwdStrength;
    }

    public void setPwdStrength(Integer pwdStrength) {
        this.pwdStrength = pwdStrength;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getSuperior() {
        return superior;
    }

    public void setSuperior(Boolean superior) {
        this.superior = superior;
    }

    public Boolean getForceModifyPwd() {
        return forceModifyPwd;
    }

    public void setForceModifyPwd(Boolean forceModifyPwd) {
        this.forceModifyPwd = forceModifyPwd;
    }
}
