package com.sxf.domain.user;

import sf.core.DBField;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 权限表
 * 
 * @author shixiafeng
 *
 */
@Entity
@Table(name = "privilege")
@Cacheable
public class Privilege implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;
	@Column
	private String name;
	@Column
	private String type;
	@Column
	private String url;
	@Column(name="parent_id")
	private Integer parentId;
	@Column(name="parent_ids")
	private String parentIds;
	@Column
	private String permission;
	@Column
	private Boolean available;

	public Privilege() {

	}

	public enum Field implements DBField {
		id, name, type, url, parentId, parentIds, permission, available
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
}
