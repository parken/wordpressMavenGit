package com.sxf.domain.blog;

import sf.core.DBField;
import sf.core.DBObject;

import javax.persistence.*;

@Entity
@Table(name = "article_mapping")
public class ArticleMapping extends DBObject {
    @Id
    @Column(name = "article_id")
    private Long articleId;

    @Id
    @Column(name = "category_id")
    private Long categoryId;

    public enum Field implements DBField {
        articleId, categoryId
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "ArticleMapping{" +
                "articleId=" + articleId +
                ", categoryId=" + categoryId +
                '}';
    }
}
