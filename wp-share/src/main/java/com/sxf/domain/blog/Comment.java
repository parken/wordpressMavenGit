package com.sxf.domain.blog;

import com.sxf.domain.enumration.CommentStatus;
import sf.core.DBObject;
import sf.database.annotations.SavedDefaultValue;

import javax.persistence.*;
import java.util.Date;

/**
 * 评论(与数据对应)
 * @author SXF
 */
@Entity
@Table(name = "wp_comments")
public class Comment extends DBObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @sf.database.annotations.Comment("主键")
    private Long id;
    /**
     * 父ID
     */
    @sf.database.annotations.Comment("父ID")
    @Column(name = "parent_id")
    private Long parentId;
    /**
     * 文章ID
     */
    @sf.database.annotations.Comment("文章ID")
    @Column(name = "article_id")
    private Long articleId;

    /**
     * 评论的用户ID
     */
    @sf.database.annotations.Comment("评论的用户ID")
    @Column(name = "from_user_id")
    private Long fromUserId;

    /**
     * 被评论的用户ID
     */
    @sf.database.annotations.Comment("被评论的用户ID")
    @Column(name = "to_user_id")
    private Long toUserId;

    /**
     * 评论的回复数量
     */
    @sf.database.annotations.Comment("主键")
    @Column(name = "reply_count")
    private Long replyCount;
    /**
     * 排序编号，常用语置顶等
     */
    @sf.database.annotations.Comment("主键")
    @Column(name = "order_number")
    private Long orderNumber;

    /**
     * 评论的IP地址
     */
    @sf.database.annotations.Comment("评论的IP地址")
    @Column
    private String ip;


    /**
     * 评论的内容
     */
    @sf.database.annotations.Comment("评论的内容")
    @Lob
    @Column
    private String content;
    /**
     * 提交评论的浏览器信息
     */
    @sf.database.annotations.Comment("提交评论的浏览器信息")
    @Lob
    @Column
    private String agent;
    /**
     * 评论的时间
     */
    @sf.database.annotations.Comment("评论的时间")
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    /**
     * 评论用户的email
     */
    @sf.database.annotations.Comment("评论用户的email")
    @Column
    private String email;
    /**
     * 评论的状态
     */
    @sf.database.annotations.Comment("评论的状态")
    @Column
    @Enumerated(EnumType.STRING)
    private CommentStatus status;
    /**
     * “顶”的数量
     */
    @SavedDefaultValue
    @sf.database.annotations.Comment("“顶”的数量")
    @Column(name = "vote_up")
    private Long voteUp=0L;
    /**
     * “踩”的数量
     */
    @SavedDefaultValue
    @sf.database.annotations.Comment("“踩”的数量")
    @Column(name = "vote_down")
    private Long voteDown=0L;

    @sf.database.annotations.Comment("乐观锁")
    @Column
    @Version
    private String version;

    public Comment() {

    }

    public enum Field implements sf.core.DBField {
        id, parentId, articleId, replyCount, orderNumber, fromUserId, toUserId, ip, content, agent, created,
        email, status, voteUp, voteDown, version
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Long getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(Long replyCount) {
        this.replyCount = replyCount;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CommentStatus getStatus() {
        return status;
    }

    public void setStatus(CommentStatus status) {
        this.status = status;
    }

    public Long getVoteUp() {
        return voteUp;
    }

    public void setVoteUp(Long voteUp) {
        this.voteUp = voteUp;
    }

    public Long getVoteDown() {
        return voteDown;
    }

    public void setVoteDown(Long voteDown) {
        this.voteDown = voteDown;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
