package com.sxf.domain.blog;

import sf.core.DBField;
import sf.core.DBObject;
import sf.database.annotations.Comment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * 配置信息表，用来保存网站的所有配置信息。
 * @author SXF
 */
@Entity
@Table(name = "wp_options")
@Comment("配置信息表，用来保存网站的所有配置信息。")
public class Option extends DBObject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 名称
     */
    @Column(length = 64, nullable = false, unique = true)
    @Comment("名称")
    private String name;

    /**
     * 配置内容
     */
    @Lob
    @Column(nullable = false)
    @Comment("配置内容")
    private String value;

    @Lob
    @Column
    @Comment("描述")
    private String description;

    private static final long serialVersionUID = 1L;

    public Option() {

    }

    public enum Field implements DBField {
        id, name, value, description
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
