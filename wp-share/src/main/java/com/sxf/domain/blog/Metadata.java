package com.sxf.domain.blog;

import com.sxf.domain.enumration.ObjectType;
import sf.core.DBField;
import sf.core.DBObject;
import sf.database.annotations.Comment;

import javax.persistence.*;

/**
 * 元数据表，用来对其他表的字段扩充。
 * @author
 */
@Entity
@Table(name = "wp_metadata")
@Comment("元数据表，用来对其他表的字段扩充。")
public class Metadata extends DBObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * 元数据key
     */
    @Column(name = "meta_key")
    @Comment("元数据key")
    private String metaKey;
    /**
     * 元数据value
     */
    @Lob
    @Column(name = "meta_value")
    @Comment("元数据value")
    private String metaValue;
    /**
     * 元数据的对象类型
     */
    @Column(name = "object_type")
    @Enumerated(EnumType.STRING)
    @Comment("元数据的对象类型")
    private ObjectType objectType;
    /**
     * 元数据的对象ID
     */
    @Column(name = "object_id")
    @Comment("元数据的对象ID")
    private Long objectId;

    private static final long serialVersionUID = 1L;

    public Metadata() {

    }

    public enum Field implements DBField {
        id, metaKey, metaValue, objectType, objectId
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(ObjectType objectType) {
        this.objectType = objectType;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

}
