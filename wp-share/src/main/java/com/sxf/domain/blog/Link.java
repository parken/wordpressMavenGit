package com.sxf.domain.blog;

import sf.core.DBField;
import sf.core.DBObject;
import sf.database.annotations.SavedDefaultValue;

import javax.persistence.*;
import java.util.Date;

/**
 * 链接
 * @author SXF
 */
@Entity
@Table(name = "wp_links")
public class Link extends DBObject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Long userId;

    @Column
    private String url;

    @Column
    private String name;

    @Column
    private String image;

    @Column
    private String target;

    @Column
    private String description;

    @Column
    @SavedDefaultValue
    private Boolean visible = Boolean.FALSE;

    @Column
    private Long owner;

    @Column
    private Integer rating;// 评分

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    @Column
    private String rel;

    @Column
    private String rss;

    @Column
    private String notes;

    private static final long serialVersionUID = 1L;

    public Link() {

    }

    public enum Field implements DBField {
        id,userId, url, name, image, target, description, visible, owner, rating, created, modified, rel, rss, notes
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getRss() {
        return rss;
    }

    public void setRss(String rss) {
        this.rss = rss;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

}
