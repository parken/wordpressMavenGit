package com.sxf.domain.blog;

import sf.core.DBField;
import sf.core.DBObject;
import sf.database.annotations.Comment;

import javax.persistence.*;
import java.util.Date;

/**
 * 附件
 */
@Entity
@Table(name = "wp_attachement")
public class Attachement extends DBObject {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 标题
     */
    @Column
    private String title;
    /**
     * 上传附件的用户ID
     */
    @Column(name = "user_id")
    private Long userId;
    /**
     * 附件所属的内容ID
     */
    @Column
    private String description;
    /**
     * 路径
     */
    @Column(length = 512)
    private String path;
    /**
     * mime
     */
    @Column(name = "mime_type")
    private String mimeType;
    /**
     * 附件的后缀
     */
    @Column
    private String suffix;
    /**
     * 类型
     */
    @Column
    private String type;
    /**
     * 标示
     */
    @Column
    private String flag;

    /**
     * 排序字段
     */
    @Column(name = "order_number")
    private Long orderNumber;
    /**
     * 上传时间
     */
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Comment("修改时间")
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date modfied;

    public enum Field implements DBField {
        id, title,userId, description, path, mimeType, suffix, type, flag, orderNumber, created, modfied
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModfied() {
        return modfied;
    }

    public void setModfied(Date modfied) {
        this.modfied = modfied;
    }

    @Override
    public String toString() {
        return "Attachement{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", userId=" + userId +
                ", path='" + path + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", suffix='" + suffix + '\'' +
                ", type='" + type + '\'' +
                ", flag='" + flag + '\'' +
                ", orderNumber=" + orderNumber +
                ", created=" + created +
                ", modfied=" + modfied +
                '}';
    }
}
