package com.sxf.domain.blog;

import com.sxf.domain.enumration.ArticleStatus;
import sf.core.DBCascadeField;
import sf.core.DBField;
import sf.core.DBObject;
import sf.database.annotations.Comment;
import sf.database.annotations.SavedDefaultValue;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 文章表
 *
 * @author SXF
 */
@Entity
@Table(name = "wp_article")
public class Article extends DBObject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 标题
     */
    @Lob
    @Column
    private String title;

    /**
     * 内容
     */
    @Lob
    @Column
    private String content;

    /**
     * 摘要
     */
    @Lob
    @Column
    private String summary;

    @Comment("链接到(常用于谋文章只是一个连接)")
    @Column(name = "link_to")
    private String linkTo;

    /**
     * 缩略图
     */
    @Column(length = 128)
    private String thumbnail;
    /**
     * 模型
     */
    @Comment("编辑模式，默认为html，其他可选项包括html，markdown ..")
    @Column(name = "edit_mode")
    @Enumerated(EnumType.STRING)
    @SavedDefaultValue
    private EditMode editMode = EditMode.html;

    public enum EditMode {
        html, markdown
    }

    /**
     * 样式
     */
    @Column
    private String style;
    /**
     * 匿名稿的用户
     */
    @Column(name = "author")
    private String author;
    /**
     * 匿名稿的邮箱
     */
    @Column(name = "user_email")
    private String userEmail;
    /**
     * IP地址
     */
    @Column(name = "user_ip")
    private String userIp;
    /**
     * 发布浏览agent
     */
    @Lob
    @Column(name = "user_agent")
    private String userAgent;
    /**
     * 父级内容ID
     */
    @Column(name = "parent_id")
    private Long parentId;
    /**
     * 排序编号
     */
    @Column(name = "order_number")
    private Long orderNumber;
    /**
     * 状态
     */
    @Comment("文章状态:publish(发布),trash(垃圾箱),draft(草稿)")
    @Column
    @Enumerated(EnumType.STRING)
    @SavedDefaultValue
    private ArticleStatus status = ArticleStatus.draft;// publish(发布),trash(垃圾箱),draft(草稿)
    /**
     * 支持人数
     */
    @Comment("点赞人数")
    @Column(name = "vote_up")
    @SavedDefaultValue
    private Long voteUp = 0L;
    /**
     * 反对人数
     */
    @Comment("踩人数")
    @Column(name = "vote_down")
    @SavedDefaultValue
    private Long voteDown = 0L;

    /**
     * 评论状态
     */
    @Comment("评论状态,true允许评论,false关闭.")
    @SavedDefaultValue
    private Boolean commentStatus = Boolean.TRUE;// open
    /**
     * 评论总数
     */
    @Column(name = "comment_count")
    @SavedDefaultValue
    private Long commentCount = 0L;
    /**
     * 最后评论时间
     */
    @Column(name = "comment_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentTime;
    /**
     * 访问量
     */
    @Column(name = "view_count")
    private Long viewCount;
    /**
     * 创建日期
     */
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    /**
     * 最后更新日期
     */
    @Column
    @Temporal(TemporalType.DATE)
    private Date modified;
    /**
     * slug
     */
    @Column
    private String slug;
    /**
     * 标识
     */
    @Comment("标识，通常用于对某几篇文章进行标识，从而实现单独查询")
    @Column
    private String flag;
    /**
     * SEO关键字
     */
    @Column(name = "meta_keywords")
    private String metaKeywords;
    /**
     * SEO描述信息
     */
    @Comment("SEO描述信息")
    @Column(name = "meta_description")
    private String metaDescription;
    /**
     * 备注信息
     */
    @Comment("备注信息")
    @Lob
    @Column
    private String remarks;

    @Comment("乐观锁")
    @Column
    @Version
    private String version;

    @ManyToMany
    @Transient
    @JoinTable(name = "article_mapping", joinColumns = {
            @JoinColumn(name = "article_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "category_id", referencedColumnName = "id")})
    private List<Category> categories;

    private static final long serialVersionUID = 1L;

    public Article() {

    }

    public enum Field implements DBField {
        id, userId, title, content, summary, linkTo, thumbnail, editMode, style, author, userEmail, userIp, userAgent, parentId,
        orderNumber, status, voteUp, voteDown, commentStatus, commentCount, commentTime, viewCount,
        created, modified, slug, flag, metaKeywords, metaDescription, remarks,version
    }

    public enum CascadeField implements DBCascadeField {
        categories
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getLinkTo() {
        return linkTo;
    }

    public void setLinkTo(String linkTo) {
        this.linkTo = linkTo;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public EditMode getEditMode() {
        return editMode;
    }

    public void setEditMode(EditMode editMode) {
        this.editMode = editMode;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public ArticleStatus getStatus() {
        return status;
    }

    public void setStatus(ArticleStatus status) {
        this.status = status;
    }

    public Long getVoteUp() {
        return voteUp;
    }

    public void setVoteUp(Long voteUp) {
        this.voteUp = voteUp;
    }

    public Long getVoteDown() {
        return voteDown;
    }

    public void setVoteDown(Long voteDown) {
        this.voteDown = voteDown;
    }

    public Boolean getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(Boolean commentStatus) {
        this.commentStatus = commentStatus;
    }

    public Long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    public Long getViewCount() {
        return viewCount;
    }

    public void setViewCount(Long viewCount) {
        this.viewCount = viewCount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", text='" + ((content != null && content.length() > 100) ? content.substring(0, 100)+" ......" : content) + '\'' +
                ", summary='" + summary + '\'' +
                ", linkTo='" + linkTo + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", module='" + editMode + '\'' +
                ", style='" + style + '\'' +
                ", author='" + author + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userIp='" + userIp + '\'' +
                ", userAgent='" + userAgent + '\'' +
                ", parentId=" + parentId +
                ", orderNumber=" + orderNumber +
                ", status=" + status +
                ", voteUp=" + voteUp +
                ", voteDown=" + voteDown +
                ", commentStatus=" + commentStatus +
                ", commentCount=" + commentCount +
                ", commentTime=" + commentTime +
                ", viewCount=" + viewCount +
                ", created=" + created +
                ", modified=" + modified +
                ", slug='" + slug + '\'' +
                ", flag='" + flag + '\'' +
                ", metaKeywords='" + metaKeywords + '\'' +
                ", metaDescription='" + metaDescription + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
