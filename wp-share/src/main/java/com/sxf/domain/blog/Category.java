package com.sxf.domain.blog;

import com.sxf.domain.enumration.CategoryType;
import sf.core.DBCascadeField;
import sf.core.DBField;
import sf.core.DBObject;
import sf.database.annotations.Comment;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 分类表
 *
 * @author phsxf01
 */
@Comment("文章分类表")
@Entity
@Table(name = "wp_category")
public class Category extends DBObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * 名称
     */
    @Column
    private String title;
    /**
     * 描述
     */
    @Comment("描述")
    @Lob
    @Column
    private String content;

    @Comment("摘要")
    @Lob
    @Column
    private String summary;
    /**
     * slug
     */
    @Column
    private String slug;
    /**
     * 类型
     */
    @Comment("类型，比如：分类、tag、专题")
    @Column
    @Enumerated(EnumType.STRING)
    private CategoryType type;
    /**
     * 图标
     */
    @Column
    private String icon;
    /**
     * 该分类的内容数量
     */
    @Column(name = "count")
    private Long count;
    /**
     * 排序编码
     */
    @Column(name = "order_number")
    private Long orderNumber;
    /**
     * 父级分类的ID
     */
    @Column(name = "parent_id")
    private Long parentId;
    /**
     * 创建人员
     */
    @Column(name = "user_id")
    private Long userId;
    /**
     * 标识
     */
    @Column
    private String flag;

    /**
     * SEO关键字
     */
    @Column(name = "meta_keywords")
    private String metaKeywords;
    /**
     * SEO描述内容
     */
    @Column(name = "meta_description")
    private String metaDescription;
    /**
     * 创建日期
     */
    @Comment("创建日期")
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @ManyToMany(mappedBy = "categories")
    @Transient
    private List<Article> articles;

    private static final long serialVersionUID = 1L;

    public Category() {

    }

    public enum Field implements DBField {
        id, title, content,summary, slug, type, icon, count, orderNumber, parentId, userId, flag, metaKeywords, metaDescription, created
    }

    public enum CascadeField implements DBCascadeField {
        articles
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
